Title: Chip Set: Intel Chipset Software Installation Utility H67 Driver
Version    : A00
OEM name   : Intel
OEM Version: 9.2.0.1015
Computers  : Vostro Desktop: Vostro 460; XPS Desktop: XPS8300
OSes       : Windows 7 32-bit Home Premium,Windows 7 32-bit Professional,Windows 7 32-bit Ultimate,Windows 7 64-bit Home Premium,Windows 7 64-bit Professional,Windows 7 64-bit Ultimate
Languages  : Arabic, Chinese-S, Chinese-T, Czech, Danish, Dutch, English, Finnish, French, German, Greek, Hebrew, Hungarian, Italian, Japanese, Korean, Norwegian, Polish, Russian, Swedish, Spanish
Created    : Monday, January 10, 2011
