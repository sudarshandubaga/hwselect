<?php

/* * ******  bonus Start ********** */
Route::get('list-bonuss', array_merge(['uses' => 'Admin\BonusController@indexbonuss'], $all_users))->name('list.bonuss');
Route::get('create-bonus', array_merge(['uses' => 'Admin\BonusController@createbonus'], $all_users))->name('create.bonus');
Route::post('store-bonus', array_merge(['uses' => 'Admin\BonusController@storebonus'], $all_users))->name('store.bonus');
Route::get('edit-bonus/{id}', array_merge(['uses' => 'Admin\BonusController@editbonus'], $all_users))->name('edit.bonus');
Route::put('update-bonus/{id}', array_merge(['uses' => 'Admin\BonusController@updatebonus'], $all_users))->name('update.bonus');
Route::delete('delete-bonus', array_merge(['uses' => 'Admin\BonusController@deletebonus'], $all_users))->name('delete.bonus');
Route::get('fetch-bonuss', array_merge(['uses' => 'Admin\BonusController@fetchbonussData'], $all_users))->name('fetch.data.bonuss');
Route::put('make-active-bonus', array_merge(['uses' => 'Admin\BonusController@makeActivebonus'], $all_users))->name('make.active.bonus');
Route::put('make-not-active-bonus', array_merge(['uses' => 'Admin\BonusController@makeNotActivebonus'], $all_users))->name('make.not.active.bonus');
Route::get('sort-bonuss', array_merge(['uses' => 'Admin\BonusController@sortbonuss'], $all_users))->name('sort.bonuss');
Route::get('bonus-sort-data', array_merge(['uses' => 'Admin\BonusController@bonusSortData'], $all_users))->name('bonus.sort.data');
Route::put('bonus-sort-update', array_merge(['uses' => 'Admin\BonusController@bonusSortUpdate'], $all_users))->name('bonus.sort.update');
/* * ****** End bonus ********** */