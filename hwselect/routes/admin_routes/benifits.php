<?php

/* * ******  benifits Start ********** */
Route::get('list-benifitss', array_merge(['uses' => 'Admin\BenifitsController@indexbenifitss'], $all_users))->name('list.benifitss');
Route::get('create-benifits', array_merge(['uses' => 'Admin\BenifitsController@createbenifits'], $all_users))->name('create.benifits');
Route::post('store-benifits', array_merge(['uses' => 'Admin\BenifitsController@storebenifits'], $all_users))->name('store.benifits');
Route::get('edit-benifits/{id}', array_merge(['uses' => 'Admin\BenifitsController@editbenifits'], $all_users))->name('edit.benifits');
Route::put('update-benifits/{id}', array_merge(['uses' => 'Admin\BenifitsController@updatebenifits'], $all_users))->name('update.benifits');
Route::delete('delete-benifits', array_merge(['uses' => 'Admin\BenifitsController@deletebenifits'], $all_users))->name('delete.benifits');
Route::get('fetch-benifitss', array_merge(['uses' => 'Admin\BenifitsController@fetchbenifitssData'], $all_users))->name('fetch.data.benifitss');
Route::put('make-active-benifits', array_merge(['uses' => 'Admin\BenifitsController@makeActivebenifits'], $all_users))->name('make.active.benifits');
Route::put('make-not-active-benifits', array_merge(['uses' => 'Admin\BenifitsController@makeNotActivebenifits'], $all_users))->name('make.not.active.benifits');
Route::get('sort-benifitss', array_merge(['uses' => 'Admin\BenifitsController@sortbenifitss'], $all_users))->name('sort.benifitss');
Route::get('benifits-sort-data', array_merge(['uses' => 'Admin\BenifitsController@benifitsSortData'], $all_users))->name('benifits.sort.data');
Route::put('benifits-sort-update', array_merge(['uses' => 'Admin\BenifitsController@benifitsSortUpdate'], $all_users))->name('benifits.sort.update');
/* * ****** End benifits ********** */