<?php



/* * ******  Job Start ********** */

Route::get('list-jobs', array_merge(['uses' => 'Admin\JobController@indexJobs'], $all_users))->name('list.jobs');
Route::get('expired-jobs', array_merge(['uses' => 'Admin\JobController@expireddJobs'], $all_users))->name('list.expired.jobs');
Route::get('pending-for-approval', array_merge(['uses' => 'Admin\JobController@pendingForApproval'], $all_users))->name('list.pending-for-approval.jobs');

Route::get('list-refused-jobs', array_merge(['uses' => 'Admin\JobController@rejectedJobs'], $all_users))->name('list.rejected.jobs');

Route::get('archived-jobs', array_merge(['uses' => 'Admin\JobController@expiredJobs'], $all_users))->name('expired.jobs');

Route::get('list-requests-to-delete-jobs', array_merge(['uses' => 'Admin\JobController@deletedJobs'], $all_users))->name('list.request.jobs');

Route::get('delete-jobs', array_merge(['uses' => 'Admin\JobController@deleteJobs'], $all_users))->name('delete.jobs');
Route::get('delete-expired-jobs', array_merge(['uses' => 'Admin\JobController@deleteExpiredJobs'], $all_users))->name('delete.expired.jobs');

Route::get('create-job', array_merge(['uses' => 'Admin\JobController@createJob'], $all_users))->name('create.job');

Route::post('store-job', array_merge(['uses' => 'Admin\JobController@storeJob'], $all_users))->name('store.job');

Route::get('edit-job/{id}', array_merge(['uses' => 'Admin\JobController@editJob'], $all_users))->name('edit.job');

Route::get('cron', array_merge(['uses' => 'Admin\JobController@cron'], $all_users))->name('cron');

Route::put('updatemeta-job/{id}', array_merge(['uses' => 'Admin\JobController@updateMetaJob'], $all_users))->name('updateMeta.job');

Route::put('update-job/{id}', array_merge(['uses' => 'Admin\JobController@updateJob'], $all_users))->name('update.job');

Route::delete('delete-job', array_merge(['uses' => 'Admin\JobController@deleteJob'], $all_users))->name('delete.job');

Route::get('fetch-jobs', array_merge(['uses' => 'Admin\JobController@fetchJobsData'], $all_users))->name('fetch.data.jobs');
Route::get('fetch-rejected-jobs', array_merge(['uses' => 'Admin\JobController@fetchRejectedJobsData'], $all_users))->name('fetch.rejected.jobs');

Route::get('fetch-expired-jobs', array_merge(['uses' => 'Admin\JobController@fetchExpiredJobsData'], $all_users))->name('fetch.expired.jobs');

Route::get('fetch-del-jobs', array_merge(['uses' => 'Admin\JobController@fetchDelJobsData'], $all_users))->name('fetch.data.del.jobs');

Route::put('make-active-job', array_merge(['uses' => 'Admin\JobController@makeActiveJob'], $all_users))->name('make.active.job');

Route::put('make-reviewed-job', array_merge(['uses' => 'Admin\JobController@makeReviewedJob'], $all_users))->name('make.reviewed.job');

Route::put('make-not-reviewed-job', array_merge(['uses' => 'Admin\JobController@makeNotReviewedJob'], $all_users))->name('make.notreviewed.job');

Route::put('make-not-active-job', array_merge(['uses' => 'Admin\JobController@makeNotActiveJob'], $all_users))->name('make.not.active.job');

Route::put('make-featured-job', array_merge(['uses' => 'Admin\JobController@makeFeaturedJob'], $all_users))->name('make.featured.job');

Route::put('make-not-featured-job', array_merge(['uses' => 'Admin\JobController@makeNotFeaturedJob'], $all_users))->name('make.not.featured.job');

Route::get('onhold-jobs', array_merge(['uses' => 'Admin\JobController@onholdJobs'], $all_users))->name('list.onhold.jobs');

Route::get('fetch-abused-jobs', array_merge(['uses' => 'Admin\JobController@fetchAbusedJobsData'], $all_users))->name('fetch.data.abused.jobs');
Route::get('abused-reported-jobs', array_merge(['uses' => 'Admin\JobController@abusedJobs'], $all_users))->name('list.abused.jobs');


/* * ****** End Job ********** */