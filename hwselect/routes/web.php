<?php



/*

  |--------------------------------------------------------------------------

  | Web Routes

  |--------------------------------------------------------------------------

  |

  | Here is where you can register web routes for your application. These

  | routes are loaded by the RouteServiceProvider within a group which

  | contains the "web" middleware group. Now create something great!

  |

 */
Route::middleware([])->group(function () {
    

$real_path = realpath(__DIR__) . DIRECTORY_SEPARATOR . 'front_routes' . DIRECTORY_SEPARATOR;
// dd($real_path);

/* * ******** JobController ************ */

include_once($real_path . 'job.php');

Route::get('/clear-optimize', function() {
  return \Artisan::call('optimize:clear');
});

/* * ******** IndexController ************ */
Route::get('/', 'IndexController@index')->name('index');
Route::get('/check-time', 'IndexController@checkTime')->name('check-time');

Route::post('set-locale', 'IndexController@setLocale')->name('set.locale');

/* * ******** HomeController ************ */

Route::get('home', 'HomeController@index')->name('home')->middleware('verified');

/* * ******** TypeAheadController ******* */

Route::get('typeahead-currency_codes', 'TypeAheadController@typeAheadCurrencyCodes')->name('typeahead.currency_codes');

/* * ******** FaqController ******* */

Route::get('job-seeker-faqs', 'FaqController@jobseeker')->name('jobseeker-faq');
Route::get('employer-faqs', 'FaqController@index')->name('faq');

/* * ******** CronController ******* */

Route::get('check-package-validity', 'CronController@checkPackageValidity');

Route::get('delete-notverified-users', 'CronController@delete_notverified_users')->name('delete_notverified_users');
Route::get('delete-notverified-companies', 'CronController@delete_notverified_companies')->name('delete_notverified_companies');

/* * ******** Verification ******* */

Route::get('email-verification/error', 'Auth\RegisterController@getVerificationError')->name('email-verification.error');

Route::get('email-verification/check/{token}', 'Auth\RegisterController@getVerification')->name('email-verification.check');

Route::get('company-email-verification/error', 'Company\Auth\RegisterController@getVerificationError')->name('company.email-verification.error');

Route::get('company-email-verification/check/{token}', 'Company\Auth\RegisterController@getVerification')->name('company.email-verification.check');

/* * ***************************** */

// Sociallite Start

Route::get('notification', 'AlertCronController@notification')->name('seeker-append-only-notifications');

Route::get('notification-count', 'AlertCronController@notification_count')->name('seeker-append-only-notifications-count');

// OAuth Routes

Route::post('otp-verify', 'Auth\LoginController@otpVerify')->name('verify_otp');

Route::get('login/jobseeker/{provider}', 'Auth\LoginController@redirectToProvider');

Route::get('login/jobseeker/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::get('login/jobseeker/linkedin/callback', 'Auth\AuthController@handleLinkedinCallback');

Route::get('login/employer/{provider}', 'Company\Auth\LoginController@redirectToProvider');

Route::get('login/employer/{provider}/callback', 'Company\Auth\LoginController@handleProviderCallback');

Route::get('login/employer/linkedin/callback', 'Company\Auth\AuthController@handleLinkedinCallback');

// Sociallite End

/* * ***************************** */

Route::post('tinymce-image_upload-front', 'TinyMceController@uploadImage')->name('tinymce.image_upload.front');



Route::get('cronjob/send-alerts', 'AlertCronController@index')->name('send-alerts');

Route::get('disable-alert/{id}', 'AlertCronController@disableAlert')->name('disable-alert');



Route::post('subscribe-newsletter', 'SubscriptionController@getSubscription')->name('subscribe.newsletter');


// Sociallite End
Route::post('/set_session', 'IndexController@set_session')->name('set_session');
/* * ******** OrderController ************ */

Route::post('set-sessionn', 'CmsController@set_session')->name('set-sessionss');

Route::get('page-not-found', 'CmsController@pageNotFound')->name('page-not-found');

include_once($real_path . 'order.php');






/* * ******** CompanyController ************ */

include_once($real_path . 'company.php');

/* * ******** AjaxController ************ */

include_once($real_path . 'ajax.php');

/* * ******** UserController ************ */

include_once($real_path . 'site_user.php');

/* * ******** User Auth ************ */

Auth::routes();

Route::get('/phone-verification', 'PhoneVerificationController@index')->name('phone-verification');

Route::get('/verify-number', 'PhoneVerificationController@verification_number')->name('verify-number');

Route::get('/verification', 'PhoneVerificationController@verification')->name('verification');

Route::get('/show-phone/{id}', 'PhoneVerificationController@show_phone')->name('show-phone');

/* * ******** Company Auth ************ */

include_once($real_path . 'company_auth.php');

/* * ******** Admin Auth ************ */

include_once($real_path . 'admin_auth.php');





Route::get('blog', 'BlogController@index')->name('blogs');

Route::get('blog/search', 'BlogController@search')->name('blog-search');

// Route::get('blog/{slug}', 'BlogController@detail')->name('blog-detail');
Route::get('/blog/{blog}', 'BlogController@categories')->name('blog-category');
Route::get('blog/{category}/{slug}', 'BlogController@details')->name('blog-details');



Route::get('/company-change-message-status', 'CompanyMessagesController@change_message_status')->name('company-change-message-status');
Route::get('/seeker-change-message-status', 'Job\SeekerSendController@change_message_status')->name('seeker-change-message-status');

Route::get('/sitemap', 'SitemapController@index');
Route::get('/hw-sitemap', 'IndexController@sitemap')->name('hw-sitemap');

Route::get('/sitemap/companies', 'SitemapController@companies');



/* * ******** CmsController ************ */

Route::get('/clear-cache', function () {

  $exitCode = Artisan::call('config:clear');

  $exitCode = Artisan::call('cache:clear');

  $exitCode = Artisan::call('config:cache');

  return 'DONE'; //Return anything

});

include_once($real_path . 'contact.php');

include_once($real_path . 'cms.php');

});






