<?php

namespace App\Traits;

use Auth;
use DB;
use Input;
use Redirect;
use Mail;
use Session;
use Carbon\Carbon;
use App\Job;
use App\Old_jobs;
use App\Company;
use App\JobSkill;
use App\JobSkillManager;
use App\Country;
use App\CountryDetail;
use App\State;
use App\City;
use App\CareerLevel;
use App\FunctionalArea;
use App\JobType;
use App\JobShift;
use App\Bonus;
use App\Benifits;
use App\Gender;
use App\JobExperience;
use App\DegreeLevel;
use App\SalaryPeriod;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\JobFormRequest;
use App\Http\Requests\Front\JobFrontFormRequest;
use App\Http\Controllers\Controller;
use App\Traits\Skills;
use App\Events\JobPosted;
use App\Mail\JobPostedMail;
use App\Mail\JobUpdatedMailable;
use App\Mail\JobDeletedMail;
use App\Mail\JobDeleted;

trait JobTrait
{

    use Skills;

    public function deleteJob(Request $request)
    {
        $id = $request->input('id');

        try {
            $job = Job::findOrFail($id);
             $company = Company::findOrFail($job->company_id);
             $title = $job->title;
             $updated_at = $job->updated_at;

            JobSkillManager::where('job_id', '=', $id)->delete();
            $job->delete();

           
            $data = array(
                'full_name'=> $company->name,
                'email'=> $company->email,
                'subject'=> 'Job Deletion Confirmation for  '.$company->name,
                'job'=> $title,
                'updated_at'=> $updated_at,

            );
            Mail::send(new JobDeleted($data));
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }


    public function deleteRequestJob(Request $request)
    {
        $id = $request->input('id');
        try {
            $job = Job::findOrFail($id);
            //JobSkillManager::where('job_id', '=', $id)->delete();
            $job->request_to_delete = 'yes';
            $job->reason = $request->reason == 'other' ? $request->other_reason : $request->reason;
            $job->status = 'request_to_delete';
            $job->update();

            $company = Auth::guard('company')->user();

      
            $data = array(
                'full_name'=> $company->name,
                'email'=> $company->email,
                'subject'=> 'Request to delete a job by '.$company->name,
                'id'=> $job->id,
                'slug'=> $job->slug,
                'job'=> $job->title,
                'reason'=> $job->reason,
                'updated_at'=> $job->updated_at,

            );
            Mail::send(new JobDeletedMail($data));
			return 'Request to delete Job Vacancy ('.$job->title.') Submitted to admin on '.date("jS F, Y").' ';


        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function updateFullTextSearch($job)
    {
        $str = '';
        $str .= $job->getCompany('name');
        $str .= ' ' . $job->getCountry('country');
        $str .= ' ' . $job->getState('state');
        $str .= ' ' . $job->getCity('city');
        $str .= ' ' . $job->title;
        $str .= ' ' . $job->description;
        $str .= $job->getJobSkillsStr();
        $str .= ((bool) $job->is_freelance) ? ' freelance remote work from home multiple cities' : '';
        $str .= ' ' . $job->getCareerLevel('career_level');
        $str .= ((bool) $job->hide_salary === false) ? ' ' . $job->salary_from . ' ' . $job->salary_to : '';
        $str .= $job->getSalaryPeriod('salary_period');
        $str .= ' ' . $job->getFunctionalArea('functional_area');
        $str .= ' ' . $job->getJobType('job_type');
        $str .= ' ' . $job->getJobShift('job_shift');
        $str .= ' ' . $job->getGender('gender');
        $str .= ' ' . $job->getDegreeLevel('degree_level');
        $str .= ' ' . $job->getJobExperience('job_experience');

        $job->search = $str;
        $job->update();
    }

    private function assignJobValues($job, $request)
    {
        $job->title = $request->input('title');
        $job->description = $request->input('description');
        $job->country_id = $request->input('country_id');
        $job->state_id = $request->input('state_id');
        $job->city_id = $request->input('city_id');
        $job->address = $request->input('address');
        $job->is_bonus = $request->input('is_bonus');
        $job->is_benifits = $request->input('is_benifits');
        $job->salary_type = $request->input('salary_type');
        if($request->input('salary_in_range')){
            $job->salary_in_range = 1;  
          }else{
            $job->salary_in_range = 0; 
          }
        
      

      

        if(null!==($request->input('longitude'))){
            $job->longitude = $request->input('longitude');
        }


        if(null!==($request->input('latitude'))){
            $job->latitude = $request->input('latitude');
        }

        $date = date("Y-m-d");
        //dd($request);
        $job->is_freelance = $request->input('is_freelance');
        $job->career_level_id = $request->input('career_level_id');
        $job->salary_from = $request->input('salary_from');
        $job->salary_to = $request->input('salary_to');
        $job->salary_currency = $request->input('salary_currency');
        $job->hide_salary = $request->input('hide_salary');
        $job->functional_area_id = $request->input('functional_area_id');
        $job->job_type_id = $request->input('job_type_id');
        $job->job_shift_id = $request->input('job_shift_id');
        $job->num_of_positions = $request->input('num_of_positions');
        $job->gender_id = $request->input('gender_id');
        $job->expiry_datee = $request->input('expiry_datee');
        $expiry_date = strtotime(date("Y-m-d", strtotime($date)) . " +".$request->input('expiry_datee'));
        $job->expiry_date = empty($request->expiry_date) ? $expiry_date : $request->expiry_date;
        $job->degree_level_id = $request->input('degree_level_id');
        $job->job_experience_id = $request->input('job_experience_id');
        $job->salary_period_id = $request->input('salary_period_id');
        //if($request->bonus){
            $job->bonus = json_encode($request->bonus);
        //}

        //if($request->benifits){
            $job->benifits = json_encode($request->benifits);
       // }
        $job->salary_period_id = $request->input('salary_period_id');
        return $job;
    }

    public function createJob()
    {
        $benifits = DataArrayHelper::langBenifitsArray();
        $bonus = DataArrayHelper::langBonusArray();
        $companies = DataArrayHelper::companiesArray();
        $countries = DataArrayHelper::defaultCountriesArray();
        $currencies = DataArrayHelper::currenciesArray();
        $careerLevels = DataArrayHelper::defaultCareerLevelsArray();
        $functionalAreas = DataArrayHelper::defaultFunctionalAreasArray();
        $jobTypes = DataArrayHelper::defaultJobTypesArray();
        $jobShifts = DataArrayHelper::defaultJobShiftsArray();
        $genders = DataArrayHelper::defaultGendersArray();
        $jobExperiences = DataArrayHelper::defaultJobExperiencesArray();
        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        $degreeLevels = DataArrayHelper::defaultDegreeLevelsArray();
        $salaryPeriods = DataArrayHelper::defaultSalaryPeriodsArray();
        $jobSkillIds = array();
        $countries_details = DataArrayHelper::langCountriesDetailsArray();
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $bonusIds = array();
        $benifitsIds = array();
        return view('admin.job.add')
                        ->with('benifits', $benifits)
                        ->with('bonus', $bonus)
                        ->with('companies', $companies)
                        ->with('countries', $countries)
                        ->with('currencies', array_unique($currencies))
                        ->with('careerLevels', $careerLevels)
                        ->with('functionalAreas', $functionalAreas)
                        ->with('jobTypes', $jobTypes)
                        ->with('jobShifts', $jobShifts)
                        ->with('countries_details', $countries_details)
                        ->with('genders', $genders)
                        ->with('jobExperiences', $jobExperiences)
                        ->with('jobSkills', $jobSkills)
                        ->with('jobSkillIds', $jobSkillIds)
                        ->with('bonusIds', $bonusIds)
                        ->with('benifitsIds', $benifitsIds)
                        ->with('degreeLevels', $degreeLevels)
                        ->with('languages', $languages)
                        ->with('salaryPeriods', $salaryPeriods);
    }

    public function storeJob(JobFormRequest $request)
    {
        foreach ($request->input('skills') as $skill) {
            $job_skill= JobSkill::where('id', $skill)->pluck('job_skill')->toArray();
            $new_data[] = $job_skill;
        }
        $skill_data = "";
        for ($i=0; $i < count($new_data); $i++) {
            $skill_data .= $new_data[$i][0]. ",";
        }
        $skill_data = substr($skill_data, 0,-1);

        $job = new Job();
        $job->company_id = $request->input('company_id');
        $job = $this->assignJobValues($job, $request);
        $job->is_active = $request->input('is_active');
        $job->status = 'active';
        $job->admin_reviewed = 1;
        $job->is_featured = $request->input('is_featured');
        $job->job_skill = $skill_data;

        $job->save();
        /*         * ******************************* */
        
        $address = explode(",", $job->getLocation());
        array_pop($address);
        $jobSlug = strtolower(\Str::slug( $job->title . '-' . implode("-", $address)));

        $slug_count = Job::where(function ($query) use ($jobSlug) {
            $query->where('slug',  $jobSlug)->orWhere('slug', 'like', $jobSlug . '-%');
        })->where('id', '!=', $job->id)->count();

        if($slug_count) {
            $jobSlug .= '-' . $slug_count;
        }

        $job->slug = $jobSlug; // \Illuminate\Support\Str::slug($job->title, '-');
        /*         * ******************************* */
        $job->meta_title = \Illuminate\Support\Str::limit(strip_tags(htmlspecialchars_decode($job->title.' in '. $job->getLocation())), 65,'');
        $job->meta_description = \Illuminate\Support\Str::limit(strip_tags(htmlspecialchars_decode($job->description)), 165,'');

        $val = explode(',', $job->job_skill);
        $data = implode(", ", $val);
        $job->meta_keywords = $data;

        $job->update();

        /*         * ************************************ */
        /*         * ************************************ */
        $this->storeJobSkills($request, $job->id);
        /*         * ************************************ */
        $this->updateFullTextSearch($job);
        /*         * ************************************ */
        flash('Your job has been posted.')->success();
        return \Redirect::route('edit.job', array($job->id));
    }

    public function editJob($id)
    {
        $benifits = DataArrayHelper::langBenifitsArray();
        $bonus = DataArrayHelper::langBonusArray();
        $companies = DataArrayHelper::companiesArray();
        $countries = DataArrayHelper::defaultCountriesArray();
        $currencies = DataArrayHelper::currenciesArray();
        $careerLevels = DataArrayHelper::defaultCareerLevelsArray();
        $functionalAreas = DataArrayHelper::defaultFunctionalAreasArray();
        $jobTypes = DataArrayHelper::defaultJobTypesArray();
        $jobShifts = DataArrayHelper::defaultJobShiftsArray();
        $genders = DataArrayHelper::defaultGendersArray();
        $jobExperiences = DataArrayHelper::defaultJobExperiencesArray();
        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        $degreeLevels = DataArrayHelper::defaultDegreeLevelsArray();
        $salaryPeriods = DataArrayHelper::defaultSalaryPeriodsArray();

        $job = Job::findOrFail($id);
        $jobSkillIds = $job->getJobSkillsArray();
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $bonusIds = json_decode($job->bonus);
        $benifitsIds = json_decode($job->benifits);
        return view('admin.job.edit')
                        ->with('benifits', $benifits)
                        ->with('bonus', $bonus)
                        ->with('companies', $companies)
                        ->with('countries', $countries)
                        ->with('currencies', array_unique($currencies))
                        ->with('careerLevels', $careerLevels)
                        ->with('functionalAreas', $functionalAreas)
                        ->with('languages', $languages)
                        ->with('jobTypes', $jobTypes)
                        ->with('jobShifts', $jobShifts)
                        ->with('genders', $genders)
                        ->with('jobExperiences', $jobExperiences)
                        ->with('jobSkills', $jobSkills)
                        ->with('jobSkillIds', $jobSkillIds)
                        ->with('bonusIds', $bonusIds)
                        ->with('benifitsIds', $benifitsIds)
                        ->with('degreeLevels', $degreeLevels)
                        ->with('salaryPeriods', $salaryPeriods)
                        ->with('job', $job);
    }

    public function updateJob($id, JobFormRequest $request)
    {
        foreach ($request->input('skills') as $skill) {
            $job_skill= JobSkill::where('id', $skill)->pluck('job_skill')->toArray();
            $new_data[] = $job_skill;
        }
        $skill_data = "";
        for ($i=0; $i < count($new_data); $i++) {
            $skill_data .= $new_data[$i][0]. ",";
        }
        $skill_data = substr($skill_data, 0,-1);
        
        $job = Job::findOrFail($id);
        $job->company_id = $request->input('company_id');
        $job = $this->assignJobValues($job, $request);
        $job->is_active = $request->input('is_active');
        $job->status = 'active';
        if($request->input('is_active')==1){
            $job->admin_reviewed = 1;
        }
        $job->is_featured = $request->input('is_featured');
        $job->job_skill = $skill_data;
        /*         * ******************************* */
        // $address = explode(",", $job->getLocation());
        // $jobSlug = strtolower(\Str::slug( $job->title . '-' . implode("-", $address)));

        // $slug_count = Job::where(function ($query) use ($jobSlug) {
        //     $query->where('slug',  $jobSlug)->orWhere('slug', 'like', $jobSlug . '-%');
        // })->where('id', '!=', $job->id)->count();

        // if($slug_count) {
        //     $jobSlug .= '-' . $slug_count;
        // }

        // $job->slug = $jobSlug; // \Illuminate\Support\Str::slug($job->title, '-');
        /*         * ******************************* */
        $job->is_active = 0;
        $job->admin_reviewed = 0;
        /*         * ************************************ */
        $job->update();
        /*         * ************************************ */
        $this->storeJobSkills($request, $job->id);
        /*         * ************************************ */
        $this->updateFullTextSearch($job);
        /*         * ************************************ */
        flash('Job details updated. Click: Approve Job, to go live.')->success();
        return \Redirect::route('edit.job', array($job->id));
    }

    public function updateMetaJob($id, Request $request)
    {
        $job = Job::findOrFail($id);
        $job->meta_title = $request->input('meta_title');
        $job->meta_description = $request->input('meta_description');
        $job->meta_keywords = $request->input('meta_keywords');
         $job->update();
        return \Redirect::route('edit.job', array($job->id));
    }

    /*     * *************************************** */
    /*     * *************************************** */

    public function createFrontJob()
    {
        $company = Auth::guard('company')->user();
		
		if ((bool)$company->is_active === false) {
            flash(__('Your account is inactive contact site admin to activate it'))->error();
            return \Redirect::route('company.home');
            exit;
        }
	
        
		$benifits = DataArrayHelper::langBenifitsArray();
        $bonus = DataArrayHelper::langBonusArray();
        $countries = DataArrayHelper::langCountriesArray();
        $currencies = DataArrayHelper::currenciesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $jobTypes = DataArrayHelper::langJobTypesArray();
        $jobShifts = DataArrayHelper::langJobShiftsArray();
        $genders = DataArrayHelper::langGendersArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $degreeLevels = DataArrayHelper::langDegreeLevelsArray();
        $salaryPeriods = DataArrayHelper::langSalaryPeriodsArray();
        $languages = DataArrayHelper::languagesNativeCodeArray();

        $jobSkillIds = array();
        $bonusIds = array();
        $benifitsIds = array();
        return view('job.add_edit_job')
                        ->with('benifits', $benifits)
                        ->with('bonus', $bonus)
                        ->with('countries', $countries)
                        ->with('currencies', array_unique($currencies))
                        ->with('careerLevels', $careerLevels)
                        ->with('functionalAreas', $functionalAreas)
                        ->with('jobTypes', $jobTypes)
                        ->with('jobShifts', $jobShifts)
                        ->with('genders', $genders)
                        ->with('jobExperiences', $jobExperiences)
                        ->with('jobSkills', $jobSkills)
                        ->with('languages', $languages)
                        ->with('jobSkillIds', $jobSkillIds)
                        ->with('bonusIds', $bonusIds)
                        ->with('benifitsIds', $benifitsIds)
                        ->with('degreeLevels', $degreeLevels)
                        ->with('salaryPeriods', $salaryPeriods);
    }

    public function storeFrontJob(JobFrontFormRequest $request)
    {
        $company = Auth::guard('company')->user();

        $job = new Job();
        $job->company_id = $company->id;
        $job = $this->assignJobValues($job, $request);
        $job->is_active = 0;
        $job->admin_reviewed = 0;
        $job->status = 'admin_review';
        $job->save();
        /*         * ******************************* */
        $address = explode(",", $job->getLocation());
        array_pop($address);
        $jobSlug = strtolower(\Str::slug( $job->title . '-' . implode("-", $address)));

        $slug_count = Job::where(function ($query) use ($jobSlug) {
            $query->where('slug',  $jobSlug)->orWhere('slug', 'like', $jobSlug . '-%');
        })->where('id', '!=', $job->id)->count();

        if($slug_count) {
            $jobSlug .= '-' . $slug_count;
        }

        $job->slug = $jobSlug; // \Illuminate\Support\Str::slug($job->title, '-');
        /*         * ******************************* */
        $job->update();
        /*         * ************************************ */
        /*         * ************************************ */
        $this->storeJobSkills($request, $job->id);
        /*         * ************************************ */
        $this->updateFullTextSearch($job);
        /*         * ************************************ */

        /*         * ******************************* */
        $company->availed_jobs_quota = $company->availed_jobs_quota + 1;
        $company->update();
        /*         * ******************************* */

        event(new JobPosted($job));
        $data = array(
            'company'=> $company,
            'job'=> $job,
            'full_name'=> $company->name,
            'email'=> $company->email,
            'subject'=> 'New Job Posted by '.$company->name,

        );
        Mail::send(new JobPostedMail($data));
        flash('Job details sent to admin for review, a member of our team will contact you shortly')->success();
        return \Redirect::route('edit.front.job', array($job->id));
    }

    public function editFrontJob($id)
    {
        $benifits = DataArrayHelper::langBenifitsArray();
        $bonus = DataArrayHelper::langBonusArray();
        $countries = DataArrayHelper::langCountriesArray();
        $currencies = DataArrayHelper::currenciesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $jobTypes = DataArrayHelper::langJobTypesArray();
        $jobShifts = DataArrayHelper::langJobShiftsArray();
        $genders = DataArrayHelper::langGendersArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $degreeLevels = DataArrayHelper::langDegreeLevelsArray();
        $salaryPeriods = DataArrayHelper::langSalaryPeriodsArray();
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $job = Job::findOrFail($id);
        $jobSkillIds = $job->getJobSkillsArray();

        $bonusIds = json_decode($job->bonus);
        $benifitsIds = json_decode($job->benifits);
        return view('job.add_edit_job')
                        ->with('benifits', $benifits)
                        ->with('bonus', $bonus)
                        ->with('countries', $countries)
                        ->with('currencies', array_unique($currencies))
                        ->with('careerLevels', $careerLevels)
                        ->with('functionalAreas', $functionalAreas)
                        ->with('jobTypes', $jobTypes)
                        ->with('jobShifts', $jobShifts)
                        ->with('genders', $genders)
                        ->with('languages', $languages)
                        ->with('jobExperiences', $jobExperiences)
                        ->with('jobSkills', $jobSkills)
                        ->with('jobSkillIds', $jobSkillIds)
                        ->with('bonusIds', $bonusIds)
                        ->with('benifitsIds', $benifitsIds)
                        ->with('degreeLevels', $degreeLevels)
                        ->with('salaryPeriods', $salaryPeriods)
                        ->with('job', $job);
    }

    public function updateFrontJob($id, JobFrontFormRequest $request)
    {
        $company = Auth::guard('company')->user();
        $job = Job::findOrFail($id);
        Session::put('job_'.$job->id, json_encode($job));
        
        $job = $this->assignJobValues($job, $request);
        /*         * ******************************* */
        // $address = explode(",", $job->getLocation());
        // $jobSlug = strtolower(\Str::slug( $job->title . '-' . implode("-", $address)));

        // $slug_count = Job::where(function ($query) use ($jobSlug) {
        //     $query->where('slug',  $jobSlug)->orWhere('slug', 'like', $jobSlug . '-%');
        // })->where('id', '!=', $job->id)->count();

        // if($slug_count) {
        //     $jobSlug .= '-' . $slug_count;
        // }

        // $job->slug = $jobSlug; // \Illuminate\Support\Str::slug($job->title, '-');
        /*         * ******************************* */

        /*         * ************************************ */
        $job->is_active = 0;
        $job->admin_reviewed = 0;
        $job->is_updated = 1;

        $job->status = 'admin_review';

        $job->update();

        $update_fields = array();

        $old_job = json_decode(Session::get('job_'.$job->id));

        if(strcmp($old_job->title,$job->title) !== 0){
            $update_fields['title'] = '<span style="color:red">'.$job->title.'</span>';
        }else{
            $update_fields['title'] = '<span>'.$job->title.'</span>';
        }

        if(strcmp($old_job->description,$job->description) !== 0){
             $update_fields['description'] = '<div style="color:red">'.$job->description.'</div>';
        }else{
            $update_fields['description'] = '<span>'.$job->description.'</span>';
        }

        $job_skillss = JobSkillManager::where('job_id',$old_job->id)->pluck('job_skill_id')->toArray();

        
        if($job_skillss){
            if(array_diff($request->skills,$job_skillss)){
                 $update_fields['skills'] = '<span style="color:red">'.$job->getJobSkillsStr().'</span>';
            }else{
                $update_fields['skills'] = '<span>'.$job->getJobSkillsStr().'</span>';
            }
        }else{
            $update_fields['skills'] = '<span>'.$job->getJobSkillsStr().'</span>';
        }

       

        if(strcmp($old_job->country_id,$job->country_id) !== 0){
             $update_fields['country'] = '<span style="color:red">'.$job->getCountry('country').'</span>';
        }else{
            $update_fields['country'] = '<span>'.$job->getCountry('country').'</span>';
        }

        if(strcmp($old_job->state_id,$job->state_id) !== 0){
             $update_fields['state'] = '<span style="color:red">'.$job->getState('state').'</span>';
        }else{
            $update_fields['state'] = '<span>'.$job->getState('state').'</span>';
        }

        if(strcmp($old_job->city_id,$job->city_id) !== 0){
             $update_fields['city'] = '<span style="color:red">'.$job->getCity('city').'</span>';
        }else{
            $update_fields['city'] = '<span>'.$job->getCity('city').'</span>';
        }

        if(strcmp($old_job->address,$job->address) !== 0){
             $update_fields['address'] = '<span style="color:red">'.$job->address.'</span>';
        }else{
            $update_fields['address'] = '<span>'.$job->address.'</span>';
        }

        if(strcmp($old_job->salary_period_id,$job->salary_period_id) !== 0){
             $update_fields['salary_period'] = '<span style="color:red">'.$job->getSalaryPeriod('salary_period').'</span>';
        }else{
            $update_fields['salary_period'] = '<span>'.$job->getSalaryPeriod('salary_period').'</span>';
        }

        if(strcmp($old_job->salary_type,$job->salary_type) !== 0){
             $update_fields['salary_type'] = '<span style="color:red">'.$job->salary_type.'</span>';
        }else{
            $update_fields['salary_type'] = '<span>'.$job->salary_type.'</span>';
        }

        if(strcmp($old_job->salary_from,$job->salary_from) !== 0){
             $update_fields['salary_from'] = '<span style="color:red">'.$job->salary_from.'</span>';
        }else{
            $update_fields['salary_from'] = '<span>'.$job->salary_from.'</span>';
        }

        if(strcmp($old_job->salary_to,$job->salary_to) !== 0){
             $update_fields['salary_to'] = '<span style="color:red">'.$job->salary_to.'</span>';
        }else{
            $update_fields['salary_to'] = '<span>'.$job->salary_to.'</span>';
        }

        if(strcmp($old_job->salary_currency,$job->salary_currency) !== 0){
             $update_fields['salary_currency'] = '<span style="color:red">'.$job->salary_currency.'</span>';
        }else{
            $update_fields['salary_currency'] = '<span>'.$job->salary_currency.'</span>';
        }


        $all_bouns = '';
        if(null!==($job->bonus)){
            $bon = json_decode($job->bonus);
            if(null!==($bon)){
                foreach ($bon as $key => $value) {
                    $bonus = Bonus::where('id',$value)->first();
                    if(null!==($bonus)){
                       $all_bouns .=$bonus->bonus.', '; 
                    }
                }
            }

        }
        if(strcmp($old_job->bonus,$job->bonus) !== 0){
            $update_fields['bonus'] = '<span style="color:red">'.$all_bouns.'</span>';
        }else{
            $update_fields['bonus'] = '<span>'.$all_bouns.'</span>';
        }



        $all_benifits = '';
        if(null!==($job->benifits)){
            $beni = json_decode($job->benifits);
            if(null!==($beni)){
                foreach ($beni as $key => $val) {
                    $benifits = Benifits::where('id',$val)->first();
                    if(null!==($benifits)){
                       $all_benifits .=$benifits->benifits.', ';
                    }
                }
            }

        }

        if(strcmp($old_job->benifits,$job->benifits) !== 0){
            $update_fields['benifits'] = '<span style="color:red">'.$all_benifits.'</span>';
        }else{
            $update_fields['benifits'] = '<span>'.$all_benifits.'</span>';
        }

        if(strcmp($old_job->career_level_id,$job->career_level_id) !== 0){
             $update_fields['career_level'] = '<span style="color:red">'.$job->getCareerLevel('career_level').'</span>';
        }else{
            $update_fields['career_level'] = '<span>'.$job->getCareerLevel('career_level').'</span>';
        }

        if(strcmp($old_job->functional_area_id,$job->functional_area_id) !== 0){
             $update_fields['functional_area'] = '<span style="color:red">'.$job->getFunctionalArea('functional_area').'</span>';
        }else{
            $update_fields['functional_area'] = '<span>'.$job->getFunctionalArea('functional_area').'</span>';
        }

        if(strcmp($old_job->job_type_id,$job->job_type_id) !== 0){
             $update_fields['job_type'] = '<span style="color:red">'.$job->getJobType('job_type').'</span>';
        }else{
            $update_fields['job_type'] = '<span>'.$job->getJobType('job_type').'</span>';
        }

        if(strcmp($old_job->job_shift_id,$job->job_shift_id) !== 0){
             $update_fields['job_shift'] = '<span style="color:red">'.$job->getJobShift('job_shift').'</span>';
        }else{
            $update_fields['job_shift'] = '<span>'.$job->getJobShift('job_shift').'</span>';
        }

        if(strcmp($old_job->num_of_positions,$job->num_of_positions) !== 0){
             $update_fields['num_of_positions'] = '<span style="color:red">'.$job->num_of_positions.'</span>';
        }else{
            $update_fields['num_of_positions'] = '<span>'.$job->num_of_positions.'</span>';
        }

        if(strcmp($old_job->expiry_datee,$job->expiry_datee) !== 0){
             $update_fields['expiry_datee'] = '<span style="color:red">'.$job->expiry_datee.'</span>';
        }else{
            $update_fields['expiry_datee'] = '<span>'.$job->expiry_datee.'</span>';
        }

        if(strcmp($old_job->degree_level_id,$job->degree_level_id) !== 0){
             $update_fields['degree_level'] = '<span style="color:red">'.$job->getDegreeLevel('degree_level').'</span>';
        }else{
            $update_fields['degree_level'] = '<span>'.$job->getDegreeLevel('degree_level').'</span>';
        }

        if(strcmp($old_job->job_experience_id,$job->job_experience_id) !== 0){
             $update_fields['job_experience'] = '<span style="color:red">'.$job->getJobExperience('job_experience').'</span>';
        }else{
            $update_fields['job_experience'] = '<span>'.$job->getJobExperience('job_experience').'</span>';
        }
        //dd($update_fields);
        $data = array(
            'company'=> $company,
            'job'=> $job,
            'full_name'=> $company->name,
            'email'=> $company->email,
            'subject'=> 'Job Updated by '.$company->name,
            'update_fields' => $update_fields,

        );

        //dd($data);
        Mail::send(new JobUpdatedMailable($data));
        /*         * ************************************ */
        $this->storeJobSkills($request, $job->id);
        /*         * ************************************ */
        $this->updateFullTextSearch($job);
        /*         * ************************************ */
        flash('Job details sent to admin for review, a member of our team will contact you shortly')->success();
        return \Redirect::route('edit.front.job', array($job->id));
    }

    public static function countNumJobs($field = 'title', $value = '')
    {
        if (!empty($value)) {
            if ($field == 'title') {
                return DB::table('jobs')->where('title', 'like', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'company_id') {
                return DB::table('jobs')->where('company_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'industry_id') {
                $company_ids = Company::where('industry_id', '=', $value)->where('is_active', '=', 1)->pluck('id')->toArray();
                return DB::table('jobs')->whereIn('company_id', $company_ids)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'job_skill_id') {
                $job_ids = JobSkillManager::where('job_skill_id', '=', $value)->pluck('job_id')->toArray();
                return DB::table('jobs')->whereIn('id', array_unique($job_ids))->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'functional_area_id') {
                return DB::table('jobs')->where('functional_area_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'career_level_id') {
                return DB::table('jobs')->where('career_level_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'job_type_id') {
                return DB::table('jobs')->where('job_type_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'job_shift_id') {
                return DB::table('jobs')->where('job_shift_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'gender_id') {
                return DB::table('jobs')->where('gender_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'degree_level_id') {
                return DB::table('jobs')->where('degree_level_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'job_experience_id') {
                return DB::table('jobs')->where('job_experience_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'country_id') {
                return DB::table('jobs')->where('country_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'state_id') {
                return DB::table('jobs')->where('state_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
            if ($field == 'city_id') {
                return DB::table('jobs')->where('city_id', '=', $value)->where('is_active', '=', 1)->where('expiry_date', '>',  \Carbon\Carbon::now())->count('id');
            }
        }
    }

    public function scopeNotExpire($query)
    {
        return $query->whereDate('expiry_date', '>', Carbon::now()); //where('expiry_date', '>=', date('Y-m-d'));
    }
    
    public function isJobExpired()
    {
        return ($this->expiry_date < Carbon::now())? true:false;
    }

}
