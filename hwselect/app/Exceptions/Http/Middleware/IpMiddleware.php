<?php



namespace App\Http\Middleware;
   
use Closure;
use App\Modules_data;
use Illuminate\Http\Request;
   
class IpMiddleware
{
    
    public $restrictIps = array();
        
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      return true;
        //dd($request->ip());
    	$ip = $request->ip();
		$json_data = file_get_contents("https://api.hostip.info/get_json.php?ip=$ip");
		
		$ip_data = json_decode($json_data, TRUE);
       /* $xml = file_get_contents("https://tools.keycdn.com/geo.json?host=".$request->ip());
        $arr = json_decode($xml);*/
        $country_name = strtolower($ip_data['country_name']);
        $country_name = ucfirst($country_name);
        $restrictIps = Modules_data::select('title')->where('modules_id',1)->where('status','blocked')->pluck('title')->toArray();
        //dd($restrictIps);
        if (in_array($country_name, $restrictIps)) {
    
            return abort(403,'Your IP is blocked by admin');
        }
    
        return $next($request);
    }

    public function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
}

