<?php



namespace App\Http\Requests\Front;



use App\Http\Requests\Request;



class ReportAbuseFormRequest extends Request

{



    /**

     * Determine if the user is authorized to make this request.

     *

     * @return bool

     */

    public function authorize()

    {

        return true;

    }



    /**

     * Get the validation rules that apply to the request.

     *

     * @return array

     */

    public function rules()

    {

        return [

            'job_id' => 'required|numeric',

            'reason' => 'required',

        ];

    }



    // public function messages()

    // {

    //     return [

    //         'your_name.required' => __('Your name is required'),

    //         'your_email.required' => __('Your email address is required'),

    //         'your_email.email' => __('A valid email address is required'),

    //         'job_url.required' => __('Job url is required'),

    //         'job_url.url' => __('Job url must be a valid URL'),

    //         'g-recaptcha-response.required' => __('Please tick, I am not a Robot to validate.'),

    //         'g-recaptcha-response.captcha' => __('Captcha error! try again later'),

    //     ];

    // }



}

