<?php



namespace App\Http\Requests\Front;



use App\Http\Requests\Request;



class ContactFormRequest extends Request

{



    /**

     * Determine if the user is authorized to make this request.

     *

     * @return bool

     */

    public function authorize()

    {

        return true;

    }



    /**

     * Get the validation rules that apply to the request.

     *

     * @return array

     */

    public function rules()

    {

        return [

            'type' => 'required',
            'full_name' => 'required|max:30',

            'email' => 'required|email|max:80',

            'phone' => 'required|max:20',

            //'subject' => 'required|max:100',

            'message_txt' => 'required',
            'best_time.*' => 'required',

            'g-recaptcha-response' => 'required|captcha',

        ];

    }



    public function messages()

    {

        return [

            'full_name.required' => __('Full Name Required'),

            'email.required' => __('E-mail address required'),

            'email.email' => __('Valid e-mail address required'),

            //'subject.required' => __('Subject required'),
            'phone.required' => __('Phone Number Required'),
            'type.required' => __('Select Company or Job Seeker'),

            'message_txt.required' => __('Message required'),
            'g-recaptcha-response.required' => __('Google recaptcha required'),

        ];

    }



}

