<?php

namespace App\Http\Requests\Front;

use Auth;
use App\Http\Requests\Request;

class CompanyFrontFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) Auth::guard('company')->user()->id;
                    $unique_id = ($id > 0) ? ',' . $id : '';
                    return [
                        "id" => "",
                        "name" => "required|max:150",
                        "ceo" => "max:60",
                        "industry_id" => "required",
                        "ownership_type_id" => "required",
                        "location" => "required|max:150",
                        //"map" => "required",
                        //"website" => "url",
                        //"fax" => "required|max:30",
                        "phone" => "required|max:30",
                        "logo" => 'image',
                        "country_id" => "required",
                        "state_id" => "required",
                        "city_id" => "required",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
            'name.required' => __('Name is required'),
            'password.required' => __('Password is required'),
            'ceo.required' => __('CEO name is required'),
            'industry_id.required' => __('Please select Industry'),
            'ownership_type_id.required' => __('Please select Ownership Type'),
            'location.required' => __('Location required'),
            //'map.required' => __('Google Map required'),
            //'website.url' => __('URL invalid, please enter correct format'),
            //'fax.required' => __('Fax number required'),
            'phone.required' => __('Phone number required'),
            'logo.image' => __('Only Images can be used as logo'),
            'logo.size' => __('File must be less then the size of 6 MB'),
            'country_id.required' => __('Please select country'),
            'state_id.required' => __('Please Select County / State / Province / District'),
            'city_id.required' => __('Please Select Town/City'),
        ];
    }

}
