<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BenifitsFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $benifits_unique = '';
                    if ($id > 0) {
                        $benifits_unique = ',id,' . $id;
                    }
                    return [
                        'benifits' => 'required|unique:benifits' . $benifits_unique,
                        'benifits_id' => 'required_if:is_default,0',
                        'is_active' => 'required',
                        'is_default' => 'required',
                        'lang' => 'required',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
            'benifits.required' => 'Please enter benifits.',
            'benifits_id.required_if' => 'Please select default/fallback benifits.',
            'is_default.required' => 'Is this benifits default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',
        ];
    }

}
