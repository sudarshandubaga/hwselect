<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use Input;
use Redirect;
use App\Language;
use App\Modules_data;
use App\Subscription;
use App\User;
use App\Job;
use App\Testimonial;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\NewsletterFormRequest;
use App\Http\Controllers\Controller;
use App\Mail\SendNewsletter;

use Mail;

class NewsletterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexTestimonials()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.newsletter.index')->with('languages', $languages);
    }

    public function sendNewsLetter($id)
    {
        $letter = Modules_data::findorFail($id);
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.newsletter.add')
                        ->with('languages', $languages)
                        ->with('letter', $letter);
    }

    public function storeNewsletter(NewsletterFormRequest $request)
    {

        if($request->send_to=='seeker'){
            $users = Subscription::where('type','seeker')->get();
            //dd($users);
            if(null!==($users)){
                foreach ($users as $key => $val) {
                    $user = User::where('email','like','%'.$val->email.'%')->first();

                    if(null!==($user->functional_area_id)){
                        $jobs = Job::where('functional_area_id',$user->functional_area_id)->orderBy('id', 'DESC')->take(3)->get();
                    }else{
                        $jobs = Job::orderBy('id', 'DESC')->take(3)->get();
                    }

                    $data['name'] = $val->name;
                    $data['email'] = $val->email;
                    $data['title'] = $request->title;
                    $data['description'] = $request->description;
                    $data['type'] = 'seeker';
                    $data['jobs'] = $jobs;
                    Mail::send(new SendNewsletter($data));
                    
                }
                
            }
            
        }else{
            $users = Subscription::where('type','company')->get();
            if(null!==($users)){
                foreach ($users as $key => $val) {
                    $user = User::where('email','like','%'.$val->email.'%')->first();
                    $data['name'] = $val->name;
                    $data['email'] = $val->email;
                    $data['title'] = $request->title;
                    $data['description'] = $request->description;
                    $data['type'] = 'company';

                    Mail::send(new SendNewsletter($data));
                    
                }
                
            }
        }
        /*         * ************************************ */
        flash('Newsletter has been sent!')->success();
        return \Redirect::route('modules-data','newsletter');
    }

   

}
