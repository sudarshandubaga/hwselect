<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use Input;
use File;
use Carbon\Carbon;
use ImgUploader;
use Redirect;
use App\Bonus;
use App\Language;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\BonusFormRequest;
use App\Http\Controllers\Controller;

class BonusController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexbonuss()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.bonus.index')->with('languages', $languages);
    }

    public function createbonus()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $bonuss = DataArrayHelper::langBonusArray();
        return view('admin.bonus.add')
                        ->with('languages', $languages)
                        ->with('bonuss', $bonuss);
    }

    public function storebonus(BonusFormRequest $request)
    {
        $bonus = new Bonus();
        $bonus->bonus = $request->input('bonus');
        $bonus->is_active = $request->input('is_active');
        $bonus->lang = $request->input('lang');
        $bonus->is_default = $request->input('is_default');
        $bonus->save();
        /*         * ************************************ */
        $bonus->sort_order = $bonus->id;
        if ((int) $request->input('is_default') == 1) {
            $bonus->bonus_id = $bonus->id;
        } else {
            $bonus->bonus_id = $request->input('bonus_id');
        }
        $bonus->update();
        flash('Bonus has been added!')->success();
        return \Redirect::route('edit.bonus', array($bonus->id));
    }

    public function editbonus($id)
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $bonuss = DataArrayHelper::langBonusArray();
        $bonus = Bonus::findOrFail($id);
        return view('admin.bonus.edit')
                        ->with('languages', $languages)
                        ->with('bonus', $bonus)
                        ->with('bonuss', $bonuss);
    }

    public function updatebonus($id, BonusFormRequest $request)
    {
        $bonus = Bonus::findOrFail($id);
        $bonus->bonus = $request->input('bonus');
        $bonus->is_active = $request->input('is_active');
        $bonus->lang = $request->input('lang');
        $bonus->is_default = $request->input('is_default');
        if ((int) $request->input('is_default') == 1) {
            $bonus->bonus_id = $bonus->id;
        } else {
            $bonus->bonus_id = $request->input('bonus_id');
        }
        $bonus->update();
        flash('Bonus has been updated!')->success();
        return \Redirect::route('edit.bonus', array($bonus->id));
    }

    public function deletebonus(Request $request)
    {
        $id = $request->input('id');
        try {
            $bonus = Bonus::findOrFail($id);
            if ((bool) $bonus->is_default) {
                Bonus::where('bonus_id', '=', $bonus->bonus_id)->delete();
            } else {
                $bonus->delete();
            }
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    public function fetchbonussData(Request $request)
    {
        $bonuss = Bonus::select(['bonus.id', 'bonus.bonus', 'bonus.is_active', 'bonus.lang', 'bonus.is_default', 'bonus.created_at', 'bonus.updated_at'])->sorted();
        return Datatables::of($bonuss)
                        ->filter(function ($query) use ($request) {
                            if ($request->has('bonus') && !empty($request->bonus)) {
                                $query->where('bonus.bonus', 'like', "%{$request->get('bonus')}%");
                            }
                            if ($request->has('lang') && !empty($request->get('lang'))) {
                                $query->where('bonus.lang', 'like', "%{$request->get('lang')}%");
                            }
                            if ($request->has('is_active') && $request->get('is_active') != -1) {
                                $query->where('bonus.is_active', '=', "{$request->get('is_active')}");
                            }
                        })
                        ->addColumn('bonus', function ($bonuss) {
                            $bonus = \Illuminate\Support\Str::limit($bonuss->bonus, 100, '...');
                            $direction = MiscHelper::getLangDirection($bonuss->lang);
                            return '<span dir="' . $direction . '">' . $bonus . '</span>';
                        })
                        ->addColumn('action', function ($bonuss) {
                            /*                             * ************************* */
                            $activeTxt = 'Make Active';
                            $activeHref = 'makeActive(' . $bonuss->id . ');';
                            $activeIcon = 'square-o';
                            if ((int) $bonuss->is_active == 1) {
                                $activeTxt = 'Make InActive';
                                $activeHref = 'makeNotActive(' . $bonuss->id . ');';
                                $activeIcon = 'square-o';
                            }
                            $title = "'".$bonuss->bonus."'";
                            return '
				<div class="btn-group">
					<button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu">
						<li>
							<a href="' . route('edit.bonus', ['id' => $bonuss->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
						</li>						
						<li>
							<a href="javascript:void(0);" onclick="deletebonus(' . $bonuss->id . ', ' . $bonuss->is_default . ','.$title.');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
						</li>
						<li>
						<a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $bonuss->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
						</li>																																		
					</ul>
				</div>';
                        })
                        ->rawColumns(['bonus', 'action'])
                        ->setRowId(function($bonuss) {
                            return 'bonusDtRow' . $bonuss->id;
                        })
                        ->make(true);
        //$query = $dataTable->getQuery()->get();
        //return $query;
    }

    public function makeActivebonus(Request $request)
    {
        $id = $request->input('id');
        try {
            $bonus = Bonus::findOrFail($id);
            $bonus->is_active = 1;
            $bonus->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeNotActivebonus(Request $request)
    {
        $id = $request->input('id');
        try {
            $bonus = Bonus::findOrFail($id);
            $bonus->is_active = 0;
            $bonus->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function sortbonuss()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.bonus.sort')->with('languages', $languages);
    }

    public function bonussortData(Request $request)
    {
        $lang = $request->input('lang');
        $bonuss = Bonus::select('bonus.id', 'bonus.bonus', 'bonus.sort_order')
                ->where('bonus.lang', 'like', $lang)
                ->orderBy('bonus.sort_order')
                ->get();
        $str = '<ul id="sortable">';
        if ($bonuss != null) {
            foreach ($bonuss as $bonus) {
                $str .= '<li id="' . $bonus->id . '"><i class="fa fa-sort"></i>' . $bonus->bonus . '</li>';
            }
        }
        echo $str . '</ul>';
    }

    public function bonussortUpdate(Request $request)
    {
        $bonusOrder = $request->input('bonusOrder');
        $bonusOrderArray = explode(',', $bonusOrder);
        $count = 1;
        foreach ($bonusOrderArray as $bonusId) {
            $bonus = Bonus::find($bonusId);
            $bonus->sort_order = $count;
            $bonus->update();
            $count++;
        }
    }

}
