<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Auth;
use App\Widget;
use Image;
use File;

use ImgUploader;

class WidgetsController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {

        $user = Widget::orderBy('id', 'desc')->get();
        if (Auth::check()) {
            return view('admin/widgets/widgets', compact('user'));
        } else {
            return redirect('login');
        }
    }


    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ], [
            'title.required' => ' The title field is required.',

            'content.required' => ' The content field is required.',
        ]);
        $widget = new Widget();
        $widget->heading = $request->title;
        $widget->location = $request->location;
        $widget->prev_link = $request->prev_link;
        $widget->content = $request->content;

        $widget->save();
        if ($widget->save() == true) {
            $request->session()->flash('message.added', 'success');
            $request->session()->flash('message.content', 'Widget added successfully !');
        } else {
            $request->session()->flash('message.added', 'danger');
            $request->session()->flash('message.content', 'Error!');
        }
        return redirect('/admin/widget');
    }
    public function edit_widget($id = '')
    {
        if ($id != '') {
            $widget = Widget::findOrFail($id);
            return view('admin/widgets/edit', compact('widget'));
        }
    }



    public function edit(Widget $widget)
    {

        if (Auth::check() && Auth::user()->id == $widget->user_id) {
            return view('widgets', compact('widget'));
        } else {
            return redirect('/widget');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'heading' => 'required',
            'content' => 'required',
            'image_update' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ], [
            'heading.required' => ' The title field is required.',
            'content.required' => ' The content field is required.',
        ]);


        $widget = Widget::findOrFail($request->id);
        $widget->heading = $request->heading;
        $widget->location = $request->location;
        $widget->prev_link = $request->prev_link;
        $widget->content = $request->content;
        if ($request->hasFile('image')) {

            $image = $request->file('image');

             $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());

              $fileName = ImgUploader::UploadImage('company_logos', $image, $nameonly, 10000, 10000, false);



                $widget->image = $fileName;

        }
        $widget->update();

        if ($widget->save() == true) {
            $request->session()->flash('message.added', 'success');
            $request->session()->flash('message.content', 'Page Content updated successfully!');
        } else {
            $request->session()->flash('message.added', 'danger');
            $request->session()->flash('message.content', 'Error!');
        }
        return redirect(route('widget'));
    }


    public function destroy($id)
    {
        $widget = Widget::findOrFail($id);
        $widget->delete();

        return response()->json($widget);
    }
}
