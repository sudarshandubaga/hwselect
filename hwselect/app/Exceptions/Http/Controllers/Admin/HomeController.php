<?php



namespace App\Http\Controllers\Admin;



use App\User;

use App\Job;
use App\Company;
use App\JobApply;

use Carbon\Carbon;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;



class HomeController extends Controller

{



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //$this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $today = Carbon::now();

        $totalActiveUsers = User::where('is_active', 1)->count();

        $totalVerifiedUsers = User::where('verified', 1)->count();
        $totalVerifiedCompanies = Company::where('verified', 1)->count();
        $totalCompanies = Company::count();

        $totalTodaysUsers = User::count();

        $recentUsers = User::orderBy('id', 'DESC')->take(25)->get();

        $totalDeleteJobs = Job::where('jobs.request_to_delete', '=', "yes")->count();

        $totalActiveJobs = Job::notExpire()->active()->count();

        $totalPendingJobs = Job::where('is_active', 0)->where('admin_reviewed', 0)->count();

        $totalFeaturedJobs = Job::where('is_featured', 1)->count();

        $totalJobs = Job::count();

        $totalTodaysJobs = Job::where('created_at', 'like', $today->toDateString() . '%')->count();
        $totalAppliedUsers = JobApply::where('created_at', 'like', $today->toDateString() . '%')->count();

        $recentJobs = Job::orderBy('id', 'DESC')->take(25)->get();

        return view('admin.home')

                        ->with('totalActiveUsers', $totalActiveUsers)

                        ->with('totalVerifiedUsers', $totalVerifiedUsers)

                        ->with('totalTodaysUsers', $totalTodaysUsers)

                        ->with('totalJobs', $totalJobs)
                        
                        ->with('totalDeleteJobs', $totalDeleteJobs)

                        ->with('recentUsers', $recentUsers)
                        ->with('totalVerifiedCompanies', $totalVerifiedCompanies)
                        ->with('totalCompanies', $totalCompanies)

                        ->with('totalActiveJobs', $totalActiveJobs)
                        
                        ->with('totalPendingJobs', $totalPendingJobs)

                        ->with('totalFeaturedJobs', $totalFeaturedJobs)

                        ->with('totalAppliedUsers', $totalAppliedUsers)
                        ->with('totalTodaysJobs', $totalTodaysJobs)

                        ->with('recentJobs', $recentJobs);

    }



}

