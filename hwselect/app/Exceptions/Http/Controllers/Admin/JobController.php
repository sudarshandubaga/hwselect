<?php



namespace App\Http\Controllers\Admin;



use Auth;

use DB;

use Input;

use Redirect;

use Carbon\Carbon;

use App\Job;
use App\JobApply;

use App\Company;

use App\Unlocked_users;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use DataTables;

use App\Http\Controllers\Controller;

use App\Traits\JobTrait;

use App\Helpers\MiscHelper;

use App\Helpers\DataArrayHelper;

use App\Mail\RecruitedJobMail;

use Mail;




class JobController extends Controller

{



    use JobTrait;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //

    }



    public function indexJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.index')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function expiredJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.expired')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function jobDetail(Request $request, $id)
    {

        $job = Job::findOrFail($id);
        
        return view('admin.job.detail')
                        ->with('job', $job);
    }

    public function deletedJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.deletedJobs')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }



    public function fetchJobsData(Request $request)

    {
        $jobs = Job::select([

                    'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','admin_reviewed','jobs.created_at',

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('description') && !empty($request->description)) {

                                $query->where('jobs.description', 'like', "%{$request->get('description')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_active') && $request->is_active != -1) {

                                $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                            }
                            if ($request->has('admin_reviewed') && $request->is_active != -1) {

                                $query->where('jobs.admin_reviewed', '=', "{$request->get('admin_reviewed')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }

                            $query->orderBy('jobs.id', "DESC");

                        })

                        ->addColumn('company_id', function ($jobs) {

                            return $jobs->getCompany('name');

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                        ->addColumn('description', function ($jobs) {

                            return strip_tags(\Illuminate\Support\Str::limit($jobs->description, 50, '...'));

                        })

                        ->addColumn('created_at', function ($jobs) {

                            return date('d/m/Y',strtotime($jobs->created_at));

                        })

                        ->addColumn('status', function ($jobs) {

                            $status = '';
                            if($jobs->admin_reviewed!==1){
                                $status = 'Pending (Not Approved)';
                            }else if($jobs->expiry_date <  \Carbon\Carbon::now()){
                                $status = 'Expired (Archive)';
                            }else if($jobs->is_active==1){
                                $status = 'Active';
                            }else{
                                $status = 'InActive '.$jobs->blocked_by;
                            }

                            return $status;

                        })

                        ->addColumn('action', function ($jobs) {

                            /*                             * ************************* */

                            $activeTxt = 'Make Active';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Make InActive';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }


                            $reviewedTxt = 'Approve Job';

                            $reviewedHref = 'makeReviewed(' . $jobs->id . ');';

                            $reviewedIcon = 'square-o';

                            if ((int) $jobs->admin_reviewed == 1) {

                                $reviewedTxt = 'Approval Refused';

                                $reviewedHref = 'makeNotReviewed(' . $jobs->id . ');';

                                $reviewedIcon = 'square-o';

                            }

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();
                             $title = '"'.$jobs->title.'"';
                             if($jobs->admin_reviewed!==1){
                                return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit - Extend</a>
                        </li>
                         <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li> 
                        <li>
                        <a href="javascript:void(0);" onClick="' . $reviewedHref . '" id="onclickReviewed' . $jobs->id . '"><i class="fa fa-' . $reviewedIcon . '" aria-hidden="true"></i>' . $reviewedTxt . '</a>
                        </li>


                    </ul>

                </div>';
            }else{
                return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                            <li>
                            <a href="' . route('admin.list.applied.users', [$jobs->id]) . '"><i class="fa fa-users" aria-hidden="true"></i>List Candidates <span class="badge badge-primary">'. $list_candidates.'</span></a>
                        </li>
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'all']) . '"><i class="fa fa-file" aria-hidden="true"></i>Short Listed Candidates<span class="badge badge-primary">'. $short_listed_candidates.'</span></a>
                        </li>
                       
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'called_for_interview']) . '"><i class="fa fa-lock" aria-hidden="true"></i>Called for Interview<span class="badge badge-primary">'. $interview.'</span></a>
                        </li>
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'interviewed']) . '"><i class="fa fa-check" aria-hidden="true"></i>Interviewed Candidates<span class="badge badge-primary">'. $interviewed.'</span></a>
                        </li>

                       

                                           

                  

                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit - Extend</a>
                        </li>

                        <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li> 
                        

                        <li>
                        <a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $jobs->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
                        </li>
                        <li>
                        <a href="javascript:void(0);" onClick="' . $reviewedHref . '" id="onclickReviewed' . $jobs->id . '"><i class="fa fa-' . $reviewedIcon . '" aria-hidden="true"></i>' . $reviewedTxt . '</a>
                        </li>

                        <li>
                        <a href="javascript:void(0);" onClick="' . $featuredHref . '" id="onclickFeatured' . $jobs->id . '"><i class="fa fa-' . $featuredIcon . '" aria-hidden="true"></i>' . $featuredTxt . '</a>
                        </li> 
                        
                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>

                    </ul>

                </div>';
            }
                            

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'description','status'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }



    public function fetchExpiredJobsData(Request $request)

    {

        $jobs = Job::select([

                    'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','jobs.is_archive',

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('expiry_date') && !empty($request->expiry_date)) {

                                $query->where('jobs.expiry_date', '>', "%{$request->get('expiry_date')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_active') && $request->is_active != -1) {

                                $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }

                            $query->where('expiry_date', '<', Carbon::now());
                            $query->orderBy('jobs.id', "DESC");
                        })

                        ->addColumn('company_id', function ($jobs) {

                            return $jobs->getCompany('name');

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                        ->addColumn('expiry_date', function ($jobs) {

                            return date('d, F Y',strtotime($jobs->expiry_date));

                        })

                        ->addColumn('action', function ($jobs) {

                            /*                             * ************************* */
                            $activeTxt = 'Archive Job';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Archive Job';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }
                          

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();
                             $title = '"'.$jobs->title.'"';
                            return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '#expiry_date"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Job or Expiry Date</a>
                        </li>

                        <li>
                        <a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $jobs->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
                        </li>

                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>

                    </ul>

                </div>';

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'expiry_date'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }




    public function fetchDelJobsData(Request $request)

    {

        $jobs = Job::select([

                    'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.reason', 'jobs.request_to_delete', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','jobs.updated_at',

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('description') && !empty($request->description)) {

                                $query->where('jobs.reason', 'like', "%{$request->get('description')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_active') && $request->is_active != -1) {

                                $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }
                            $query->where('jobs.request_to_delete', '=', "yes");
                            $query->orderBy('jobs.id', "DESC");

                        })

                        ->addColumn('company_id', function ($jobs) {

                            return $jobs->getCompany('name');

                        })

                        ->addColumn('reason', function ($jobs) {

                            return $jobs->reason;

                        })

                        ->addColumn('updated_at', function ($jobs) {

                            return date('d/m/Y',strtotime($jobs->updated_at));

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                        ->addColumn('description', function ($jobs) {

                            return strip_tags(\Illuminate\Support\Str::limit($jobs->reason, 50, '...'));

                        })

                        ->addColumn('action', function ($jobs) {

                            /*                             * ************************* */

                            $activeTxt = 'Make Active';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Make InActive';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();

                             $title = '"'.$jobs->title.'"';

                            return '

                <div class="btn-group">

                    <button class="btn red dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                          
                       
                        

                  
                         <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li>
                       
                        <li>
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete Job</a>
                        </li>
      

                    </ul>

                </div>';

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'description'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }




    public function makeActiveJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 1;
            $job->is_archive = 1;
            $job->admin_reviewed = 1;

            $job->blocked_by = '';

            $job->update();

            echo 'ok';

            $contactCompany = Company::findOrFail($job->company_id);
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] = 'Job Vacancy Now Live';
            $data['msg2'] = 'is now live and can be reviewed on the link below';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'Approved';
            $data['vacancy_status'] = 'Job Vacancy Updated by Admin Now Live';
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }

    public function makeReviewedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 1;
            $job->is_archive = 1;
            $job->admin_reviewed = 1;

            $job->update();

            echo 'ok';

            $contactCompany = Company::findOrFail($job->company_id);
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] = 'Job Vacancy Now Live';
            $data['msg2'] = 'is now live and can be reviewed on the link below';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'Approved';
            $data['vacancy_status'] = 'Job Vacancy Reviewed by Admin on '.date('d/m/Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }



    public function makeNotActiveJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 0;

            $job->is_archive = 0;

            $job->blocked_by = Auth::guard('admin')->user()->name;


            $job->update();

            echo 'ok';
            $contactCompany = Company::findOrFail($job->company_id);
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] = 'Job Vacancy Deactivated';
            $data['msg2'] = 'is deactivated and can no longer be viewed on live listing';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['subject'] = 'Deactivated';
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'Deactivated';
            $data['vacancy_status'] = 'Job Vacancy Deactivated by Admin: '.date('d/m/Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }



    public function makeFeaturedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_featured = 1;

            $job->update();

            echo 'ok';

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }



    public function makeNotFeaturedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_featured = 0;

            $job->update();

            echo 'ok';

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }



}

