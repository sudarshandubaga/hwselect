<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Modules;
use Str;

class ModulesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data['modules'] = Modules::get();
        return view('admin.modules.view_modules')->with($data);
    }
    public function add_module()
    {
        return view('admin.modules.add_module');
    }

    public function edit_module($id)
    {
        $data = array();
        $data['module'] = Modules::where('id', $id)->first();
        return view('admin.modules.edit_module')->with($data);
    }
    public function post_module(Request $request)
    {
        $this->validate($request, [
            'module_name' => 'required',
            'module_term' => 'required',
        ], [
            'module_name.required' => 'Module Name is required.',
            'module_term.required' => 'Module Term is required.',
        ]);
        //dd($request); 
        $slug = Str::slug($request->module_name, '-');
        $slugs = unique_slug($slug, 'modules', $field = 'slug', $key = NULL, $value = NULL);
        $module = new Modules();
        $module->module_name = $request->module_name;
        $module->module_term = $request->module_term;
        $module->thumbnail_height = $request->thumbnail_height;
        $module->thumbnail_width = $request->thumbnail_width;
        $module->slug = $slugs;
        $module->page_slug = $request->page_slug;
        /*$module->menu = $request->menu;*/
        $module->page_description = $request->page_description;
        $module->featured_image = $request->featured_image;
        $module->seo = $request->seo;
        $module->save();
        if ($module->save() == true) {
            $request->session()->flash('message.added', 'success');
            $request->session()->flash('message.content', 'A module has been successfully Created!');
        }
        return redirect(route('modules'));
    }
    public function destroy($id)
    {
        $module = Modules::findOrFail($id);
        $module->delete();
        return redirect(route('modules'))->with('warning', 'A module has been successfully Deleted!');
    }
}
