<?php



namespace App\Http\Controllers;



use App;

use App\Seo;

use App\Job;

use App\Company;

use App\FunctionalArea;

use App\Country;

use App\Video;

use App\Testimonial;

use App\Slider;

use App\Blog;

use App\JobSkill;
use App\SiteSetting;

use Illuminate\Http\Request;

use Redirect;

use App\Traits\CompanyTrait;

use App\Traits\FunctionalAreaTrait;

use App\Traits\CityTrait;

use App\Traits\JobTrait;

use App\Traits\Active;

use Cache;
use Artisan;

use Carbon\Carbon;

use App\Helpers\DataArrayHelper;



class IndexController extends Controller

{



    use CompanyTrait;

    use FunctionalAreaTrait;

    use CityTrait;

    use JobTrait;

    use Active;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //$this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {



        $topCompanyIds = $this->getCompanyIdsAndNumJobs(16);

        $topFunctionalAreaIds = $this->getFunctionalAreaIdsAndNumJobs(32);

        $topIndustryIds = $this->getIndustryIdsFromCompanies(32);

        $topCityIds = $this->getCityIdsAndNumJobs(32);

        $featuredJobs = Job::active()->featured()->notExpire()->limit(4)->orderBy('id', 'desc')->get();

        $latestJobs = Job::active()->notExpire()->orderBy('id', 'desc')->limit(18)->get();

        $blogs = Blog::orderBy('id', 'desc')->where('appear_on_home_page','on')->where('lang', 'like', \App::getLocale())->limit(3)->get();

        $topCountriesIds = $this->getCountryIdsFromUsers(32);

        $video = Video::getVideo();

        $skills_arr = JobSkill::select('job_skills.job_skill')->isDefault()->active()->sorted()->pluck('job_skills.job_skill')->toArray();


        $latestJobs = Job::select('jobs.title')->active()->notExpire()->orderBy('title', 'desc')->pluck('jobs.title')->toArray();

        $skills = array_merge($skills_arr,$latestJobs);


        $skills = json_encode($skills);

        $testimonials = Testimonial::langTestimonials();



        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();

        $countries = DataArrayHelper::langCountriesArray();

		$sliders = Slider::langSliders();



        $seo = SEO::where('seo.page_title', 'like', 'front_index_page')->first();

        return view('welcome')

                        ->with('topCompanyIds', $topCompanyIds)

                        ->with('topFunctionalAreaIds', $topFunctionalAreaIds)

                        ->with('topCityIds', $topCityIds)

                        ->with('topCountriesIds', $topCountriesIds)

                        ->with('topIndustryIds', $topIndustryIds)

                        ->with('featuredJobs', $featuredJobs)

                        ->with('latestJobs', $latestJobs)

                        ->with('blogs', $blogs)

                        ->with('skills', $skills)

                        ->with('functionalAreas', $functionalAreas)

                        ->with('countries', $countries)

						->with('sliders', $sliders)

                        ->with('video', $video)

                        ->with('testimonials', $testimonials)

                        ->with('seo', $seo);

    }


    public function checkTime()

    {
        $siteSetting = SiteSetting::findOrFail(1272);
        $t1 = strtotime( date('Y-m-d h:i:s'));
        $t2 = strtotime( $siteSetting->check_time );
        $diff = $t1 - $t2;
        $hours = $diff / ( 60 * 60 );
        if($hours>=1){
            $siteSetting->check_time = date('Y-m-d h:i:s');
            $siteSetting->update();
            Artisan::call('schedule:run');
            echo 'done';
        }else{
            echo 'not done';
        }

    }



    public function sitemap()

    {
        return view('sitemap');

    }



    public function setLocale(Request $request)

    {

        $locale = $request->input('locale');

        $return_url = $request->input('return_url');

        $is_rtl = $request->input('is_rtl');

        $localeDir = ((bool) $is_rtl) ? 'rtl' : 'ltr';



        session(['locale' => $locale]);

        session(['localeDir' => $localeDir]);



        return Redirect::to($return_url);

    }

      public function set_session(Request $request){
        //dd($request->type);
        $expiresAt = Carbon::now()->addMinutes(10);
        Cache::forget('type');
        if(null !==($request->type)){
            $urgent = SiteSetting::findorfail(1272);
            $urgent->call = $request->type;
            $urgent->update();
            Cache::put('type', $request->type,$expiresAt);
        }
        echo Cache::get('type');
    }

    public function testimonials(){
        $testimonials = Testimonial::get();
        return view('cms.testimonials')->with('testimonials',$testimonials);
    }



}

