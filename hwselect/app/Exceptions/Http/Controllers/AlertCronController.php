<?php



namespace App\Http\Controllers;

use App\Job;

use App\Alert;

use App\Industry;
use App\Company;
use App\CompanyMessage;

use DB;
use Auth;

use Mail;

use App\Mail\AlertJobsMail;

use Carbon\Carbon;

use App\Http\Controllers\Controller;



class AlertCronController extends Controller

{



    public function index(){

    	$alerts = Alert::get();
     // dd($alerts);
    	if(null!==($alerts)){

    		foreach ($alerts as $key => $alert) {

    			$search = $alert->search_title;


    			$country_id = $alert->country_id;

    			$state_id = $alert->state_id;

    			$city_id = $alert->city_id;

    			$functional_area_id = $alert->functional_area_id;

    		   	$query = Job::select('*');

    		   	//$query->where('created_at', '>=', Carbon::now()->subDay());

	           	if (null !== $search) {


	                     $query->where('title', 'like', '%' . trim($search) . '%');

	            }

	            if (null !==$country_id) {

	                $query->where('country_id',$country_id);

	                       

	            }

	            if (null !==$state_id) {

	                

	                $query->where('state_id',$state_id);

	                       

	            }

	            if (null !==$city_id) {

	                

	                $query->where('city_id',$city_id);

	                       

	            }

	            if (null !==$functional_area_id) {

	                

	                $query->where('functional_area_id',$functional_area_id);

	                       

	            }



	            

                $query->orderBy('jobs.id', 'DESC'); 

                



            	$jobs = $query->active()->take(10)->get();
            	$query = encrypt($alert->id);
            	$enc_string = str_replace(array('+', '/', '='), array('-', '_', '~'), $query);

        		$verify_account = 'disable-alert/';
        		$link = url('/') .'/'. $verify_account . $enc_string;

            //dd($jobs);
            	if(null!==($jobs) && count($jobs)>0){

            		
              
			        $data['email'] = $alert->email;
              $ss = 'job';
              if(count($jobs)>1){
                $ss = 'jobs';
              }
			        $data['subject'] = count($jobs).' New Matching '.$ss.' is posted based on your profile';

			        $data['jobs'] = $jobs;
			        $data['link'] = $link;





			        Mail::send(new AlertJobsMail($data));

            	}



            	

    		}

    	}

    }

    public function disableAlert($query){
    	$dec_string=str_replace(array('-', '_', '~'), array('+', '/', '='), $query);
        $user_id = decrypt($dec_string);

        $alert = Alert::findorFail($user_id);
        $alert->delete();
        return redirect(url('/'));
    }


    public function notification(){

       

      if(Auth::user()){

           

        $alerts = Alert::where('email',Auth::user()->email)->get();

        $array = array();

        $str = '';

        if(null!==($alerts)){

        foreach ($alerts as $key => $alert) {

          $search = $alert->search_title;

          $country_id = $alert->country_id;

          $state_id = $alert->state_id;

          $city_id = $alert->city_id;

          $functional_area_id = $alert->functional_area_id;

            $query = Job::select('*');

            $query->where('created_at', '>=', Carbon::now()->subDay());

              if ($search != '') {

                       $query->Where('title', 'like', '%' . $search . '%');

              }

              if ($country_id != '') {

                  

                  $query->where('country_id',$country_id);

                         

              }

              if ($state_id != '') {

                  

                  $query->where('state_id',$state_id);

                         

              }

              if ($city_id != '') {

                  

                  $query->where('city_id',$city_id);

                         

              }

              if ($functional_area_id != '') {

                  

                  $query->where('functional_area_id',$functional_area_id);

                         

              }



              

                $query->orderBy('jobs.id', 'DESC'); 

                



              $jobs = $query->active()->take(10)->get();

              

              if(null!==($jobs)){

                  foreach($jobs as $job){

                      $company = $job->getCompany();

                      $str .="<li><a href='route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')), $job->slug])'>$company->printCompanyImage()".'    '."$job->title</a> </li>";

                  }

              }

              
              

              

        }

      }

      $messages = CompanyMessage::where('seeker_id',Auth::user()->id)->where('type','reply')->where('status','unviewed')->get();

      if(null!==($messages)){

                  foreach($messages as $message){
                    

                      $company = Company::where('id',$message->company_id)->first();
                      if(null!==($company)){
                        $str .="<li><a href='".route('my.messages')."'>$company->name".'     '."New Message</a> </li>";
                      }
                      

                  }

              }


      //$data['search'] = view("append_only_notifications", compact('array'))->render();



      if($str!==''){

         return $str;

      }else{

        return "<li class='noalert'>You have no notifications</li>";

      }

     

      }

      

    }



    public function notification_count(){

      if(Auth::user()){

        $alerts = Alert::where('email',Auth::user()->email)->get();

      if(null!==($alerts)){

        foreach ($alerts as $key => $alert) {

          $search = $alert->search_title;

          $country_id = $alert->country_id;

          $state_id = $alert->state_id;

          $city_id = $alert->city_id;

          $functional_area_id = $alert->functional_area_id;

            $query = Job::select('*');

            $query->where('created_at', '>=', Carbon::now()->subDay());

              if ($search != '') {

                       $query->Where('title', 'like', '%' . $search . '%');

              }

              if ($country_id != '') {

                  

                  $query->where('country_id',$country_id);

                         

              }

              if ($state_id != '') {

                  

                  $query->where('state_id',$state_id);

                         

              }

              if ($city_id != '') {

                  

                  $query->where('city_id',$city_id);

                         

              }

              if ($functional_area_id != '') {

                  

                  $query->where('functional_area_id',$functional_area_id);

                         

              }



              

                $query->orderBy('jobs.id', 'DESC'); 

                



              $jobs = $query->active()->take(10)->get();





              

        }

      }
      if(isset($jobs)){
        $count = count($jobs); 
      }else{
        $count = 0;
      }
      
      $messages = CompanyMessage::where('seeker_id',Auth::user()->id)->where('type','reply')->where('status','unviewed')->get();
      $count = $count + count($messages);
      return $count;

      }

      

    }

    public function savedjobs_count(Request $request){
        $myFavouriteJobSlugs = isset($_COOKIE['saved_jobs'])?$_COOKIE['saved_jobs']:null;
        $dd = trim(str_replace('null,', '', $myFavouriteJobSlugs),'"');
        $arr = explode(',', $dd);

        echo count($arr);
    }

}

