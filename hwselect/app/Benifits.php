<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;

class Benifits extends Model
{
	use Lang;
    use IsDefault;
    use Active;
    use Sorted;
   protected $table = 'benifits';
}
