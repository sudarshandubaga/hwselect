<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $table = 'modules';
    public function modules_data()
    {
        return $this->hasMany('App\Modules_data');
    }

}
