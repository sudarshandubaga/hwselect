<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules_data extends Model
{
    protected $table = 'modules_data';
    public function modules()
    {
        return $this->belongsTo('App\Modules');
    }

}
