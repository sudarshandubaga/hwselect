<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\SiteSetting;

class JobAppliedJobSeekerMailable extends Mailable
{

    use SerializesModels;

    public $job;
    public $jobApply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($job, $jobApply)
    {
        $this->job = $job;
        $this->jobApply = $jobApply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = $this->job->getCompany();
        $user = $this->jobApply->getUser();
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        
          
        return $this->from(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->replyTo(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->to($user->email, $user->name)
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject($user->name . '" you have applied for this job "' . $this->job->title)
                        ->view('emails.job_applied_job_seeker_message')
                        ->with(
                                [
                                    'job_title' => $this->job->title,
                                    'company_name' => null!==($company->name)?$company->name:'',
                                    'user_name' => $user->name,
                                    'company_link' => route('company.detail', $company->slug),
                                    'job_link' => url('jobs/'.\Str::slug($this->job->getFunctionalArea('functional_area')).'/'.$this->job->slug)
                                ]
        );
    }

}
