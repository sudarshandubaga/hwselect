<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;
class ReportAbuse extends Mailable
{

    use Queueable,
        SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        } else {
            $cc_email = $siteSetting->mail_cc_address;
        }

        $your_name = $this->data['user'] ? $this->data['user']['name'] : $this->data['company']['name'];
        $your_email = $this->data['user'] ? $this->data['user']['email'] : $this->data['company']['email'];

        $this->data['your_name'] = $your_name;
        $this->data['your_email'] = $your_email;
        
        $email = $siteSetting->mail_to_address;
        return $this->from($siteSetting->mail_from_address, $your_name)
                        ->replyTo($your_email, $your_name)
                        ->to($email, config('mail.recieve_to.name'))
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject($your_name . '" has reported a "' . config('app.name') . '" link')
                        ->view('emails.report_abuse_message')
                        ->with($this->data);
    }

}
