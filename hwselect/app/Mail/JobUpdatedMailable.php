<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\SiteSetting;

class JobUpdatedMailable extends Mailable
{

    use SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        
        $company = $this->data['company'];
        $job = $this->data['job'];
        $update_fields = $this->data['update_fields'];
        //dd($update_fields);
        $data = array();
        $data['name'] = $company->name;
        $data['job_title'] = $job->title;
        $data['link'] = route('public.job', [$job->id]);
        $data['link_admin'] = route('edit.job', ['id' => $job->id]);
        $data['update_fields'] = $update_fields;
        $data['job'] = $job->toArray();
        

        // dd($data);
        return $this->to(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject('Company "' . $company->name . '" has Updated a job on "' . config('app.name'))
                        ->view('emails.job_updated_message')
                        ->with($data);

    }

}
