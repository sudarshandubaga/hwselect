<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;

class EmailToFriend extends Mailable
{

    use Queueable,
        SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        return $this->from('support@hwselect.com', $this->data['your_name'])
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->replyTo($this->data['your_email'], $this->data['your_name'])
                        ->to($this->data['friend_email'], $this->data['friend_name'])
                        ->subject(__('Your friend') . ' ' . $this->data['your_name'] . ' ' . __('has shared a link with you'))
                        ->view('emails.send_to_friend_message')
                        ->with($this->data);
    }

}
