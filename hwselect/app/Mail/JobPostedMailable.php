<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\SiteSetting;

class JobPostedMailable extends Mailable
{

    use SerializesModels;

    public $job;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($job)
    {
        $this->job = $job;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        $company = $this->job->getCompany();
        return $this->to(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject('Company "' . $company->name . '" has posted a new job on "' . config('app.name'))
                        ->view('emails.job_posted_message')
                        ->with(
                                [
                                    'name' => $company->name,
                                    'job_title' => $this->job->title,
                                    'link' => route('public.job', [$this->job->id]),
                                    'link_admin' => route('edit.job', ['id' => $this->job->id])
                                ]
        );
    }

}
