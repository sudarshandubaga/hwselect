<?php



namespace App\Mail;



use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;


class ApplyJobMail extends Mailable

{



    use Queueable,

        SerializesModels;



    public $data;



    /**

     * Create a new message instance.

     *

     * @return void

     */

    public function __construct($data)

    {

        $this->data = $data;

    }



    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()

    {

       // dd($this->data['file_name']);
        $at = asset('job_files/'.$this->data['file_name']);
        // dd($at);
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        return $this->from('support@hwselect.com', 'HW Select')
                        ->replyTo($this->data['email'], $this->data['name'])

                        ->to(config('mail.recieve_to.address'), config('mail.recieve_to.address'))
                        ->bcc('jayen.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)

                        ->subject($this->data['name'].' HAS UPLOADED A NEW JOB VACANCY ON '.date('d-m-Y'))

                        ->attach(asset('job_files/'.$this->data['file_name']))

                        ->view('emails.send_new_job')

                        ->with($this->data);
    }



}

