<?php



namespace App\Mail;



use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;



class ContactUs extends Mailable

{



    use Queueable,
        SerializesModels;



    public $data;



    /**

     * Create a new message instance.

     *

     * @return void

     */

    public function __construct($data)

    {

        $this->data = $data;

    }



    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()

    {
        $siteSetting = SiteSetting::findOrFail(1272);
        // dd($siteSetting);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        
        if($this->data['email']=='Company'){
            $email = $siteSetting->mail_to_address;
        }else{
            $email = $siteSetting->mail_to_address;
        }

        $this->data['subject'] = 'Contact form submitted from '.$this->data['type']. '-' .$this->data['full_name'];
        //dd($email);
        return $this->from($siteSetting->mail_from_address, config('mail.recieve_to.name'))
                        ->to($email, $email)
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject('Contact form submitted from '.$this->data['type']. '-' .$this->data['full_name'])

                        ->view('emails.send_contact_message')

                        ->with($this->data);

    }



}

