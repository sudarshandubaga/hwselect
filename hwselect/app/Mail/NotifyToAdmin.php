<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;

class NotifyToAdmin extends Mailable
{

    use Queueable,
        SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd(config('mail.recieve_to.address'));
        //dd(asset('cvs/'.$this->data['cv']));
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        return $this->from('support@hwselect.com', config('mail.recieve_to.name'))
                        //->replyTo($this->data['email'], $this->data['name'])
                        ->to(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject('New CV Uploaded')
                        ->attach(asset('cvs/'.$this->data['cv']))
                        ->view('emails.send_notity_to_admin')
                        ->with($this->data);
    }

}
