<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\SiteSetting;
class JobAppliedCompanyMailable extends Mailable
{

    use SerializesModels;

    public $job;
    public $jobApply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($job, $jobApply)
    {
        $this->job = $job;
        $this->jobApply = $jobApply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }

        $company = $this->job->getCompany();
        $user = $this->jobApply->getUser();
        $email = $siteSetting->mail_to_address;
        $profileCv = $user->getDefaultCv();
        
        //dd($email);
        return $this->from($siteSetting->mail_from_address, config('mail.recieve_to.name'))
                        ->replyTo($user->email, $user->name)
            			->to($email, config('mail.recieve_to.name'))
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
						//->to($company->email, $company->name)			
                        ->subject('Jobseeker "' . $user->name . '" has applied for "' . $this->job->title)
                        ->attach(asset('cvs/'.$profileCv->cv_file))
                        ->view('emails.job_applied_company_message')
                        ->with(
                                [
                                    'job_title' => $this->job->title,
                                    'job' => $this->job,
                                    'company_name' => $company->name,
                                    'user_name' => $user->name,
                                    'user' => $user,
                                    'jobapply' => $this->jobApply,
                                    'company' => $company,
                                    'user_link' => route('user.profile', $user->id),
                                    'job_link' => route('job.detail', [\Str::slug($this->job->getFunctionalArea('functional_area')),$this->job->slug])
                                ]
        );
    }

}
