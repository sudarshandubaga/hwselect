<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;

class MessageSendCompanyMail extends Mailable
{

    use Queueable,
        SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        return $this->from(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
            ->subject('Employer/Company "' . $this->data['company_name'] . '" has sent new message on "' . config('app.name'))
            ->to($this->data['email'], $this->data['name'])
            ->bcc('aesha.isync@gmail.com')
            ->cc($cc_email, $siteSetting->mail_cc_name)
            ->view('emails.send_email_message')
            ->with($this->data);
    }
}
