<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\SiteSetting;

class UserRegisteredMailable extends Mailable
{

    use SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        return $this->to(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject('Job Seeker "' . $this->user->name . '" has registered with "' . config('app.name'))
                        ->view('emails.user_registered_message')
                        ->with(
                                [
                                    'name' => $this->user->name,
                                    'email' => $this->user->email,
                                    'link' => route('user.profile', $this->user->id),
                                    'link_admin' => route('edit.user', ['id' => $this->user->id])
                                ]
        );
    }

}
