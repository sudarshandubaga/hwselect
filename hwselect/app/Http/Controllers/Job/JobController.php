<?php

namespace App\Http\Controllers\Job;

use Auth;
use DB;
use Input;
use Redirect;
use Carbon\Carbon;
use App\Job;
use App\JobApply;
use App\FavouriteJob;
use App\Company;
use App\JobSkill;
use App\JobSkillManager;
use App\Country;
use App\Searches;
use App\CountryDetail;
use App\State;
use App\City;
use App\CareerLevel;
use App\FunctionalArea;
use App\JobType;
use App\JobShift;
use App\Gender;
use App\JobExperience;
use App\SiteSetting;
use App\DegreeLevel;
use App\ProfileCv;
use App\Seo;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use Form;
use App\Http\Requests\JobFormRequest;
use App\Http\Requests\JobSkillFormRequest;
use App\Http\Requests\Front\ApplyJobFormRequest;
use App\Http\Controllers\Controller;
use App\Traits\FetchJobs;
use App\Traits\CityTrait;
use App\Events\JobApplied;
use App\Mail\RecruitedJobMail;
use App\Mail\ApplyJobMail;
use Illuminate\Http\UploadedFile;

use ImgUploader;

use Mail;
use Illuminate\Support\Facades\Crypt;

class JobController extends Controller
{

    //use Skills;
    use FetchJobs;
    use CityTrait;

    private $functionalAreas = '';
    private $countries = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth', ['except' => ['jobsBySearch', 'jobsByFeatured', 'myFavouriteJobs','jobDetail','storeJobSkillForm','makeNotActiveJob','applyToAdmin','makeActiveJob','jobsByCategory']]);

        $this->functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $this->countries = DataArrayHelper::langCountriesArray();
    }

    public function jobsByFeatured(Request $request)
    {
        // error_reporting(E_ALL);
        // ini_set('display_errors',1);

        $search = $request->query('search', '');
        $address = $request->query('address', '');
        $radius = $request->query('radius', '');
        $latitude = $request->query('latitude', '');
        $longitude = $request->query('longitude', '');

        $job_titles = $request->query('job_title', array());
        $company_ids = $request->query('company_id', array());
        $industry_ids = $request->query('industry_id', array());
        $job_skill_ids = $request->query('job_skill_id', array());
        $functional_area_ids = $request->query('functional_area_id', array());
        $country_ids = $request->query('country_id', array());
        $state_ids = $request->query('state_id', array());
        $city_ids = $request->query('city_id', array());
        $is_freelance = $request->query('is_freelance', array());
        $career_level_ids = $request->query('career_level_id', array());
        $job_type_ids = $request->query('job_type_id', array());
        $job_shift_ids = $request->query('job_shift_id', array());
        $gender_ids = $request->query('gender_id', array());
        $degree_level_ids = $request->query('degree_level_id', array());
        $job_experience_ids = $request->query('job_experience_id', array());
        $salary_from = $request->query('salary_from', '');
        $salary_to = $request->query('salary_to', '');
        $salary_currency = $request->query('salary_currency', '');
        $is_featured = $request->query('is_featured', 1);
        $order_by = $request->query('order_by', 'id');
        $limit = 10;
        
        if(!empty($search)){
            $check = Searches::where('search','like',"%$search%")->first();

            
            if(!$check){
                $searches = new Searches();
                $searches->search = $search;
                $searches->save();
            }
        }

        $jobs = $this->fetchJobs($search, $address,$radius, $latitude, $longitude, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, $order_by, $limit);

        // echo "<pre>";print_r($jobs);exit;
        /*         * ************************************************** */

        $jobTitlesArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.title');

        /*         * ************************************************* */

        $jobIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.id');

        /*         * ************************************************** */

        $skillIdsArray = $this->fetchSkillIdsArray($jobIdsArray);

        /*         * ************************************************** */

        $countryIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.country_id');

        /*         * ************************************************** */

        $stateIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.state_id');

        /*         * ************************************************** */

        $cityIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.city_id');

        /*         * ************************************************** */

        $companyIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.company_id');

        /*         * ************************************************** */

        $industryIdsArray = $this->fetchIndustryIdsArray($companyIdsArray);

        /*         * ************************************************** */


        /*         * ************************************************** */

        $functionalAreaIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.functional_area_id');

        /*         * ************************************************** */

        $careerLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.career_level_id');

        /*         * ************************************************** */

        $jobTypeIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_type_id');

        /*         * ************************************************** */

        $jobShiftIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_shift_id');

        /*         * ************************************************** */

        $genderIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.gender_id');

        /*         * ************************************************** */

        $degreeLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.degree_level_id');

        /*         * ************************************************** */

        $jobExperienceIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_experience_id');

        /*         * ************************************************** */

        $seoArray = $this->getSEO($functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids);

        /*         * ************************************************** */

        $currencies = DataArrayHelper::currenciesArray();

        /*         * ************************************************** */
        $skills_arr = JobSkill::select('job_skills.job_skill')->isDefault()->active()->sorted()->pluck('job_skills.job_skill')->toArray();


        $latestJobs = Job::select('jobs.title')->active()->notExpire()->orderBy('title', 'desc')->pluck('jobs.title')->toArray();

        $skills = array_merge($skills_arr,$latestJobs);
        
        $topCityIds = $this->getCityIdsAndNumJobs(32);

        $skills = json_encode($skills);
        $seo = Seo::where('seo.page_title', 'like', 'jobs')->first();
        return view('job.featuredList')
        ->with('functionalAreas', $this->functionalAreas)
        ->with('countries', $this->countries)
        ->with('currencies', array_unique($currencies))
        ->with('jobs', $jobs)
        ->with('jobTitlesArray', $jobTitlesArray)
        ->with('topCityIds', $topCityIds)
        ->with('skillIdsArray', $skillIdsArray)
        ->with('countryIdsArray', $countryIdsArray)
        ->with('stateIdsArray', $stateIdsArray)
        ->with('cityIdsArray', $cityIdsArray)
        ->with('companyIdsArray', $companyIdsArray)
        ->with('industryIdsArray', $industryIdsArray)
        ->with('functionalAreaIdsArray', $functionalAreaIdsArray)
        ->with('careerLevelIdsArray', $careerLevelIdsArray)
        ->with('jobTypeIdsArray', $jobTypeIdsArray)
        ->with('jobShiftIdsArray', $jobShiftIdsArray)
        ->with('genderIdsArray', $genderIdsArray)
        ->with('degreeLevelIdsArray', $degreeLevelIdsArray)
        ->with('jobExperienceIdsArray', $jobExperienceIdsArray)
        ->with('skills', $skills)
        ->with('seo', $seo);
    }

    public function jobsBySearch(Request $request)
    {
        // $jobs = Job::get();

        // dd($jobs);

        // foreach($jobs as $key => $job) {

        //     $address = explode(",", $job->getLocation());
        //     array_pop($address);
        //     $jobSlug = strtolower(\Str::slug( $job->title . '-' . implode("-", $address)));

        //     $slug_count = Job::where('slug', $jobSlug)->where('id', '!=', $job->id)->count();

        //     if($slug_count) $jobSlug .= '-'.$slug_count;

        //     $job->slug = $jobSlug;
        //     $job->save();
        // }


        // error_reporting(E_ALL);
        // ini_set('display_errors',1);

        $search = $request->query('search', '');
        $address = $request->query('address', '');
        $radius = $request->query('radius', '');
        $latitude = $request->query('latitude', '');
        $longitude = $request->query('longitude', '');

        $job_titles = $request->query('job_title', array());
        $company_ids = $request->query('company_id', array());
        $industry_ids = $request->query('industry_id', array());
        $job_skill_ids = $request->query('job_skill_id', array());
        $functional_area_ids = $request->query('functional_area_id', array());
        $country_ids = $request->query('country_id', array());
        $state_ids = $request->query('state_id', array());
        $city_ids = $request->query('city_id', array());
        $is_freelance = $request->query('is_freelance', array());
        $career_level_ids = $request->query('career_level_id', array());
        $job_type_ids = $request->query('job_type_id', array());
        $job_shift_ids = $request->query('job_shift_id', array());
        $gender_ids = $request->query('gender_id', array());
        $degree_level_ids = $request->query('degree_level_id', array());
        $job_experience_ids = $request->query('job_experience_id', array());
        $salary_from = $request->query('salary_from', '');
        $salary_to = $request->query('salary_to', '');
        $salary_currency = $request->query('salary_currency', '');
        $is_featured = $request->query('is_featured', 2);
        $order_by = $request->query('order_by', 'id');
        $limit = 10;
        
        if(!empty($search)){
            $check = Searches::where('search','like',"%$search%")->first();

            
            if(!$check){
                $searches = new Searches();
                $searches->search = $search;
                $searches->save();
            }
        }

        $jobs = $this->fetchJobs($search, $address,$radius, $latitude, $longitude, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, $order_by, $limit);

        // echo "<pre>";print_r($jobs);exit;
        /*         * ************************************************** */

        $jobTitlesArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.title');

        /*         * ************************************************* */

        $jobIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.id');

        /*         * ************************************************** */

        $skillIdsArray = $this->fetchSkillIdsArray($jobIdsArray);

        /*         * ************************************************** */

        $countryIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.country_id');

        /*         * ************************************************** */

        $stateIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.state_id');

        /*         * ************************************************** */

        $cityIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.city_id');

        /*         * ************************************************** */

        $companyIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.company_id');

        /*         * ************************************************** */

        $industryIdsArray = $this->fetchIndustryIdsArray($companyIdsArray);

        /*         * ************************************************** */


        /*         * ************************************************** */

        $functionalAreaIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.functional_area_id');

        /*         * ************************************************** */

        $careerLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.career_level_id');

        /*         * ************************************************** */

        $jobTypeIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_type_id');

        /*         * ************************************************** */

        $jobShiftIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_shift_id');

        /*         * ************************************************** */

        $genderIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.gender_id');

        /*         * ************************************************** */

        $degreeLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.degree_level_id');

        /*         * ************************************************** */

        $jobExperienceIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_experience_id');

        /*         * ************************************************** */

        $seoArray = $this->getSEO($functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids);

        /*         * ************************************************** */

        $currencies = DataArrayHelper::currenciesArray();

        /*         * ************************************************** */
        $skills_arr = JobSkill::select('job_skills.job_skill')->isDefault()->active()->sorted()->pluck('job_skills.job_skill')->toArray();


        $latestJobs = Job::select('jobs.title')->active()->notExpire()->orderBy('title', 'desc')->pluck('jobs.title')->toArray();

        $skills = array_merge($skills_arr,$latestJobs);
        
        $topCityIds = $this->getCityIdsAndNumJobs(32);

        $skills = json_encode($skills);
        $seo = Seo::where('seo.page_title', 'like', 'jobs')->first();
        return view('job.list')
        ->with('functionalAreas', $this->functionalAreas)
        ->with('countries', $this->countries)
        ->with('currencies', array_unique($currencies))
        ->with('jobs', $jobs)
        ->with('jobTitlesArray', $jobTitlesArray)
        ->with('topCityIds', $topCityIds)
        ->with('skillIdsArray', $skillIdsArray)
        ->with('countryIdsArray', $countryIdsArray)
        ->with('stateIdsArray', $stateIdsArray)
        ->with('cityIdsArray', $cityIdsArray)
        ->with('companyIdsArray', $companyIdsArray)
        ->with('industryIdsArray', $industryIdsArray)
        ->with('functionalAreaIdsArray', $functionalAreaIdsArray)
        ->with('careerLevelIdsArray', $careerLevelIdsArray)
        ->with('jobTypeIdsArray', $jobTypeIdsArray)
        ->with('jobShiftIdsArray', $jobShiftIdsArray)
        ->with('genderIdsArray', $genderIdsArray)
        ->with('degreeLevelIdsArray', $degreeLevelIdsArray)
        ->with('jobExperienceIdsArray', $jobExperienceIdsArray)
        ->with('skills', $skills)
        ->with('seo', $seo);
    }


    public function jobsByCategory(Request $request,$slug,$fun_id)
    {
        $search = $request->query('search', '');
        $address = $request->query('address', '');
        $radius = $request->query('radius', '');
        $latitude = $request->query('latitude', '');
        $longitude = $request->query('longitude', '');

        $job_titles = $request->query('job_title', array());
        $company_ids = $request->query('company_id', array());
        $industry_ids = $request->query('industry_id', array());
        $job_skill_ids = $request->query('job_skill_id', array());
        $functional_area_ids = $request->query('functional_area_id', array());
        $country_ids = $request->query('country_id', array());
        $state_ids = $request->query('state_id', array());
        $city_ids = $request->query('city_id', array());
        $is_freelance = $request->query('is_freelance', array());
        $career_level_ids = $request->query('career_level_id', array());
        $job_type_ids = $request->query('job_type_id', array());
        $job_shift_ids = $request->query('job_shift_id', array());
        $gender_ids = $request->query('gender_id', array());
        $degree_level_ids = $request->query('degree_level_id', array());
        $job_experience_ids = $request->query('job_experience_id', array());
        $salary_from = $request->query('salary_from', '');
        $salary_to = $request->query('salary_to', '');
        $salary_currency = $request->query('salary_currency', '');
        $is_featured = $request->query('is_featured', 2);
        $order_by = $request->query('order_by', 'id');
        $limit = 10;

        if(!empty($search)){
            $check = Searches::where('search','like',"%$search%")->first();

            
            if(!$check){
                $searches = new Searches();
                $searches->search = $search;
                $searches->save();
            }
        }

        
        // $slug = str_replace('-', ' ', $slug);

        // if($slug=='cnet'){
        //     $slug = 'C#.Net';
        // }

        

        
        $slug_data = explode("-", $slug);

        if($slug_data[0] == 'jobsector'){
            $id_fun = FunctionalArea::where('id','like','%'.$fun_id.'%')->pluck('id')->toArray();

            if(null!==($id_fun) && !empty($id_fun)){
             $functional_area_ids = $id_fun; 
             $seo_functional_area = Seo::where('seo.page_title', 'like', 'functional_area')->first();
         }

     }
     else if($slug_data[0] == 'joblocation'){
        $id_city = City::where('id','like','%'.$fun_id.'%')->pluck('id')->toArray();

        if(null!==($id_city) && !empty($id_city)){
         $city_ids = $id_city; 
         $seo_functional_area = Seo::where('seo.page_title', 'like', 'location')->first();
     }

 }
 else if($slug_data[0] == 'jobskills'){
    $id_skill = JobSkill::where('id','like','%'.$fun_id.'%')->pluck('id')->toArray();
    if(null!==($id_skill) && !empty($id_skill)){
     $job_skill_ids = $id_skill; 
     $seo_functional_area = Seo::where('seo.page_title', 'like', 'skills')->first();
 }

}
else{

    $id_role = CareerLevel::where('id','like','%'.$fun_id.'%')->pluck('id')->toArray();


    if(null!==($id_role) && !empty($id_role)){
     $career_level_ids = $id_role;
     $seo_functional_area = Seo::where('seo.page_title', 'like', 'role')->first(); 
 }
}


$jobs = $this->fetchJobs($search, $address,$radius, $latitude, $longitude, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, $order_by, $limit);

/*         * ************************************************** */

$jobTitlesArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.title');

/*         * ************************************************* */

$jobIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.id');

/*         * ************************************************** */

$skillIdsArray = $this->fetchSkillIdsArray($jobIdsArray);

/*         * ************************************************** */

$countryIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.country_id');

/*         * ************************************************** */

$stateIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.state_id');

/*         * ************************************************** */

$cityIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.city_id');

/*         * ************************************************** */

$companyIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.company_id');

/*         * ************************************************** */

$industryIdsArray = $this->fetchIndustryIdsArray($companyIdsArray);

/*         * ************************************************** */


/*         * ************************************************** */

$functionalAreaIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.functional_area_id');

/*         * ************************************************** */

$careerLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.career_level_id');

/*         * ************************************************** */

$jobTypeIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_type_id');

/*         * ************************************************** */

$jobShiftIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_shift_id');

/*         * ************************************************** */

$genderIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.gender_id');

/*         * ************************************************** */

$degreeLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.degree_level_id');

/*         * ************************************************** */

$jobExperienceIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_experience_id');

/*         * ************************************************** */

$seoArray = $this->getSEO($functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids);

/*         * ************************************************** */
$topCityIds = $this->getCityIdsAndNumJobs(32);
$currencies = DataArrayHelper::currenciesArray();

/*         * ************************************************** */
$skills_arr = JobSkill::select('job_skills.job_skill')->isDefault()->active()->sorted()->pluck('job_skills.job_skill')->toArray();


$latestJobs = Job::select('jobs.title')->active()->notExpire()->orderBy('title', 'desc')->pluck('jobs.title')->toArray();

$skills = array_merge($skills_arr,$latestJobs);


$skills = json_encode($skills);
$seo_jobs = Seo::where('seo.page_title', 'like', 'jobs')->first();

$seo_data = ucwords($slug).' - '.$seo_functional_area->seo_title;
$seotitle = explode("-",$seo_data, 3);
if($seotitle[0] == "Jobskill"){
    $seo_jobs_data = JobSkill::where('id', $job_skill_ids)->get();
    $seo = (object) array(
        'seo_title' => $seo_jobs_data[0]->job_skill,                    
        'seo_description' => ucwords($slug).' - '.$seo_functional_area->seo_description,
        'seo_keywords' => ucwords($slug).' - '.$seo_functional_area->seo_keywords,
        'seo_other' => '');
}
else{
    $seo = (object) array(
        'seo_title' => ucwords($slug).' - '.$seo_functional_area->seo_title,                    
        'seo_description' => ucwords($slug).' - '.$seo_functional_area->seo_description,
        'seo_keywords' => ucwords($slug).' - '.$seo_functional_area->seo_keywords,
        'seo_other' => '');
}

$slugArr = explode("-", $slug);
// dd($city_id);

switch ($slugArr[0]) {
    case 'joblocation':
        $seo = City::findOrFail($fun_id);
        $seo = (object) [
            'seo_title'         => $seo->seo_title ?? $seo->seo_title,
            'seo_keywords'      => $seo->seo_keywords ?? $seo->seo_keywords,
            'seo_description'   => $seo->seo_description ?? $seo->seo_description,
            'seo_other'         => $seo->seo_other ?? $seo->seo_other
        ];
        break;

    case 'jobrole':
        $seo = CareerLevel::findOrFail($fun_id);
        $seo = (object) [
            'seo_title'         => $seo->seo_title ?? $seo->seo_title,
            'seo_keywords'      => $seo->seo_keywords ?? $seo->seo_keywords,
            'seo_description'   => $seo->seo_description ?? $seo->seo_description,
            'seo_other'         => $seo->seo_other ?? $seo->seo_other
        ];
        break;

    case 'jobskills':
        $seo = JobSkill::findOrFail($fun_id);
        $seo = (object) [
            'seo_title'         => $seo->seo_title ?? $seo->seo_title,
            'seo_keywords'      => $seo->seo_keywords ?? $seo->seo_keywords,
            'seo_description'   => $seo->seo_description ?? $seo->seo_description,
            'seo_other'         => $seo->seo_other ?? $seo->seo_other
        ];
        break;

    case 'jobsector':
        $seo = FunctionalArea::findOrFail($fun_id);
        $seo = (object) [
            'seo_title'         => $seo->seo_title ?? $seo->seo_title,
            'seo_keywords'      => $seo->seo_keywords ?? $seo->seo_keywords,
            'seo_description'   => $seo->seo_description ?? $seo->seo_description,
            'seo_other'         => $seo->seo_other ?? $seo->seo_other
        ];
        break;
    
    default:
        break;
}

return view('job.list')
->with('functionalAreas', $this->functionalAreas)
->with('countries', $this->countries)
->with('currencies', array_unique($currencies))
->with('jobs', $jobs)
->with('topCityIds', $topCityIds)
->with('jobTitlesArray', $jobTitlesArray)
->with('skillIdsArray', $skillIdsArray)
->with('countryIdsArray', $countryIdsArray)
->with('stateIdsArray', $stateIdsArray)
->with('cityIdsArray', $cityIdsArray)
->with('companyIdsArray', $companyIdsArray)
->with('industryIdsArray', $industryIdsArray)
->with('functionalAreaIdsArray', $functionalAreaIdsArray)
->with('careerLevelIdsArray', $careerLevelIdsArray)
->with('jobTypeIdsArray', $jobTypeIdsArray)
->with('jobShiftIdsArray', $jobShiftIdsArray)
->with('genderIdsArray', $genderIdsArray)
->with('degreeLevelIdsArray', $degreeLevelIdsArray)
->with('jobExperienceIdsArray', $jobExperienceIdsArray)
->with('skills', $skills)
->with('seo', $seo);
}
public function storeJobSkillForm(JobSkillFormRequest $request)
{
    $jobSkill = new JobSkill();
    $jobSkill->job_skill = $request->input('job_skill');
    $jobSkill->is_active = $request->input('is_active');
    $jobSkill->lang = $request->input('lang');
    $jobSkill->is_default = $request->input('is_default');
    $jobSkill->save();
    /*         * ************************************ */
    $jobSkill->sort_order = $jobSkill->id;
    if ((int) $request->input('is_default') == 1) {
        $jobSkill->job_skill_id = $jobSkill->id;
    } else {
        $jobSkill->job_skill_id = $request->input('job_skill_id');
    }
    $jobSkill->update();
    $jobSkills = DataArrayHelper::defaultJobSkillsArray();
    $skills = $request->skills.','.$jobSkill->job_skill_id;

    $exp_skills = explode(',', $skills);


    $dd = Form::select('skills[]', $jobSkills,$exp_skills , array('class'=>'form-control select2-multiple', 'id'=>'skills', 'multiple'=>'multiple'));

    echo $dd;
}
public function jobDetail(Request $request,$functional_area, $job_slug)
{
    $siteSettings = SiteSetting::first();
    $job = Job::where('slug', $job_slug)->firstOrFail();
    if(null ==($job)){
        return abort(404);
    }
    $radius = $request->query('radius', '');
    $latitude = $request->query('latitude', '');
    $longitude = $request->query('longitude', '');
    /*         * ************************************************** */
    $search = '';
    $address = '';
    $job_titles = array();
    $company_ids = array();
    $industry_ids = array();
    $job_skill_ids = (array) $job->getJobSkillsArray();
    $functional_area_ids = (array) $job->getFunctionalArea('functional_area_id');
    $country_ids = (array) $job->getCountry('country_id');
    $state_ids = (array) $job->getState('state_id');
    $city_ids = (array) $job->getCity('city_id');
    $is_freelance = $job->is_freelance;
    $career_level_ids = (array) $job->getCareerLevel('career_level_id');
    $job_type_ids = (array) $job->getJobType('job_type_id');
    $job_shift_ids = (array) $job->getJobShift('job_shift_id');
    $gender_ids = (array) $job->getGender('gender_id');
    $degree_level_ids = (array) $job->getDegreeLevel('degree_level_id');
    $job_experience_ids = (array) $job->getJobExperience('job_experience_id');
    $salary_from = 0;
    $salary_to = 0;
    $salary_currency = '';
    $is_featured = 2;
    $order_by = 'id';
    $limit = 5;
    
    $relatedJobs = $this->fetchJobs($search, $address,$radius, $latitude, $longitude,$job_titles, $company_ids, $industry_ids, $job_skill_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, $order_by, $limit);
    /*         * ***************************************** */

    $seoArray = $this->getSEO((array) $job->functional_area_id, (array) $job->country_id, (array) $job->state_id, (array) $job->city_id, (array) $job->career_level_id, (array) $job->job_type_id, (array) $job->job_shift_id, (array) $job->gender_id, (array) $job->degree_level_id, (array) $job->job_experience_id);
    /*         * ************************************************** */
    $seo = (object) array(
        'seo_title' => $job->meta_title,                    
        'seo_description' =>$job->meta_description,
        'seo_keywords' => $job->meta_keywords,
        'seo_other' => ''
    );
    return view('job.detail')
    ->with('job', $job)
    ->with('relatedJobs', $relatedJobs)
    ->with('seo', $seo);
}

/*     * ************************************************** */

public function addToFavouriteJob(Request $request)
{
    $job_id = $request->id;
    $job = Job::findorFail($job_id);
    $job_slug = $job->slug;
    $data['job_slug'] = $job_slug;
    $data['job_id']   = $job_id;
    $data['user_id'] = Auth::user()->id;
    $data_save = FavouriteJob::create($data);
}

public function removeFromFavouriteJob(Request $request)
{
    $job_id = $request->id;
    $job = Job::findorFail($job_id);
    $user_id = Auth::user()->id;
    FavouriteJob::where('job_id', 'like', $job_id)->where('user_id', $user_id)->delete();
}

public function applyJob(Request $request, $job_slug)
{
    $user = Auth::user();
    $job = Job::where('slug', 'like', $job_slug)->first();
    $currencies = DataArrayHelper::currenciesArray();
    
    if ((bool)$user->is_active === false) {
        flash(__('Your account is inactive contact site admin to activate it'))->error();
        return \Redirect::route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug]);
        exit;
    }
    
    if ((bool) config('jobseeker.is_jobseeker_package_active')) {
        if (
            ($user->jobs_quota <= $user->availed_jobs_quota) ||
            ($user->package_end_date->lt(Carbon::now()))
        ) {
            flash(__('Please subscribe to package first'))->error();
            return \Redirect::route('home');
            exit;
        }
    }
    if ($user->isAppliedOnJob($job->id)) {
        flash(__('You have already applied for this job'))->success();
        return \Redirect::route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug]);
        exit;
    }
    
    

    $myCvss = ProfileCv::where('user_id', '=', $user->id)->get();
    $myCvs = array();

    if(null!==($myCvss)){
        foreach ($myCvss as $key => $val) {
            if($val->is_default==1){
                $myCvs[$val->id] = $val->title.' ('.date('d-M-Y',strtotime($val->created_at)).')'.' Default CV';
            }else{
             $myCvs[$val->id] = $val->title.' ('.date('d-M-Y',strtotime($val->created_at)).')'; 
         }
         
     }
 }


 return view('job.apply_job_form')
 ->with('job_slug', $job_slug)
 ->with('job', $job)
 ->with('currencies', $currencies)
 ->with('myCvs', $myCvs);
}

public function postApplyJob(ApplyJobFormRequest $request, $job_slug)
{
    $user = Auth::user();
    if(date('Y-m-d') !== $request->date_from) {
        $user->immediate_date_from = $request->date_from;
        $user->save();
    }
    // dd($request->all());
    $user_id = $user->id;
    $job = Job::where('slug', 'like', $job_slug)->first();

    $jobApply = new JobApply();
    $jobApply->user_id = $user_id;
    $jobApply->job_id = $job->id;
    $jobApply->cv_id = $request->post('cv_id');
    $jobApply->current_salary = $request->post('current_salary');
    $jobApply->expected_salary = $request->post('expected_salary');
    $jobApply->salary_currency = $request->post('salary_currency');
    $jobApply->save();

    /*         * ******************************* */
    if ((bool) config('jobseeker.is_jobseeker_package_active')) {
        $user->availed_jobs_quota = $user->availed_jobs_quota + 1;
        $user->update();
    }
    /*         * ******************************* */
    event(new JobApplied($job, $jobApply));

    flash(__('Thank You ('.$user->name.') your CV has been received, if successful you will be contacted in due course. This is now saved in your job application history'))->success();
    return \Redirect::route('my.job.applications', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug,$job->id])->with( ['message' => 'Thank You ('.$user->name.') your CV has been received, if successful you will be contacted in due course. This is now saved in your job application history'] );
}

public function deleteapplyJob(Request $request, $job_id, $job_slug)
{
    $user = Auth::user();
    $user_id = $user->id;

    // dd([$user->id, $job_id]);

    $jobApply = JobApply::where('user_id',$user_id)->where('job_id',$job_id)->delete();
    
    flash(__('Successfully deleted'))->success();
    return \Redirect::back()->with( ['message' => 'Application Successfully deleted'] );
} 


public function deleteAllapplyJob(Request $request)
{
    $user = Auth::user();
    $user_id = $user->id;

    $jobApply = JobApply::where('user_id',$user_id)->delete();
    
    

    flash(__('Successfully deleted'))->success();
    return \Redirect::back()->with( ['message' => 'Application Successfully deleted'] );
}

public function myJobApplications(Request $request)
{
    $myAppliedJobIds = Auth::user()->getAppliedJobIdsArray();
    $jobs = Job::whereIn('id', $myAppliedJobIds)->orderBy('id', 'DESC')->paginate(10);
    return view('job.my_applied_jobs')
    ->with('jobs', $jobs);
}

public function myFavouriteJobs(Request $request)
{
    if(null!==(Auth::user())){
            // $myFavouriteJobSlugs = Auth::user()->getFavouriteJobSlugsArray();
        $myFavouriteJobIds = Auth::user()->getFavouriteJobIdArray();
        
        $jobs = Job::whereIn('id', $myFavouriteJobIds)->paginate(10);  
            //dd($jobs);
        return view('job.my_favourite_jobs')
        ->with('jobs', $jobs);
    }else{
            // $myFavouriteJobSlugs = isset($_COOKIE['saved_jobs'])?$_COOKIE['saved_jobs']:null;
        $myFavouriteJobIds = isset($_COOKIE['saved_jobs'])?$_COOKIE['saved_jobs']:null;
        
        $dd = trim(str_replace('null,', '', $myFavouriteJobIds),'"');
        $arr = explode(',', $dd);

        
        $jobs = Job::whereIn('id', $arr)->take(5)->get();
        return view('job.my_favourite_jobs')
        ->with('jobs', $jobs);
    }
    
}

public function removeAllFromFavouriteJob(Request $request)
{
    if(null!==(Auth::user())){
        $user_id = Auth::id();
        $fev = FavouriteJob::where('user_id',$user_id)->delete();
            /*$myFavouriteJobSlugs = Auth::user()->getFavouriteJobSlugsArray();
            $jobs = Job::whereIn('slug', $myFavouriteJobSlugs)->paginate(10);
            foreach ($jobs as $key => $val) {
                $fev = FavouriteJob::where('user_id',$user_id)->delete();
            }*/
        }
        
    }

    public function makeNotActiveJob(Request $request)

    {

        $id = $request->input('id');


        try {

            $job = Job::findOrFail($id);

            $job->is_active = 0;

            $job->is_archive = 0;
            $job->blocked_by = Auth::guard('company')->user()->name;
            $job->reason = $request->reason;

            $job->status = 'blocked';


            $job->update();
            //dd($job);

            $dataa = array();

            echo 'ok';
            
            /*$dataa['job_title'] = $job->title;
            echo json_encode($dataa);*/

            $contactCompany = Company::findOrFail($job->company_id);
            $data['job'] = $job;
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['msg'] = 'Job Vacancy Deactivated by '.Auth::guard('company')->user()->name.' on '.date('d-M-Y',strtotime($job->updated_at));
            //$data['msg'] = 'Job Vacancy Deactivated on';
            $data['msg2'] = 'Deactivated and can no longer be viewed on live listing';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['subject'] = '';
            $data['vacancy_status'] = 'Job Vacancy Deactivated by '.Auth::guard('company')->user()->name.' on '.date('d-M-Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {
            $dataa = array();
            echo 'notok';
            /*$dataa['job_title'] = '';

            echo json_encode($dataa);*/

            //echo 'notok';

        }

    }


    public function makeActiveJob(Request $request) {
        // dd($this->_setting);
        $id = $request->input('id');

        // $future = new \DateTime('+ 30 days');
        $future = new \DateTime('+' . $this->_setting->default_months . 's');

        $expiry_date = $future;

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 1;

            $job->is_archive = 1;

            $job->expiry_date = $expiry_date;

            $job->blocked_by = '';
            $job->status = 'active';

            $job->reason = $request->reason;


            $job->update();

            $dataa = array();

            echo 'ok';
            /*$dataa['job_title'] = $job->title;

            echo json_encode($dataa);*/
            $contactCompany = Company::findOrFail($job->company_id);
            $data['job'] = $job;
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['msg'] =  'Job Vacancy ['; // 'Job Vacancy Activated  on '.date('d-M-Y',strtotime($job->updated_at));
            $data['msg2'] = '] Activated on ' . date('d-M-Y',strtotime($job->updated_at)); // 'is now live and can be reviewed on the link below';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['subject'] = '';
            $data['vacancy_status'] = 'Job Vacancy Activated by '.Auth::guard('company')->user()->name.' on '.date('d-M-Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            $dataa = array();

            echo 'notok';
            /*$dataa['job_title'] = '';

            echo json_encode($dataa);*/

        }

    }

    public function applyToAdmin(Request $request)

    {
    	$company = Auth::guard('company')->user();

        if ($request->hasFile('job_detail')) {
            $job_detail = $request->file('job_detail');
            $fileName = ImgUploader::UploadDoc('job_files', $job_detail, $company->name);
        }
        $data['name'] = $company->name;
        $data['email'] = $company->email;
        $data['phone'] = $company->phone;
        $data['date'] = date('d-M-Y',strtotime($company->updated_at));
        $data['file_name'] = $fileName;      
        // echo "<pre>";print_r($data);exit;
        Mail::send(new ApplyJobMail($data));
        return Redirect::back()->with('success','Thank you for submitting your job vacancy details, a member of our team will contact you shortly before making this vacancy live on the website.');

    }

}