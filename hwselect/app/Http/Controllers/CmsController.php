<?php

namespace App\Http\Controllers;

use App\Seo;
use App\Cms;
use App\CmsContent;
use Illuminate\Http\Request;

class CmsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPage($slug)
    {
        $cms = Cms::where('page_slug', 'like', $slug)->firstOrFail();

        if (null === $cms) {//echo 'kasdhfkasd';exit;
           return abort(404);
        }else{
            $cmsContent = CmsContent::getContentByPageId($cms->id);
        }

        $seo = (object) array(
                    'seo_title' => $cms->seo_title,
                    'seo_description' => $cms->seo_description,
                    'seo_keywords' => $cms->seo_keywords,
                    'seo_other' => $cms->seo_other
        );

        return view('cms.cms_page')
                        ->with('cmsContent', $cmsContent)
                        ->with('seo', $seo);
    }

    public function set_session(Request $request)
    {
        //dd($request->set_session);
        $set_session = session()->get('set_session');
        setcookie("set_session", "", time()-3600);
        setcookie('set_session', $request->set_session, time() + (86400 * 30), "/");
        //cookie('set_session', $request->set_session);
        session()->put('set_session', '');
        session()->put('set_session', $request->set_session);
        session()->save();
    }

    public function pageNotFound(Request $request)
    {
        return view('errors.404');
    }

}
