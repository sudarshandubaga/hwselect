<?php

namespace App\Http\Controllers;

use Auth;

use DB;

use Input;

use File;

use Image;

use Mail;

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\UploadedFile;

use ImgUploader;

use Carbon\Carbon;

use Redirect;

use Newsletter;

use App\User;

use App\Subscription;

use App\ApplicantMessage;

use App\Close_suspend_requests;

use App\Company;

use App\FavouriteCompany;

use App\Gender;

use App\MaritalStatus;

use App\Country;

use App\State;

use App\City;

use App\JobExperience;

use App\JobApply;

use App\CareerLevel;

use App\Industry;

use App\Alert;
use App\ProfileSummary;

//use Redirect;

use App\FunctionalArea;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Controllers\Controller;

use App\Traits\CommonUserFunctions;

use App\Traits\ProfileSummaryTrait;

use App\Traits\ProfileCvsTrait;

use App\Traits\ProfileProjectsTrait;

use App\Traits\ProfileExperienceTrait;

use App\Traits\ProfileEducationTrait;

use App\Traits\ProfileSkillTrait;

use App\Traits\ProfileLanguageTrait;

use App\Traits\Skills;

use App\Http\Requests\Front\UserFrontFormRequest;

use App\Helpers\DataArrayHelper;

use App\Mail\CloseAccount;
use App\Mail\CloseAccountToAdmin;

class UserController extends Controller
{



    use CommonUserFunctions;

    use ProfileSummaryTrait;

    use ProfileCvsTrait;

    use ProfileProjectsTrait;

    use ProfileExperienceTrait;

    use ProfileEducationTrait;

    use ProfileSkillTrait;

    use ProfileLanguageTrait;

    use Skills;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //$this->middleware('auth', ['only' => ['myProfile', 'updateMyProfile', 'viewPublicProfile']]);

        $this->middleware('auth', ['except' => ['showApplicantProfileEducation', 'showApplicantProfileProjects', 'showApplicantProfileExperience', 'showApplicantProfileSkills', 'showApplicantProfileLanguages','onlyskills','matchedskills']]);
        
        $this->middleware(function ($request, $next) {
            //dd(Auth::user());
            if(Auth::user() && Auth::user()->phone_verify=='no') {
                return Redirect::route('phone-verification');
            }

            return $next($request);    
        });

    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }

     public function changePassword(Request $request){

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not match with the password provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ], [
            'current_password.required' => 'Current Password is required.',
            'password.min' => 'Password must at least 6 characters',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back()->with('message.added','success');
       // return redirect()->back()->with("success","Password changed successfully !");

    }

    public function viewPublicProfile($id)

    {


        if(Auth::user() && Auth::user()->id==$id){
            $user = User::findOrFail($id);

            $profileCv = $user->getDefaultCv();

            return view('user.applicant_profile')

                        ->with('user', $user)

                        ->with('profileCv', $profileCv)

                        ->with('page_title', $user->getName())

                        ->with('form_title', 'Contact ' . $user->getName());
        }else{
             return abort(404);
        }
        

    }

    public function updateTwoStepVerificationStatus(Request $request) {

        $id = $request->input('user_id');

        $old_status = $request->input('old_status');

        if (Hash::check($request->password, Auth::user()->password))
            {
                $user = User::findOrFail($id);
                
                $user->two_step_verification = !$old_status;

                $user->update();
                $request->session()->flash('message.added', 'Once signed out, future logins will require you to enter a 4 digit code sent to your registered mobile number to access your account.');
                $request->session()->flash('message.status', $user->two_step_verification);
                return redirect()->back();
            
            }
        else{
                $request->session()->flash('message.error', 'Password does not match account password ');
                return redirect()->back();
            }

       

    }

    public function myProfile()

    {

        $genders = DataArrayHelper::langGendersArray();

        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();

        $nationalities = DataArrayHelper::langNationalitiesArray();

        $countries = DataArrayHelper::langCountriesArray();

        $jobExperiences = DataArrayHelper::langJobExperiencesArray();

        $careerLevels = DataArrayHelper::langCareerLevelsArray();

        $currencies = DataArrayHelper::defaultCurrencesArray();

        $industries = DataArrayHelper::langIndustriesArray();

        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();

        //$currencies = DataArrayHelper::currenciesArray();

        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);

        $user = User::findOrFail(Auth::user()->id);

        return view('user.edit_profile')

                        ->with('genders', $genders)

                        ->with('maritalStatuses', $maritalStatuses)

                        ->with('nationalities', $nationalities)

                        ->with('countries', $countries)
                        
                        ->with('currencies', $currencies)

                        ->with('jobExperiences', $jobExperiences)

                        ->with('careerLevels', $careerLevels)

                        ->with('industries', $industries)

                        ->with('functionalAreas', $functionalAreas)

                        ->with('user', $user)

                        ->with('upload_max_filesize', $upload_max_filesize);

    }



    public function updateMyProfile(UserFrontFormRequest $request)

    {

        $user = User::findOrFail(Auth::user()->id);

        /*         * **************************************** */

        if ($request->hasFile('image')) {

            $image = $request->file('image');


            $is_deleted = $this->deleteUserImage($user->id);

            $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());

           





            

            

            $fileName = ImgUploader::UploadImage('user_images', $image, $nameonly, 300, 300, true);

            $user->image = $fileName;

            

        }

        /*         * ************************************** */

        $user->first_name = $request->input('first_name');

        $user->middle_name = $request->input('middle_name');

        $user->last_name = $request->input('last_name');

        /*         * *********************** */

        $user->name = $user->getName();

        /*         * *********************** */

        $user->email = $request->input('email');

        if (!empty($request->input('password'))) {

            $user->password = Hash::make($request->input('password'));
            $user->show_password = $request->input('password');

        }

        $user->father_name = $request->input('father_name');

        $user->date_of_birth = $request->input('date_of_birth');
        $user->age = $request->input('age');

        $user->gender_id = $request->input('gender_id');

        $user->marital_status_id = $request->input('marital_status_id');

        $user->nationality_id = $request->input('nationality_id');

        $user->national_id_card_number = $request->input('national_id_card_number');

        $user->is_immediate_available = $request->input('is_immediate_available');
        $user->immediate_date_from = $request->input('date_from');
        $user->country_id = $request->input('country_id');

        $user->state_id = $request->input('state_id');

        $user->city_id = $request->input('city_id');

        $user->phone = $request->input('phone');

        $user->phone_code = $request->input('phone_code');
        $user->phone_num_code = $request->input('phone_num_code');

        $user->mobile_num = $request->input('mobile_num');

        $user->job_experience_id = $request->input('job_experience_id');

        $user->career_level_id = $request->input('career_level_id');

        $user->industry_id = $request->input('industry_id');

        $user->functional_area_id = $request->input('functional_area_id');

        $user->current_salary = $request->input('current_salary');

        $user->expected_salary = $request->input('expected_salary');

        $user->salary_currency = $request->input('salary_currency');

        $user->street_address = $request->input('street_address');

        $user->eligible_uk = $request->input('eligible_uk');
        
        $user->eligible_eu = $request->input('eligible_eu');
        $user->driving_license = $request->input('driving_license');
        $user->willing_to_relocate = $request->input('willing_to_relocate');
        $user->unit_type = $request->input('unit_type');
        $user->eu_distance = $request->input('eu_distance');

		$user->is_subscribed = $request->input('is_subscribed', 0);

		

        $user->update();



        $this->updateUserFullTextSearch($user);
        if($request->input('summary')){
            ProfileSummary::where('user_id', '=', $user->id)->delete();
            $summary = $request->input('summary');
            $ProfileSummary = new ProfileSummary();
            $ProfileSummary->user_id = $user->id;
            $ProfileSummary->summary = $summary;
            $ProfileSummary->save();
        }
        


		/*************************/

		Subscription::where('email', 'like', $user->email)->delete();


		

        flash(__('Profile Updated Successfully'))->success();

        return \Redirect::route('my.profile');

    }


    public function updateImage(Request $request)

    {

        $user = User::findOrFail(Auth::user()->id);

        /*         * **************************************** */

        if ($request->hasFile('image')) {

            $image = $request->file('image');


            $is_deleted = $this->deleteUserImage($user->id);

            $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());

            $fileName = ImgUploader::UploadImage('user_images', $image, $nameonly, 300, 300, true);

            $user->image = $fileName;

            

        }

        

        $user->update();


        echo 'done';

    }

    public function deleteProfileImage(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $is_deleted = $this->deleteUserImage($user->id);
        return $is_deleted;
    }

    public function addToFavouriteCompany(Request $request, $company_slug)

    {

        $data['company_slug'] = $company_slug;

        $data['user_id'] = Auth::user()->id;

        $data_save = FavouriteCompany::create($data);

        flash(__('Company has been added in favorites list'))->success();

        return \Redirect::route('company.detail', $company_slug);

    }



    public function removeFromFavouriteCompany(Request $request, $company_slug)

    {

        $user_id = Auth::user()->id;

        FavouriteCompany::where('company_slug', 'like', $company_slug)->where('user_id', $user_id)->delete();



        flash(__('Company has been removed from favorites list'))->success();

        return \Redirect::route('company.detail', $company_slug);

    }



    public function myFollowings()

    {

        $user = User::findOrFail(Auth::user()->id);

        $companiesSlugArray = $user->getFollowingCompaniesSlugArray();

        $companies = Company::whereIn('slug', $companiesSlugArray)->get();



        return view('user.following_companies')

                        ->with('user', $user)

                        ->with('companies', $companies);

    }



    public function myMessages()

    {

        $user = User::findOrFail(Auth::user()->id);

        $messages = ApplicantMessage::where('user_id', '=', $user->id)

                ->orderBy('is_read', 'asc')

                ->orderBy('created_at', 'desc')

                ->get();



        return view('user.applicant_messages')

                        ->with('user', $user)

                        ->with('messages', $messages);

    }



    public function applicantMessageDetail($message_id)

    {

        $user = User::findOrFail(Auth::user()->id);

        $message = ApplicantMessage::findOrFail($message_id);

        $message->update(['is_read' => 1]);



        return view('user.applicant_message_detail')

                        ->with('user', $user)

                        ->with('message', $message);

    }



    public function myAlerts()

    {

        $alerts = Alert::where('email','like','%'.Auth::user()->email.'%')

            ->orderBy('created_at', 'desc')

            ->get();

        //dd($alerts);

        return view('user.applicant_alerts')

            ->with('alerts', $alerts);

    }

    public function delete_alert($id)

    {

        $alert = Alert::findOrFail($id);

        $alert->delete();

        $arr = array('msg' => 'A Alert has been successfully deleted. ', 'status' => true);

        return Response()->json($arr);

    }

    public function delete_all_alert(Request $request)

    {
        $ids = $request->input('ids');
        $arr = explode(',', $ids);
        $alert = Alert::whereIn('id',$arr)->delete();

        echo 'Successfully deleted';

    }



    public function submitActionRequestUser(Request $request)

    {

        $this->validate($request, [
            'option' => 'required',
        ], [
            'option.required' => 'Option is required.',
        ]);
        $message = new Close_suspend_requests();
        $message->user_id = Auth::user()->id;
        $message->message = !empty($request->reason) ? $request->reason : $request->message;
        $message->type = $request->option;
        if($request->option=='close_account'){
            $message->user_object = json_encode(Auth::user());
        }
        $message->save();

        $user = User::findOrFail(Auth::user()->id);
        if($request->option=='close_account'){

            $data['is_subscribed'] = 'Closed';
           
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['phone'] = $user->phone;
            $data['reason'] = $message->message;
            $data['type'] = 'Jobseeker';
            Mail::send(new CloseAccountToAdmin($data));
            Mail::send(new CloseAccount($data));


            $user->delete();
        }else{
            $user->is_active = 0;
            $user->update();
        }
        if ($message->save() == true) {
            $arr = array('msg' => 'You have successfully closed your account, all personal information has been removed from our database. ', 'status' => true);
        }
        return Response()->json($arr);

    }



}

