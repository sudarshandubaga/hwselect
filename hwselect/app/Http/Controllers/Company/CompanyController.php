<?php



namespace App\Http\Controllers\Company;



use Mail;

use Hash;

use File;

use ImgUploader;

use Auth;

use Validator;

use DB;

use Image;

use Input;

use Redirect;

use App\Subscription;

use App\Mail\Newsletter;

use App\User;

use App\Company;

use App\CompanyMessage;

use App\ApplicantMessage;

use App\Unlocked_users;

use App\Country;

use App\CountryDetail;

use App\State;

use App\City;

use App\Industry;

use App\FavouriteCompany;

use App\FavouriteApplicant;

use App\OwnershipType;

use App\JobApply;

use App\Close_suspend_requests;

use Carbon\Carbon;

use App\Helpers\MiscHelper;

use App\Helpers\DataArrayHelper;

use App\Http\Requests;

use App\Mail\CompanyContactMail;

use App\Mail\ApplicantContactMail;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests\Front\CompanyFrontFormRequest;

use App\Http\Controllers\Controller;

use App\Traits\CompanyTrait;

use App\Traits\Cron;

use App\Mail\CloseAccount;

use App\Mail\CloseAccountToAdmin;



class CompanyController extends Controller
{
    use CompanyTrait;

    use Cron;

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()
    {
        parent::__construct();
        $this->middleware('company', ['except' => ['companyDetail', 'sendContactForm']]);

        $this->runCheckPackageValidity();
        $this->middleware(function ($request, $next) {
           
            if(Auth::guard('company')->check() && Auth::guard('company')->user()->phone_verify=='no') {
                return Redirect::route('phone-verification');
            }
            return $next($request);   
        });

    }

    public function showChangePasswordForm(){
        return view('company_auth.changepassword');
    }

     public function changePassword(Request $request){

        if (!(Hash::check($request->get('current_password'), Auth::guard('company')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ], [
            'current_password.required' => 'Current Password is required.',
            'password.min' => 'Password must include one uppercase one lowercase and one numeric number',
        ]);

        //Change Password
        $user = Auth::guard('company')->user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back()->with('message.added','success');
       // return redirect()->back()->with("success","Password changed successfully !");

    }

    public function index()

    {
        $jobs = Auth::guard('company')->user()->jobs()->orderBy('id', 'DESC')->get();
        $total = Auth::guard('company')->user()->jobs()->get();
       // dd($total);
        $expired = Auth::guard('company')->user()->jobs()->whereDate('expiry_date', '<', Carbon::now())->count();
        //dd($expired);
        $active = Auth::guard('company')->user()->jobs()->where(['status'=>'active'])->whereDate('expiry_date', '>', Carbon::now())->count();
        $inactive = Auth::guard('company')->user()->jobs()->where('is_active',0)->count();
        $rejected = Auth::guard('company')->user()->jobs()->where('is_rejected',1)->count();
        return view('company_home')->with('total', $total)
                        ->with('expired', $expired)
                        ->with('active', $active)
                        ->with('inactive', $inactive)
                        ->with('rejected', $rejected)
                        ->with('jobs', $jobs);

    }


    public function submitActionRequestUser(Request $request)

    {

        $this->validate($request, [
            'option' => 'required',
        ], [
            'option.required' => 'Option is required.',
        ]);
        $message = new Close_suspend_requests();
        $message->company_id = Auth::guard('company')->user()->id;
        $message->message = $request->reason === 'other' ? $request->message : $request->reason;
        $message->type = $request->option;
        if($request->option=='close_account'){
            $message->user_object = json_encode(Auth::guard('company')->user());
        }
        $message->save();

        $user = Company::findOrFail(Auth::guard('company')->user()->id);
        if($request->option=='close_account'){

            $data['is_subscribed'] = 'Closed';
           
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['phone'] = $user->phone;
            $data['reason'] = $message->message;
            $data['type'] = 'Company';

            Mail::send(new CloseAccount($data));
            Mail::send(new CloseAccountToAdmin($data));

            $user->delete();
        }else{
            $user->is_active = 0;
            $user->update();
        }
        if ($message->save() == true) {
            $arr = array('msg' => 'You have successfully closed your account, all personal information has been removed from our database. ', 'status' => true);
        }
        return Response()->json($arr);

    }

    public function settings()

    {
        return view('company.settings');

    }

    public function evaluateQuestions()

    {
        return view('company.evaluateQuestions');

    }

    public function assesmentResults()

    {
        return view('company.assesmentResults');

    }

    public function overview()

    {
        return view('company.overview');

    }

    public function inviteCandidates()

    {
        return view('company.inviteCandidates');

    }

    public function company_listing()

    {

        $data['companies']=Company::paginate(20);

        return view('company.listing')->with($data);

    }
    public function updateCompanySubscribtionStatus(Request $request)

    {

        $id = $request->input('user_id');

        $old_status = $request->input('old_status');

        try {

            $user = Company::findOrFail($id);

            $user->is_subscribed = !$old_status;

            $user->update();

            if($user->is_subscribed==1){
                $data['is_subscribed'] = 'subscribed';
            }else{
                $data['is_subscribed'] = 'Unsubscribed';
            }
            

            $data['name'] = $user->name;
            $data['email'] = $user->email;
            Mail::send(new Newsletter($data));
           
            echo 'ok';
            

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }

    public function updateTwoStepVerificationStatus(Request $request)

    {

        $id = $request->input('user_id');

        $old_status = $request->input('old_status');
        if (Hash::check($request->password, Auth::guard('company')->user()->password))
            {
                $user = Company::findOrFail($id);

                $user->two_step_verification = !$old_status;

                $user->update();
                $request->session()->flash('message.added', 'Once signed out, future logins will require you to enter a 4 digit code sent to your registered mobile number to access your account.');
                $request->session()->flash('message.status', $user->two_step_verification);
                return redirect()->back();
            
            }
        else{
                $request->session()->flash('message.error', 'Password does not match account password');
                return redirect()->back();
            }
        

    }


    public function companyProfile()

    {

        $countries = DataArrayHelper::defaultCountriesArray();

        $industries = DataArrayHelper::defaultIndustriesArray();

        $ownershipTypes = DataArrayHelper::defaultOwnershipTypesArray();

        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        return view('company.edit_profile')

                        ->with('company', $company)

                        ->with('countries', $countries)

                        ->with('industries', $industries)

                        ->with('ownershipTypes', $ownershipTypes);

    }



    public function updateCompanyProfile(CompanyFrontFormRequest $request)

    {

        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        /*         * **************************************** */
        if ($request->hasFile('image')) {

            

            $image = $request->file('image');

            $is_deleted = $this->deleteCompanyLogo($company->id);

            //$is_deleted = $this->deleteUserImage($company->id);

           $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());
            /*
            $input['imagename'] = $nameonly . '_' . rand(1, 999) . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('/company_logos/thumb');

            $img = Image::make($image->getRealPath());

            $img->resize(100, 100, function ($constraint) {

                $constraint->aspectRatio();

            })->save($destinationPath . '/' . $input['imagename']);

            $destinationPath = public_path('/company_logos/');

            $image->move($destinationPath, $input['imagename']);

            $company->logo = $input['imagename'];*/

            $fileName = ImgUploader::UploadImage('company_logos', $image, $nameonly, 300, 300, true);

            $company->logo = $fileName;

        }

        /*         * ************************************** */

        $company->name = $request->input('name');

        //$company->email = $request->input('email');

        if (!empty($request->input('password'))) {

            $company->password = Hash::make($request->input('password'));

        }

        $company->ceo = $request->input('ceo');
        $company->ceo_number = $request->input('ceo_number');
        $company->ceo_2 = $request->input('ceo_2');
        $company->ceo_number_2 = $request->input('ceo_number_2');
        $company->contact_details = $request->input('contact_details');

        $company->industry_id = $request->input('industry_id');

        $company->ownership_type_id = $request->input('ownership_type_id');

        $company->description = $request->input('description');

        $company->location = $request->input('location');

        $company->map = $request->input('map');

        $company->no_of_offices = $request->input('no_of_offices');

        $website = $request->input('website');

        if(!empty($website))
        $company->website = (false === strpos($website, 'http')) ? 'http://' . $website : $website;
        else 
        $company->website = null;
        
        $company->no_of_employees = $request->input('no_of_employees');

        $company->established_in = $request->input('established_in');

        $company->fax = $request->input('fax');

        $company->phone = $request->input('phone');
        $company->phone_code = $request->input('phone_code');
        $company->phone_num_code = $request->input('phone_num_code');
        $company->ceo_number_code2 = $request->input('ceo_number_code2');

        $company->facebook = $request->input('facebook');

        $company->twitter = $request->input('twitter');

        $company->linkedin = $request->input('linkedin');

        $company->google_plus = $request->input('google_plus');

        $company->pinterest = $request->input('pinterest');

        $company->country_id = $request->input('country_id');

        $company->state_id = $request->input('state_id');

        $company->city_id = $request->input('city_id');

		$company->is_subscribed = $request->input('is_subscribed', 0);

		

        $company->slug = \Illuminate\Support\Str::slug($company->name, '-') . '-' . $company->id;

        $company->update();

		/*************************/

	



        

        flash(__('Company Information Updated'))->success();

        return \Redirect::route('company.profile');

    }


    public function updateCompanyImage(Request $request)

    {

        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        /*         * **************************************** */
        if ($request->hasFile('image')) {

            

            $image = $request->file('image');

            $is_deleted = $this->deleteCompanyLogo($company->id);

            //$is_deleted = $this->deleteUserImage($company->id);

           $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());
            /*
            $input['imagename'] = $nameonly . '_' . rand(1, 999) . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('/company_logos/thumb');

            $img = Image::make($image->getRealPath());

            $img->resize(100, 100, function ($constraint) {

                $constraint->aspectRatio();

            })->save($destinationPath . '/' . $input['imagename']);

            $destinationPath = public_path('/company_logos/');

            $image->move($destinationPath, $input['imagename']);

            $company->logo = $input['imagename'];*/

            $fileName = ImgUploader::UploadImage('company_logos', $image, $nameonly, 300, 300, true);

            $company->logo = $fileName;

        }

       

        $company->update();

        /*************************/

        echo 'done';

    }



    public function addToFavouriteApplicant(Request $request, $application_id, $user_id, $job_id, $company_id)

    {

        $data['user_id'] = $user_id;

        $data['job_id'] = $job_id;

        $data['company_id'] = $company_id;



        $data_save = FavouriteApplicant::create($data);

        flash(__('Job seeker has been added in favorites list'))->success();

        return \Redirect::route('applicant.profile', $application_id);

    }



    public function removeFromFavouriteApplicant(Request $request, $application_id, $user_id, $job_id, $company_id)

    {

        $data['user_id'] = $user_id;

        $data['job_id'] = $job_id;

        $data['company_id'] = $company_id;

        FavouriteApplicant::where('user_id', $user_id)

                ->where('job_id', '=', $job_id)

                ->where('company_id', '=', $company_id)

                ->delete();



        flash(__('Job seeker has been removed from favorites list'))->success();

        return \Redirect::route('applicant.profile', $application_id);

    }



    public function companyDetail(Request $request, $company_slug)

    {

        $company = Company::where('slug', 'like', $company_slug)->firstOrFail();

        /*         * ************************************************** */

        $seo = $this->getCompanySEO($company);

        /*         * ************************************************** */

        return view('company.detail')

                        ->with('company', $company)

                        ->with('seo', $seo);

    }



    public function sendContactForm(Request $request)

    {

        $msgresponse = Array();

        $rules = array(

            'from_name' => 'required|max:100|between:4,70',

            'from_email' => 'required|email|max:100',

            'subject' => 'required|max:200',

            'message' => 'required',

            'to_id' => 'required',

            'g-recaptcha-response' => 'required|captcha',

        );

        $rules_messages = array(

            'from_name.required' => __('Name is required'),

            'from_email.required' => __('E-mail address is required'),

            'from_email.email' => __('Valid e-mail address is required'),

            'subject.required' => __('Subject is required'),

            'message.required' => __('Message is required'),

            'to_id.required' => __('Recieving Company details missing'),

            'g-recaptcha-response.required' => __('Please verify that you are not a robot'),

            'g-recaptcha-response.captcha' => __('Captcha error! try again'),

        );

        $validation = Validator::make($request->all(), $rules, $rules_messages);

        if ($validation->fails()) {

            $msgresponse = $validation->messages()->toJson();

            echo $msgresponse;

            exit;

        } else {

            $receiver_company = Company::findOrFail($request->input('to_id'));

            $data['company_id'] = $request->input('company_id');

            $data['company_name'] = $request->input('company_name');

            $data['from_id'] = $request->input('from_id');

            $data['to_id'] = $request->input('to_id');

            $data['from_name'] = $request->input('from_name');

            $data['from_email'] = $request->input('from_email');

            $data['from_phone'] = $request->input('from_phone');

            $data['subject'] = $request->input('subject');

            $data['message_txt'] = $request->input('message');

            $data['to_email'] = $receiver_company->email;

            $data['to_name'] = $receiver_company->name;

            $msg_save = CompanyMessage::create($data);

            $when = Carbon::now()->addMinutes(5);

            Mail::send(new CompanyContactMail($data));

            $msgresponse = ['success' => 'success', 'message' => __('Message sent successfully')];

            echo json_encode($msgresponse);

            exit;

        }

    }



    public function sendApplicantContactForm(Request $request)

    {

        $msgresponse = Array();

        $rules = array(

            'from_name' => 'required|max:100|between:4,70',

            'from_email' => 'required|email|max:100',

            'subject' => 'required|max:200',

            'message' => 'required',

            'to_id' => 'required',

        );

        $rules_messages = array(

            'from_name.required' => __('Name is required'),

            'from_email.required' => __('E-mail address is required'),

            'from_email.email' => __('Valid e-mail address is required'),

            'subject.required' => __('Subject is required'),

            'message.required' => __('Message is required'),

            'to_id.required' => __('Recieving applicant details missing'),

            'g-recaptcha-response.required' => __('Please verify that you are not a robot'),

            'g-recaptcha-response.captcha' => __('Captcha error! try again'),

        );

        $validation = Validator::make($request->all(), $rules, $rules_messages);

        if ($validation->fails()) {

            $msgresponse = $validation->messages()->toJson();

            echo $msgresponse;

            exit;

        } else {

            $receiver_user = User::findOrFail($request->input('to_id'));

            $data['user_id'] = $request->input('user_id');

            $data['user_name'] = $request->input('user_name');

            $data['from_id'] = $request->input('from_id');

            $data['to_id'] = $request->input('to_id');

            $data['from_name'] = $request->input('from_name');

            $data['from_email'] = $request->input('from_email');

            $data['from_phone'] = $request->input('from_phone');

            $data['subject'] = $request->input('subject');

            $data['message_txt'] = $request->input('message');

            $data['to_email'] = $receiver_user->email;

            $data['to_name'] = $receiver_user->getName();

            $msg_save = ApplicantMessage::create($data);

            $when = Carbon::now()->addMinutes(5);

            Mail::send(new ApplicantContactMail($data));

            $msgresponse = ['success' => 'success', 'message' => __('Message sent successfully')];

            echo json_encode($msgresponse);

            exit;

        }

    }



    public function postedJobs(Request $request)

    {

        $jobs = Auth::guard('company')->user()->jobs()->orderBy('id', 'DESC')->get();
        $total = Auth::guard('company')->user()->jobs()->get();
        $expired = Auth::guard('company')->user()->jobs()->whereDate('expiry_date', '<', Carbon::now())->count();
        $active = Auth::guard('company')->user()->jobs()->where('is_active',1)->whereDate('expiry_date', '>', Carbon::now())->count();
        $inactive = Auth::guard('company')->user()->jobs()->where('is_active',0)->count();

        return view('job.company_posted_jobs')

                        ->with('total', $total)
                        ->with('expired', $expired)
                        ->with('active', $active)
                        ->with('inactive', $inactive)
                        ->with('jobs', $jobs);

    }


    public function rejectedJobs(Request $request)

    {

        $jobs = Auth::guard('company')->user()->jobs()->where('is_rejected',1)->orderBy('id', 'DESC')->get();
        $total = Auth::guard('company')->user()->jobs()->get();
        $expired = Auth::guard('company')->user()->jobs()->whereDate('expiry_date', '<', Carbon::now())->count();
        $active = Auth::guard('company')->user()->jobs()->where('is_active',1)->whereDate('expiry_date', '>', Carbon::now())->count();
        $inactive = Auth::guard('company')->user()->jobs()->where('is_active',0)->count();

        return view('job.company_refused_jobs')

                        ->with('total', $total)
                        ->with('expired', $expired)
                        ->with('active', $active)
                        ->with('inactive', $inactive)
                        ->with('jobs', $jobs);

    }



    public function listAppliedUsers(Request $request, $job_id)

    {

        $job_applications = JobApply::where('job_id', '=', $job_id)->get();



        return view('job.job_applications')

                        ->with('job_applications', $job_applications);

    }



    public function listFavouriteAppliedUsers(Request $request, $job_id)

    {

        $company_id = Auth::guard('company')->user()->id;

        $user_ids = FavouriteApplicant::where('job_id', '=', $job_id)->where('company_id', '=', $company_id)->pluck('user_id')->toArray();

        $job_applications = JobApply::where('job_id', '=', $job_id)->whereIn('user_id', $user_ids)->get();



        return view('job.job_applications')

                        ->with('job_applications', $job_applications);

    }



    public function applicantProfile($application_id)

    {



        $job_application = JobApply::findOrFail($application_id);

        $user = $job_application->getUser();

        $job = $job_application->getJob();

        $company = $job->getCompany();

        $profileCv = $job_application->getProfileCv();



        /*         * ********************************************** */

        $num_profile_views = $user->num_profile_views + 1;

        $user->num_profile_views = $num_profile_views;

        $user->update();

        /*         * ********************************************** */

        return view('user.applicant_profile')

                        ->with('job_application', $job_application)

                        ->with('user', $user)

                        ->with('job', $job)

                        ->with('company', $company)

                        ->with('profileCv', $profileCv)

                        ->with('page_title', 'Applicant Profile')

                        ->with('form_title', 'Contact Applicant');

    }



    public function userProfile($id)

    {



        $user = User::findOrFail($id);

        $profileCv = $user->getDefaultCv();



        /*         * ********************************************** */

        $num_profile_views = $user->num_profile_views + 1;

        $user->num_profile_views = $num_profile_views;

        $user->update();

        /*         * ********************************************** */

        return view('user.applicant_profile')

                        ->with('user', $user)

                        ->with('profileCv', $profileCv)

                        ->with('page_title', 'Job Seeker Profile')

                        ->with('form_title', 'Contact Job Seeker');

    }



    public function companyFollowers()

    {

        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        $userIdsArray = $company->getFollowerIdsArray();

        $users = User::whereIn('id', $userIdsArray)->get();



        return view('company.follower_users')

                        ->with('users', $users)

                        ->with('company', $company);

    }



    public function companyMessages()

    {

        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        $messages = CompanyMessage::where('company_id', '=', $company->id)

                ->orderBy('is_read', 'asc')

                ->orderBy('created_at', 'desc')

                ->get();



        return view('company.company_messages')

                        ->with('company', $company)

                        ->with('messages', $messages);

    }



    public function companyMessageDetail($message_id)

    {

        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        $message = CompanyMessage::findOrFail($message_id);

        $message->update(['is_read' => 1]);



        return view('company.company_message_detail')

                        ->with('company', $company)

                        ->with('message', $message);

    }

    public function unlocked_users($job_id,$status)

    {

        $data = array();
        if($status=='all'){
            $unlocked_users = Unlocked_users::where('company_id', Auth::guard('company')->user()->id)->where('job_id',$job_id)->get();
        }else{
            $unlocked_users = Unlocked_users::where('company_id', Auth::guard('company')->user()->id)->where('job_id',$job_id)->where('status',$status)->get();
        }
        

        $ids = array();

        if (null !== ($unlocked_users)) {

            foreach ($unlocked_users as $key => $value) {

                 $ids[] = $value->unlocked_users_ids;

            }

            $data['users'] = User::whereIn('id', $ids)->get();

            $data['job_id'] = $job_id;

            $data['status'] = $status;

        }



        return view('company.unlocked_users')->with($data);

    }



    public function unlock(Request $request)

    {

        $company_id = $request->company_id;

        $user_id = $request->user_id;

        $job_id = $request->job_id;

        $cvsSearch = Company::findOrFail($company_id);

        if (null !== ($cvsSearch)) {



            $unlock = Unlocked_users::where('company_id', $company_id)->where('job_id',$job_id)->where('unlocked_users_ids',$user_id)->first();

            if (null !== ($unlock)) {

                 if ($cvsSearch->availed_cvs_ids != '') {

                    $newString = $this->removetoString($cvsSearch->availed_cvs_ids, $user_id); 

                    $cvsSearch->availed_cvs_ids  = $newString;

                    $cvsSearch->availed_cvs_quota -= 1;

                    $cvsSearch->update();

                } 



                $dd = Unlocked_users::findOrFail($unlock->id);

                $dd->delete();

                echo  'You have successully locked this profile';



            }else{

                if ($cvsSearch->availed_cvs_ids != '') {



                    $newString = $this->addtoString($cvsSearch->availed_cvs_ids, $user_id);

                } else {

                    $newString = $user_id;

                }



                $cvsSearch->availed_cvs_ids  = $newString;

                $cvsSearch->availed_cvs_quota += 1;

                $cvsSearch->update();

                $unlock = new Unlocked_users();

                $unlock->company_id  = $company_id;

                $unlock->job_id  = $job_id;

                $unlock->unlocked_users_ids  = $user_id;

                $unlock->save();

                echo 'You have successully Unlocked this profile';

            }







            

        } else {

            return redirect('/company-packages');

        }

    }





   

    public function lock(Request $request)

    {

        $company_id = $request->company_id;

        $user_id = $request->user_id;

        $job_id = $request->job_id;

        $cvsSearch = Company::findOrFail($company_id);

        if (null !== ($cvsSearch)) {

            if ($cvsSearch->availed_cvs_ids != '') {

                $newString = $this->removetoString($cvsSearch->availed_cvs_ids, $user_id); 

                $cvsSearch->availed_cvs_ids  = $newString;

                $cvsSearch->availed_cvs_quota -= 1;

                $cvsSearch->update();

            } 

           



            $unlock = Unlocked_users::where('company_id', $company_id)->where('job_id',$job_id)->where('unlocked_users_ids',$user_id)->first();

            if (null !== ($unlock)) {

                $dd = Unlocked_users::findOrFail($unlock->id);

                $dd->delete()();

            }

            echo 'ok';

        } 

    }

    function addtoString($str, $item)

    {

        $parts = explode(',', $str);

        $parts[] = $item;



        return implode(',', $parts);

    }





    function removetoString($str, $item)

    {

        $array1 = array($item);

        $array2 = explode(',', $str);

        $array3 = array_diff($array2, $array1);

        return implode(',', $array3);

    }



}

