<?php



namespace App\Http\Controllers;



use Mail;

use View;

use DB;

use Auth;

use Input;

use Carbon\Carbon;

use Redirect;

use App\Seo;

use App\Job;

use App\Company;

use App\ContactMessage;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use App\ReportAbuseMessage;

use App\ReportAbuseCompanyMessage;

use App\SendToFriendMessage;

use App\Mail\ContactUs;

use App\Mail\EmailToFriend;

use App\Mail\ReportAbuse;

use App\Mail\ReportAbuseCompany;

use App\Http\Requests\Front\ContactFormRequest;

use App\Http\Requests\Front\EmailToFriendFormRequest;

use App\Http\Requests\Front\ReportAbuseFormRequest;

use App\Http\Requests\Front\ReportAbuseCompanyFormRequest;



class ContactController extends Controller

{



    public function index(Request $request)

    {
        $user = null;
        if(auth()->check()) {
            $user = auth()->user()->toArray();
            $user['user_type'] = 'Job Seeker';
        } elseif(auth()->guard('company')->check()) {
            $user = auth()->guard('company')->user()->toArray();
            $user['user_type'] = 'Company';
        }

        // dd($user);
        if(!empty($user)) {
            $request->replace([
                'type'  => $user['user_type'],
                'full_name' => $user['name'],
                'email' => $user['email'],
                'phone' => $user['phone']
            ]);
            $request->flash();
        } else {
            $request->flush();
        }


        $seo = SEO::where('seo.page_title', 'like', 'contact')->first();

        return view('contact.contact_page')->with('seo', $seo);

    }

    public function contactRecruiters(){
        $seo = SEO::where('seo.page_title', 'like', 'contact')->first();
        return view('contact.contact_recruiters_page')->with('seo', $seo);
    }

    public function contactJobseekers(){
        $seo = SEO::where('seo.page_title', 'like', 'contact')->first();
        return view('contact.contact_jobseekers_page')->with('seo', $seo);
    }



    public function postContact(ContactFormRequest $request)

    {

        $data['type'] = $request->input('type');
        $data['full_name'] = $request->input('full_name');

        $data['email'] = $request->input('email');

        $data['phone'] = $request->input('phone');

        $data['subject'] = $request->input('subject');

        $data['message_txt'] = $request->input('message_txt');
       

        $best_time = $request->best_time;
        $data['best_time'] = json_encode($best_time);

        $msg_save = ContactMessage::create($data);


        //$when = Carbon::now()->addMinutes(5);

        Mail::send(new ContactUs($data));

        return Redirect::route('contact.us.thanks');

    }



    public function thanks()

    {

        $seo = SEO::where('seo.page_title', 'like', 'contact')->first();

        return view('contact.contact_page_thanks')->with('seo', $seo);

    }



    /*     * ******************************************************** */



    public function emailToFriend($slug)

    {

        $job = Job::where('slug', $slug)->first();
        if(null==($job)){
            return abort(404);
        }

        $seo = SEO::where('seo.page_title', 'like', 'email_to_friend')->first();

        return view('contact.email_to_friend_page')->with('job', $job)->with('slug', $slug)->with('seo', $seo);

    }



    public function emailToFriendPost(EmailToFriendFormRequest $request, $slug)

    {

        $data['your_name'] = $request->input('your_name');

        $data['your_email'] = $request->input('your_email');

        $data['friend_name'] = $request->input('friend_name');

        $data['friend_email'] = $request->input('friend_email');

        $data['job_url'] = $request->input('job_url');
        $data['job_title'] = $request->input('title');

        $msg_save = SendToFriendMessage::create($data);

        $when = Carbon::now()->addMinutes(5);

        Mail::send(new EmailToFriend($data));

        return Redirect::route('email.to.friend.thanks','title='.$request->input('title'))->with('data',$data);

    }



    public function emailToFriendThanks(Request $request)

    {
        //dd($request);
        $seo = SEO::where('seo.page_title', 'like', 'email_to_friend')->first();

        return view('contact.email_to_friend_page_thanks')->with('seo', $seo)->with('title',$request->title);

    }



    /*     * ******************************************************** */



    public function reportAbuse($slug)

    {

        if(!auth()->check() && !auth()->guard('company')->check()) {
            return redirect( url('/') );
        }

        $job = Job::where('slug', $slug)->first();

        $seo = SEO::where('seo.page_title', 'like', 'report_abuse')->first();

        return view('contact.report_abuse_page')->with('job', $job)->with('slug', $slug)->with('seo', $seo);

    }



    public function reportAbusePost(ReportAbuseFormRequest $request, $slug)

    {
        $r_abuse_check = ReportAbuseMessage::where('job_id', $request->job_id);
        if(!empty($request->user_id)) {
            $r_abuse_check->where('user_id', $request->user_id);
        } else {
            $r_abuse_check->where('company_id', $request->company_id);
        }

        if(!$r_abuse_check->count()) :

            $reportAbuse = new ReportAbuseMessage();
            $reportAbuse->job_id = $request->job_id;
            $reportAbuse->reason = $request->reason !== 'other' ? $request->reason : $request->message;
            if(!empty($request->user_id)) :
                $reportAbuse->user_id = $request->user_id;
            elseif(!empty($request->company_id)) :
                $reportAbuse->company_id = $request->company_id;
            endif;
            $reportAbuse->save();

            $report_abuse = ReportAbuseMessage::find($reportAbuse->id);
            $data = $report_abuse->toArray();
            $data['job_url'] = route('job.detail', [\Str::slug($report_abuse->job->getFunctionalArea('functional_area')),$report_abuse->job->slug]);

            $when = Carbon::now()->addMinutes(5);

            Mail::send(new ReportAbuse($data));
        endif;

        return Redirect::route('report.abuse.thanks');

    }



    public function reportAbuseThanks()

    {

        $seo = SEO::where('seo.page_title', 'like', 'report_abuse')->first();

        return view('contact.report_abuse_page_thanks')->with('seo', $seo);

    }



    /*     * ******************************************************** */



    public function reportAbuseCompany($slug)

    {

        $company = Company::where('slug', $slug)->first();

        $seo = SEO::where('seo.page_title', 'like', 'report_abuse')->first();

        return view('contact.report_abuse_company_page')->with('company', $company)->with('slug', $slug)->with('seo', $seo);

    }



    public function reportAbuseCompanyPost(ReportAbuseCompanyFormRequest $request, $slug)

    {

        $data['your_name'] = $request->input('your_name');

        $data['your_email'] = $request->input('your_email');

        $data['company_url'] = $request->input('company_url');

        $msg_save = ReportAbuseCompanyMessage::create($data);

        $when = Carbon::now()->addMinutes(5);

        Mail::send(new ReportAbuseCompany($data));

        return Redirect::route('report.abuse.company.thanks');

    }



    public function reportAbuseCompanyThanks()

    {

        $seo = SEO::where('seo.page_title', 'like', 'report_abuse')->first();

        return view('contact.report_abuse_company_page_thanks')->with('seo', $seo);

    }



}

