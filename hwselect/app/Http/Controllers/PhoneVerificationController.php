<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\User;
use App\Job;
use App\FavouriteJob;

use App\Company;
use App\CountryDetail;

use Auth;
use Redirect;
use Session;

use App\Helpers\DataArrayHelper;

class PhoneVerificationController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //$this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */

    public function index(Request $request)

    {
        //error_reporting("E_ALL");
        if(Auth::user()){
            
            //dd($_COOKIE['saved_jobs']);
            if(!empty($_COOKIE['saved_jobs']) && $_COOKIE['saved_jobs']!='' && $_COOKIE['saved_jobs']!='null'){
                $cookie = $_COOKIE['saved_jobs'];
                $cookie_arr = explode(',', $cookie);
            
                
                foreach ($cookie_arr as $key => $value) {
                    $job = Job::findOrFail($value);
                    $favouritejob = FavouriteJob::where('job_slug',$job->slug)->where('user_id',Auth::user()->id)->first();
                    if(empty($favouritejob) && null==($favouritejob)){
                        $data['job_slug'] = $job->slug;
                        $data['user_id'] = Auth::user()->id;
                        $data_save = FavouriteJob::create($data); 
                    }
                    
                    //$jobs_arr[] = $job->id;
                }
            }
            $jobs_arr = array();
            $favouritejobs = FavouriteJob::where('user_id',Auth::user()->id)->get();
            foreach ($favouritejobs as $key => $val) {
                $jo = Job::where('slug',$val->job_slug)->first();
                $jobs_arr[] = $jo->id;
            }

            $jobs_comma = implode(',', $jobs_arr);
            setcookie('saved_jobs', $jobs_comma, time() + (86400 * 30), "/");
            
            $verified = Auth::user()->phone_verify;

            if($verified=='yes'){

                $set_session = isset($_COOKIE['set_session'])?$_COOKIE['set_session']:null;
                if($set_session!='') {
                  return redirect($set_session);
                }else{
                   return redirect(route('home'));
                } 

            }else if(!(bool)Auth::user()->two_step_verification){
                $user = User::findOrFail(Auth::user()->id);
                $user->phone_verify = 'yes';
                $user->update();
                $set_session = isset($_COOKIE['set_session'])?$_COOKIE['set_session']:null;
                if($set_session!='') {
                  return redirect($set_session);
                }else{
                   return redirect(route('home'));
                } 
        
                 
            }else{
                $countries_details = DataArrayHelper::countriesDetailsArray();
                return view('phone_verification')->with('countries_details',$countries_details);

            }

           

        }else if(Auth::guard('company')->check()){

            if(Auth::guard('company')->user()->verified=='0') {
                Auth::logout();
                return Redirect::route('login')->with('not_verified','Not verified');
            }

            $verified = Auth::guard('company')->user()->phone_verify;

            if($verified=='yes'){

                return redirect(route('company.home')); 

            }else if(!(bool)Auth::guard('company')->user()->two_step_verification){
                $user = Company::findOrFail(Auth::guard('company')->user()->id);
                $user->phone_verify = 'yes';
                $user->update();
                return redirect(route('company.home')); 
            }else{
                $countries_details = DataArrayHelper::countriesDetailsArray();
                return view('phone_verification')->with('countries_details',$countries_details);

            } 

        }else{

            return redirect('/login'); 

        }

        

    }



    public function verification(Request $request)

    {

        if(Auth::user($request)){

            $id = Auth::user()->id;

            $user = User::findOrFail($id);

            $user->phone_verify = 'yes';

            $user->update();

            echo route('home'); 

        }else if(Auth::guard('company')->check()){

            $id = Auth::guard('company')->user()->id;

            $user = Company::findOrFail($id);

            $user->phone_verify = 'yes';


            $user->update();

            echo route('company.home'); 

        }else{

            return redirect('/login'); 

        }

        

    }

    public function verification_number(Request $request)
    {

        if(Auth::user()){
            $id = Auth::user()->id;
            $user = User::findOrFail($id);
            $phone = $user->phone;
            if($phone == $request->get('phone')){
                echo 'ok';  
            }else{
                echo 'unknown';
            }
             
        }else if(Auth::guard('company')->check()){
            $id = Auth::guard('company')->user()->id;
            $user = Company::findOrFail($id);
            if($user->phone == $request->get('phone')){
                echo 'ok';  
            }else{
                echo 'unknown';
            }
        }else{
            return redirect('/login'); 
        }
    }

}

