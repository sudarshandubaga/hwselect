<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\Job;
use App\SiteSetting;

class Controller extends BaseController
{

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public $_setting = null;

    public function __construct()
    {
        // $jobs = Job::get();

        // // dd($jobs);

        // $slugArr = [];

        // foreach($jobs as $key => $job) {

        //     $address = explode(",", $job->getLocation());
        //     array_pop($address);
        //     $jobSlug = strtolower(\Str::slug( $job->title . '-' . implode("-", $address)));

        //     // $slug_count = Job::where('slug',  $jobSlug)->orWhere('slug', 'like', $jobSlug . '-%')->where('id', '!=', $job->id)->count();

        //     // if($slug_count) $jobSlug .= '-'.$slug_count;

        //     $slugArr[$jobSlug] = !empty($slugArr[$jobSlug]) ? $slugArr[$jobSlug] + 1 : 1;

        //     if(!empty($slugArr[$jobSlug])) {
        //         $jobSlug .= '-' . $slugArr[$jobSlug];
        //     }

        //     $job->slug = $jobSlug;

        //     $job->save();
        // }

        $this->_setting = SiteSetting::first();
        // dd($setting);
        \View::share('setting', $this->_setting);
    }

    public static function JobCount() {
    	if(null !== (Auth::user())) {
            // $myFavouriteJobSlugs = Auth::user()->getFavouriteJobSlugsArray();
            $myFavouriteJobIds = Auth::user()->getFavouriteJobIdArray();
            
            $jobs = Job::whereIn('id', $myFavouriteJobIds)->count();  
            //dd($jobs);
            return $jobs;
        }
    }
}
