<?php

namespace App\Http\Controllers;

use App\Traits\Cron;
use App\User;
use App\Company;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CronController extends Controller
{

    use Cron;

    public function checkPackageValidity()
    {
        $this->runCheckPackageValidity();
    }


    public function delete_notverified_users(){
    	$date = Carbon::today()->subDays(7);
    	$user_ids = User::select('id')->where('created_at', '>=', $date)->where('verified','!=',1)->get();
    	if(null!==($user_ids)){
    		foreach ($user_ids as $key => $value) {
    			$user = User::findorFail($value->id);
    			$user->delete();
    		}
    	}
    }

    public function delete_notverified_companies(){
    	$date = Carbon::today()->subDays(7);
    	$user_ids = Company::select('id')->where('created_at', '>=', $date)->where('verified','!=',1)->get();
    	if(null!==($user_ids)){
    		foreach ($user_ids as $key => $value) {
    			$user = Company::findorFail($value->id);
    			$user->delete();
    		}
    	}
    }

}
