<?php



namespace App\Http\Controllers\Admin;



use Auth;

use DB;

use Input;

use Redirect;

use Carbon\Carbon;

use App\Job;
use App\JobApply;

use App\Company;

use App\Unlocked_users;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use DataTables;

use App\Http\Controllers\Controller;

use App\Traits\JobTrait;

use App\Helpers\MiscHelper;

use App\Helpers\DataArrayHelper;

use App\Mail\RecruitedJobMail;

use Mail;




class JobController extends Controller

{



    use JobTrait;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //

    }



    public function indexJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.index')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function abusedJobs()
    {
        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.abusedJobs')

                        ->with('companies', $companies)

                        ->with('countries', $countries);
    }

    public function expiredJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.expired')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function rejectedJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.rejected')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function onholdJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.onhold')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function pendingForApproval()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.pendingForApproval')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function expireddJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.expiredJobs')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }

    public function jobDetail(Request $request, $id)
    {

        $job = Job::findOrFail($id);
        
        return view('admin.job.detail')
                        ->with('job', $job);
    }

    public function deletedJobs()

    {

        $companies = DataArrayHelper::companiesArray();

        $countries = DataArrayHelper::defaultCountriesArray();

        return view('admin.job.deletedJobs')

                        ->with('companies', $companies)

                        ->with('countries', $countries);

    }



    public function fetchJobsData(Request $request) {

        Job::where('expiry_date', '<', Carbon::now())->update(['is_active' => 0]);

        $jobs = Job::select([

                    'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','admin_reviewed','jobs.created_at','jobs.is_rejected',
                    'jobs.updated_at','jobs.rejected_reason','jobs.reason','jobs.status'

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('description') && !empty($request->description)) {

                                $query->where('jobs.description', 'like', "%{$request->get('description')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_active') && $request->is_active != -1) {

                                $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                            }

                            if ($request->has('status') && $request->status == 'expired') {

                                $query->where('expiry_date', '<', Carbon::now());

                            }
                            if ($request->has('admin_reviewed') && $request->admin_reviewed != -1) {

                                $query->where('jobs.admin_reviewed', '=', "{$request->get('admin_reviewed')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }

                            $query->orderBy('jobs.id', "DESC");

                        })

                        ->addColumn('company_id', function ($jobs) {

                            return $jobs->getCompany('name');

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                        ->addColumn('description', function ($jobs) {

                            return strip_tags(\Illuminate\Support\Str::limit($jobs->description, 50, '...'));

                        })

                        ->addColumn('created_at', function ($jobs) {

                            return date('d-M-Y',strtotime($jobs->created_at));

                        })

                        ->addColumn('status', function ($jobs) {

                            $status = '';
                            if($jobs->admin_reviewed!==1){
                                if($jobs->is_rejected==1){
                                    $status = 'On-Hold (Expired)'.' '.date('d-M-Y',strtotime($jobs->expiry_date));
                                }else{
                                    $status = 'Pending Not Approved (Expires '.date('d-M-Y',strtotime($jobs->expiry_date)). ')';
                                }
                                
                            }else if($jobs->expiry_date <  \Carbon\Carbon::now() && $jobs->is_active!=1){
                                $status = 'Expired'.' '.date('d-M-Y',strtotime($jobs->expiry_date));
                            }else if($jobs->is_active==1){
                                $status = 'Active (Expires '.date('d-M-Y',strtotime($jobs->expiry_date)). ')';
                            }else{
                                $reason = '';
                                if($jobs->status=='rejected' && $jobs->rejected_reason!==''){
                                    $reason = '<a href="javascript:;"  class="btn btn-default show-reason" data-reason="'.$jobs->rejected_reason.'">Reason</a>';
                                }else if(($jobs->status=='blocked' || $jobs->status=='active') &&  $jobs->reason!==''){
                                    $reason = '<a href="javascript:;"  class="btn btn-default show-reason" data-reason="'.$jobs->reason.'">Reason</a>';
                                }

                                $status = 'InActive '.$jobs->blocked_by .'on '. date('d-M-Y',strtotime($jobs->updated_at)).'<br>'.$reason;
                            }

                            return $status;

                        })

                        ->addColumn('checkbox', function ($jobs) {

                            return '<input class="checkboxes" type="checkbox" id="check_'.$jobs->id.'" name="job_ids[]" autocomplete="off" value="'.$jobs->id.'">';

                        })

                        ->addColumn('action', function ($jobs) use ($request) {

                            /*                             * ************************* */

                            $activeTxt = 'Make Active';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Make InActive';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }


                            $reviewedTxt = 'Approve Job';

                            $reviewedHref = 'makeReviewed(' . $jobs->id . ');';

                            $reviewedIcon = 'square-o';

                            if ((int) $jobs->admin_reviewed != 1) {

                                $reviewedTxt = 'On Hold';

                                $reviewedHref = 'makeNotReviewed(' . $jobs->id . ');';

                                $reviewedIcon = 'square-o';

                            }

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();
                             $title = '"'.$jobs->title.'"';
                             if($request->status && $request->status=='expired'){

                                return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit - Extend</a>
                        </li>
                         <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li> 
                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>


                    </ul>

                </div>';

                             }else if($jobs->admin_reviewed!==1){
                                return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit - Extend</a>
                        </li>
                         <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li> 
                       


                    </ul>

                </div>';
                 /*<li>
                        <a href="javascript:void(0);" onClick="' . $reviewedHref . '" id="onclickReviewed' . $jobs->id . '"><i class="fa fa-' . $reviewedIcon . '" aria-hidden="true"></i>' . $reviewedTxt . '</a>
                        </li>*/
            }else if($jobs->admin_reviewed == 1 && $jobs->is_active==1){
                return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                            <li>
                            <a href="' . route('admin.list.applied.users', [$jobs->id]) . '"><i class="fa fa-users" aria-hidden="true"></i>List Candidates <span class="badge badge-primary">'. $list_candidates.'</span></a>
                        </li>
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'all']) . '"><i class="fa fa-file" aria-hidden="true"></i>Short Listed Candidates<span class="badge badge-primary">'. $short_listed_candidates.'</span></a>
                        </li>
                       
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'called_for_interview']) . '"><i class="fa fa-lock" aria-hidden="true"></i>Called for Interview<span class="badge badge-primary">'. $interview.'</span></a>
                        </li>
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'interviewed']) . '"><i class="fa fa-check" aria-hidden="true"></i>Interviewed Candidates<span class="badge badge-primary">'. $interviewed.'</span></a>
                        </li>

                       

                                           

                  

                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit - Extend</a>
                        </li>

                        <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li> 
                        

                        <li>
                        <a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $jobs->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
                        </li>

                        <li>
                        <a href="javascript:void(0);" onClick="' . $featuredHref . '" id="onclickFeatured' . $jobs->id . '"><i class="fa fa-' . $featuredIcon . '" aria-hidden="true"></i>' . $featuredTxt . '</a>
                        </li> 
                        
                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>

                    </ul>

                </div>';
            }else{
                return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                            <li>
                            <a href="' . route('admin.list.applied.users', [$jobs->id]) . '"><i class="fa fa-users" aria-hidden="true"></i>List Candidates <span class="badge badge-primary">'. $list_candidates.'</span></a>
                        </li>
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'all']) . '"><i class="fa fa-file" aria-hidden="true"></i>Short Listed Candidates<span class="badge badge-primary">'. $short_listed_candidates.'</span></a>
                        </li>
                       
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'called_for_interview']) . '"><i class="fa fa-lock" aria-hidden="true"></i>Called for Interview<span class="badge badge-primary">'. $interview.'</span></a>
                        </li>
                        <li>
                            <a href="' . route('admin.company.unloced-users', [$jobs->id,'interviewed']) . '"><i class="fa fa-check" aria-hidden="true"></i>Interviewed Candidates<span class="badge badge-primary">'. $interviewed.'</span></a>
                        </li>

                       

                                           

                  

                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit - Extend</a>
                        </li>

                        <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li> 
                        

                        <li>
                        <a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $jobs->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
                        </li>
                        <li>
                        <a href="javascript:void(0);" onClick="' . $reviewedHref . '" id="onclickReviewed' . $jobs->id . '"><i class="fa fa-' . $reviewedIcon . '" aria-hidden="true"></i>' . $reviewedTxt . '</a>
                        </li>

                        <li>
                        <a href="javascript:void(0);" onClick="' . $featuredHref . '" id="onclickFeatured' . $jobs->id . '"><i class="fa fa-' . $featuredIcon . '" aria-hidden="true"></i>' . $featuredTxt . '</a>
                        </li> 
                        
                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>

                    </ul>

                </div>';
            }
                            

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'description','status','checkbox'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }



    public function fetchExpiredJobsData(Request $request)

    {

        $jobs = Job::select([

                    'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','jobs.is_archive',

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('expiry_date') && !empty($request->expiry_date)) {

                                $query->where('jobs.expiry_date', '>', "%{$request->get('expiry_date')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_active') && $request->is_active != -1) {

                                $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }

                            $query->where('expiry_date', '<', Carbon::now());
                            $query->orderBy('jobs.id', "DESC");
                        })

                        ->addColumn('company_id', function ($jobs) {
                            
                            return $jobs->getCompany('name');

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                         ->addColumn('checkbox', function ($jobs) {

                            return '<input class="checkboxes" type="checkbox" id="check_'.$jobs->id.'" name="job_ids[]" autocomplete="off" value="'.$jobs->id.'">';

                        })

                        ->addColumn('expiry_date', function ($jobs) {

                            return date('d-M-Y',strtotime($jobs->expiry_date));

                        })

                        ->addColumn('action', function ($jobs) {

                            /*                             * ************************* */
                            $activeTxt = 'Archive Job';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Archive Job';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }
                          

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();
                             $title = '"'.$jobs->title.'"';
                            return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li>

                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '#expiry_date"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Job or Expiry Date</a>
                        </li>

                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>

                    </ul>

                </div>';

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'expiry_date','checkbox'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }


    public function fetchRejectedJobsData(Request $request)

    {

        $jobs = Job::select([

                    'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','jobs.is_archive','jobs.is_rejected','rejected_reason'

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('rejected_reason') && !empty($request->rejected_reason)) {

                                $query->where('jobs.rejected_reason', 'like', "%{$request->get('rejected_reason')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }

                            $query->where('is_rejected', 1);
                            $query->orderBy('jobs.id', "DESC");
                        })

                        ->addColumn('company_id', function ($jobs) {

                            return $jobs->getCompany('name');

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                         ->addColumn('checkbox', function ($jobs) {

                            return '<input class="checkboxes" type="checkbox" id="check_'.$jobs->id.'" name="job_ids[]" autocomplete="off" value="'.$jobs->id.'">';

                        })

                        ->addColumn('rejected_reason', function ($jobs) {

                            return $jobs->rejected_reason;

                        })

                        ->addColumn('action', function ($jobs) {

                            /*                             * ************************* */
                            $activeTxt = 'Archive Job';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Archive Job';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }
                          

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $reviewedTxt = 'Approve Job';

                            $reviewedHref = 'makeReviewed(' . $jobs->id . ');';

                            $reviewedIcon = 'square-o';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();
                             $title = '"'.$jobs->title.'"';
                            return '

                <div class="btn-group">

                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="' . route('edit.job', ['id' => $jobs->id]) . '#expiry_date"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Job or Expiry Date</a>
                        </li>

                        <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li>

                        <li>
                        <a href="javascript:void(0);" onClick="' . $reviewedHref . '" id="onclickReviewed' . $jobs->id . '"><i class="fa fa-' . $reviewedIcon . '" aria-hidden="true"></i>' . $reviewedTxt . '</a>
                        </li>

                        <li>
                        <a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $jobs->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
                        </li>

                        <li class="deletejb">
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
                        </li>

                    </ul>

                </div>';

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'rejected_reason','checkbox'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }

    public function fetchAbusedJobsData(Request $request)
    {
        // $jobs = Job::with(['abuse_messages'])->get();
        
        $jobs = Job::withCount('abuse_messages')->with(['abuse_messages'])->select([

            'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.reason', 'jobs.request_to_delete', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','jobs.updated_at',

        ])->has('abuse_messages');

        return Datatables::of($jobs)

                ->filter(function ($query) use ($request) {

                    if ($request->has('company_id') && !empty($request->company_id)) {

                        $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                    }

                    if ($request->has('title') && !empty($request->title)) {

                        $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                    }

                    if ($request->has('description') && !empty($request->description)) {

                        $query->where('jobs.reason', 'like', "%{$request->get('description')}%");

                    }

                    if ($request->has('country_id') && !empty($request->country_id)) {

                        $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                    }

                    if ($request->has('state_id') && !empty($request->state_id)) {

                        $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                    }

                    if ($request->has('city_id') && !empty($request->city_id)) {

                        $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                    }

                    if ($request->has('is_active') && $request->is_active != -1) {

                        $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                    }

                    if ($request->has('is_featured') && $request->is_featured != -1) {

                        $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                    }
                    // $query->whereRaw("SELECT COUNT(*) FROM " . env('DB_PREFIX') . "report_abuse_messages WHERE id = ");
                    $query->orderBy('jobs.updated_at', "DESC");

                })

                ->addColumn('company_id', function ($jobs) {

                    return $jobs->getCompany('name');

                })

                ->addColumn('updated_at', function ($jobs) {

                    return date('d-M-Y',strtotime($jobs->updated_at));

                })

                ->addColumn('city_id', function ($jobs) {

                    return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                })

                ->addColumn('description', function ($jobs) {

                    return strip_tags(\Illuminate\Support\Str::limit($jobs->reason, 50, '...'));

                })

                ->addColumn('reports', function ($jobs) {
                    // dd($jobs->toArray());

                    $html = '
                    <a href="#reportModal'.$jobs->id.'" data-toggle="modal">Reports ('.$jobs->abuse_messages->count().')</a>
                    <div class="modal fade" id="reportModal'.$jobs->id.'" tabindex="-1" role="dialog" aria-labelledby="reportModal'.$jobs->id.'Label" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="reportModal'.$jobs->id.'Label">Reported By</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="table-repsonsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Reason</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                        
                        foreach ($jobs->abuse_messages as $key => $a_msg) {
                            if(!empty($a_msg->user)) {
                                $name = $a_msg->user->name;
                                $email = $a_msg->user->email;
                                $role = "Jobseeker";
                            } else {
                                $name = $a_msg->company->name;
                                $email = $a_msg->company->email;
                                $role = "Company";
                            }

                            $html .= '<tr>
                                <td>'.($key + 1).'</td>
                                <td>'.$name.'</td>
                                <td>'.$email.'</td>
                                <td>'.$role.'</td>
                                <td>'.$a_msg->reason.'</td>
                            </tr>';
                        }            

                        $html .= '</tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                ';

                    return $html;

                })

                 ->addColumn('checkbox', function ($jobs) {

                    return '<input class="checkboxes" type="checkbox" id="check_'.$jobs->id.'" name="job_ids[]" autocomplete="off" value="'.$jobs->id.'">';

                })

                ->addColumn('action', function ($jobs) {

                    /*                             * ************************* */

                    $activeTxt = 'Make Active';

                    $activeHref = 'makeActive(' . $jobs->id . ');';

                    $activeIcon = 'square-o';

                    if ((int) $jobs->is_active == 1) {

                        $activeTxt = 'Make InActive';

                        $activeHref = 'makeNotActive(' . $jobs->id . ');';

                        $activeIcon = 'square-o';

                    }

                    $featuredTxt = 'Make Featured';

                    $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                    $featuredIcon = 'square-o';

                    if ((int) $jobs->is_featured == 1) {

                        $featuredTxt = 'Make Not Featured';

                        $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                        $featuredIcon = 'square-o';

                    }
                    
                     $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                     $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                     $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                     $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                     $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                     $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                     $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();

                     $title = '"'.$jobs->title.'"';

                    return '

        <div class="btn-group">

            <button class="btn red dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                <i class="fa fa-angle-down"></i>

            </button>

            <ul class="dropdown-menu">
                  
               
                

          
                 <li>
                    <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                </li>
               
                <li>
                    <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete Job</a>
                </li>


            </ul>

        </div>';

                })

                ->rawColumns(['action', 'company_id', 'city_id', 'description','checkbox', 'reports'])

                ->setRowId(function($jobs) {

                    return 'jobDtRow' . $jobs->id;

                })

                ->make(true);
    }


    public function fetchDelJobsData(Request $request)

    {

        $jobs = Job::select([

            'jobs.id', 'jobs.company_id', 'jobs.title', 'jobs.reason', 'jobs.request_to_delete', 'jobs.description', 'jobs.country_id', 'jobs.state_id', 'jobs.city_id', 'jobs.is_freelance', 'jobs.career_level_id', 'jobs.salary_from', 'jobs.salary_to', 'jobs.hide_salary', 'jobs.functional_area_id', 'jobs.job_type_id', 'jobs.job_shift_id', 'jobs.num_of_positions', 'jobs.gender_id', 'jobs.expiry_date', 'jobs.degree_level_id', 'jobs.job_experience_id', 'jobs.is_active', 'jobs.is_featured','jobs.updated_at',

        ]);

        return Datatables::of($jobs)

                        ->filter(function ($query) use ($request) {

                            if ($request->has('company_id') && !empty($request->company_id)) {

                                $query->where('jobs.company_id', '=', "{$request->get('company_id')}");

                            }

                            if ($request->has('title') && !empty($request->title)) {

                                $query->where('jobs.title', 'like', "%{$request->get('title')}%");

                            }

                            if ($request->has('description') && !empty($request->description)) {

                                $query->where('jobs.reason', 'like', "%{$request->get('description')}%");

                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {

                                $query->where('jobs.country_id', '=', "{$request->get('country_id')}");

                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {

                                $query->where('jobs.state_id', '=', "{$request->get('state_id')}");

                            }

                            if ($request->has('city_id') && !empty($request->city_id)) {

                                $query->where('jobs.city_id', '=', "{$request->get('city_id')}");

                            }

                            if ($request->has('is_active') && $request->is_active != -1) {

                                $query->where('jobs.is_active', '=', "{$request->get('is_active')}");

                            }

                            if ($request->has('is_featured') && $request->is_featured != -1) {

                                $query->where('jobs.is_featured', '=', "{$request->get('is_featured')}");

                            }
                            $query->where('jobs.request_to_delete', '=', "yes");
                            $query->orderBy('jobs.updated_at', "DESC");

                        })

                        ->addColumn('company_id', function ($jobs) {

                            return $jobs->getCompany('name');

                        })

                        ->addColumn('reason', function ($jobs) {

                            return $jobs->reason;

                        })

                        ->addColumn('updated_at', function ($jobs) {

                            return date('d-M-Y',strtotime($jobs->updated_at));

                        })

                        ->addColumn('city_id', function ($jobs) {

                            return $jobs->getCity('city') . '(' . $jobs->getState('state') . '-' . $jobs->getCountry('country') . ')';

                        })

                        ->addColumn('description', function ($jobs) {

                            return strip_tags(\Illuminate\Support\Str::limit($jobs->reason, 50, '...'));

                        })

                         ->addColumn('checkbox', function ($jobs) {

                            return '<input class="checkboxes" type="checkbox" id="check_'.$jobs->id.'" name="job_ids[]" autocomplete="off" value="'.$jobs->id.'">';

                        })

                        ->addColumn('action', function ($jobs) {

                            /*                             * ************************* */

                            $activeTxt = 'Make Active';

                            $activeHref = 'makeActive(' . $jobs->id . ');';

                            $activeIcon = 'square-o';

                            if ((int) $jobs->is_active == 1) {

                                $activeTxt = 'Make InActive';

                                $activeHref = 'makeNotActive(' . $jobs->id . ');';

                                $activeIcon = 'square-o';

                            }

                            $featuredTxt = 'Make Featured';

                            $featuredHref = 'makeFeatured(' . $jobs->id . ');';

                            $featuredIcon = 'square-o';

                            if ((int) $jobs->is_featured == 1) {

                                $featuredTxt = 'Make Not Featured';

                                $featuredHref = 'makeNotFeatured(' . $jobs->id . ');';

                                $featuredIcon = 'square-o';

                            }
                            
                             $list_candidates = JobApply::where('job_id', '=', $jobs->id)->count();
                             $short_listed_candidates = Unlocked_users::where('job_id', '=', $jobs->id)->count();
                             $rejected = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','rejected')->count();
                             $interview = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','called_for_interview')->count();
                             $interviewed = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','interviewed')->count();
                             $recruited = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','recruited')->count();
                             $resigned = Unlocked_users::where('job_id', '=', $jobs->id)->where('status','resigned')->count();

                             $title = '"'.$jobs->title.'"';

                            return '

                <div class="btn-group">

                    <button class="btn red dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

                        <i class="fa fa-angle-down"></i>

                    </button>

                    <ul class="dropdown-menu">
                          
                       
                        

                  
                         <li>
                            <a href="' . route('public.job', ['id' => $jobs->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>View Job Details</a>
                        </li>
                       
                        <li>
                            <a href="javascript:void(0);" data-job='.$title.' id=job-'.$jobs->id.'  onclick="deleteJob(' . $jobs->id . ', ' . $jobs->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete Job</a>
                        </li>
      

                    </ul>

                </div>';

                        })

                        ->rawColumns(['action', 'company_id', 'city_id', 'description','checkbox'])

                        ->setRowId(function($jobs) {

                            return 'jobDtRow' . $jobs->id;

                        })

                        ->make(true);

        //$query = $dataTable->getQuery()->get();

        //return $query;

    }




    public function makeActiveJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 1;
            $job->is_archive = 1;
            $job->admin_reviewed = 1;
            $job->is_rejected = 0;
            $job->request_to_delete  = 'no';

            $job->status = 'active';

            $job->blocked_by = '';

            $job->update();

            echo 'ok';

            $contactCompany = Company::findOrFail($job->company_id);
            $data['job'] = $job;
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] =  'Job Vacancy ['; // 'Job Vacancy Activated  on '.date('d-M-Y',strtotime($job->updated_at));
            $data['msg2'] = '] Activated on ' . date('d-M-Y',strtotime($job->updated_at)); // 'is now live and can be reviewed on the link below';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'Approved';
            $data['vacancy_status'] = 'Job Vacancy Activated by Admin on '.date('d-M-Y',strtotime($job->updated_at)).' Now Live';
            Mail::send(new RecruitedJobMail($data));


        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }

    public function makeReviewedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 1;
            $job->is_archive = 1;
            $job->admin_reviewed = 1;
            $job->is_rejected = 0;
            $job->is_updated = 0;
            $job->status = 'active';

            $job->update();

            echo 'ok';

            $contactCompany = Company::findOrFail($job->company_id);
            $data['job'] = $job;
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] = 'Job Vacancy Now Live on';
            $data['msg2'] = 'is now live and can be reviewed on the link below';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'Approved';
            $data['vacancy_status'] = 'Job Vacancy Reviewed by Admin on '.date('d-M-Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }

    public function makeNotReviewedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 0;
            $job->is_archive = 0;
            $job->admin_reviewed = 0;
            $job->is_rejected = 1;
            $job->rejected_reason = $request->input('reason');
            $job->status = 'rejected';

            $job->update();

            echo 'ok';

            $contactCompany = Company::findOrFail($job->company_id);
            $data['job'] = $job;
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] = 'Job Vacancy ['; // 'Job Vacancy On-Hold';
            $data['msg2'] = '] on hold, a member of our team will contact you shortly to discuss the reason job has been put On-Hold.'; // 'is On-Hold, a member of our team will contact you shortly to discuss the reason for being placed On-Hold';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'On Hold';
            $data['vacancy_status'] = 'Job Vacancy Rejected by Admin on '.date('d-M-Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }



    public function makeNotActiveJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_active = 0;

            $job->is_archive = 0;

            $job->admin_reviewed = 1;

            $job->blocked_by = Auth::guard('admin')->user()->name;

            $job->reason = $request->deactivation_reason;

            $job->status = 'blocked';


            $job->update();

            echo 'ok';
            $contactCompany = Company::findOrFail($job->company_id);
            $data['job'] = $job;
            $data['id'] = $job->id;
            $data['title'] = $job->title;
            $data['slug'] = $job->slug;
            $data['updated_at'] = $job->updated_at;
            $data['msg'] = 'Job Vacancy Deactivated by Admin: ';
            $data['msg2'] = ' Deactivated and can not viewed on listing';
            $data['contact_name'] = $contactCompany->name;
            $data['contact_email'] = $contactCompany->email;
            $data['subject'] = 'Deactivated';
            $data['contact_phone'] = $contactCompany->phone;
            $data['contact_url'] = route('company.detail',$contactCompany->slug);
            $data['subject'] = 'Deactivated';
            $data['vacancy_status'] = 'Job Vacancy Deactivated by Admin: '.date('d-M-Y',strtotime($job->updated_at));
            Mail::send(new RecruitedJobMail($data));

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }

    public function deleteJobs(Request $request)

    {

        $ids = $request->input('ids');
        $arr = explode(',', $ids);
        $user = Job::whereIn('id',$arr)->delete();
        echo 'done';

    }

    public function makeFeaturedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_featured = 1;

            $job->update();

            echo 'ok';

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }



    public function makeNotFeaturedJob(Request $request)

    {

        $id = $request->input('id');

        try {

            $job = Job::findOrFail($id);

            $job->is_featured = 0;

            $job->update();

            echo 'ok';

        } catch (ModelNotFoundException $e) {

            echo 'notok';

        }

    }

    public function cron(){
        $jobs = Job::get();
        // echo "<pre>";print_r($job);exit;

        foreach ($jobs as $key => $value) {
            // echo "<pre>";print_r($value->job_skill);exit();
            $job = Job::findOrFail($value->id);
            /*$job->meta_title = \Illuminate\Support\Str::limit(strip_tags(htmlspecialchars_decode($job->title.' in '. $job->getLocation())), 65,'');
            $job->meta_description = \Illuminate\Support\Str::limit(strip_tags(htmlspecialchars_decode($value->description)), 165,'');
            $job->meta_keywords = $value->job_skill;*/

            //cron for addding space into skills 
            $val = explode(', ', $value->job_skill);
            $data = implode(",", $val);
            $job->job_skill = $data;

            $job->update();
        }
        echo "<pre>";print_r('ok');exit;
    }


}

