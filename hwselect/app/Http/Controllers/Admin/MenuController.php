<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Menu;
use Image;

class MenuController extends Base
{
    

        protected $layout = 'layout';

	public function getIndex()
	{	
		$items= Menu::orderBy('order')->get();
                $data['items']=$items;
		$menu= new Menu;
		$data['menu']= $menu->getHTML($items);
                
                return view('admin/menu/index')->with($data);
	}

	public function getEdit($id)
	{	
		$item = Menu::findOrFail($id);
		$json_data = json_encode($item);
                echo $json_data;
                return;
	}

	public function postEdit(Request $request)
	{	
             $this->validate($request,[
    			'title_edit' => 'required',
    			'label_edit' => 'required',
    			'url_edit' => 'required',
    		],[
    			'title_edit.required' => ' The title field is required.',
    			'label_edit.required' => ' The lable field is required.',
    			'url_edit.required' => ' The Url field is required.',
    		]);
		$item = Menu::find($request->id);
		$item->title 	= $request->title_edit;
		$item->label 	= $request->label_edit;	
		$item->url 	= $request->url_edit;	

		$item->update();
		if($item->update()==true){
                $request->session()->flash('message.updated', 'success');
                $request->session()->flash('message.content', 'Menu was successfully updated!');
              }else{
                 $request->session()->flash('message.updated', 'danger');
                 $request->session()->flash('message.content', 'Error!');
            }
                return redirect('admin/menu'); 
	}

	// AJAX Reordering function
	public function post_index(Request $request)
	{         
	    $source       =$request->source;
	    
            if($request->destination!=''){
              $destination  =$request->destination;  
            }else{
                $destination=0;
            }
	    $item             = Menu::find($source);
	    $item->parent_id  = $destination;  
	    $item->save();

	    $ordering       = json_decode($request->order);
	    $rootOrdering   = json_decode($request->rootOrder );

	    if($ordering){
	      foreach($ordering as $order=>$item_id){
	        if($itemToOrder = Menu::find($item_id)){
	            $itemToOrder->order = $order;
	            $itemToOrder->save();
	        }
	      }
	    } else {
	      foreach($rootOrdering as $order=>$item_id){
	        if($itemToOrder = Menu::find($item_id)){
	            $itemToOrder->order = $order;
	            $itemToOrder->save();
	        }
	      }
	    }

	    return 'ok ';
	}
        
	public function postNew(Request $request)
	{
		// Create a new menu item and save it
                
		$item = new Menu;

		$item->title 	=$request->title;
		$item->label 	= $request->label;	
		$item->url 	=$request->url;	
		$item->parent_id =0;	
		$item->order 	= Menu::max('order')+1;
                $item->is_external 	= 'Y';

		$item->save();
                if($item->save()==true){
                $request->session()->flash('message.updated', 'success');
                $request->session()->flash('message.content', 'Menu was added Successfully!');
              }else{
                 $request->session()->flash('message.updated', 'danger');
                 $request->session()->flash('message.content', 'Error!');
            }
                return redirect('admin/menu'); 
	}

    public function destroy($id) 
    {
        $post = Menu::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }

    
    
}
