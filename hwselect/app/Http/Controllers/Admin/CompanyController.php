<?php

namespace App\Http\Controllers\Admin;

use Hash;
use File;
use ImgUploader;
use Auth;
use DB;
use Input;
use Form;
use Redirect;
use App\Package;
use App\Company;
use App\Unlocked_users;
use App\Job;
use App\User;
use App\JobApply;
use App\Country;
use App\State;
use App\City;
use App\Industry;
use App\OwnershipType;
use App\Action_on_shortlisted_candidated;
use Carbon\Carbon;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\CompanyFormRequest;
use App\Http\Controllers\Controller;
use App\Traits\CompanyTrait;
use App\Traits\CompanyPackageTrait;
use Illuminate\Support\Str;

class CompanyController extends Controller
{

    use CompanyTrait;
    use CompanyPackageTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexCompanies()
    {
        $companies = DataArrayHelper::companiesArray();
        return view('admin.company.index')->with('companies', $companies);
    }

    public function createCompany()
    {
        $countries = DataArrayHelper::defaultCountriesArray();
        $industries = DataArrayHelper::defaultIndustriesArray();
        $ownershipTypes = DataArrayHelper::defaultOwnershipTypesArray();
        $packages = Package::select('id', DB::raw("CONCAT(`package_title`, ', $', `package_price`, ', Days:', `package_num_days`, ', Listings:', `package_num_listings`) AS package_detail"))->where('package_for', 'like', 'employer')->pluck('package_detail', 'id')->toArray();

        return view('admin.company.add')
                        ->with('countries', $countries)
                        ->with('industries', $industries)
                        ->with('ownershipTypes', $ownershipTypes)
                        ->with('packages', $packages);
    }
    
    public function storeCompany(CompanyFormRequest $request)
    {
        $company = new Company();
        /*         * **************************************** */
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $nameonly = preg_replace('/\..+$/', '', $request->logo->getClientOriginalName());
            $fileName = ImgUploader::UploadImage('company_logos', $image, $nameonly, 300, 300, true);
            $company->logo = $fileName;
        }
        /*         * ************************************** */
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        if (!empty($request->input('password'))) {
            $company->password = Hash::make($request->input('password'));
        }
        $company->ceo = $request->input('ceo');
        $company->phone_code = $request->input('phone_code');
        $company->ceo_number = $request->input('ceo_number');
        $company->ceo_2 = $request->input('ceo_2');
        $company->ceo_number_2 = $request->input('ceo_number_2');
        $company->industry_id = $request->input('industry_id');
        $company->ownership_type_id = $request->input('ownership_type_id');
        $company->description = $request->input('description');
        $company->location = $request->input('location');
        $company->map = $request->input('map');
        $company->no_of_offices = $request->input('no_of_offices');
        $website = $request->input('website');
        $company->website = (false === strpos($website, 'http')) ? 'http://' . $website : $website;
        $company->no_of_employees = $request->input('no_of_employees');
        $company->established_in = $request->input('established_in');
        $company->fax = $request->input('fax');
        $company->phone = $request->input('phone');
        $company->facebook = $request->input('facebook');
        $company->twitter = $request->input('twitter');
        $company->linkedin = $request->input('linkedin');
        $company->google_plus = $request->input('google_plus');
        $company->pinterest = $request->input('pinterest');
        $company->country_id = $request->input('country_id');
        $company->state_id = $request->input('state_id');
        $company->city_id = $request->input('city_id');
        $company->is_active = $request->input('is_active');
        $company->is_featured = $request->input('is_featured');
        $company->two_step_verification = $request->input('two_step_verification');
        $company->save();
        /*         * ******************************* */
        $company->slug = Str::slug($company->name, '-') . '-' . $company->id;
        /*         * ******************************* */
        $company->update();
        /*         * ************************************ */
        if ($request->has('company_package_id') && $request->input('company_package_id') > 0) {
            $package_id = $request->input('company_package_id');
            $package = Package::find($package_id);
            $this->addCompanyPackage($company, $package);
        }
        /*         * ************************************ */
        flash('Company Added Successfully!')->success();
        return \Redirect::route('edit.company', array($company->id));
    }

    public function editCompany($id)
    {
        $countries = DataArrayHelper::defaultCountriesArray();
        $industries = DataArrayHelper::defaultIndustriesArray();
        $ownershipTypes = DataArrayHelper::defaultOwnershipTypesArray();

        $company = Company::findOrFail($id);
        if ($company->package_id > 0) {
            $package = Package::find($company->package_id);
            $packages = Package::select('id', DB::raw("CONCAT(`package_title`, ', $', `package_price`, ', Days:', `package_num_days`, ', Listings:', `package_num_listings`) AS package_detail"))->where('package_for', 'like', 'employer')->where('id', '<>', $company->package_id)->where('package_price', '>=', $package->package_price)->pluck('package_detail', 'id')->toArray();
        } else {
            $packages = Package::select('id', DB::raw("CONCAT(`package_title`, ', $', `package_price`, ', Days:', `package_num_days`, ', Listings:', `package_num_listings`) AS package_detail"))->where('package_for', 'like', 'employer')->pluck('package_detail', 'id')->toArray();
        }

        return view('admin.company.edit')
                        ->with('company', $company)
                        ->with('countries', $countries)
                        ->with('industries', $industries)
                        ->with('ownershipTypes', $ownershipTypes)
                        ->with('packages', $packages);
    }

    public function updateCompany($id, CompanyFormRequest $request)
    {
        $company = Company::findOrFail($id);
        /*         * **************************************** */
        if ($request->hasFile('logo')) {
            $is_deleted = $this->deleteCompanyLogo($company->id);
            $image = $request->file('logo');
            $nameonly = preg_replace('/\..+$/', '', $request->logo->getClientOriginalName());
            $fileName = ImgUploader::UploadImage('company_logos', $image, $nameonly, 300, 300, true);
            $company->logo = $fileName;
        }
        /*         * ************************************** */
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        if (!empty($request->input('password'))) {
            $company->password = Hash::make($request->input('password'));
        }
        $company->ceo = $request->input('ceo');
        $company->ceo_number = $request->input('ceo_number');
        $company->ceo_2 = $request->input('ceo_2');
        $company->ceo_number_2 = $request->input('ceo_number_2');
        $company->industry_id = $request->input('industry_id');
        $company->ownership_type_id = $request->input('ownership_type_id');
        $company->description = $request->input('description');
        $company->location = $request->input('location');
        $company->map = $request->input('map');
        $company->no_of_offices = $request->input('no_of_offices');
        $website = $request->input('website');
        $company->website = (false === strpos($website, 'http')) ? 'http://' . $website : $website;
        $company->no_of_employees = $request->input('no_of_employees');
        $company->established_in = $request->input('established_in');
        $company->fax = $request->input('fax');
        $company->phone = $request->input('phone');
        $company->facebook = $request->input('facebook');
        $company->twitter = $request->input('twitter');
        $company->linkedin = $request->input('linkedin');
        $company->google_plus = $request->input('google_plus');
        $company->pinterest = $request->input('pinterest');
        $company->country_id = $request->input('country_id');
        $company->state_id = $request->input('state_id');
        $company->city_id = $request->input('city_id');
        $company->is_active = $request->input('is_active');
        $company->is_featured = $request->input('is_featured');
        $company->two_step_verification = $request->input('two_step_verification');

        $company->slug = Str::slug($company->name, '-') . '-' . $company->id;
        $company->update();

        /*         * ************************************ */
        if ($request->has('company_package_id') && $request->input('company_package_id') > 0) {
            $package_id = $request->input('company_package_id');
            $package = Package::find($package_id);
            if ($company->package_id > 0) {
                $this->updateCompanyPackage($company, $package);
            } else {
                $this->addCompanyPackage($company, $package);
            }
        }
        /*         * ************************************ */
        flash('Company Updated Successfully!')->success();
        return \Redirect::route('edit.company', array($company->id));
    }

    public function deleteCompany(Request $request)
    {
        $id = $request->input('id');
        try {
        	Job::where('company_id',$id)->delete();
            $company = Company::findOrFail($id);
            $this->deleteCompanyLogo($company->id);
            $company->delete();
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    public function fetchCompaniesData(Request $request)
    {
        $companies = Company::select([
                    'companies.id',
                    'companies.name',
                    'companies.email',
                    'companies.password',
                    'companies.ceo',
                    'companies.industry_id',
                    'companies.ownership_type_id',
                    'companies.description',
                    'companies.location',
                    'companies.no_of_offices',
                    'companies.website',
                    'companies.no_of_employees',
                    'companies.established_in',
                    'companies.fax',
                    'companies.phone',
                    'companies.logo',
                    'companies.country_id',
                    'companies.state_id',
                    'companies.city_id',
                    'companies.is_active',
                    'companies.is_featured',
                    'companies.created_at',
        ]);
        return Datatables::of($companies)
                        ->filter(function ($query) use ($request) {
                            if ($request->has('name') && !empty($request->name)) {
                                $query->where('companies.id',$request->get('name'));
                            }
                            if ($request->has('email') && !empty($request->email)) {
                                $query->where('companies.email', 'like', "%{$request->get('email')}%");
                            }

                            if ($request->has('date_from') && !empty($request->date_from) && $request->has('date_to') && !empty($request->date_to)) {
                                $date_from = date('Y-m-d H:i:s',strtotime($request->date_from));
                                $date_to = date('Y-m-d H:i:s',strtotime($request->date_to));
                                $query->where('created_at', '>', $date_from)->where('created_at', '<', $date_to);

                            }
                            if ($request->has('is_active') && $request->is_active != -1) {
                                $query->where('companies.is_active', '=', "{$request->get('is_active')}");
                            }
                            if ($request->has('is_featured') && $request->is_featured != -1) {
                                $query->where('companies.is_featured', '=', "{$request->get('is_featured')}");
                            }

                            $query->orderBy('id',"DESC");
                        })
                        ->addColumn('is_active', function ($companies) {
                            return ((bool) $companies->is_active) ? 'Yes' : 'No';
                        })
                        
                        ->addColumn('created_at', function ($companies) {

                            return date('d-M-Y', strtotime($companies->created_at));

                        })
                        ->addColumn('is_featured', function ($companies) {
                            return ((bool) $companies->is_featured) ? 'Yes' : 'No';
                        })

                        ->addColumn('checkbox', function ($companies) {

                            return '<input class="checkboxes" type="checkbox" id="check_'.$companies->id.'" name="company_ids[]" autocomplete="off" value="'.$companies->id.'">';

                        })

                        ->addColumn('action', function ($companies) {
                            /*                             * ************************* */
                            $activeTxt = 'Make Active';
                            $activeHref = 'makeActive(' . $companies->id . ');';
                            $activeIcon = 'square-o';
                            if ((int) $companies->is_active == 1) {
                                $activeTxt = 'Make InActive';
                                $activeHref = 'makeNotActive(' . $companies->id . ');';
                                $activeIcon = 'square-o';
                            }
                            /*                             * ************************* */
                            $featuredTxt = 'Make Featured';
                            $featuredHref = 'makeFeatured(' . $companies->id . ');';
                            $featuredIcon = 'square-o';
                            if ((int) $companies->is_featured == 1) {
                                $featuredTxt = 'Make Not Featured';
                                $featuredHref = 'makeNotFeatured(' . $companies->id . ');';
                                $featuredIcon = 'square-o';

                            }

                            $company_name = "'".$companies->name."'";
                            return '
				<div class="btn-group">
					<button class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu">
						<li>
							<a href="' . route('list.jobs', ['company_id' => $companies->id]) . '" target="_blank"><i class="fa fa-list" aria-hidden="true"></i>List all Jobs</a>
						</li>
                        <li>
                            <a href="' . route('list.jobs', ['company_id' => $companies->id,'is_active=0']) . '" target="_blank"><i class="fa fa-list" aria-hidden="true"></i>View Archived Jobs</a>
                        </li>
						<li>
                            <a href="' . route('public.company', ['id' => $companies->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>View Company Details</a>
                        </li> 

						<li>
							<a href="' . route('edit.company', ['id' => $companies->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
						</li>	


						<li>
							<a href="javascript:void(0);" onclick="deleteCompany(' . $companies->id . ','.$company_name.');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
						</li>
						
<li><a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $companies->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a></li>
						
<li style="display:none;"><a href="javascript:void(0);" onClick="' . $featuredHref . '" id="onclickFeatured' . $companies->id . '"><i class="fa fa-' . $featuredIcon . '" aria-hidden="true"></i>' . $featuredTxt . '</a></li>
					</ul>
				</div>';
                        })
                        ->rawColumns(['action', 'is_active', 'is_featured','checkbox'])
                        ->setRowId(function($companies) {
                            return 'companyDtRow' . $companies->id;
                        })
                        ->make(true);
        //$query = $dataTable->getQuery()->get();
        //return $query;
    }

    public function makeActiveCompany(Request $request)
    {
        $id = $request->input('id');
        try {
            $company = Company::findOrFail($id);
            $company->is_active = 1;
            $company->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeNotActiveCompany(Request $request)
    {
        $id = $request->input('id');
        try {
            $company = Company::findOrFail($id);
            $company->is_active = 0;
            $company->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeFeaturedCompany(Request $request)
    {
        $id = $request->input('id');
        try {
            $company = Company::findOrFail($id);
            $company->is_featured = 1;
            $company->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeNotFeaturedCompany(Request $request)
    {
        $id = $request->input('id');
        try {
            $company = Company::findOrFail($id);
            $company->is_featured = 0;
            $company->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function register(Request $request)

    {

        $company = new Company();

        $company->name = $request->input('name');

        $company->email = $request->input('email');

        $company->phone = $request->input('phone');

        $company->password = bcrypt($request->input('password'));

        $company->is_active = 1;

        $company->verified = 1;

        $company->save();

        /*         * ******************** */

        $company->slug = Str::slug($company->name, '-') . '-' . $company->id;

        $company->update();

        /*         * ******************** */

        $companies = DataArrayHelper::companiesArray();

        $dd = Form::select('company_id', ['' => __('Select Company')] + $companies, $company->id, array('id' => 'company_id', 'class' => 'form-control company_id'));

        echo $dd;

    }

     public function unlock(Request $request)

    {

        

        $user_id = $request->user_id;

        $job_ids = $request->job_ids;

        $contact_detail = $request->contact_detail;
        $abc = trim($job_ids,',');
        $array = explode(',',$abc);

        if(null!==($array)){
            $count = 1;
            $arr = array();
            $flag = false;
            $jobs_comma = array();
            foreach ($array as $key => $value) {

               $check = Unlocked_users::where('job_id',$value)->where('unlocked_users_ids',$user_id)->first(); 
               //dd($value);
               $job = Job::findOrFail($value); 
               $job_id = $job->id;
               $cvsSearch = Company::findOrFail($job->company_id);

               $company_id = $job->company_id;
                if(!$check){
                    if (null !== ($cvsSearch)) {

                    $unlock = Unlocked_users::where('company_id', $company_id)->where('job_id',$job_id)->where('unlocked_users_ids',$user_id)->first();

                    if (null !== ($unlock)) {

                         if ($cvsSearch->availed_cvs_ids != '') {

                            $newString = $this->removetoString($cvsSearch->availed_cvs_ids, $user_id); 

                            $cvsSearch->availed_cvs_ids  = $newString;

                            $cvsSearch->availed_cvs_quota -= 1;

                            $cvsSearch->update();

                        } 



                        $dd = Unlocked_users::findOrFail($unlock->id);

                        $dd->delete();

                        $flag = false;



                    }else{

                        if ($cvsSearch->availed_cvs_ids != '') {



                            $newString = $this->addtoString($cvsSearch->availed_cvs_ids, $user_id);

                        } else {

                            $newString = $user_id;

                        }



                        $cvsSearch->availed_cvs_ids  = $newString;

                        $cvsSearch->availed_cvs_quota += 1;

                        $cvsSearch->update();

                        $unlock = new Unlocked_users();

                        $unlock->company_id  = $company_id;

                        $unlock->job_id  = $job_id;

                        $unlock->unlocked_users_ids  = $user_id;

                        $unlock->short_listed_by  = 'regular';
                        $unlock->contact_detail  = $contact_detail;

                        $unlock->save();


                        $applicant = JobApply::where('user_id',$user_id)->where('job_id',$job_id)->first();
                        if(null!==($applicant)){
                            $applicant->delete();
                        }
                        
                        $job = Job::where('id',$job_id)->first();

                        $jobs_comma[] = $job->title;

                        $flag = true;

                    }







                    

                } 
                }
                $count++;
                 
            }
            if($flag==true){
                $arr['msg'] = 'You have shortlisted this candidate against Job(s): '.implode(', ', $jobs_comma);
                $arr['flag'] = 'unlocked';
            }else{
                $arr['msg'] = 'You have successully locked this profile';
                $arr['flag'] = 'locked';
            }

            return json_encode($arr);
        }

        

    }
public function unlocked_users($job_id,$status)

    {

        $data = array();
        $job = Job::findOrFail($job_id);
        if($status=='all'){
            $unlocked_users = Unlocked_users::where('job_id', '=',$job_id)->get();
        }else{
            $unlocked_users = Unlocked_users::where('job_id',$job_id)->where('status',$status)->get();
        }
        
        //dd($unlocked_users);
        $ids = array();

        if (null !== ($unlocked_users)) {

            foreach ($unlocked_users as $key => $value) {

                 $ids[] = $value->unlocked_users_ids;

            }

            $data['users'] = User::whereIn('id', $ids)->get();

            $data['job_id'] = $job_id;

            $data['status'] = $status;

        }



        return view('admin.company.unlocked_users')->with($data);

    }

    public function show_hide(Request $request)
    {

        $user = User::findOrFail($request->user_id);
        $user->contact_detail = $request->contact_detail;
        $user->update();

    }

    function submitnew_action_seeker(Request $request)

    {

        $this->validate($request, [

            'message' => 'required',

        ], [

            'message.required' => 'Message is required.',

        ]);
        $company = Job::findOrFail($request->job_id);
        $message = new Action_on_shortlisted_candidated();

        $message->company_id =$company->company_id;

        $message->date = $request->date;

        $message->job_id = $request->job_id;

        $message->action = $request->option;

        $message->note = $request->message;

        $message->user_id = $request->seeker_id;

        

        $message->save();



        $add = Unlocked_users::where('company_id',$company->company_id)->where('job_id',$request->job_id)->where('unlocked_users_ids',$request->seeker_id)->first();

        //dd($add);

        $add->status = $request->option;

        $add->update();



        if ($message->save() == true) {

            $arr = array('msg' => 'Your Note have successfully been posted. ', 'status' => true);

        }

        return Response()->json($arr);

    }





    function view_action_seeker(Request $request)

    {
        $company = Job::findOrFail($request->job_id);
        $messages = Action_on_shortlisted_candidated::where('company_id',$company->company_id)->where('user_id',$request->id)->orderBy('id', 'DESC')->get();

        $search = view("company.view_notes", compact('messages'))->render();

        return $search;

    }

    function view_notes(Request $request)

    {
        $messages = Action_on_shortlisted_candidated::where('user_id',$request->id)->get();

        $search = view("company.view_all_notes", compact('messages'))->render();

        return $search;

    }

    public function listAppliedUsers(Request $request, $job_id)

    {

        $job_applications = JobApply::where('job_id', '=', $job_id)->get();
        
        $job = Job::findorFail($job_id);



        return view('admin.job.job_applications')

                        ->with('job_applications', $job_applications)
                        ->with('job_id', $job->id)
                        ->with('company_id', $job->company_id);

    }

    function addtoString($str, $item)

    {

        $parts = explode(',', $str);

        $parts[] = $item;



        return implode(',', $parts);

    }

     public function deleteCompanies(Request $request)

    {

        $ids = $request->input('ids');
        $arr = explode(',', $ids);
        $user = Company::whereIn('id',$arr)->delete();
        echo 'done';

    }


}
