<?php



namespace App\Http\Controllers\Admin;



use App\User;

use App\Job;
use App\Company;
use App\JobApply;
use App\Seo;
use App\ContactMessage;
use App\ReportAbuseMessage;

use Carbon\Carbon;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()
    {

        //$this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $today = Carbon::now();

        $totalActiveUsers = User::where('is_active', 1)->count();

        $totalVerifiedUsers = User::where('verified', 1)->count();
        $totalVerifiedCompanies = Company::where('verified', 1)->count();
        $totalCompanies = Company::count();

        $totalTodaysUsers = User::count();

        $recentUsers = User::orderBy('id', 'DESC')->take(25)->get();
        $recentCompanies = Company::orderBy('id', 'DESC')->take(25)->get();

        $totalDeleteJobs = Job::where('jobs.request_to_delete', '=', "yes")->count();

        $totalActiveJobs = Job::notExpire()->active()->count();

        $totalPendingJobs = Job::where('is_active', 0)->where('admin_reviewed', 0)->count();

        $totalRejectedJobs = Job::where('is_active', 0)->where('is_archive', 0)->where('admin_reviewed', 0)->where('is_rejected', 1)->count();

        $totalAbusedJobs = ReportAbuseMessage::query()->count();

        $totalFeaturedJobs = Job::where('is_featured', 1)->count();

        $totalJobs = Job::count();

        $totalTodaysJobs = Job::where('created_at', 'like', $today->toDateString() . '%')->count();
        $totalAppliedUsers = JobApply::where('created_at', 'like', $today->toDateString() . '%')->count();

        $totalJobseekerEnquiries = ContactMessage::where('type', 'Job Seeker')->count();
        $totalCompanyEnquiries = ContactMessage::where('type', 'Company')->count();

        $recentJobs = Job::orderBy('id', 'DESC')->take(25)->get();

        /*$ss = new Seo();


        $ss->page_title = 'functional_area';
        $ss->seo_title = 'Functional Areas';
        $ss->seo_keywords = 'functional areas, job seeker, job vacancies, it jobs, Company IT Vacancies, looking to recruit';
        $ss->seo_other = '';
        $ss->save();*/

        return view('admin.home', compact('totalJobseekerEnquiries', 'totalCompanyEnquiries'))

                        ->with('totalActiveUsers', $totalActiveUsers)

                        ->with('totalVerifiedUsers', $totalVerifiedUsers)

                        ->with('totalTodaysUsers', $totalTodaysUsers)

                        ->with('recentCompanies', $recentCompanies)

                        ->with('totalJobs', $totalJobs)
                        
                        ->with('totalDeleteJobs', $totalDeleteJobs)

                        ->with('recentUsers', $recentUsers)
                        ->with('totalVerifiedCompanies', $totalVerifiedCompanies)
                        ->with('totalCompanies', $totalCompanies)

                        ->with('totalActiveJobs', $totalActiveJobs)
                        ->with('totalAbusedJobs', $totalAbusedJobs)
                        
                        ->with('totalPendingJobs', $totalPendingJobs)

                        ->with('totalRejectedJobs', $totalRejectedJobs)

                        ->with('totalFeaturedJobs', $totalFeaturedJobs)

                        ->with('totalAppliedUsers', $totalAppliedUsers)
                        ->with('totalTodaysJobs', $totalTodaysJobs)

                        ->with('recentJobs', $recentJobs);

    }



}

