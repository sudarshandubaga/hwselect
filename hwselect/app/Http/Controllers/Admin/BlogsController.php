<?php



namespace App\Http\Controllers\Admin;



use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Blog;

use App\Blog_category;

use App\Helpers\DataArrayHelper;

use Image;
use ImgUploader;



class BlogsController extends Base

{

    public function index(Request $request)

    {
        $data['cate'] = '';
        if(null != $request->cate_id){
            $category = Blog_category::findOrFail($request->cate_id);
            $data['cate'] = $category;
            $data['user'] = Blog::whereRaw("FIND_IN_SET('$category->id',cate_id)")->where('lang', 'like', \App::getLocale())->orderBy('id', 'DESC')->paginate(10);
        }else{
           $data['user'] = Blog::get(); 
        }
        

        $categories = Blog_category::get();

        $data['categories'] = $categories;

        return view('admin/blogs/blogs')->with($data);

    }
    public function fetchModulesData(Request $request)
    {
        $blog_data = Blog::select(['*']);
        return Datatables::of($blog_data)
                        ->filter(function ($query) use ($request) {
                            //dd($request);
                            if ($request->has('title') && !empty($request->title)) {
                                $query->where('title', 'like', "%{$request->get('title')}%");
                            }
                            if ($request->has('category') && !empty($request->category)) {
                                $query->where('cate_id', 'like', "%{$request->get('category')}%");
                            }
                            if ($request->has('status') && !empty($request->status)) {
                                //dd($request->status);
                                $query->where('status',$request->status);
                            }

                            $query->orderBy('id', 'DESC');
                        })
                        ->addColumn('image', function ($modules_data) {
                            $image = '<img style="width: 57%;" src="'.asset('/images/thumb/'.$modules_data->image).'" alt="">';
                            return $image;
                        })
                        ->addColumn('title', function ($modules_data) {
                            $title = Str::limit($modules_data->title, 100, '...');
                            return $title;
                        })
                        ->addColumn('created_date', function ($modules_data) {
                            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $modules_data->created_at);
                        })
                        ->addColumn('category', function ($modules_data) use ($module) {
                            if($module->multiple_category){
                                $cate_ids = explode(",", $modules_data->category_ids);
                                $categories = ModulesData::whereIn('id', $cate_ids)->get();

                              $cate_array = array();
                              foreach ($categories as $cat) {
                                  $cate_array[] = "<a href='" . route('blogs.list','cate='.$cat->id) . "'>$cat->title</a>";
                              }
                              return implode(' | ', $cate_array);
                            }else{
                               return title($modules_data->category); 
                            }
                            
                        })
                        ->addColumn('status', function ($modules_data) {
                            $status_type = '';
                            if($modules_data->status=='active'){
                                $status_type = '<span style="font-size: 12px;" class="btn btn-success">'.$modules_data->status.'</span>';
                            }else{
                                $status_type = '<span style="font-size: 12px;" class="btn btn-warning">'.$modules_data->status.'</span>';
                            }
                            $status = '<a class="waves-effect status waves-light" onclick="update_status('.$modules_data->id.');" href="javascript:void(0);" id="sts_'.$modules_data->id.'"> '.$status_type.'</a>';
                            return  $status;
                            
                        })
                        ->addColumn('action', function ($modules_data) use ($module) {
                            if($module->is_preview){
                                if($modules_data->id==45){
                                    $preview = '<a target="_blank" href="'.url('/').'" class="tabledit-edit-button btn btn-success waves-effect waves-light" style="float: none;margin: 5px; font-size: 12px;"><span class="icofont icofont-eye-alt"></span>&nbsp Preview</a>';
                                }else if($module->id==6){
                                    $preview = '<a target="_blank" href="'.url('/'.$modules_data->slug).'" class="tabledit-edit-button btn btn-success waves-effect waves-light" style="float: none;margin: 5px; font-size: 12px;"><span class="icofont icofont-eye-alt"></span>&nbsp Preview</a>';
                                }else{
                                    $preview = '<a target="_blank" href="'.url('/'.strtolower($module->term).'/'.$modules_data->slug).'" class="tabledit-edit-button btn btn-success waves-effect waves-light" style="float: none;margin: 5px; font-size: 12px;"><span class="icofont icofont-eye-alt"></span>&nbsp Preview</a>';
                                }
                            }else{
                                $preview ='';
                            }
                            
                            $action = $preview.'<a href="'.route('admin.modules.data.edit',[$module->slug,$modules_data->id]).'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px; font-size: 12px;"><span class="icofont icofont-ui-edit"></span>&nbsp Edit</a>
                                <a href="'.route('admin.modules.data.delete',[$module->slug,$modules_data->id]).'" class="tabledit-delete-button btn btn-danger waves-effect waves-light" style="float: none;margin: 5px; font-size: 12px;"><span class="icofont icofont-ui-delete"></span>&nbsp Delete</a>';
                            return $action;
                        })
                        ->rawColumns(['title', 'status', 'action', 'image', 'category'])
                        ->setRowId(function($modules_data) {
                            return 'countryDtRow' . $modules_data->id;
                        })
                        ->make(true);
    }
    public function show_form()

    {

        $languages = DataArrayHelper::languagesNativeCodeArray();

        $categories = Blog_category::get();

        $data['categories'] = $categories;

        return view('admin/blogs/post_form')->with('categories',$categories)->with('languages',$languages);

    }

    public function create(Request $request)

    {

        $this->validate($request, [

            'title' => 'required',

            'slug' => 'required',

            'content' => 'required',

            'cate_id.*' => 'required',

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ], [

            'title.required' => ' Title Field Required',

            'slug.required' => ' Slug Field Required',

            'content.required' => ' Content Field Required',

        ]);



        

        $page_slug = $request->slug;

        $slugs = unique_slug($page_slug, 'blogs', $field = 'slug', $key = NULL, $value = NULL);

        if ($request->cate_id != '') {

            $category_Ids = implode(",", $request->cate_id);

        } else {

            return redirect()->back()->withInput($request->input())->withErrors(['Select a Category for Blog', 'cate_id']);

        }



        $blog = new Blog();

        $blog->heading = $request->title;

        $image = $request->file('image');

        if ($image != '') {

            $nameonly = preg_replace('/\..+$/', '', $image->getClientOriginalName());

            $image = $request->file('image');

            $fileName = ImgUploader::UploadImage('uploads/blogs/', $image, $nameonly, 300, 300, false);

            $blog->image = $fileName;

        }

        $blog->slug = $slugs;

        $blog->cate_id = $category_Ids;

        $blog->content = $request->content;

        $blog->meta_title = $request->meta_title;

        $blog->meta_keywords = $request->meta_keywords;

        $blog->lang = $request->lang;

        $blog->meta_descriptions = $request->meta_descriptions;


        $blog->save();

        if ($blog->save() == true) {

            $request->session()->flash('message.added', 'success');

            $request->session()->flash('message.content', 'Blog was successfully added!');

        } else {

            $request->session()->flash('message.added', 'danger');

            $request->session()->flash('message.content', 'Error!');

        }

        return redirect(route('edit-blog',$blog->id));

    }



    public function get_blog_by_id($id = '')

    {

        if ($id != '') {

            $row = Blog::findOrFail($id);

            $json_data = json_encode($row);

            echo $json_data;

            return;

        }

    }

    public function get_blog($id = '')

    {

        if ($id != '') {

            $data['languages'] = DataArrayHelper::languagesNativeCodeArray();

            $row = Blog::findOrFail($id);

            $data['blog'] = $row;

            $categories = Blog_category::get();

            $data['categories'] = $categories;

            return view('admin/blogs/update_form')->with($data);

        }

    }



    public function update(Request $request)

    {



        $this->validate($request, [

            'title_update' => 'required',

            'slug_update' => 'required',

            'content_update' => 'required',
            'cate_id_update.*' => 'required',

            'imageupdate' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ], [

            'title_update.required' => 'Title Field Required',

            'slug_update.required' => 'Slug Field Required',

            'content_update.required' => 'Content Field Required',

        ]);



        if ($request->cate_id_update != '') {

            $category_Ids = implode(",", $request->cate_id_update);

        } else {

            return redirect()->back()->withInput($request->input())->withErrors(['Select a Category for Blog', 'cate_id']);

        }



        $blog = Blog::findOrFail($request->id);

        $blog->heading = $request->title_update;

        $blog->slug = $request->slug_update;;

        $blog->cate_id = $category_Ids;

        $blog->content = $request->content_update;

        $blog->lang = $request->lang;

        $blog->meta_title = $request->meta_title_update;
        $blog->appear_on_home_page = $request->appear_on_home_page;

        $blog->meta_keywords = $request->meta_keywords_update;

        $blog->meta_descriptions = $request->meta_descriptions_update;

        $blog->update();

        $this->validate($request, [

            'imageupdate' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $image = $request->file('imageupdate');

        if ($image != '') {

            $nameonly = preg_replace('/\..+$/', '', $image->getClientOriginalName());

            $fileName = ImgUploader::UploadImage('uploads/blogs/', $image, $nameonly, 300, 300, false);

            $blog->image = $fileName;

        }

        $blog->update();

        if ($blog->save() == true) {

            $request->session()->flash('message.updated', 'success');

            $request->session()->flash('message.content', 'Blog was successfully updated!');

        } else {

            $request->session()->flash('message.updated', 'danger');

            $request->session()->flash('message.content', 'Error!');

        }

        return redirect(route('edit-blog',$blog->id));

    }



    public function destroy($id)

    {

        $blog = Blog::findOrFail($id);

        $blog->delete();



        return response()->json($blog);

    }


    public function destroyBlog(Request $request,$id)

    {

        $blog = Blog::findOrFail($id);

        $blog->delete();



        $request->session()->flash('message.updated', 'success');

        $request->session()->flash('message.content', 'Blog successfully deleted!');

        return redirect(route('blog'));

    }



    public function remove_blog_feature_image($id)

    {

        $image = Blog::findOrFail($id);

        $file = $image->image;

        $sts = 'done';

        $imagename = '';

        $filenamethumb = public_path() . '/uploads/blogs/thumbnail/' . $file;

        $filename = public_path() . '/uploads/blogs/' . $file;

        \File::delete([

            $filename,

            $filenamethumb

        ]);

        $image->image = $imagename;

        $image->update();

        return response()->json($sts);

    }

}