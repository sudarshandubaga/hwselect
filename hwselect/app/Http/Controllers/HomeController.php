<?php



namespace App\Http\Controllers;



use App\Traits\Cron;

use App\Job;

use App\FavouriteCompany;
use App\ProfileSkill;

use Auth;
use Redirect;



use Illuminate\Http\Request;



class HomeController extends Controller

{



    use Cron;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {
        parent::__construct();
        $this->middleware('auth');

        $this->runCheckPackageValidity();
        $this->middleware(function ($request, $next) {
            //dd(Auth::user());
            if(Auth::user() && Auth::user()->verified=='0') {
                Auth::logout();
                return Redirect::route('login')->with('not_verified','Not verified');
            } 

            return $next($request);   
        });
        $this->middleware(function ($request, $next) {
            //dd(Auth::user());
            if(Auth::user() && Auth::user()->phone_verify=='no') {
                return Redirect::route('phone-verification');
            } 

            return $next($request);   
        });

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
        $ids = ProfileSkill::where('user_id',auth()->user()->id)->get();

        $matchingJobs = Job::where('functional_area_id', auth()->user()->industry_id)->where('is_active', '=', 1)->whereHas('jobSkills', function($query) use ($ids) {

                $query->whereIn('job_skill_id', $ids);

            })->where('expiry_date', '>',  \Carbon\Carbon::now())->paginate(7);

		$followers = FavouriteCompany::where('user_id', auth()->user()->id)->get();

		$chart = '';
        //dd(Auth::user()->first_time_login);
        //if(Auth::user()->country_id == '' || ) 

        return view('home', compact('chart', 'matchingJobs', 'followers'));

    }



}

