<?php



namespace App\Http\Requests;



use App\Http\Requests\Request;
// use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Rule;


class ProfileSkillFormRequest extends Request

{



    /**

     * Determine if the user is authorized to make this request.

     *

     * @return bool

     */

    public function authorize()

    {

        return true;

    }



    /**

     * Get the validation rules that apply to the request.

     *

     * @return array

     */

    public function rules()

    {

        switch ($this->method()) {

            case 'PUT':

            case 'POST': {

                    $id = (int) $this->input('id', 0);
                    $user_id = (int) $this->route('id');

                    return [

                        "job_skill_id" => [
                            "required",
                            Rule::unique('profile_skills', 'job_skill_id')->using(function ($q) use ($user_id) {
                                $q->where('user_id', $user_id);
                            })
                        ],

                        "job_experience_id" => "required",

                    ];

                }

            default:break;

        }

    }



    public function messages()

    {

        return [

            'job_skill_id.required' => 'Please select or add acquired skills',
            'job_skill_id.unique' => 'Job Skill Already Added',

            'job_experience_id.required' => 'Please Select Length of Experience',

        ];

    }



}

