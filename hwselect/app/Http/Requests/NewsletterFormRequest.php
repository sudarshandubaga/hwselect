<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    
                    return [
                        "id" => "",
                        "title" => "required",
                        "send_to" => "required",
                        "description" => "required",
                        
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required.',
            'send_to.required' => 'Please Select where you want to send?.',
            'description.required' => 'Description is required.',
        ];
    }

}
