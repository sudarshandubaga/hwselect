<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-globe" aria-hidden="true"></i> <span class="title">State / Counties</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
        <li class="nav-item  "> <a href="<?php echo e(route('list.states')); ?>" class="nav-link "> <span class="title">List Counties, States </span> </a> </li>
        <li class="nav-item  "> <a href="<?php echo e(route('create.state')); ?>" class="nav-link ">  <span class="title">Add New County, State</span> </a> </li>
        <li class="nav-item  "> <a href="<?php echo e(route('sort.states')); ?>" class="nav-link ">  <span class="title">Sort County State</span> </a> </li>
    </ul>
</li><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/side_bars/state.blade.php ENDPATH**/ ?>