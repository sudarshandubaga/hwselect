<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-mars-double" aria-hidden="true"></i> <span class="title">Marital Status</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
        <li class="nav-item  "> <a href="<?php echo e(route('list.marital.statuses')); ?>" class="nav-link "> <span class="title">List Marital Status</span> </a> </li>
        <li class="nav-item  "> <a href="<?php echo e(route('create.marital.status')); ?>" class="nav-link "> <span class="title">Add new Marital Status</span> </a> </li>
        <li class="nav-item  "> <a href="<?php echo e(route('sort.marital.statuses')); ?>" class="nav-link "> <span class="title">Sort Marital Status</span> </a> </li>
    </ul>
</li><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/side_bars/marital_status.blade.php ENDPATH**/ ?>