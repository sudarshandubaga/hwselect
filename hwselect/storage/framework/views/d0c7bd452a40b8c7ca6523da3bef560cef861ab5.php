<li class="nav-item "><a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Manage Reasons</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="<?php echo e(route('modules-data','deactivate-reasons')); ?>" class="nav-link "> <span class="title">List Job Deactivate Reasons</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('modules-data','reactivate-reasons')); ?>" class="nav-link "> <span class="title">List Job Reactivate Reasons</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('modules-data','deleted-reasons')); ?>" class="nav-link "> <span class="title">List Job Deleted Reasons</span> </a> </li>

        
        <li class="nav-item  "> <a href="<?php echo e(route('modules-data','account-deleted-reasons')); ?>" class="nav-link "> <span class="title">List Account Deleted Reasons</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('modules-data','report-abuse-reasons')); ?>" class="nav-link "> <span class="title">List Report Abuse Reasons</span> </a> </li>
 	

    </ul>

</li>



<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/side_bars/reasons.blade.php ENDPATH**/ ?>