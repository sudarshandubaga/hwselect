<?php $__env->startPush('styles'); ?>
<style type="text/css">
	.searchwrap {
	    background: url(<?php echo e(asset('/')); ?>/company_logos/<?php echo e(get_widget(10)->image); ?>) no-repeat;
	    background-size: cover;
	}
</style>
<?php $__env->stopPush(); ?>
<?php if(Auth::guard('company')->check()): ?>


<div class="uploadjobinfo">
	<p>Don't have time to post a job? Don't worry just upload your specifications and we will take care of it.</p>
	<a href="javascript:void(0)" data-toggle="modal" data-target="#uploadjobdetail">Upload Job Details</a>
</div>


<?php else: ?>

<form action="<?php echo e(route('job.list')); ?>" method="get">

    <div class="searchbar">

		<div class="srchbox">		

		<div class="row">

			<div class="col-lg-12 col-md-12 mb-3">

				<label for=""> <?php echo e(__('Keywords / Job Title')); ?></label>
				<div class="clearable-input">
				<input type="text"  name="search" id="jbsearch" value="<?php echo e(Request::get('search', '')); ?>" class="form-control" placeholder="<?php echo e(__('e.g. Project Manager')); ?>" autocomplete="off" />
				<span data-clear-input><i class="fas fa-times"></i></span>
				</div>
			</div>

			

			<div class="col-lg-10 col-md-4">
				<div class="location-input">
				<label for=""> <?php echo e(__('Where')); ?></label>
				<input type="text"  name="address" id="current_location" value="<?php echo e(Request::get('address', '')); ?>" class="form-control"  placeholder="<?php echo e(__('Town, City or Postcode')); ?>" autocomplete="off" />
			</div>
			<div class="radius">
				<label for=""> <?php echo e(__('Radius')); ?></label>
				<select name="radius" class="form-control search-radius search-locationsearchtype" id="Radius">
					<option value="0">0 miles</option>
					<option value="5">5 miles</option>
					<option value="10">10 miles</option>
					<option selected="selected" value="20">20 miles</option>
					<option value="30">30 miles</option>
				</select>
			</div>
			<div class="clearfix"></div>
			</div>

			
			<input type="hidden" name="longitude" id="longitude" value="<?php echo e(Request::get('longitude', '')); ?>">
			<input type="hidden" name="latitude" id="latitude" value="<?php echo e(Request::get('latitude', '')); ?>">
			

			<div class="col-lg-2 col-md-3">

				<label for="">&nbsp;</label>

				<button type="submit" class="btn"><i class="fas fa-search"></i></button></div>
				
		</div>

		</div>	

		<div class="jobcount"><?php echo e(get_widget(10)->content); ?></div>

    </div>

</form>

<?php endif; ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/includes/search_form.blade.php ENDPATH**/ ?>