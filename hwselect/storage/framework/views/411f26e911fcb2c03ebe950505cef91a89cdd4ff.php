<!-- BEGIN SIDEBAR -->

<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->

<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->

<div class="page-sidebar navbar-collapse collapse">

    <!-- BEGIN SIDEBAR MENU -->

    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->

    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->

    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->

    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->

    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->

    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

    <ul class="page-sidebar-menu page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->

        <li class="sidebar-toggler-wrapper hide">

            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->

            <div class="sidebar-toggler"> </div>

            <!-- END SIDEBAR TOGGLER BUTTON -->

        </li>

        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->

        <li class="sidebar-search-wrapper">

            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->

            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->

            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->

            <!-- END RESPONSIVE QUICK SEARCH FORM -->

        </li>

        <li class="nav-item start active"> <a href="<?php echo e(route('admin.home')); ?>" class="nav-link"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>

        <?php if(Auth::guard('admin')->user()->admin_users): ?>

            <?php echo $__env->make('admin/shared/side_bars/admin_user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <?php echo $__env->make('admin/shared/side_bars/contact_enquiry', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <li class="heading">

            <h3 class="uppercase">Modules</h3>

        </li>

        <?php if(Auth::guard('admin')->user()->job): ?>

        <?php echo $__env->make('admin/shared/side_bars/job', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/reasons', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->company): ?>

        <?php echo $__env->make('admin/shared/side_bars/company', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->site_user): ?>

        <?php echo $__env->make('admin/shared/side_bars/site_user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->cms): ?>

        <?php echo $__env->make('admin/shared/side_bars/cms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->blogs): ?>

        <?php echo $__env->make('admin/shared/side_bars/blogs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>





        <?php if(Auth::guard('admin')->user()->widget): ?>

        <?php echo $__env->make('admin/shared/side_bars/widget', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->seo): ?>

        <?php echo $__env->make('admin/shared/side_bars/seo', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->faq): ?>

        <?php echo $__env->make('admin/shared/side_bars/faq', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->video): ?>

        <?php echo $__env->make('admin/shared/side_bars/video', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->testimonial): ?>

        <?php echo $__env->make('admin/shared/side_bars/testimonial', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>



        <?php if(Auth::guard('admin')->user()->slider): ?>

        <?php echo $__env->make('admin/shared/side_bars/slider', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>

        
        <?php if(APAuthHelp::check(['SUP_ADM'])): ?>
        <li class="heading">

            <h3 class="uppercase">Manage Location</h3>

        </li>

        <?php echo $__env->make('admin/shared/side_bars/country', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/country_detail', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/state', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/city', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        
        <li class="heading">

            <h3 class="uppercase">Job Attributes</h3>

        </li>

        <?php echo $__env->make('admin/shared/side_bars/benifits', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('admin/shared/side_bars/bonus', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('admin/shared/side_bars/language_level', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/career_level', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/functional_area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/gender', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/industry', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/reasons', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/job_experience', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/job_skill', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/job_type', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/job_shift', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/degree_level', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/degree_type', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/major_subject', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>  

        <?php echo $__env->make('admin/shared/side_bars/result_type', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/marital_status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->make('admin/shared/side_bars/ownership_type', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

        <?php echo $__env->make('admin/shared/side_bars/salary_period', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

		

        <li class="heading">

            <h3 class="uppercase">Manage</h3>

        </li>

        <?php echo $__env->make('admin/shared/side_bars/menus', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>		 
        <?php echo $__env->make('admin/shared/side_bars/blockedips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>         

        <?php echo $__env->make('admin/shared/side_bars/site_setting', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>   

		<?php endif; ?>

		

		

		

    </ul>

    <!-- END SIDEBAR MENU -->

    <!-- END SIDEBAR MENU -->

</div>

<!-- END SIDEBAR -->







<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/sidebar.blade.php ENDPATH**/ ?>