<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-briefcase" aria-hidden="true"></i> <span class="title">Contractual Hours</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="<?php echo e(route('list.job.types')); ?>" class="nav-link"> <span class="title">List Contractual Hours</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('create.job.type')); ?>" class="nav-link"> <span class="title">Add new Contractual Hours</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('sort.job.types')); ?>" class="nav-link"> <span class="title">Sort Contractual Hours</span> </a> </li>

    </ul>

</li><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/side_bars/job_type.blade.php ENDPATH**/ ?>