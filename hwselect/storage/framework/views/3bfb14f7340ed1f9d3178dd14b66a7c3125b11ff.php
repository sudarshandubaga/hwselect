<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en">

    <!--<![endif]-->

    <!-- BEGIN HEAD -->

    <head>

        <meta charset="utf-8" />

        <title><?php echo e($siteSetting->site_name); ?> | Admin Login</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <meta content="" name="description" />

        <meta content="" name="author" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN THEME GLOBAL STYLES -->

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

        <!-- END THEME GLOBAL STYLES -->

        <!-- BEGIN THEME LAYOUT STYLES -->

        <link href="<?php echo e(asset('/')); ?>admin_assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />

        <!-- END THEME LAYOUT STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />


        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo e(asset('/')); ?>admin_assets/layout/css/themes/custom.css" rel="stylesheet" type="text/css" />

        <!-- END PAGE LEVEL PLUGINS -->

        <link type="text/css" rel="stylesheet" media="all" href="<?php echo e(asset('/')); ?>admin_assets/custom.css?v=13" />

        <link rel="shortcut icon" href="<?php echo e(asset('/')); ?>favicon.ico" />

        <?php echo $__env->yieldPushContent('css'); ?>

        <script>

        var APP_URL = "<?php echo url('/'); ?>"

        </script>
        <style type="text/css">
            .error{
                color: red;
            }
        </style>
        <style type="text/css">
            .formpanel.mt0{margin-top: 0;}

.lang-select{position:relative; border: 1px solid #ddd; border-right: none;}
.lang-select-mobile_num{position:relative; border: 1px solid #ddd; border-right: none;}
.lang-select-phone_ceo_num{position:relative; border: 1px solid #ddd; border-right: none;}
.vodiapicker{
  display: none !important; 
}

.vodiapicker_mobile_num{
  display: none; 
}

.vodiapicker_phone_ceo_num{
  display: none; 
}

#a{
  padding-left: 0px;
}

#a-mobile_num{
  padding-left: 0px;
}

#a-phone_ceo_num{
  padding-left: 0px;
}

#a li{
  list-style: none;
  padding-top: 5px;
  padding-bottom: 5px;
}
.btn-select:after, #a li:after{clear:both; display:table; content:'';}
#a-mobile_num li{
  list-style: none;
  padding-top: 5px;
  padding-bottom: 5px;
}

#a-phone_ceo_num li{
  list-style: none;
  padding-top: 5px;
  padding-bottom: 5px;
}

#a li:hover{
 background-color: #F4F3F3;
}


#a-mobile_num li:hover{
 background-color: #F4F3F3;
}

#a-phone_ceo_num li:hover{
 background-color: #F4F3F3;
}

#a li img{
  margin: 5px;
      float: left;
}

        </style>
        

    </head>

    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <!-- BEGIN HEADER -->

        <div class="page-header navbar navbar-fixed-top"> 

            <!-- BEGIN HEADER INNER -->

            <div class="page-header-inner "> 

                <!-- BEGIN LOGO -->

                <div class="page-logo"> <a href="<?php echo e(route('admin.home')); ?>"> <img src="<?php echo e(asset('/')); ?>sitesetting_images/thumb/<?php echo e($siteSetting->site_logo); ?>" alt="logo" style="max-width:170px; max-height:40px;" /> <!-- class="logo-default" --></a>

                    <div class="menu-toggler sidebar-toggler"> </div>

                </div>

                <!-- END LOGO --> 

                <!-- BEGIN RESPONSIVE MENU TOGGLER --> 

                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a> 

                <!-- END RESPONSIVE MENU TOGGLER --> 

                <!-- BEGIN TOP NAVIGATION MENU --> 

                <?php echo $__env->make('admin.shared.top_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

                <!-- END TOP NAVIGATION MENU --> 

            </div>

            <!-- END HEADER INNER --> 

        </div>

        <!-- END HEADER --> 

        <!-- BEGIN HEADER & CONTENT DIVIDER -->

        <div class="clearfix"> </div>

        <!-- END HEADER & CONTENT DIVIDER --> 

        <!-- BEGIN CONTAINER -->

        <div class="page-container"> 

            <!-- BEGIN SIDEBAR -->

            <div class="page-sidebar-wrapper"> <?php echo $__env->make('admin.shared.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> </div>

            <!-- END SIDEBAR --> 

            <!-- BEGIN CONTENT --> 

            <?php echo $__env->yieldContent('content'); ?> 

            <!-- END CONTENT --> 

        </div>

        <!-- END CONTAINER --> 

        <!-- BEGIN FOOTER -->

        <div class="page-footer">

            <div class="page-footer-inner"> <?php echo e(date('Y')); ?> © <?php echo e($siteSetting->site_name); ?>. Admin Panel. </div>

            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>

        </div>

        <!-- END FOOTER --> 

        <!--[if lt IE 9]>

                <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/respond.min.js"></script>

                <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/excanvas.min.js"></script> 

                <![endif]--> 

        <!-- BEGIN CORE PLUGINS --> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/jquery.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 

        <!-- END CORE PLUGINS --> 

        <!-- BEGIN THEME GLOBAL SCRIPTS --> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/scripts/app.min.js" type="text/javascript"></script> 

        <!-- END THEME GLOBAL SCRIPTS --> 

        <!-- BEGIN THEME LAYOUT SCRIPTS --> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/layouts/layout/scripts/demo.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 

        <!-- END THEME LAYOUT SCRIPTS --> 

        <!-- BEGIN PAGE LEVEL PLUGINS --> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/scripts/datatable.js" type="text/javascript"></script> 

        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript"></script> 


        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/jquery.scrollTo.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/Bootstrap-3-Typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script> 

        <!-- END PAGE LEVEL PLUGINS --> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/scripts/app.min.js" type="text/javascript"></script> 

        <!-- BEGIN PAGE LEVEL SCRIPTS --> 

        <script src="<?php echo e(asset('/')); ?>admin_assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

        <script src="<?php echo e(asset('/')); ?>admin_assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <!-- END PAGE LEVEL SCRIPTS --> 
        <script src="<?php echo e(asset('/')); ?>js/jquery.validate.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>

         <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsKIwI70lePVwWa-T4X8SfnHQRxj7Vw6M&libraries=places"></script> -->
         <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA&libraries=places"></script>
         <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>



        <script>

$('#flash-overlay-modal').modal();
        function setCookie(cname, cvalue, exdays) {

          var d = new Date();

          d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

          var expires = "expires=" + d.toUTCString();

          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

        }

        function getCookie(cname) {

          var name = cname + "=";

          var decodedCookie = decodeURIComponent(document.cookie);

          var ca = decodedCookie.split(';');

          for (var i = 0; i < ca.length; i++) {

            var c = ca[i];

            while (c.charAt(0) == ' ') {

              c = c.substring(1);

            }

            if (c.indexOf(name) == 0) {

              return c.substring(name.length, c.length);

            }

          }

          return "";

        }

        var href = "<?php echo e(collect(request()->segments())->last()); ?>";
        //alert(href);
        if(href=="admin"){
            $('.nav-item').removeClass('active');
            $('a[href="home"]').parent().addClass('active');
        }else{
            $('.nav-item').removeClass('active');
            if($('a[href="'+href+'"]').parent().parents('.sub-menu').length){
               $('a[href="'+href+'"]').parent().parent().parent().addClass('active open'); 
            }
            
            $('a[href="'+href+'"]').parent().addClass('active');
        }
       
        

        </script> 

        <?php echo $__env->yieldPushContent('scripts'); ?> 

        <script type="text/JavaScript">

            $(document).ready(function(){

            $(document).scrollTo('.msg_cls_for_focus', 2000);

            });

            function showProcessingForm(btn_id){		

            $("#"+btn_id).val( 'Processing .....' );

            $("#"+btn_id).attr('disabled','disabled');

            }

            

            
          var input_current_location = document.getElementById('current_location');
          var autocomplete_current_location = new google.maps.places.Autocomplete(input_current_location);

          google.maps.event.addListener(autocomplete_current_location, 'place_changed', function () 
          {
            var place = autocomplete_current_location.getPlace();
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
        });


          /*$('#current_location').on('keyup',function(){
            var geocoder = new google.maps.Geocoder();
            var address = $(this).val();

            geocoder.geocode( { 'address': address}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
              } 
            }); 
          })

          $('#current_location').on('change',function(){
            var geocoder = new google.maps.Geocoder();
            var address = $(this).val();

            geocoder.geocode( { 'address': address}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
              } 
            }); 
          })*/
			
		

        </script>

    </body>

</html>

<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/layouts/admin_layout.blade.php ENDPATH**/ ?>