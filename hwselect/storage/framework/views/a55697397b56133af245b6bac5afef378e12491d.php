<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-area-chart" aria-hidden="true"></i> <span class="title">Skill Acquired</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="<?php echo e(route('list.job.skills')); ?>" class="nav-link "> <span class="title">List Skill Acquired</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('create.job.skill')); ?>" class="nav-link "> <span class="title">Add new Skill Acquired</span> </a> </li>

        <li class="nav-item  "> <a href="<?php echo e(route('sort.job.skills')); ?>" class="nav-link "> <span class="title">Sort Skill Acquired</span> </a> </li>

    </ul>

</li><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/side_bars/job_skill.blade.php ENDPATH**/ ?>