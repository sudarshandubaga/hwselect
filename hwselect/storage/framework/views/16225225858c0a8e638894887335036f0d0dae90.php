<?php if(Request::segment(1)==null && $siteSetting->index_page_below_cities_ads==1){?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>
<?php }else if(Request::segment(1)=='hire-it-talent' && $siteSetting->hire_it_talent_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='why-us' && $siteSetting->why_us_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='jobs' && $siteSetting->jobs_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='contact-us' && $siteSetting->contact_us_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='blog' && $siteSetting->blog_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>
<?php }else if(Request::segment(2)!=null && $siteSetting->blog_details_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='job' && $siteSetting->jobs_detail_ads==1){ ?>


<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='login' && $siteSetting->login_ads==1){ ?>

<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>
<?php }else if(Request::segment(1)=='register' && $siteSetting->register_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    <?php echo $siteSetting->above_footer_ad; ?>

  </div>
  <div class="clearfix"></div>
</div>
<?php } ?>


<div class="footerWrap">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <img src="<?php echo e(asset('/')); ?>sitesetting_images/thumb/<?php echo e($siteSetting->site_logo); ?>"
          alt="<?php echo e($siteSetting->site_name); ?>" />

        <!-- Social Icons -->
        <div class="social"><?php echo $__env->make('includes.footer_social', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
        <!-- Social Icons end -->
      </div>


      <!--Quick Links-->
      <div class="col-lg-3 col-md-6">
        <h5><?php echo e(__('HW Search')); ?></h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">

          <?php echo get_menus_footer(); ?>


        </ul>
      </div>
      <!--Quick Links menu end-->

      <div class="col-lg-3 col-md-6">
        <h5><?php echo e(__('Jobseeker')); ?></h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          <?php echo get_menus_seeker(); ?>


          <?php if(!Auth::user() && !Auth::guard('company')->user()): ?>
          <li><a href="javascript:void(0)" data-toggle="modal" data-target="#uploadresume">Upload CV</a></li>
          <?php else: ?>
          <li><a href="<?php echo e(url('my-profile#cvs')); ?>">Upload CV</a></li>
          <?php endif; ?>

        </ul>
      </div>

      <!--Jobs By Industry-->
      <div class="col-lg-3 col-md-6">
        <h5><?php echo e(__('Employer')); ?></h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">

          <?php echo get_menus_employerr(); ?>


        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>




    </div>
  </div>
</div>
<!--Footer end-->
<!--Copyright-->
<div class="copyright">
  <div class="container">
    <div class="bttxt"><?php echo e(__('Copyright')); ?> &copy; <?php echo e(date('Y')); ?> <?php echo e($siteSetting->site_name); ?>.
      <?php echo e(__('All Rights Reserved')); ?>. <?php echo e(__('Created by')); ?>: <a href="https://www.seoguru.co.uk" target="_blank"
        style="color: #fff;">SEOGURU</a></div>

  </div>
</div>

<?php echo $siteSetting->google_tag_manager_for_body; ?>




<div class="modal fade" id="uploadresume" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fas fa-times"></i>
        </button>

        <div class="invitereval">
          <h3><?php echo e(__('Upload Resume')); ?></h3>

          <p> <?php echo e(__('You need to be Registered and Signed into your account to Upload Resume.')); ?></p>
          <div class="btn2s">
            <a href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
            <a href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="uploadjobdetail" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fas fa-times"></i>
        </button>

        <div class="invitereval">
          <h3><?php echo e(__('Upload Job Details')); ?></h3>

          <p> <?php echo e(__('Please upload your job specifications and our team will post on your behalf.')); ?></p>
          <form method="post" action="<?php echo e(route('apply-to-admin')); ?>" enctype='multipart/form-data'>
            <?php echo csrf_field(); ?>
            <div class="formpanel">
              <div class="formrow">
                <label for="">Choose FIle</label>
                <input type="file" class="form-control" name="job_detail" id="job_detail" required="required"
                  oninvalid="this.setCustomValidity('Please select pdf, doc, docx files')"
                  oninput="this.setCustomValidity('')">
                <p>Please upload as a PDF or Word Doc file.</p>
              </div>
              <div class="formrow">
                <input type="submit" class="btn" name="" id="" value="Submit">
              </div>
            </div>
          </form>


        </div>

      </div>

    </div>
  </div>
</div>






<script>
  Array.prototype.forEach.call(document.querySelectorAll('.clearable-input'), function (el) {
    var input = el.querySelector('input');

    conditionallyHideClearIcon();
    input.addEventListener('input', conditionallyHideClearIcon);
    el.querySelector('[data-clear-input]').addEventListener('click', function (e) {
      input.value = '';
      conditionallyHideClearIcon();
    });

    function conditionallyHideClearIcon(e) {
      var target = (e && e.target) || input;
      target.nextElementSibling.style.display = target.value ? 'block' : 'none';
    }
  });
</script><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/includes/footer.blade.php ENDPATH**/ ?>