<li class="nav-item"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user"></i> <span class="title">Jobseekers</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">
        <li class="nav-item <?php echo e(Request::url() == route('list.users') ? 'active' : ''); ?>">
			<a href="<?php echo e(route('list.users')); ?>" class="nav-link "> <i class="icon-user"></i> <span class="title">List all Jobseekers</span> </a>
		</li>

        <li class="nav-item <?php echo e(Request::url() == route('create.user') ? 'active' : ''); ?>">
			<a href="<?php echo e(route('create.user')); ?>" class="nav-link "> <i class="icon-user"></i> <span class="title">Add new Jobseeker</span> </a>
		</li>

        <li class="nav-item <?php echo e(Request::url() == route('admin.search-cvs') ? 'active' : ''); ?>">
			<a href="<?php echo e(route('admin.search-cvs')); ?>" class="nav-link "> <i class="icon-user"></i> <span class="title">Search CVs</span> </a>
		</li>
    </ul>
</li><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/admin/shared/side_bars/site_user.blade.php ENDPATH**/ ?>