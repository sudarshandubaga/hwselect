<?php $__env->startSection('content'); ?>
<!-- Header start -->
<?php echo $__env->make('includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style>
.searchbar .form-control::placeholder {
    color: inherit !important;
}
</style>
<!-- Header end --> 
<!-- Inner Page Title start -->
<!-- Inner Page Title end -->
<div class="about-wraper page-404" style="background-image: url('<?php echo e(url('company_logos/hw-main-banner-1616940090.jpg')); ?>'); background-size: cover;"> 
    <!-- About -->
    <div class="container text-center">
        <div style="margin: 30px 0;">
            <div class="text-white" style="font-size: 192px; font-weight: bold;">
                404
            </div>
            <h2 class="text-white"><?php echo e(__('Oops! Page Not Found')); ?></h2>
            <p class="text-white"><?php echo e(__('Sorry, the page you are looking for could not be found')); ?></p>

            <div style="max-width: 700px; margin: auto;">
                <?php echo $__env->make('includes.search_form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>


            <p class="mt-3">
                <a href="<?php echo e(url('/')); ?>" class="btn btn-primary">Go Back To Homepage</a>
            </p>
        </div>     
    </div>  
</div>
<?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php
$skills_arr = \Cache::rememberForever('skills_arr', function() {
    return \App\JobSkill::select('job_skills.job_skill')->isDefault()->active()->sorted()->pluck('job_skills.job_skill')->toArray();
});

$latestJobs = \App\Job::select('jobs.title')->active()->notExpire()->orderBy('title', 'desc')->pluck('jobs.title')->toArray();

$skills = array_merge($skills_arr,$latestJobs);


$skills = json_encode($skills);
?>


<?php $__env->startPush('scripts'); ?> 
<script>
  $( function() {

    var mySource  = <?php echo $skills; ?>;
    $( "#jbsearch" ).autocomplete({
    minLength: 1,
    source: function(req, resp) {
      var q = req.term;
      var myResponse = [];
      $.each(mySource, function(key, item) {
        if (item.toLowerCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.toUpperCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.indexOf(q) === 0){
          myResponse.push(item);
        }
      });
      resp(myResponse);
    },
    
  });
    });
  </script>	

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/hwselect/hwselect/resources/views/errors/404.blade.php ENDPATH**/ ?>