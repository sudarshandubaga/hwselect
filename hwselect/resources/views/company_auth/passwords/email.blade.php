@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>'Reset Password'])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        <div class="forgotpassbox">
              <div class="userccount">

               

                    <h3>{{__('Reset Password')}}</h3>

                   

                        @if (session('status'))

                        <div class="alert alert-success">

                            {!! session('status') !!}

                        </div>

                        @endif
						
				  		<div class="formpanel">
                        <form class="form-horizontal" method="POST" action="{{ route('company.password.email') }}">

                            {{ csrf_field() }}

                            <div class="formrow {{ $errors->has('email') ? ' has-error' : '' }}">


                                

                                    <input id="email" type="email" class="form-control" placeholder="Enter Registered Email Address" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))

                                    <span class="help-block">

                                        <strong>{{ $errors->first('email') }}</strong>

                                    </span>

                                    @endif

                               

                            </div>

                            <div class="formrow">

                              

                                    <button type="submit" class="btn">

                                        {{__('Send Password Reset Link')}}

                                    </button>

                               

                            </div>

                        </form>
							</div>
                  
               

            </div>

        </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('scripts')
<script type="text/javascript">
    if ($("form").length > 0) {
            $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    email: {
                        required: true,
                        email: true,
                    }, 


                    
                },
                messages: {

                    email: {
                        required: "Please enter your registered email address",
                        email: "Please enter a valid email address",
                    },


                  

                },
            
            })
        }  

        function show_hidden_div(){
            $('.formpanel').show();
            $('.alert-success').hide();
        }          

</script>
<style type="text/css">
    .error{
        color: red !important;
    }
</style>
@endpush