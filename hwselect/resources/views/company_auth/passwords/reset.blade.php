@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>'Reset Password'])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">


            <div class="forgotpassbox">
              <div class="userccount">

                    <h3>{{__('Reset Password')}}</h3>

                    <div class="formpanel">

                        <form class="form-horizontal" method="POST" action="{{ route('company.password.request') }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="formrow {{ $errors->has('email') ? ' has-error' : '' }}">

                                <label for="email" class="control-label">{{__('Email Address')}}</label>

                              

                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email')?old('email'):$email }}" required autofocus>

                                    @if ($errors->has('email'))

                                    <span class="help-block">

                                        <strong>{{ $errors->first('email') }}</strong>

                                    </span>

                                    @endif

                              

                            </div>

                            <div class="formrow {{ $errors->has('password') ? ' has-error' : '' }}">

                                <label for="password" class="control-label">{{__('Password')}}</label>

                                

                                    <input id="password" type="password" class="form-control" name="password" maxlength="12" required>

                                    @if ($errors->has('password'))

                                    <span class="help-block">

                                        <strong>{{ $errors->first('password') }}</strong>

                                    </span>

                                    @endif

                               

                            </div>

                            <div class="formrow {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                                <label for="password-confirm" class="control-label">{{__('Confirm Password')}}</label>

                               

                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" maxlength="12" required>

                                    @if ($errors->has('password_confirmation'))

                                    <span class="help-block">

                                        <strong>{{ $errors->first('password_confirmation') }}</strong>

                                    </span>

                                    @endif

                               

                            </div>

                            <div class="formrow">

                               
                                    <button type="submit" class="btn">

                                        {{__('Reset Password')}}

                                    </button>

                             

                            </div>

                        </form>

                    </div>

                </div>

            </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('scripts')
<script type="text/javascript">
    if ($("form").length > 0) {
            $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    email: {
                        required: true,
                        email: true,
                    }, 

                    password: {
                        required: true,
                    }, 

                    password_confirmation: {
                        required: true,
                    },  


                    
                },
                messages: {

                    email: {
                        required: "Please enter your registered email address",
                        email: "Please enter a valid email address",
                    },
                    password: {
                        required: "Please enter your new password",
                    },

                    password_confirmation: {
                        required: "Password and Confirm Password fields do not match",
                    },


                  

                },
            
            })
        }         
$("#password").focus();  
</script>
<style type="text/css">
    .error{
        color: red !important;
    }
</style>
@endpush