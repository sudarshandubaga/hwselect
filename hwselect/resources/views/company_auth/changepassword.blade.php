@extends('layouts.app')
@push('styles')
<link href="{{asset('admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin_assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
<style type="text/css">
    .error{color: red !important}
</style>
@endpush
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('Change Password')]) 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
            @include('includes.company_dashboard_menu')
            </div>
            
            <div class="col-md-9 col-sm-8"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="userccount">
                        @if(Session::has('error'))
                            <p class="alert alert-danger">{{ Session::get('error') }}</p>
                        @endif  
                            <div class="formpanel mt0"> @include('flash::message') 
                                <!-- Personal Information -->
                               <h5>{{__('Change Password')}}</h5>
                               <form class="form-horizontal" method="POST" action="{{ route('company.changePassword') }}">
                                {{ csrf_field() }}
                                <div class="row">

                                <div class="col-md-12">

                                        <div class="formrow">

                                            <label for="">{{__('Current Password')}}</label>

                                            <input id="current_password" type="password" class="form-control" name="current_password" oninvalid="this.setCustomValidity('Please enter current password')" oninput="setCustomValidity('')" required>

                                            @if ($errors->has('current_password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('current_password') }}</strong>
                                                </span>
                                            @endif 
                                    </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="formrow">

                                            <label for="">{{__('New Password')}}</label>

                                            <input id="password" type="password" class="form-control" name="password" maxlength="12" oninvalid="this.setCustomValidity('Please enter new password')" oninput="setCustomValidity('')" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif 
                                    </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="formrow">

                                            <label for="">{{__('Confirm New Password')}}</label>

                                            <input id="password_confirmation" type="password" class="form-control" maxlength="12" name="password_confirmation" required oninvalid="this.setCustomValidity('Please confirm new password')" oninput="setCustomValidity('')">

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif 
                                    </div>

                                    </div>

                                    <div class="col-md-12">

                                    <div class="formrow"><button type="submit" class="btn">{{__('Change Password')}}  <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button></div>

                                </div>

                                    

                                </div>  
                                </form>                           
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
@push('scripts')
<script type="text/javascript">
    @if(Session::get('message.added'))
            const el = document.createElement('div')
            el.innerHTML = 'Password Changed Successfully';
            swal({

                title: "Password Changed",

                content: el,

                icon: "success",

                button: "OK",

            });
        @endif
</script>

@endpush