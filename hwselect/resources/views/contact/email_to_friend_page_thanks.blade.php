@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Email to Friend')])


<div class="listpgWraper">

    <div class="container">

        @include('flash::message')

        

            <div class="userfrnd">

                <div class="userccount">

                    <h5 class="text-center">{{__('Thank you')}} {{Session::get('data')['your_name']}}</h5>

                    <p>{{__('A copy of this job vacancy ('.$title.') has been emailed to a friend at ')}}{{Session::get('data')['friend_email']}}<br /><br /> <strong>Regards {{ $siteSetting->site_name }}</strong></p>

                </div>

            </div>

       

    </div>

</div>

@include('includes.footer')

@endsection