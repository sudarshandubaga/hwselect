@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Report Abuse')])

<!-- Inner Page Title end -->

<!-- Page Title End -->

<div class="listpgWraper">

    <div class="container">

        @include('flash::message')

        

            <div class="userfrnd">

                <div class="userccount"> {!! Form::open(array('method' => 'post', 'route' => ['report.abuse', $slug] ,'novalidate'=>'novalidate') ) !!}

                    <div class="formpanel"> 

                        <!-- Ad Information -->

                        <h5>{{__('Report Abuse')}}</h5>
                        
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{$error}}</div>
                            @endforeach
                        </div>
                        @endif

                        @php
                            $reasons = \App\Modules_data::whereHas('modules', function ($q) {
                                $q->where('slug', 'report-abuse-reasons');
                            })->pluck('title', 'title');
                            // $reasons['other'] = 'Other';
                        @endphp

                        <div class="form-group">
                          {{ Form::select('reason', $reasons, null, ['placeholder' => 'Select Reason', 'required' => 'required', 'class' => 'form-control', 'onchange' => 'checkreason(this.value)']) }}
                        </div>
                        
                        <div class="formrow">
                            {!! Form::textarea('message', '', ['placeholder' => 'Please enter reason for report.', 'class' => 'form-control', 'required' => 'required', 'id' => 'message', 'style' => 'display: none;']) !!}
                        </div>
                        
                        @if(Auth::check())

                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        
                        @elseif(Auth::guard('company')->check())

                            <input type="hidden" name="company_id" value="{{ Auth::guard('company')->user()->id }}">
                        
                        @endif

                        <input type="hidden" name="job_id" value="{{ $job->id }}">

                        <input type="submit" id="post_ad_btn" class="btn" value="{{__('Report')}}">

                    </div>

                </div>

            </div>

      

    </div>

</div>

@include('includes.footer')

<script>
function checkreason(reason) {
  if(reason === 'other') {
      $('#message').show();
  } else {
    $('#message').hide();
  }
}
</script>

@endsection