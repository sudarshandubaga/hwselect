@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end --> 
<!-- Inner Page Title start -->
@include('includes.inner_page_title', ['page_title'=>__('Contact Us')])
<!-- Inner Page Title end -->
<div class="inner-page"> 
    <!-- About -->
    <div class="container">
        <div class="contact-wrap">
            <div class="title"> <span>{{get_widget(14)->heading}}</span>
                {!!get_widget(14)->content!!}
            </div>            
                <!-- Contact Info -->
                <div class="contact-now">
				<div class="row"> 
                    <div class="col-lg-4 column">
                        <div class="contact"> <span><i class="fa fa-home"></i></span>
                            <div class="information"> <strong>{{__('Address')}}:</strong>
                                <p>{{ $siteSetting->site_street_address }}</p>
                            </div>
                        </div>
                    </div>
                    <!-- Contact Info -->
                    <div class="col-lg-4 column">
                        <div class="contact"> <span><i class="fa fa-envelope"></i></span>
                            <div class="information"> <strong>{{__('Email Address')}}:</strong>
                                <p><a href="mailto:{{ $siteSetting->mail_to_address }}">{{ $siteSetting->mail_to_address }}</a></p>
                            </div>
                        </div>
                    </div>
                    <!-- Contact Info -->
                    <div class="col-lg-4 column">
                        <div class="contact"> <span><i class="fa fa-phone"></i></span>
                            <div class="information"> <strong>{{__('Phone')}}:</strong>
                                <p><a href="tel:{{ $siteSetting->site_phone_primary }}">{{ $siteSetting->site_phone_primary }}</a></p>
                                <p><a href="tel:{{ $siteSetting->site_phone_secondary }}">{{ $siteSetting->site_phone_secondary }}</a></p>
                            </div>
                        </div>
                    </div>
                    <!-- Contact Info --> 
                </div>
					<div class="row"> 
                <div class="col-lg-4 column"> 
                    <!-- Google Map -->
                    <div class="googlemap">
                        {!! $siteSetting->site_google_map !!}
                    </div>
                </div>
                <!-- Contact form -->
                <div class="col-lg-8 column">
                    <div class="contact-form">
                        <div id="message"></div>
                        <?php 

                            $types = array('Company'=>'Company','Job Seeker'=>'Job Seeker');
                         ?>
                        <form method="post" action="{{ route('contact.us')}}" name="contactform" id="contactform">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12 cntsps{{ $errors->has('type') ? ' has-error' : '' }}">        
                                    <?php 
                                        // if(request()->type=='jobseekers'){
                                        //     $type = old('type')?old('type'):'Job Seeker';
                                        // }else if(request()->type=='recruiters'){
                                        //     $type = old('type')?old('type'):'Company';
                                        // }else{
                                        //     $type = old('type')?old('type'):'';
                                        // }
                                        $type = 'Company';


                                     ?>



                                    {!! Form::select('type', ['' => __('Select If you are a Company or a Job Seeker')]+$types, $type, array('class'=>'form-control', 'id'=>'type')) !!}     

                                     @if ($errors->has('type')) <span class="help-block"> <strong>{{ $errors->first('type') }}</strong> </span> @endif          
                                   
                                </div>


                                <div class="col-md-6 cntsps{{ $errors->has('full_name') ? ' has-error' : '' }}">                  
                                    {!! Form::text('full_name', null, array('id'=>'full_name', 'maxlength'=>'30', 'placeholder'=>__('Full Name'),  'autofocus'=>'autofocus')) !!}                
                                    @if ($errors->has('full_name')) <span class="help-block"> <strong>{{ $errors->first('full_name') }}</strong> </span> @endif
                                </div>
                                <div class="col-md-6 cntsps {{ $errors->has('email') ? ' has-error' : '' }}">                  
                                    {!! Form::text('email', null, array('id'=>'email', 'maxlength'=>'40', 'placeholder'=>__('Email'))) !!}                
                                    @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif
                                </div>
                                <div class="col-md-12 cntsps{{ $errors->has('phone') ? ' has-error' : '' }}">                  
                                    {!! Form::text('phone', null, array('id'=>'phone', 'maxlength'=>'16', 'placeholder'=>__('Phone Number'),"oninput"=>"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');")) !!}                
                                    @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif
                                </div>
                                <div class="col-md-6 cntsps{{ $errors->has('subject') ? ' has-error' : '' }}" style="display: none;">                  
                                    {!! Form::text('subject', null, array('id'=>'subject', 'maxlength'=>'30', 'placeholder'=>__('Subject'))) !!}                
                                    @if ($errors->has('subject')) <span class="help-block"> <strong>{{ $errors->first('subject') }}</strong> </span> @endif
                                </div>
                                <div class="col-md-12 cntsps{{ $errors->has('message_txt') ? ' has-error' : '' }}">                  
                                    {!! Form::textarea('message_txt', null, array('id'=>'message_txt', 'maxlength'=>'1000', 'placeholder'=>__('Message'))) !!}                
                                    @if ($errors->has('message_txt')) <span class="help-block"> <strong>{{ $errors->first('message_txt') }}</strong> </span> @endif
                                </div>
								 </div>

                                <div style="display: none;">
                                <h3>When is a good time to call you?</h3>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, '9am_12am') !!}">											
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','9am - 12pm', null, array('id'=>'9am_12am')) }}
											<i class="checkmark"></i> 9am - 12pm</span></strong>
                                        </div>                                    
									
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, '9am_12am') !!}">											
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','12pm - 3pm', null, array('id'=>'9am_12am')) }}
											<i class="checkmark"></i> 12pm - 3pm</span></strong>											
                                        </div>                                    
										
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, 'why_us_ads') !!}">
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','3 pm - 6 pm', null, array('id'=>'why_us_ads')) }}
											<i class="checkmark"></i> 3 pm - 6 pm</span></strong>											
                                        </div>                                    
										
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, 'jobs_ads') !!}"> 
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','6 pm - 9 pm', null, array('id'=>'jobs_ads')) }}
											<i class="checkmark"></i> 6 pm - 9 pm</span></strong>
                                        </div>
                                  
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, 'jobs_detail_ads') !!}">											
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','Any', null, array('id'=>'jobs_detail_ads')) }}
											<i class="checkmark"></i> Any</span></strong>											
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, 'contact_us_ads') !!}">
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','Call me back', null, array('id'=>'contact_us_ads')) }}
											<i class="checkmark"></i> Call me back</span></strong>
                                        </div>
                                    
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, 'blog_ads') !!}">
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','Please send me further information', null, array('id'=>'blog_ads')) }}
											<i class="checkmark"></i> Please send me further information</span></strong>											
                                        </div>
                                    
                                        <div class="form-time {!! APFrmErrHelp::hasError($errors, 'blog_details_ads') !!}">											
											<strong><span class="checkbtn">
											{{ Form::checkbox('best_time[]','Call me now', null, array('id'=>'blog_details_ads')) }}
											<i class="checkmark"></i> Call me now</span></strong>
                                        </div>
                                    </div>


                                   

                                    
                                </div>
							</div>
								<div class="row">
                                <div class="col-md-8{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                    {!! app('captcha')->display() !!}
                                    @if ($errors->has('g-recaptcha-response')) <span class="help-block"> <strong>{{ $errors->first('g-recaptcha-response') }}</strong> </span> @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="contactbtn">
                                    <button title="" class="button" type="submit" id="submit">{{__('Submit Now')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
					 </div>
        </div>
    </div>
</div>






@include('includes.footer')
@endsection


@push('scripts') 
<script>
// On textarea change

 $('#phone').on('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
  });

$('#message_txt').keyup(function(){
    // Get value of textarea
    var str = $('#message_txt').val();
    // Check if value has more than 20 words
    if (str.split(" ").length > 20) {
        // Create string with first 20 words
        var str_new = str.split(" ").splice(0, 20).join(" ");
        // Overwrite the content with the first 20 words
        $('#message_txt').val(str_new);
    }
});
</script>
@push('scripts') 