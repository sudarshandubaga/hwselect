@push('styles')
<link href="{{asset('admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin_assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
<style type="text/css">
    .error{color: red !important}
</style>
@endpush
<?php $country = App\Country::select('countries.flag', 'countries.id')->whereNotIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();

// dd($country);
    
    $country2 = App\Country::select('countries.flag', 'countries.id')->whereIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();

 ?>
{!! Form::model($company, array('method' => 'put', 'route' => array('update.company.profile'), 'class' => 'form', 'id'=>'profile-form', 'files'=>true)) !!}
<p class="toptxtcomp">Filling in your company details allows us to carefully match your requirements with suitable candidates</p>
<h5>{{__('Acount Information')}}</h5>

<div class="row">

<div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'email') !!}">

			<label>{{__('Email')}}</label>

			{!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email', 'readonly'=>'readonly', 'placeholder'=>__('Company Email'),'maxlength'=>50)) !!}

            {!! APFrmErrHelp::showErrors($errors, 'email') !!} </div>
	<p class="mb-0">To change email address, <a href="{{ route('contact.us') }}">Contact Support</a></p>

    </div>

</div>

<hr>





<h5>{{__('Company Information')}}</h5>
<p class="mt-0 mb-2">On adding or updating Profile Image, Click <strong>Save</strong> Button</p>
<div class="row">
<div class="col-md-6">
<div class="formrow ">
	

<div class="fileinput fileinput-new" data-provides="fileinput">
<div class="fileinput-preview thumbnail" style="max-width: 200px; max-height: 150px;">{{ImgUploader::print_image("company_logos/$company->logo", 200, 150) }} </div>
<div>
    <span style="width: auto;" class="btn default btn-file">
        <span class="fileinput-new"> Select Company Logo </span>
        <span class="fileinput-exists"> Change </span> 
        <input id="image" name="image" value="" type="file"> 
    </span> 
    <a style="width: auto; @if(!$company->logo) display: none;  @endif" href="javascript:;" class="btn red " onclick="remvoe_iamge()" data-dismiss="fileinput"> Remove </a>
    <a style="width: auto; display:none; opacity: 0;" href="javascript:; " class="btn btn-success save-image"> Save Image </a>
</div>
<p class="allowed_message font-weight-bold" style="margin-top: 4px; color: #888;">
    Image Formats Allowed: .jpg .jpeg .png .gif
</p>

<!-- Modal -->
<div class="modal fade" id="imageHelpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="d-flex align-items-center">
    <p class="m-0">Max Image Size 100 KB</p>
    <a href="#imageHelpModal" data-toggle="modal" class="ml-auto" >
        <i class="fa fa-question-circle-o"></i> Help
    </a>
</div>
</div>
</div>
</div>
</div>
<!-- <div class="row">

    <div class="col-md-6">

        <div class="formrow">

			<label>{{__('Company Logo')}}</label>

			{{ ImgUploader::print_image("company_logos/$company->logo", 100, 100) }} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow">

            <div id="thumbnail"></div>

            <label class="btn btn-default"> {{__('Select Company Logo')}}

                <input type="file" name="logo" id="logo" style="display: none;">

            </label>

            {!! APFrmErrHelp::showErrors($errors, 'logo') !!} </div>

    </div>

</div> -->

<div class="row">

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'name') !!}">

			<label>{{__('Company Name')}}</label>

			{!! Form::text('name', null, array('class'=>'form-control employer_name', 'id'=>'name', 'placeholder'=>__('Company Name'),'maxlength'=>50)) !!}

            {!! APFrmErrHelp::showErrors($errors, 'name') !!} </div>

    </div>

    

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'ceo') !!}">

			<label>{{__('Company Contact Name 1')}}</label>

			{!! Form::text('ceo', null, array('class'=>'form-control', 'id'=>'ceo', 'placeholder'=>__('Company Contact Name 1'),'maxlength'=>25)) !!}

            {!! APFrmErrHelp::showErrors($errors, 'ceo') !!} </div>

    </div>


    <div class="col-md-6">

        <div class="formrow {{ $errors->has('phone') ? ' has-error' : '' }}"> 
                                {!! Form::label('phone', '1st Point of Contact Phone Number', ['class' => 'bold']) !!}                         
                                <div class="step_1">
                                <div class="userphbox">
                                    <select class="form-control vodiapicker" value="{{null !==(old('phone_code'))?old('phone_code'):$company->phone_code}}" name="phone_code" id="phone_code">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('id',$key)->first();
                if(old('phone_code')){
                    $phone_code =  old('phone_code');
                }else{
                    $phone_code =  $company->phone_code;
                }
                

                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if($phone_code==$detail->sort_name ){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <div class="lang-select">
                <?php  
                if(old('phone_num_code')){
                    $phone_num_code =  old('phone_num_code');
                }else{
                    $phone_num_code =  $company->phone_num_code;
                }?>

                <a  class="btn-select cselect" value="{{old('phone_num_code')?old('phone_num_code'):$company->phone_num_code}}">
                    
                   <?php 

                    if($phone_num_code!=''){
                        $phone_detail = DB::table('countries_details')->where('phone_code',$phone_num_code)->first(); 
                    }else{
                       $phone_detail = DB::table('countries_details')->where('id',230)->first();  
                    }
                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('id',$phone_detail->country_id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>
                    @endif
                    
                    <i class="fa fa-caret-down"></i>

                </a>
                <input type="hidden" id="phone_num_code" name="phone_num_code" class="phonecode" value="{{old('phone_num_code')?old('phone_num_code'):'+'.$phone_detail->phone_code}}">
                <div class="b"> 
                    <input type="text" onkeyup="myFunction()" name="search" id="search" placeholder="Search"  style="width: 100px;">
                <ul id="a">
                    
                </ul>
                </div>
                </div>
              {!! Form::text('phone',null, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('1st Point of Contact Phone Number'), 'maxlength'=>18)) !!}
                           
                                  
                                  </div>
                              </div>
                              
                                
                                  
                                   
                                     @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif                      
                                                                      
                                </div>

    </div>


    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'ceo_2') !!}">

            <label>{{__('Company Contact Name 2')}}</label>

            {!! Form::text('ceo_2', null, array('class'=>'form-control', 'id'=>'ceo_2', 'placeholder'=>__('Company Contact Name 2'),'maxlength'=>25)) !!}

            {!! APFrmErrHelp::showErrors($errors, 'ceo_2') !!} </div>

    </div>




    <div class="col-md-6">

        <div class="formrow {{ $errors->has('ceo_number') ? ' has-error' : '' }}"> 
                                {!! Form::label('ceo_number_2', '2nd Point of Contact Phone', ['class' => 'bold']) !!}                                   
                               
                                <div class="step_ceo_number_1">
                                <div class="userphbox">
                                    <select class="form-control vodiapicker_phone_ceo_num" value="{{isset(session()->get('ceo_number_code2')['last_name'])?session()->get('ceo_number_code2')['ceo_number_code2']:old('ceo_number_code2')}}" name="ceo_number_code22" id="ceo_number_code22">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('ceo_number_code2')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('ceo_number_code2')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <div class="lang-select">
                 <?php  
                if(old('ceo_number_code2')){
                    $ceo_number_code2 =  old('ceo_number_code2');
                }else{
                    $ceo_number_code2 =  $company->ceo_number_code2;
                }?>
                <a  class="btn-select-phone_ceo_num2 cselect" value="{{old('ceo_number_code2')?old('ceo_number_code2'):$company->ceo_number_code2}}">
                    
                   <?php 
                   if(null!==($ceo_number_code2)){
                    $phone_detail = DB::table('countries_details')->where('phone_code',$ceo_number_code2)->first(); 
                  }else{
                    $phone_detail = DB::table('countries_details')->where('id',230)->first(); 
                  }


                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('id',$phone_detail->country_id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>
                    @endif
                    
                    <i class="fa fa-caret-down"></i>

                </a>
                <input type="hidden" id="ceo_number_code2" name="ceo_number_code2" class="phonecode" value="{{old('phone_num_code2')?old('ceo_number_code2'):$company->ceo_number_code2}}">
                <div class="b-phone_ceo_num2"> 
                    <input type="text" onkeyup="myFunction('a-phone_ceo_num2', 'search2')" name="search" id="search2" placeholder="Search"  style="width: 100px;">
                <ul id="a-phone_ceo_num2">
                    
                </ul>
                </div>
                </div>
                <?php
                // dd($company);
                if($company->ceo_number_2=='' || null==($company->ceo_number_2)){
                    $ceo_number_2 = '+44';
                }else{
                    $ceo_number_2 = $company->ceo_number_2;
                } ?>
              {!! Form::text('ceo_number_2', $ceo_number_2, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'ceo_number_2', 'placeholder'=>__('2nd Point of Contact Phone'), 'maxlength'=>18)) !!}
                           
                                  
                                  </div>
                              </div>
                              
                                
                                  
                                   
                                     @if ($errors->has('ceo_number')) <span class="help-block"> <strong>{{ $errors->first('ceo_number') }}</strong> </span> @endif                      
                                                                      
                                </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">

			<label>{{__('Industry')}}</label>

			{!! Form::select('industry_id', ['' => __('Select Industry')]+$industries, null, array('class'=>'form-control', 'id'=>'industry_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'ownership_type') !!}">

			<label>{{__('Ownership')}}</label>

			{!! Form::select('ownership_type_id', ['' => __('Select Ownership type')]+$ownershipTypes, null, array('class'=>'form-control', 'id'=>'ownership_type_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'ownership_type_id') !!} </div>

    </div>

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'description') !!}">

			<label>{{__('Brief description of company and values')}}</label>

			{!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>__('Company details'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'description') !!} </div>

    </div>

    

    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'no_of_offices') !!}">

			<label>{{__('No of Offices')}}</label>

			{!! Form::select('no_of_offices', ['' => __('Select num. of offices')]+MiscHelper::getNumOffices(), null, array('class'=>'form-control', 'id'=>'no_of_offices')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'no_of_offices') !!} </div>

    </div>

	<div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'no_of_employees') !!}">

			<label>{{__('No of Employees')}}</label>

			{!! Form::select('no_of_employees', ['' => __('Select num. of employees')]+MiscHelper::getNumEmployees(), null, array('class'=>'form-control', 'id'=>'no_of_employees')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'no_of_employees') !!} </div>

    </div>

	<div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'established_in') !!}">

			<label>{{__('Established In')}}</label>

			{!! Form::select('established_in', ['' => __('Select Established In')]+MiscHelper::getEstablishedIn(), null, array('class'=>'form-control', 'id'=>'established_in')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'established_in') !!} </div>

    </div>

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'website') !!}">

			<label>{{__('Website URL')}} Example: https://www.companywebsite.com </label>

			{!! Form::text('website', null, array('class'=>'form-control', 'id'=>'website', 'maxlength'=>'60', 'placeholder'=>__('Website'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'website') !!} </div>

    </div>

    

    @if($setting->show_social_links)

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'fax') !!}">

			<label>{{__('Fax')}}</label>

			{!! Form::text('fax', null, array('class'=>'form-control', 'id'=>'fax', 'maxlength'=>'18', 'placeholder'=>__('Fax'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'fax') !!} </div>

    </div>

    

    <div class="clearfix"></div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'facebook') !!}">

			<label>{{__('Facebook')}} <i class="fab fa-facebook"></i></label>

			{!! Form::text('facebook', null, array('class'=>'form-control', 'id'=>'facebook', 'placeholder'=>__('Facebook'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'facebook') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'twitter') !!}">

			<label>{{__('Twitter')}} <i class="fab fa-twitter"></i></label>

			{!! Form::text('twitter', null, array('class'=>'form-control', 'id'=>'twitter', 'placeholder'=>__('Twitter'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'twitter') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'linkedin') !!}">

			<label>{{__('LinkedIn')}} <i class="fab fa-linkedin"></i></label>

			{!! Form::text('linkedin', null, array('class'=>'form-control', 'id'=>'linkedin', 'placeholder'=>__('Linkedin'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'linkedin') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'google_plus') !!}">

			<label>{{__('Google Plus')}} <i class="fab fa-google-plus"></i></label>

			{!! Form::text('google_plus', null, array('class'=>'form-control', 'id'=>'google_plus', 'placeholder'=>__('Google+'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'google_plus') !!} </div>

    </div>

    <div class="col-md-6">
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'pinterest') !!}">
			<label>{{__('Pinterest')}} <i class="fab fa-pinterest"></i></label>
			{!! Form::text('pinterest', null, array('class'=>'form-control', 'id'=>'pinterest', 'placeholder'=>__('Pinterest'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'pinterest') !!}
        </div>
    </div>
    @endif
	</div>

	<div class="row">
        <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'location') !!}">

            <label>{{__('Full Company Address including post/zip code')}}</label>

            {!! Form::text('location', null, array('class'=>'form-control', 'id'=>'current_location', 'placeholder'=>__('Location'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'location') !!} </div>

    </div>
    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">

			<label>{{__('Country')}}</label>
            <?php $arra = array(
                230=>'United Kingdom',
                231=>'United States of America',
            ); ?>
			{!! Form::select('country_id', $arra+$countries, old('country_id', (isset($company))? $company->country_id:$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>

    </div>

    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">

			<label>{{__('County / State / Province')}}</label>

			<span id="default_state_dd"> {!! Form::select('state_id', ['' => __('Select State')], null, array('class'=>'form-control', 'id'=>'state_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>

    </div>

    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">

			<label>{{__('Town / City')}}</label>

			<span id="default_city_dd"> {!! Form::select('city_id', ['' => __('Select Town/City')], null, array('class'=>'form-control', 'id'=>'city_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>

    </div>

    <div class="col-md-12" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'map') !!}">

			<label>{{__('Google Map Iframe')}}</label>

			{!! Form::textarea('map', null, array('class'=>'form-control', 'id'=>'map', 'placeholder'=>__('Google Map'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'map') !!} </div>

    </div>

    <div class="col-md-12">

    <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_subscribed') !!}">

    <?php

	$is_checked = 'checked="checked"';	

	if (old('is_subscribed', ((isset($company)) ? $company->is_subscribed : 1)) == 0) {

		$is_checked = '';

	}

	?>

      <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />

      {{__('Subscribe to news letter')}}

      {!! APFrmErrHelp::showErrors($errors, 'is_subscribed') !!}

      </div>

  </div>

    <div class="col-md-12">

        <div class="formrow">

            <button type="submit" class="btn">{{__('Update Profile and Save')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>

        </div>

    </div>

</div>



{!! Form::close() !!}

<hr>

@push('styles')

<style type="text/css">

    .datepicker>div {

        display: block;

    }

</style>

@endpush

@push('scripts')


<script src="{{asset('admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

@include('includes.tinyMCEFront') 

<script type="text/javascript">
    $(document).on('blur', '#phone, #ceo_number_2', function () {
        let phone1 = $('#phone').val(),
            phone2 = $('#ceo_number_2').val();
        
        let self = this;

        let code = $(self).parent().find('.phonecode').val();

        if(phone1 != '' && phone1 == phone2) {
            swal('Warning!', 'Contact numbers cannot be duplicated', 'warning')
            .then(function(res) {
                if(res) {
                    $(self).val(code).focus();

                }

            });

        }
    });

    $( ".form" ).validate( {
      rules: {
        website: {
          url: true,
          normalizer: function( value ) {
            var url = value;
     
            // Check if it doesn't start with http:// or https:// or ftp://
            if ( url && url.substr( 0, 7 ) !== "http://"
                && url.substr( 0, 8 ) !== "https://"
                && url.substr( 0, 6 ) !== "ftp://" ) {
              // then prefix with http://
              url = "http://" + url;
            }
     
            // Return the new url
            return url;
          }
        }
      }
} );

    (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);
        return this.each(function() {
            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
})(jQuery);
$(function() {
    $('#image').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'bmp' , 'gif' , 'png' ],
        error: function() {
            // if($('#image').val()!=''){
            //     if (confirm('Confirm to remove image')) {
            //         return true;
            //     }else{
            //         return false;
            //     }
                
            // }
            if($('#image').val()!=''){
                alert('Image Formats Allowed: .jpg .jpeg .png .gif');
                $('#image').val('').trigger('change');
            }
            $('#image').val('');
            $('.save-image').hide();
        }
    });
});
    $('#image').on('change',function(){
        if($(this).val() && $(this).val != ''){
            $('.red').show();

            // $('.save-image').trigger('click');
        }else{
          $('.red').hide();  
        }
    })

    $(document).ready(function () {

        $('#country_id').on('change', function (e) {

            e.preventDefault();

            filterLangStates(0);

        });

        $(document).on('change', '#state_id', function (e) {

            e.preventDefault();

            filterLangCities(0);

        });

        filterLangStates(<?php echo old('state_id', (isset($company)) ? $company->state_id : 0); ?>);



        /*******************************/

        var fileInput = document.getElementById("image");

        fileInput.addEventListener("change", function (e) {

            var files = this.files;

            const size =  (this.files[0].size / 1024).toFixed(2); 

            // alert(size);
  
            if (size > 100) { 
                fileInput.value = null;
                $('#image').trigger('change');
                alert("A maximum image size of 100 KB allowed, please reduce image size and try again"); 
                return false;
            } else {
                $('.save-image').trigger('click');
                
            }

            showThumbnail(files)

        }, false)

    });



    function showThumbnail(files) {

        $('#thumbnail').html('');

        for (var i = 0; i < files.length; i++) {

            var file = files[i]

            var imageType = /image.*/

            if (!file.type.match(imageType)) {

                console.log("Not an Image");

                continue;

            }

            var reader = new FileReader()

            reader.onload = (function (theFile) {

                return function (e) {

                    $('#thumbnail').append('<div class="fileattached"><img height="100px" src="' + e.target.result + '" > <div>' + theFile.name + '</div><div class="clearfix"></div></div>');

                };

            }(file))

            var ret = reader.readAsDataURL(file);

        }

    }





    function filterLangStates(state_id)

    {

        var country_id = $('#country_id').val();

        if (country_id != '') {

            $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_state_dd').html(response);

                        filterLangCities(<?php echo old('city_id', (isset($company)) ? $company->city_id : 0); ?>);

                    });

        }

    }

    function filterLangCities(city_id)

    {

        var state_id = $('#state_id').val();

        if (state_id != '') {

            $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_city_dd').html(response);

                    });

        }

    }

     $('#phone').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

     $('#fax').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

     $('.phone_v').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

var langArray = [];

$('.vodiapicker option').each(function(){
  var img = $(this).attr("data-thumbnail");
  var val = $(this).attr("data-val");
  var text = this.innerText;
  var value = $(this).val();
  var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
  langArray.push(item);
})

// $('.vodiapicker_phone_ceo_num option').each(function(){
//   var img = $(this).attr("data-thumbnail");
//   var val = $(this).attr("data-val");
//   var text = this.innerText;
//   var value = $(this).val();
//   var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
//   langArray.push(item);
// })

$('#a').html(langArray);
$('#a-phone_ceo_num').html(langArray);
$('#a-phone_ceo_num2').html(langArray);

//Set the button value to the first el of the array


//change button stuff on click
$('#a li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fas fa-caret-down"></i></li>';
  $('.btn-select').html(item);
  $('.btn-select').attr('value', value);
  $('#phone').val(val);
  $('#phone_num_code').val(val);
  $(".b").toggle();
  //console.log(value);
});

$('#a-phone_ceo_num li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fas fa-caret-down"></i></li>';
  $('.btn-select-phone_ceo_num').html(item);
  $('.btn-select-phone_ceo_num').attr('value', value);
  $('#ceo_number').val(val);
  $('#ceo_number_code').val(val);
  $(".b-phone_ceo_num").toggle();
  //console.log(value);
});

$('#a-phone_ceo_num2 li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fas fa-caret-down"></i></li>';
  $('.btn-select-phone_ceo_num2').html(item);
  $('.btn-select-phone_ceo_num2').attr('value', value);
  $('#ceo_number_2').val(val);
  $('#ceo_number_code2').val(val);
  $(".b-phone_ceo_num2").toggle();
  //console.log(value);
});
$(document).click(function(){
  $(".b").hide();
  $(".b-phone_ceo_num2").hide();
});
$(".btn-select").click(function(e){
    e.stopPropagation();
    $(".b").toggle().toggleClass('show');
});
$('#search, #search2').click(function (e) {
    e.stopPropagation();
});
$(".btn-select-phone_ceo_num").click(function(e){
    $(".b-phone_ceo_num").toggle();
});
$(".btn-select-phone_ceo_num2").click(function(e){
    e.stopPropagation();
    $(".b-phone_ceo_num2").toggle();
});



 $('#mobile_num').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
function myFunction(sel_id = 'a', search_id = 'search') {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById(search_id);
    filter = input.value.toUpperCase();
    ul = document.getElementById(sel_id);
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
     $('.employer_name').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9,:\S/-:\S/&:\S/@:\S/£:\S/$:\S/€:\S/¥:\S/#:\S/.:\S/,:\S/::\S/;:\S/-]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });

function staticData(){
  var data=$("#phone");
  
    var code = $('#phone_num_code').val();

  if(data.val().length<3)
    {
     data.val(code);
     data.attr("readonly",true);
    }
    else
    {
     data.attr("readonly",false);
    }
}

$('#image').on('change',function(){
        if($(this).val() != '')
        $('.save-image').show();
    })
    $( '.save-image' ).on('click', function ( evt ) {
        evt.preventDefault();
        let myForm = document.getElementById('profile-form');
        var formData = new FormData(myForm);

        $.ajax( {
            type: 'POST',
            url: "{{route('update.company.image')}}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function ( data ) {
                $( '.save-image' ).hide();
                swal("Done", "Image Successfully Saved", "success");
            },
            error: function ( data ) {
                $( '#imagedisplay' ).html( "<h2>this file type is not supported</h2>" );
            }
        } );
    } );

</script> 

@endpush