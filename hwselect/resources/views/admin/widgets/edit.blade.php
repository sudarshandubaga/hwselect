@extends('admin.layouts.admin_layout')
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('widget') }}">Page Text</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Edit Page Text</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->        
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Page Text- {{$widget->location}}</span> </div>
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                        </ul>
                        {!! Form::model($widget, array('method' => 'post', 'route' => 'update.widget', 'class' => 'form', 'files'=>true)) !!}
                                    <input type="hidden" name="id" value="{{$widget->id}}">
                                        @if($widget->id==10)
                                            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'image') !!}">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">

                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> 
                                                        @if($widget->image)
                                                        {{ ImgUploader::print_image("company_logos/$widget->image") }}
                                                        @else
                                                        <img src="{{ asset('/') }}admin_assets/no-image.png" alt="" /> 
                                                        @endif

                                                    </div>

                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>

                                                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select Image </span> <span class="fileinput-exists"> Change </span> {!! Form::file('image', null, array('id'=>'image')) !!} </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>

                                                </div>

                                                {!! APFrmErrHelp::showErrors($errors, 'image') !!} </div>

                                        @endif
                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'title') !!}">
                                           	<label class="bold">Title <span>(Maximum Length 100 Characters)</span></label>
                                            
                                            {!! Form::text('heading', null, array('class'=>'form-control', 'id'=>'title','maxlength'=>100, 'placeholder'=>'Title')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'title') !!}

                                        </div>


                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'location') !!}">

                                            {!! Form::label('location', 'Location', ['class' => 'bold']) !!}

                                            {!! Form::text('location', null, array('class'=>'form-control', 'id'=>'location', 'placeholder'=>'Location')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'location') !!}

                                        </div>
                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'prev_link') !!}">

                                            {!! Form::label('prev_link', 'Link', ['class' => 'bold']) !!}

                                            {!! Form::text('prev_link', null, array('class'=>'form-control', 'id'=>'prev_link', 'placeholder'=>'Link')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'prev_link') !!}

                                        </div>
                                        @if($widget->id==10)
                                            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}">
											<label class="bold">Banner Bottom Section Text - Home page <span>(Maximum Length 250 Characters)</span></label>
												
                                            {!! Form::label('description', 'Description', ['class' => 'bold']) !!}

                                           {!! Form::text('content', null, array('class'=>'form-control', 'maxlength'=>250, 'placeholder'=>'Description')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'description') !!}

                                        </div>
                                        @else
                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}">

                                            {!! Form::label('description', 'Description', ['class' => 'bold']) !!}

                                            {!! Form::textarea('content', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>'Description')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'description') !!}

                                        </div>
                                        @endif

                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'button_1') !!}">
                                            {!! Form::label('button_1', 'Button 1', ['class' => 'bold']) !!}

                                            {!! Form::text('button_1', null, array('class'=>'form-control', 'id'=>'button_1', 'placeholder'=>'Button 1')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'button_1') !!}

                                        </div>
                                       
                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'button_2') !!}">
                                            {!! Form::label('button_2', 'Button 2', ['class' => 'bold']) !!}

                                            {!! Form::text('button_2', null, array('class'=>'form-control', 'id'=>'button_2', 'placeholder'=>'Button 2')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'button_2') !!}

                                        </div>


                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-2 float-right">Submit</button>
                                            </div>
                                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY --> 
</div>
@endsection

@push('scripts')
@include('admin.shared.tinyMCEFront') 
@endpush