@extends('admin.layouts.admin_layout')
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('widget') }}">Widget</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Add Widget</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->        
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Widget Form</span> </div>
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                        </ul>
                        <form class="form-horizontal form-material form" action="{{ route('create.widget') }}" method="post">
                                            @csrf


                                           <div class="form-group {!! APFrmErrHelp::hasError($errors, 'title') !!}">

                                            {!! Form::label('title', 'Title', ['class' => 'bold']) !!}

                                            {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Title')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'title') !!}

                                        </div>
                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'location') !!}">

                                            {!! Form::label('location', 'Location', ['class' => 'bold']) !!}

                                            {!! Form::text('location', null, array('class'=>'form-control', 'id'=>'location', 'placeholder'=>'Location')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'location') !!}

                                        </div>

                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'prev_link') !!}">

                                            {!! Form::label('prev_link', 'Link', ['class' => 'bold']) !!}

                                            {!! Form::text('prev_link', null, array('class'=>'form-control', 'id'=>'prev_link', 'placeholder'=>'Link')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'prev_link') !!}

                                        </div>
                                            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}">

                                            {!! Form::label('description', 'Description', ['class' => 'bold']) !!}

                                            {!! Form::textarea('content', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>'Description')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'description') !!}

                                        </div>

                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'button_1') !!}">
                                            {!! Form::label('button_1', 'Button 1', ['class' => 'bold']) !!}

                                            {!! Form::text('button_1', null, array('class'=>'form-control', 'id'=>'button_1', 'placeholder'=>'Button 1')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'button_1') !!}

                                        </div>
                                       
                                        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'button_2') !!}">
                                            {!! Form::label('button_2', 'Button 2', ['class' => 'bold']) !!}

                                            {!! Form::text('button_2', null, array('class'=>'form-control', 'id'=>'button_2', 'placeholder'=>'Button 2')) !!}

                                            {!! APFrmErrHelp::showErrors($errors, 'button_2') !!}

                                        </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-2 float-right">Submit</button>
                                            </div>
                                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY --> 
</div>
@endsection

@push('scripts')
@include('admin.shared.tinyMCEFront') 
@endpush