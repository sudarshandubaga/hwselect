@if (count($errors) > 0)
<div class="alert alert-danger">
    There were some problems with your input.
</div>
@foreach ($errors->all() as $error)
{{ $error }}<br/>
@endforeach
@endif
@include('flash::message')
<div class="form-body">
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'name') !!}">
        {!! Form::label('name', 'Name', ['class' => 'bold']) !!}                    
        {!! Form::text('name', null, array('required', 'class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Name')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'name') !!}                                       
    </div>

    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'email') !!}">
        {!! Form::label('email', 'Email Address', ['class' => 'bold']) !!}
        {!! Form::text('email', null, array('required', 'class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Email Address')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'email') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'password') !!}">
        {!! Form::label('password', 'Password', ['class' => 'bold']) !!}
        {!! Form::password('password', array('required', 'class'=>'form-control', 'maxlength'=>'16', 'placeholder'=>'Password')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'password') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'role_id') !!}">
        {!! Form::label('role', 'Role', ['class' => 'bold']) !!}
        {!! Form::select('role_id', ['' => 'Select a Role']+$roles, null, ['class' => 'form-control']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'role_id') !!}
    </div>

    <h4 style="font-weight: bold;text-align: left">Permissions</h4>
    <p>Select All <input type="checkbox" id="ckbCheckAll" /></p>
    <div class="row">
        @if(!empty($user))
            @if($user->role_id == 1)
            <div class="col-md-4">
                <div class="form-group {!! APFrmErrHelp::hasError($errors, 'admin_users') !!}">
                    
                    {{ Form::checkbox('admin_users',null, null, array('id'=>'admin_users')) }}
    				{!! Form::label('admin_users', 'Admin User', ['class' => 'bold']) !!}
                </div>
            </div>
            @endif
        @else
            <div class="col-md-4">
                <div class="form-group {!! APFrmErrHelp::hasError($errors, 'admin_users') !!}">
                    
                    {{ Form::checkbox('admin_users',null, null, array('id'=>'admin_users')) }}
                    {!! Form::label('admin_users', 'Admin User', ['class' => 'bold']) !!}
                </div>
            </div>
        @endif
        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job') !!}">
                
                {{ Form::checkbox('job',null, null, array('id'=>'job')) }}
				{!! Form::label('job', 'Jobs', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company') !!}">
                
                {{ Form::checkbox('company',null, null, array('id'=>'company')) }}
				{!! Form::label('company', 'Companies', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_user') !!}">
                
                {{ Form::checkbox('site_user',null, null, array('id'=>'site_user')) }}
				{!! Form::label('site_user', 'User Profiles', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'cms') !!}">
                
                {{ Form::checkbox('cms',null, null, array('id'=>'cms')) }}
				{!! Form::label('cms', 'C.M.S', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'blogs') !!}">
                
                {{ Form::checkbox('blogs',null, null, array('id'=>'blogs')) }}
				{!! Form::label('blogs', 'Manage Blogs', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'widget') !!}">
                
                {{ Form::checkbox('widget',null, null, array('id'=>'widget')) }}
				{!! Form::label('widget', 'Manage Widgets', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'seo') !!}">
                
                {{ Form::checkbox('seo',null, null, array('id'=>'seo')) }}
				{!! Form::label('seo', 'S.E.O', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'faq') !!}">
                
                {{ Form::checkbox('faq',null, null, array('id'=>'faq')) }}
				{!! Form::label('faq', 'FAQs', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'video') !!}">
                
                {{ Form::checkbox('video',null, null, array('id'=>'video')) }}
				{!! Form::label('video', 'Home Page Video', ['class' => 'bold']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'testimonial') !!}">
                
                {{ Form::checkbox('testimonial',null, null, array('id'=>'testimonial')) }}
				{!! Form::label('testimonial', 'Testimonials', ['class' => 'bold']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'slider') !!}">
                
                {{ Form::checkbox('slider',null, null, array('id'=>'slider')) }}
				{!! Form::label('slider', 'Sliders', ['class' => 'bold']) !!}
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
     $('input[type="checkbox"]').each(function() {
       this.value =  1;
     });
    $('input[type="checkbox"]').change(function(){
        this.value = this.checked ? 1 : 0;
    });

    $("#ckbCheckAll").click(function()
    {
        if(this.checked){
            $('input[type="checkbox"]').each(function(){
                $("input[type='checkbox']").prop('checked', true);
                $('span').addClass("checked");
                this.value =  1;
            });
        }else{
            $('input[type="checkbox"]').each(function(){
                $("input[type='checkbox']").prop('checked', false);
                $('span').removeClass("checked");
                this.value = 0;
            });
        }
    });
</script>
@endpush