@extends('admin.layouts.admin_layout')
@push('css')
<link rel="stylesheet" href="{{ asset('modules/blogs/css/blogs.css') }}">
@endpush
@section('content')
<?php
   $lang = config('default_lang');
   
   $lang = MiscHelper::getLang($lang);
   
   $direction = MiscHelper::getLangDirection($lang);
   
   $queryString = MiscHelper::getLangQueryStr();
   
   ?>
<style type="text/css">
   .table td,
   .table th {
   font-size: 12px;
   line-height: 2.42857 !important;
   }
   .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
</style>
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
   <ul class="page-breadcrumb">
      <li> <a>Add Blog</a> <i class="fa fa-circle"></i> </li>
      <li> <span>Blogs</span> </li>
   </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h3 class="page-title">Add New Post </h3>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
   <div class="col-lg-12">
      <div class="card m-b-30">
         <div class="card-body">
            <div class="">
               @if(session()->has('message.added'))
               <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                  {!! session('message.content') !!}
               </div>
               @endif
               @if(session()->has('message.updated'))
               <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                  {!! session('message.content') !!}
               </div>
               @endif
<div class="tab-content">
<div class="tab-pane active show" id="settings">
<div class="row">
<div class="col-lg-12">
<div class="">
<form method="POST" files="true"
action="{{ asset('admin/blog/create')}}"
enctype="multipart/form-data">
{{csrf_field()}}
<div class="row">
<div class="col-lg-9">
<div
class="form-group {{ $errors->has('lang') ? 'has-error' : '' }}">
<label class="control-label" for="lang">Select Language</label>
{!! Form::select('lang', ['' => 'Select Language']+$languages, $lang, array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
<span
class="text-danger">{{ $errors->first('lang') }}</span>
</div>
<div
class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
<label class="control-label" for="title">Title</label>
<input type="text" class="form-control" maxlength="60" name="title"
id="title" autofocus value="{{ old('title') }}">
<span
class="text-danger">{{ $errors->first('title') }}</span>
</div>
<div
class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
<label class="control-label" for="Slug">Slug</label>
<input type="text" class="form-control" maxlength="60" name="slug"
id="slug" autofocus value="{{ old('slug') }}">
<span
class="text-danger">{{ $errors->first('slug') }}</span>
</div>
<div
class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
<label class="control-label"
for="content">Content</label>
<textarea class="form-control" name="content"
id="description" cols="40" rows="5"
autofocus>{{ old('content') }}</textarea>
<span
class="text-danger">{{ $errors->first('content') }}</span>
</div>
<br /><br /><br />
<div class="clearfix"></div>
<div class="blogboxint">
<h3>SEO</h3>
<div id="div_show_seo_fields">
<div class="form-group">
<label class="control-label" for="title">Meta
Title</label>
<input type="text" maxlength="100" class="form-control"
   name="meta_title" id="meta_title" autofocus
   value="{{ old('meta_title') }}">
</div>
<div class="form-group">
<label class="control-label" for="title">Meta
Keywords</label>
<input type="text" maxlength="150" class="form-control"
   name="meta_keywords" id="meta_keywords"
   autofocus
   value="{{ old('meta_keywords') }}">
</div>
<div class="form-group">
<label class="control-label" for="title">Meta
Description</label>
<textarea class="form-control"
   name="meta_descriptions"
   id="meta_descriptions" maxlength="200" cols="40" rows="5"
   autofocus>{{ old('meta_description') }}</textarea>
</div>
</div>
</div>
</div>
<div class="col-lg-3">
<div class="blogboxint">
<input type="submit" value="Publish"
class="btn btn-primary">
<!--<input type="submit" value="Close" class="btn btn-warning"
data-dismiss="modal">-->
</div>
<div class="blogboxint">
<div class="form-group">
<label class="control-label" for="title">Appear on Home Page</label>
<ul class="optionlist">
<li>
   <input type="checkbox"
      name="appear_on_home_page"
      id="appear_on_home_page"
      <?php if(old('appear_on_home_page')=='on'){echo 'checked';} ?>>
   Appear on Home Page
</li>
</ul>
@if($errors->has('cate_id'))
<span
class="text-danger">{{ $errors->first('cate_id') }}</span>
@endif
</div>
</div>
<div class="blogboxint">
{{csrf_field()}}
@if($categories!='')
<div class="form-group">
<label class="control-label" for="title">Select
Category</label>
<ul class="optionlist">
@foreach($categories as $cate)
@php
$count = App\Blog::whereRaw("FIND_IN_SET('$cate->id',cate_id)")->orderBy('id', 'DESC')->count();
@endphp
<li>
   <input type="checkbox" name="cate_id[]"
      id="cate_id"
      value="<?php echo $cate->id; ?>">
   <label for="webdesigner"></label>
   {!!$cate->heading!!} <span class="badge badge-light">{{$count}}</span>  
</li>
@endforeach
</ul>
@if($errors->any())
<span
class="text-danger">{{$errors->first()}}</span>
@endif
</div>
@endif
</div>
<div class="blogboxint">
<div class="form-group">
<label class="control-label" for="Upload Image">Featured Image</label>
<input type="file" class="form-control" name="image" id="logo" required autofocus>
<p class="allowed_message" style="margin-top: 0;">Please upload  JPG, GIF or PNG</p>
<div id="thumbnail"></div>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .float-right .custom-control-label {
   color: #fff !important;
   }
</style>
@endsection
@push('scripts')
@include('admin.shared.tinyMCEFront') 
<script src="{{ asset('modules/blogs/js/blogs.js') }}"></script>
<script type="text/javascript">
   $(document).ready(function () {
   
   
   
   /*******************************/
   var fileInput = document.getElementById("logo");
   fileInput.addEventListener("change", function (e) {
   var files = this.files
   showThumbnail(files)
   }, false)
   });
   
   
   if ($("form").length > 0) {
   $("form").validate({
      validateHiddenInputs: true,
      ignore: "",
   
      rules: {
          lang: {
              required: true,
          },  
   
          title: {
              required: true,
              maxlength: 60,
          }, 
         
          slug: {
              required: true,
              maxlength: 60,
          },
   
          content: {
              required: true,
          },
   
          'cate_id[]': {
              required: true,
          },
   
          
      },
      messages: {
   
          lang: {
              required: "Language Required",
          },
   
   
          title: {
              required: "Title Required",
          }, 
   
           slug: {
              required: "Slug Required",
          }, 
   
          content: {
              required: "Content Required",
          },
   image: {
              required: "Image Required",
          },
   
          'cate_id[]': {
              required: "Select a Category for Blog",
          },
          
   
      },
   
   })
   }         
   
   function showThumbnail(files) {
   $('#thumbnail').html('');
   for (var i = 0; i < files.length; i++) {
   var file = files[i]
   var imageType = /image.*/
   if (!file.type.match(imageType)) {
      console.log("Not an Image");
      continue;
   }
   var reader = new FileReader()
   reader.onload = (function (theFile) {
      return function (e) {
          $('#thumbnail').append('<div class="fileattached"><img height="100px" src="' + e.target.result + '" > <div>' + theFile.name + '</div><div class="clearfix"></div></div>');
      };
   }(file))
   var ret = reader.readAsDataURL(file);
   }
   }

       (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {

                 

                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                const size =  
                   (this.files[0].size / 1024 / 1024).toFixed(2); 
      
                if (size > 1) { 
                    alert("A maximum image size of 1 MB allowed, please reduce image size and try again"); 
                    options.error();
                    $(this).focus();
                }else{
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }
                }    

                

            });

        });
    };

})(jQuery);

$(function() {
    $("input[type='file']").checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            $('.allowed_message').css("color", "#888");
        },
        error: function() {
            $('.allowed_message').css("color", "red");
            $('.allowed_message').html('Wrong format uploaded, please upload JPG, GIF or BMP, Maximum size 1 meg.');
            $("input[type='file']").val('');
        }
    });

});
   
</script>
<style type="text/css">
   #fea_img {
   border: 2px dashed #ddd;
   /* background: #2a2f3e; */
   padding: 50px 30px;
   text-align: center;
   }
   .jFiler-input {
   max-width: 401px;
   margin: 0 auto 15px auto !important;
   }
   .jFiler-items-grid .jFiler-item .jFiler-item-container {
   margin: 0 14px 30px 0;
   }
   .cropper-bg {
   background-image: none !important;
   height: 100% !important;
   }
   .img-crop {
   display: block;
   width: 100%;
   height: 100%;
   canvas {
   margin: 0 !important;
   }
   }
</style>
@endpush