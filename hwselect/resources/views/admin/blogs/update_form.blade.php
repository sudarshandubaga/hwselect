@extends('admin.layouts.admin_layout')

@push('css')

<link rel="stylesheet" href="{{ asset('modules/blogs/css/blogs.css') }}">



@endpush

@section('content')

<?php

$lang = config('default_lang');

if (isset($blog))

    $lang = $blog->lang;

$lang = MiscHelper::getLang($lang);

$direction = MiscHelper::getLangDirection($lang);

$queryString = MiscHelper::getLangQueryStr();

?>

<style type="text/css">

.table td,

.table th {

    font-size: 12px;

    line-height: 2.42857 !important;

}

.dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }

</style>

<div class="page-content-wrapper">

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a>Update Blog</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Blogs</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR -->

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Update Post </h3>

        <!-- END PAGE TITLE-->

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-lg-12">

                <div class="card m-b-30">

                    <div class="card-body">

                        <div class="">



                           @if(session()->has('message.added'))



                            <div class="alert alert-success fade in">

                                <a href="#" class="close" data-dismiss="alert">&times;</a>

                                {!! session('message.content') !!}

                            </div>

                            @endif

                            @if(session()->has('message.updated'))

                            <div class="alert alert-success fade in">

                                <a href="#" class="close" data-dismiss="alert">&times;</a>

                                {!! session('message.content') !!}

                            </div>

                            @endif





                            <div class="tab-content">

<div class="tab-pane active show" id="settings">

<div class="row">

<div class="col-lg-12">

<div class="">



<form method="POST" files="true"

action="{{ asset('/admin/blog/update')}}"

enctype="multipart/form-data">

{{csrf_field()}}



<input type="hidden" value="{{ $blog-> id }}" name="id" id="id">
<div class="blogboxint" style="margin-bottom: 20px;text-align: right; margin-right: 10px;">
<a  class="btn btn-warning" href="{{route('blog')}}">Manage Blog Pages</a>
<a  class="btn btn-danger" href="{{route('delete-blog',$blog->id)}}"><i class="fa fa-trash">&nbsp Delete</i></a>
<a  class="btn btn-primary" href="{{route('blog-detail',$blog->slug)}}" target="_blank"><i class="fa fa-eye">&nbsp Preview</i></a>
<input type="submit" value="Publish"

    class="btn btn-primary">

<!--<input type="submit" value="Close" class="btn btn-warning"

    data-dismiss="modal">-->

</div>


<div class="row">

<div class="col-lg-9">

<div

class="form-group {{ $errors->has('lang') ? 'has-error' : '' }}">

<label class="control-label" for="lang">Select Language</label>



{!! Form::select('lang', ['' => 'Select Language']+$languages, $lang, array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}

<span

    class="text-danger">{{ $errors->first('lang') }}</span>



</div>

<div

class="form-group {{ $errors->has('title_update') ? 'has-error' : '' }}">

<label class="control-label" for="title">Title</label>



<input type="text" class="form-control"

    name="title_update" maxlength="60" id="title_update" autofocus

    value="{{ $blog-> heading }}">

<span

    class="text-danger">{{ $errors->first('title_update') }}</span>



</div>



<div

class="form-group {{ $errors->has('slug_update') ? 'has-error' : '' }}">

<label class="control-label"

    for="Slug_update">Slug</label>



<input type="text" class="form-control"

    name="slug_update" maxlength="60" id="slug_update" autofocus

    value="{{ $blog-> slug }}">

<span

    class="text-danger">{{ $errors->first('slug_update') }}</span>



</div>

<div

class="form-group {{ $errors->has('content_update') ? 'has-error' : '' }}">

<label class="control-label"

    for="content">Content</label>



<textarea class="form-control" name="content_update"

    id="description" cols="40" rows="5"

    autofocus>{{ $blog->content }}</textarea>

<span

    class="text-danger">{{ $errors->first('content_update') }}</span>



</div>

<br /><br /><br />

<div class="clearfix"></div>

<div class="blogboxint">

<h3>SEO</h3>

<div id="div_show_seo_fields">



    <div class="form-group">

        <label class="control-label" for="title">Meta

            Title</label>



        <input type="text" maxlength="100" class="form-control"

            name="meta_title_update"

            id="meta_title_update" autofocus

            value="{{ $blog->meta_title }}">



    </div>

    <div class="form-group">

        <label class="control-label" for="title">Meta

            Keywords</label>



        <input type="text" maxlength="150" class="form-control"

            name="meta_keywords_update"

            id="meta_keywords_update" autofocus

            value="{{ $blog->meta_keywords }}">



    </div>

    <div class="form-group">

        <label class="control-label" for="title">Meta

            Description</label>



        <textarea class="form-control"

            name="meta_descriptions_update"

            id="meta_descriptions_update" maxlength="200" cols="40"

            rows="5"

            autofocus>{{ $blog->meta_descriptions }}</textarea>



    </div>

</div>

</div>

</div>



<div class="col-lg-3">







<div class="blogboxint">



<div class="form-group">

    <label class="control-label" for="title">Appear on Home Page</label>



    <ul class="optionlist">

      

        <li>

            <input type="checkbox"

                name="appear_on_home_page"

                id="appear_on_home_page"
                <?php if($blog->appear_on_home_page=='on' || old('appear_on_home_page')=='on'){echo 'checked';} ?>>
            Appear on Home Page</li>





    </ul>
    @if($errors->any())
    <span

    class="text-danger">{{$errors->first()}}</span>
    @endif




</div>

</div>


<div class="blogboxint">

{{csrf_field()}}



<?php

$cate_ids = explode(',', $blog->cate_id);

?>

@if($categories!='')

<div class="form-group" style="padding-bottom: 15px;">

    <label class="control-label" for="title">Select

        Category</label>



    <ul class="optionlist">

        @foreach($categories as $cate)
        @php
        $count = App\Blog::whereRaw("FIND_IN_SET('$cate->id',cate_id)")->orderBy('id', 'DESC')->count();
        @endphp
        <li>

            <input type="checkbox"

                name="cate_id_update[]"

                id="cate_id_update"

                value="<?php echo $cate->id; ?>"

                <?php if (in_array($cate->id, $cate_ids)) {

                                                                                                                    echo 'checked';

                                                                                                                } ?>>

            <label for="webdesigner"></label>

            {!!$cate->heading!!} <span class="badge badge-light">{{$count}}</span></li>



        @endforeach





    </ul>
    @if($errors->has('cate_id'))
    <span

    class="text-danger">{{ $errors->first('cate_id') }}</span>
    @endif




</div>

@endif

</div>



<div class="blogboxint">

<div class="form-group">

<label class="control-label" for="Upload Image">Post Created On: {{ date('d-M-Y',strtotime($blog->created_at)) }}</label>


</div>

</div>



<div class="blogboxint">

<div class="form-group">

    <label class="control-label"

        for="Upload Image">Featured Image</label>



    <input type="file" class="form-control"

        name="imageupdate" id="logo" autofocus>
<p class="allowed_message" style="margin-top: 0;">Please upload  JPG, GIF or PNG</p>
        <br>
    <div id="thumbnail"> @if($blog->image!='')

        <img  src="{{asset('uploads/blogs/')}}/{{$blog->image}}">
		
		<i onClick='remove_blog_featured_image("{{$blog->id}}");' class='fa fa-times'></i> <span>Remove Image</span>

        @endif</div>

</div>

</div>

</div>

</div>











</form>

</div>

</div>

</div>

</div>

                            </div>

                        </div>

                    </div>

                </div>



            </div>

        </div>



        <style type="text/css">

        .float-right .custom-control-label {

            color: #fff !important;

        }

        </style>

        @endsection

        @push('scripts')
        @include('admin.shared.tinyMCEFront') 
        <script src="{{ asset('modules/blogs/js/blogs.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function () {
       
        

        /*******************************/
        var fileInput = document.getElementById("logo");
        fileInput.addEventListener("change", function (e) {
            var files = this.files
            showThumbnail(files)
        }, false)
    });
    if ($("form").length > 0) {
            $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    lang: {
                        required: true,
                    },  

                    title_update: {
                        required: true,
                        maxlength: 60,
                    }, 
                   
                    slug_update: {
                        required: true,
                        maxlength: 60,
                    },

                    content_update: {
                        required: true,
                    },

                    'cate_id_update[]': {
                        required: true,
                    },


                    
                },
                messages: {

                    lang: {
                        required: "Language Required",
                    },


                    title_update: {
                        required: "Title Required",
                    }, 

                     slug_update: {
                        required: "Slug Required",
                    }, 

                    content_update: {
                        required: "Content Required",
                    },

                    'cate_id_update[]': {
                        required: "Select a Category for Blog",
                    },

                    

                },
            
            })
        }         
    function showThumbnail(files) {
        $('#thumbnail').html('');
        for (var i = 0; i < files.length; i++) {
            var file = files[i]
            var imageType = /image.*/
            if (!file.type.match(imageType)) {
                console.log("Not an Image");
                continue;
            }
            var reader = new FileReader()
            reader.onload = (function (theFile) {
                return function (e) {
                    $('#thumbnail').html('');
                    $('#thumbnail').append('<div class="fileattached"><img height="100px" src="' + e.target.result + '" > <div>' + theFile.name + '</div><div class="clearfix"></div></div>');
                };
            }(file))
            var ret = reader.readAsDataURL(file);
        }
    }

      (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {

                 

                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                const size =  
                   (this.files[0].size / 1024 / 1024).toFixed(2); 
      
                if (size > 1) { 
                    alert("A maximum image size of 1 MB allowed, please reduce image size and try again"); 
                    options.error();
                    $(this).focus();
                }else{
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }
                }    

                

            });

        });
    };

})(jQuery);

$(function() {
    $("input[type='file']").checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            $('.allowed_message').css("color", "#888");
        },
        error: function() {
            $('.allowed_message').css("color", "red");
            $('.allowed_message').html('Wrong format uploaded, please upload JPG, GIF or BMP, Maximum size 1 meg.');
            $("input[type='file']").val('');
        }
    });

});
        </script>
        <style type="text/css">

        #fea_img {

            border: 2px dashed #ddd;

            /* background: #2a2f3e; */

            padding: 50px 30px;

            text-align: center;

        }



        .jFiler-input {

            max-width: 401px;

            margin: 0 auto 15px auto !important;

        }



        .jFiler-items-grid .jFiler-item .jFiler-item-container {

            margin: 0 14px 30px 0;

        }



        .cropper-bg {

            background-image: none !important;

            height: 100% !important;

        }



        .img-crop {

            display: block;

            width: 100%;

            height: 100%;



            canvas {

                margin: 0 !important;

            }

        }

        </style>

        @endpush