{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort bonuss</h3>
    {!! Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'refreshbonusSortData();')) !!}
    <div id="bonusSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshbonusSortData();
    });
    function refreshbonusSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('bonus.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#bonusSortDataDiv").html('');
                $("#bonusSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var bonusOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('bonus.sort.update') }}", {bonusOrder: bonusOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
