{!! APFrmErrHelp::showErrorsNotice($errors) !!}

@include('flash::message')

<div class="form-body">


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'linkedin_app_id') !!}">

        {!! Form::label('linkedin_app_id', 'Linkedin App ID', ['class' => 'bold']) !!}

        {!! Form::text('linkedin_app_id', null, array('class'=>'form-control', 'id'=>'linkedin_app_id', 'placeholder'=>'Linkedin App ID')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'linkedin_app_id') !!}

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'linkedin_app_secret') !!}">

        {!! Form::label('linkedin_app_secret', 'Linkedin App Secret', ['class' => 'bold']) !!}

        {!! Form::text('linkedin_app_secret', null, array('class'=>'form-control', 'id'=>'linkedin_app_secret', 'placeholder'=>'Linkedin App Secret')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'linkedin_app_secret') !!}

    </div>

</div>