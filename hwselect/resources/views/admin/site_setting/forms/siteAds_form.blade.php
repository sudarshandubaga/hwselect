{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">  
         
	
	
	<table class="table table-striped">
  <tbody>
	<tr>
      <td width="50"><div class="form-group {!! APFrmErrHelp::hasError($errors, 'index_page_below_top_employes_ads') !!}">
       
        {{ Form::checkbox('index_page_below_top_employes_ads',null, null, array('id'=>'index_page_below_top_employes_ads')) }}


        
    </div> </td>
      <td> {!! Form::label('index_page_below_top_employes_ads', 'Show Index Page Below Top Employees', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="{{url('/')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr> 
	  
	  
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'index_page_below_cities_ads') !!}">                
                {{ Form::checkbox('index_page_below_cities_ads',null, null, array('id'=>'index_page_below_cities_ads')) }}               
            </div></td>
      <td>{!! Form::label('index_page_below_cities_ads', 'Show Index Page Below Cities', ['class' => 'bold']) !!}</td>
      <td> <a style="font-size: 12px;" href="{{url('/')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'hire_it_talent_ads') !!}">
                
                {{ Form::checkbox('hire_it_talent_ads',null, null, array('id'=>'hire_it_talent_ads')) }}
                
            </div></td>
      <td>{!! Form::label('hire_it_talent_ads', 'Show Hire IT Talent', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="{{url('/hire-it-talent')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'why_us_ads') !!}">               
                {{ Form::checkbox('why_us_ads',null, null, array('id'=>'why_us_ads')) }}                
         </div></td>
      <td>{!! Form::label('why_us_ads', 'Show Why Us Ads', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="{{url('/why-us-for-Jobs')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a></td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'jobs_ads') !!}">
                
                {{ Form::checkbox('jobs_ads',null, null, array('id'=>'jobs_ads')) }}
                
            </div></td>
      <td>{!! Form::label('jobs_ads', 'Show Jobs Ads', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="{{url('/jobs')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'jobs_detail_ads') !!}">
               
                {{ Form::checkbox('jobs_detail_ads',null, null, array('id'=>'jobs_detail_ads')) }}
                
            </div></td>
      <td> {!! Form::label('jobs_detail_ads', 'Show Job  Detail Ads', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'contact_us_ads') !!}">
               
                {{ Form::checkbox('contact_us_ads',null, null, array('id'=>'contact_us_ads')) }}
               
            </div></td>
      <td> {!! Form::label('contact_us_ads', 'Show Contact Us Ads', ['class' => 'bold']) !!}</td>
      <td> <a style="font-size: 12px;" href="{{url('/contact-us')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'blog_ads') !!}">
               
                {{ Form::checkbox('blog_ads',null, null, array('id'=>'blog_ads')) }}
                 
            </div></td>
      <td> {!! Form::label('blog_ads', 'Show Blog Ads', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="{{url('/blog')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a></td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'blog_details_ads') !!}">
                
                {{ Form::checkbox('blog_details_ads',null, null, array('id'=>'blog_details_ads')) }}
               
            </div></td>
      <td>{!! Form::label('blog_details_ads', 'Show Blog Details Ads', ['class' => 'bold']) !!}</td>
      <td> <span>Managed Dynamic</span> </td>
    </tr>
    <tr>
      <td> <div class="form-group {!! APFrmErrHelp::hasError($errors, 'login_ads') !!}">
               
                {{ Form::checkbox('login_ads',null, null, array('id'=>'login_ads')) }}
               
            </div></td>
      <td> {!! Form::label('login_ads', 'Show Login Ads', ['class' => 'bold']) !!}</td>
      <td> <a style="font-size: 12px;" href="{{url('/login')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
    <tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'register_ads') !!}">
               
                {{ Form::checkbox('register_ads',null, null, array('id'=>'register_ads')) }}
               
            </div></td>
      <td> {!! Form::label('register_ads', 'Show Register Ads', ['class' => 'bold']) !!}</td>
      <td> <a style="font-size: 12px;" href="{{url('/register')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a> </td>
    </tr>
	  
	<tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'dashboard_below_menu_verticle_ads') !!}">        
        {{ Form::checkbox('dashboard_below_menu_verticle_ads',null, null, array('id'=>'dashboard_below_menu_verticle_ads')) }}       
    </div> </td>
      <td>{!! Form::label('dashboard_below_menu_verticle_ads', 'Show Dashboard Below Menu Vertical Ad', ['class' => 'bold']) !!}</td>
      <td> <span>Managed Dynamic</span></td>
    </tr>
	<tr>
      <td> <div class="form-group {!! APFrmErrHelp::hasError($errors, 'cms_page_below_content_ads') !!}">
        
        {{ Form::checkbox('cms_page_below_content_ads',null, null, array('id'=>'cms_page_below_content_ads')) }}
       
    </div> </td>
      <td>{!! Form::label('cms_page_below_content_ads', 'Show CMS Page Below Content Ad', ['class' => 'bold']) !!}</td>
      <td> <span>Managed Dynamic</span></td>
    </tr>
	<tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'listing_page_sidebar_verticle_ads') !!}">
        
        {{ Form::checkbox('listing_page_sidebar_verticle_ads',null, null, array('id'=>'listing_page_sidebar_verticle_ads')) }}
         
    </div> </td>
      <td>{!! Form::label('listing_page_sidebar_verticle_ads', 'Show Listing Page Sidebar Vertical Ad', ['class' => 'bold']) !!}</td>
      <td><a style="font-size: 12px;" href="{{url('jobs')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a></td>
    </tr>
	<tr>
      <td><div class="form-group {!! APFrmErrHelp::hasError($errors, 'listing_page_below_listings_horizantal_ads') !!}">
       
        {{ Form::checkbox('listing_page_below_listings_horizantal_ads',null, null, array('id'=>'listing_page_below_listings_horizantal_ads')) }}
        
    </div> </td>
      <td> {!! Form::label('listing_page_below_listings_horizantal_ads', 'Show Listing Page Below Listings Horizontal Ad', ['class' => 'bold']) !!}</td>
      <td> <a style="font-size: 12px;" href="{{url('jobs')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a></td>
    </tr>
	
	  
	  
	  
  </tbody>
</table>

	
	
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'index_page_below_top_employes_ad') !!}">
        {!! Form::label('index_page_below_top_employes_ad', 'Index Page Below Top Employees', ['class' => 'bold']) !!}                    
        {!! Form::textarea('index_page_below_top_employes_ad', null, array('class'=>'form-control', 'id'=>'index_page_below_top_employes_ad', 'placeholder'=>'Index Page Below Top Employes')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'index_page_below_top_employes_ad') !!}    
<br>
        <a style="font-size: 12px;" href="{{url('/')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a>                                    
    </div>  
    
	
<hr>
       
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'above_footer_ad') !!}">
        {!! Form::label('above_footer_ad', 'Index Page Below Cities', ['class' => 'bold']) !!}                    
        {!! Form::textarea('above_footer_ad', null, array('class'=>'form-control', 'id'=>'above_footer_ad', 'placeholder'=>'Index Page Below Cities')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'above_footer_ad') !!}   
		<br>
        <a style="font-size: 12px;" href="{{url('/')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a>                                    
    </div> 
    <hr>

        
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'dashboard_page_ad') !!}">
        {!! Form::label('dashboard_page_ad', 'Dashboard Below Menu Vertical Ad', ['class' => 'bold']) !!}                    
        {!! Form::textarea('dashboard_page_ad', null, array('class'=>'form-control', 'id'=>'dashboard_page_ad', 'placeholder'=>'Dashboard Below Menu Verticle Ad')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'dashboard_page_ad') !!}   
        <span>Managed Dynamic</span>                                   
    </div> 

    <hr>

       
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'cms_page_ad') !!}">
        {!! Form::label('cms_page_ad', 'CMS Page Below Content Ad', ['class' => 'bold']) !!}                    
        {!! Form::textarea('cms_page_ad', null, array('class'=>'form-control', 'id'=>'cms_page_ad', 'placeholder'=>'CMS Page Below Content Ad')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'cms_page_ad') !!}  
        <span>Managed Dynamic</span>                                    
    </div> 

    <hr>

       
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'listing_page_vertical_ad') !!}">
        {!! Form::label('listing_page_vertical_ad', 'Listing Page Sidebar Vertical Ad', ['class' => 'bold']) !!}                    
        {!! Form::textarea('listing_page_vertical_ad', null, array('class'=>'form-control', 'id'=>'listing_page_vertical_ad', 'placeholder'=>'Listing Page Sidebar Verticle Ad')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'listing_page_vertical_ad') !!}        
		<br>
        <a style="font-size: 12px;" href="{{url('jobs')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a>                              
    </div>

    <hr>

        
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'listing_page_horizontal_ad') !!}">
        {!! Form::label('listing_page_horizontal_ad', 'Listing Page Below Listings Horizontal Ad', ['class' => 'bold']) !!}                    
        {!! Form::textarea('listing_page_horizontal_ad', null, array('class'=>'form-control', 'id'=>'listing_page_horizontal_ad', 'placeholder'=>'Listing Page Below Listings Horizantal Ad')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'listing_page_horizontal_ad') !!}            
		<br>
        <a style="font-size: 12px;" href="{{url('jobs')}}" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp Preview</a>                         
    </div>
</div>
@push('scripts')
<script type="text/javascript">
     $('input[type="checkbox"]').each(function() {
       this.value =  1;
     });
    $('input[type="checkbox"]').change(function(){
        this.value = this.checked ? 1 : 0;
    });
</script>
@endpush
