{!! APFrmErrHelp::showOnlyErrorsNotice($errors) !!}
<?php $country = App\Country::select('countries.flag', 'countries.id')->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();
    
    $country2 = []; // App\Country::select('countries.flag', 'countries.id')->whereIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();

 ?>
@include('flash::message')

<p style="font-weight: 700; font-size: 16px;">All fields marked with <strong style="color: red;">*</strong> are mandatory</p>

<div class="form-body">

    <div class="row">

        <div class="col-md-6">

            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'logo') !!}">

                <div class="fileinput fileinput-new" data-provides="fileinput">

                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						
						 @if(isset($company))

            {{ ImgUploader::print_image("company_logos/$company->logo") }}      

           
@else
          
						<img src="{{ asset('/') }}admin_assets/no-image.png" alt="" /> 
					@endif
					
					</div>

                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>

                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Add Company Logo </span> <span class="fileinput-exists"> Change </span> {!! Form::file('logo', null, array('id'=>'logo')) !!} </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
<p class="allowed_message">Image Formats Allowed: .jpg .jpeg .png .gif</p>
<p class="allowed_message">Max Image Size 100kb</p>
                </div>

                {!! APFrmErrHelp::showErrors($errors, 'logo') !!} </div>

        </div>

       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'name') !!}">
		
		{!! Form::label('name', 'Company Name', ['class' => 'bold']) !!} <strong style="color: red;">*</strong>

        {!! Form::text('name', null, array('class'=>'form-control', 'id'=>'name', 'placeholder'=>'Company Name','maxlength'=>50)) !!}

        {!! APFrmErrHelp::showErrors($errors, 'name') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'email') !!}">
		{!! Form::label('email', 'Company Email', ['class' => 'bold']) !!} <strong style="color: red;">*</strong>

        {!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Company Email','maxlength'=>50)) !!}

        {!! APFrmErrHelp::showErrors($errors, 'email') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'password') !!}"> {!! Form::label('password', 'Password', ['class' => 'bold']) !!} <strong style="color: red;">*</strong>

        {!! Form::password('password', array('class'=>'form-control', 'id'=>'password', 'placeholder'=>'Password','maxlength'=>30)) !!}

        {!! APFrmErrHelp::showErrors($errors, 'password') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ceo') !!}"> {!! Form::label('ceo', 'Company Contact Name 1', ['class' => 'bold']) !!} <strong style="color: red;">*</strong>

        {!! Form::text('ceo', null, array('class'=>'form-control', 'id'=>'ceo', 'maxlength'=>'25', 'placeholder'=>'Contact Name 1')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'ceo') !!} </div>

    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}"> 
        {!! Form::label('phone', '1st Point of Contact Phone Number', ['class' => 'bold']) !!}   <strong style="color: red;">*</strong>                
        <div class="step_1">
            <div class="userphbox">
                <select class="form-control vodiapicker" value="{{isset(session()->get('seeker_form_data')['last_name'])?session()->get('seeker_form_data')['last_name']:old('last_name')}}" name="phone_code" id="phone_code">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <div class="lang-select">
                
                <a  class="btn-select cselect" value="{{old('phone_num_code')?old('phone_num_code'):null}}">
                    
                   <?php 

                    $phone_detail = DB::table('countries_details')->where('id',230)->first(); 
                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>
                    @endif
                    
                    <i class="fa fa-caret-down"></i>

                </a>
                <input type="hidden" id="phone_num_code" name="phone_num_code" value="{{old('phone_num_code')?old('phone_num_code'):'+'.$phone_detail->phone_code}}">
                <div class="b"> 
                    <input type="text" onkeyup="myFunction(this, 'a')" name="search" id="search" placeholder="Search"  style="width: 100px;">
                <ul id="a">
                    
                </ul>
                </div>
                </div>

                 <?php if(!isset($company) || $company->phone=='' || null==($company->phone)){
                    $phone = '+44';
                }else{
                    $phone = $company->phone;
                } ?>
              {!! Form::text('phone',$phone, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('1st Point of Contact Phone Number'), 'maxlength'=>15)) !!}
                           
                                  
                                  </div>
                              </div>
                              
                                
                                  
                                   
                                     @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif                      
                                                                      
                                </div>


        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ceo') !!}"> {!! Form::label('ceo', 'Company Contact Name 2', ['class' => 'bold']) !!}

        {!! Form::text('ceo_2', null, array('class'=>'form-control', 'id'=>'ceo_2', 'maxlength'=>'25', 'placeholder'=>'Contact Name 2')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'ceo_2') !!} </div>


        <div class="form-group {{ $errors->has('ceo_number') ? ' has-error' : '' }}"> 
            {!! Form::label('ceo_number_2', '2nd Point of Contact Phone Number', ['class' => 'bold']) !!}                                   
            
            <div class="step_ceo_number_1">
            <div class="userphbox">
                <select class="form-control vodiapicker_phone_ceo_num" value="{{isset(session()->get('ceo_number_code2')['last_name'])?session()->get('ceo_number_code2')['ceo_number_code2']:old('ceo_number_code2')}}" name="ceo_number_code2" id="ceo_number_code2">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('ceo_number_code2')==$detail->sort_name) { echo 'selected'; } ?> value="{{ $detail->sort_name }}" data-val="+{{ $detail->phone_code }}" data-thumbnail="{{ asset('flags/') }}/{{$count2}}"> {{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('ceo_number_code2')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <div class="lang-select">
                
                <a  class="btn-select-phone_ceo_num2 cselect" value="{{old('ceo_number_code2')?old('ceo_number_code2'):null}}">
                    
                   <?php 

                    $phone_detail = DB::table('countries_details')->where('country_id',230)->first(); 
                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>
                    @endif
                    
                    <i class="fa fa-caret-down"></i>

                </a>
                <input type="hidden" id="ceo_number_code2" name="phone_num_code2" value="{{old('phone_num_code2')?old('ceo_number_code2'):'+'.$phone_detail->phone_code}}">
                <div class="b-phone_ceo_num2"> 
                    <input type="text" onkeyup="myFunction(this, 'a-phone_ceo_num2')" name="search" id="search2" placeholder="Search"  style="width: 100px;">
                <ul id="a-phone_ceo_num2">
                    
                </ul>
                </div>
                </div>
                <?php if(!isset($company) || $company->ceo_number=='' || null==($company->ceo_number)){
                    $ceo_number = '+44';
                }else{
                    $ceo_number = $company->ceo_number;
                } ?>
              {!! Form::text('ceo_number_2', $ceo_number, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'ceo_number_2', 'placeholder'=>__('2nd Point of Contact Phone Number'), 'maxlength'=>15)) !!}
                           
                                  
                                  </div>
                              </div>
                              
                                
                                  
                                   
                                     @if ($errors->has('ceo_number')) <span class="help-block"> <strong>{{ $errors->first('ceo_number') }}</strong> </span> @endif                      
                                                                      
                                </div>


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}"> {!! Form::label('industry_id', 'Industry', ['class' => 'bold']) !!}                    

        {!! Form::select('industry_id', ['' => 'Select Industry']+$industries, null, array('class'=>'form-control', 'id'=>'industry_id')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ownership_type') !!}"> {!! Form::label('ownership_type', 'Ownership Type', ['class' => 'bold']) !!}

        {!! Form::select('ownership_type_id', ['' => 'Select Ownership type']+$ownershipTypes, null, array('class'=>'form-control', 'id'=>'ownership_type_id')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'ownership_type_id') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}"> {!! Form::label('description', 'Company details', ['class' => 'bold']) !!}

        {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>'Company details')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'description') !!} </div>

         

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'map') !!}" style="display: none;"> {!! Form::label('map', 'Google Map', ['class' => 'bold']) !!}

        {!! Form::textarea('map', null, array('class'=>'form-control', 'id'=>'map', 'placeholder'=>'Google Map')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'map') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'no_of_offices') !!}"> {!! Form::label('no_of_offices', 'Number of offices', ['class' => 'bold']) !!}

        {!! Form::select('no_of_offices', ['' => 'Select num. of offices']+MiscHelper::getNumOffices(), null, array('class'=>'form-control', 'id'=>'no_of_offices')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'no_of_offices') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'website') !!}"> {!! Form::label('website', 'Website', ['class' => 'bold']) !!} When adding a URL please include http:// or https://

        {!! Form::text('website', null, array('class'=>'form-control', 'id'=>'website', 'placeholder'=>'https://')) !!} 

        {!! APFrmErrHelp::showErrors($errors, 'website') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'no_of_employees') !!}"> {!! Form::label('no_of_employees', 'Number of employees', ['class' => 'bold']) !!}

        {!! Form::select('no_of_employees', ['' => 'Select num. of employees']+MiscHelper::getNumEmployees(), null, array('class'=>'form-control', 'id'=>'no_of_employees')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'no_of_employees') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'established_in') !!}"> {!! Form::label('established_in', 'Established in', ['class' => 'bold']) !!}

        {!! Form::select('established_in', ['' => 'Select Established In']+MiscHelper::getEstablishedIn(), null, array('class'=>'form-control', 'id'=>'established_in')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'established_in') !!} </div>
    <?php
    /*
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'fax') !!}"> {!! Form::label('fax', 'Fax #', ['class' => 'bold']) !!}

        {!! Form::text('fax', null, array('class'=>'form-control', 'id'=>'fax', 'placeholder'=>'Fax #')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'fax') !!} </div>

    









    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook') !!}"> {!! Form::label('facebook', 'Facebook Address', ['class' => 'bold']) !!}

        {!! Form::text('facebook', null, array('class'=>'form-control', 'id'=>'facebook', 'placeholder'=>'Facebook address')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'facebook') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter') !!}"> {!! Form::label('twitter', 'Twitter', ['class' => 'bold']) !!}

        {!! Form::text('twitter', null, array('class'=>'form-control', 'id'=>'twitter', 'placeholder'=>'Twitter')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'twitter') !!} </div>



    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'linkedin') !!}"> {!! Form::label('linkedin', 'Linkedin', ['class' => 'bold']) !!}

        {!! Form::text('linkedin', null, array('class'=>'form-control', 'id'=>'linkedin', 'placeholder'=>'Linkedin')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'linkedin') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'google_plus') !!}"> {!! Form::label('google_plus', 'Google+', ['class' => 'bold']) !!}

        {!! Form::text('google_plus', null, array('class'=>'form-control', 'id'=>'google_plus', 'placeholder'=>'Google+')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'google_plus') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'pinterest') !!}"> {!! Form::label('pinterest', 'Pinterest', ['class' => 'bold']) !!}

        {!! Form::text('pinterest', null, array('class'=>'form-control', 'id'=>'pinterest', 'placeholder'=>'Pinterest')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'pinterest') !!} </div>
    */ ?>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'location') !!}"> {!! Form::label('location', 'Location', ['class' => 'bold']) !!}

        {!! Form::text('location', null, array('class'=>'form-control', 'id'=>'location', 'placeholder'=>'Location')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'location') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}"> {!! Form::label('country_id', 'Country', ['class' => 'bold']) !!}                    
<?php $arra = array(
            230=>'United Kingdom',
            231=>'United States of America',
        ); ?>
        {!! Form::select('country_id', $arra+$countries, old('country_id', (isset($company))? $company->country_id:$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}"> {!! Form::label('state_id', 'County / State / Province / District', ['class' => 'bold']) !!}

        <span id="default_state_dd">                    

            {!! Form::select('state_id', ['' => 'Select County / State / Province /District'], null, array('class'=>'form-control', 'id'=>'state_id')) !!}

        </span>

        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city_id') !!}"> {!! Form::label('city_id', 'Town / City', ['class' => 'bold']) !!}  

        <span id="default_city_dd">                  

            {!! Form::select('city_id', ['' => 'Select Town / City'], null, array('class'=>'form-control', 'id'=>'city_id')) !!}

        </span>

        {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>











    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">

        {!! Form::label('is_active', 'Is Active?', ['class' => 'bold']) !!}

        <div class="radio-list">

            <?php

            $is_active_1 = 'checked="checked"';

            $is_active_2 = '';

            if (old('is_active', ((isset($company)) ? $company->is_active : 1)) == 0) {

                $is_active_1 = '';

                $is_active_2 = 'checked="checked"';

            }

            ?>

            <label class="radio-inline">

                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>

                Active </label>

            <label class="radio-inline">

                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>

                In-Active </label>

        </div>

        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_featured') !!}" style="display: none;">

        {!! Form::label('is_featured', 'Is Featured?', ['class' => 'bold']) !!}

        <div class="radio-list">

            <?php

            $is_featured_1 = '';

            $is_featured_2 = 'checked="checked"';

            if (old('is_featured', ((isset($company)) ? $company->is_featured : 0)) == 1) {

                $is_featured_1 = 'checked="checked"';

                $is_featured_2 = '';

            }

            ?>

            <label class="radio-inline">

                <input id="featured" name="is_featured" type="radio" value="1" {{$is_featured_1}}>

                Featured </label>

            <label class="radio-inline">

                <input id="not_featured" name="is_featured" type="radio" value="0" {{$is_featured_2}}>

                Not Featured </label>

        </div>

        {!! APFrmErrHelp::showErrors($errors, 'is_featured') !!} </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'two_step_verification') !!}">
        {!! Form::label('two_step_verification', '2 step verification:', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = '';
            $is_active_2 = '';
            if (old('two_step_verification', ((isset($company)) ? $company->two_step_verification : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }else if(old('two_step_verification', ((isset($company)) ? $company->two_step_verification : 0)) == 1){
                $is_active_1 = 'checked="checked"';
                $is_active_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="two_step_verification" type="radio" value="1" {{$is_active_1}}>
                Yes </label>
            <label class="radio-inline">
                <input id="not_active" name="two_step_verification" type="radio" value="0" {{$is_active_2}}>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'two_step_verification') !!}
    </div>

    <div class="form-actions"> {!! Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!} </div>

</div>

@push('scripts')

@include('admin.shared.tinyMCEFront') 

<script type="text/javascript">
     /*$( "form" ).validate( {
      rules: {
        website: {
          url: true,
          normalizer: function( value ) {
            var url = value;
     
            // Check if it doesn't start with http:// or https:// or ftp://
            if ( url && url.substr( 0, 7 ) !== "http://"
                && url.substr( 0, 8 ) !== "https://"
                && url.substr( 0, 6 ) !== "ftp://" ) {
              // then prefix with http://
              url = "http://" + url;
            }
     
            // Return the new url
            return url;
          }
        }
      }
} );*/

    $(document).ready(function () {

        $('#country_id').on('change', function (e) {

            e.preventDefault();

            filterDefaultStates(0);

        });

        $(document).on('change', '#state_id', function (e) {

            e.preventDefault();

            filterDefaultCities(0);

        });

        filterDefaultStates(<?php echo old('state_id', (isset($company)) ? $company->state_id : 0); ?>);

    });
    (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {

                 

                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                const size =  
                   (this.files[0].size / 1024).toFixed(2); 
      
                if (size > 100) { 
                    alert("A maximum image size of 100 kb allowed, please reduce image size and try again"); 
                    options.error();
                    $(this).focus();
                }else{
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }
                }    

                

            });

        });
    };

})(jQuery);

$(function() {
    $("input[type='file']").checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            $('.allowed_message').css("color", "#888");
        },
        error: function() {
            $('.allowed_message').css("color", "red");
            $("input[type='file']").reset();
        }
    });

});
    function filterDefaultStates(state_id)

    {

        var country_id = $('#country_id').val();

        if (country_id != '') {

            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_state_dd').html(response);

                        filterDefaultCities(<?php echo old('city_id', (isset($company)) ? $company->city_id : 0); ?>);

                    });

        }

    }

     $('.phone_v').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

     $('.employer_name').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9,:\S/-:\S/&:\S/@:\S/£:\S/$:\S/€:\S/¥:\S/#:\S/.:\S/,:\S/::\S/;:\S/-]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });

    function filterDefaultCities(city_id)

    {

        var state_id = $('#state_id').val();

        if (state_id != '') {

            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_city_dd').html(response);

                    });

        }

    }

     $('#phone').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
var langArray = [];

$('.vodiapicker option').each(function(){
  var img = $(this).attr("data-thumbnail");
  var val = $(this).attr("data-val");
  var text = this.innerText;
  var value = $(this).val();
  var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
  langArray.push(item);
})

// $('.vodiapicker_phone_ceo_num option').each(function(){
//   var img = $(this).attr("data-thumbnail");
//   var val = $(this).attr("data-val");
//   var text = this.innerText;
//   var value = $(this).val();
//   var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
//   langArray.push(item);
// })

$('#a').html(langArray);
$('#a-phone_ceo_num').html(langArray);
$('#a-phone_ceo_num2').html(langArray);

//Set the button value to the first el of the array


//change button stuff on click
$('#a li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fa fa-caret-down"></i></li>';
  $('.btn-select').html(item);
  $('.btn-select').attr('value', value);
  $('#phone').val(val);
  $('#phone_num_code').val(val);
  $(".b").toggle();
  //console.log(value);
});

$('#a-phone_ceo_num li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fa fa-caret-down"></i></li>';
  $('.btn-select-phone_ceo_num').html(item);
  $('.btn-select-phone_ceo_num').attr('value', value);
  $('#ceo_number').val(val);
  $('#ceo_number_code').val(val);
  $(".b-phone_ceo_num").toggle();
  //console.log(value);
});

$('#a-phone_ceo_num2 li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fa fa-caret-down"></i></li>';
  $('.btn-select-phone_ceo_num2').html(item);
  $('.btn-select-phone_ceo_num2').attr('value', value);
  $('#ceo_number_2').val(val);
  $('#ceo_number_code2').val(val);
  $(".b-phone_ceo_num2").toggle();
  //console.log(value);
});

$(".btn-select").click(function(){
        $(".b").toggle();
    });


$(".btn-select-phone_ceo_num").click(function(){
        $(".b-phone_ceo_num").toggle();
    });

$(".btn-select-phone_ceo_num2").click(function(){
        $(".b-phone_ceo_num2").toggle();
    });



 $('#mobile_num').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
function myFunction(input, target_id = 'a') {
    var filter, ul, li, a, i, txtValue;
    // input = document.getElementById("search");
    filter = input.value.toUpperCase();
    ul = document.getElementById(target_id);
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}


function staticData(){
  var data=$("#phone");
  
    var code = $('#phone_num_code').val();

  if(data.val().length<3)
    {
     data.val(code);
     data.attr("readonly",true);
    }
    else
    {
     data.attr("readonly",false);
    }
}

</script>

@endpush