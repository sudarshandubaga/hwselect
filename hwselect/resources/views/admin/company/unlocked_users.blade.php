@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }    
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }

    #uniform-inlineCheckbox1 #option-error{
            position: absolute;
    margin-left: -7px;
    width: 250px;
    display: inline-table;
    text-align: left;
    margin-top: -20px;
    }

    label.error{
        color: red;
    }
    
    .datepicker>div {

        display: block;

    }

    .button-group a{  
        width: 48% !important;
        font-size: 8px !important;
        margin-top: 28px;
        padding: .70rem 1.5rem !important;
    }

</style>


   


<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Users</span> </li>

            </ul>

        </div>
 <?php
            if(Request::segment(3)){
                $job = App\Job::findorFail(Request::segment(3));
            }
            $company_link = '<a target="_blank" href="'.route('public.company',$job->getCompany('id')).'">'.$job->getCompany('name').'</a>';
?>
        <div class="row">



            <div class="col-md-12 col-sm-12"> 

                <div class="myads">
                    <?php 

                    $st = '';
                    if($status=='called_for_interview'){
                        $st = 'Called for Interview';
                    }else if($status=='interviewed'){
                        $st = 'Interviewed';
                    }else if($status=='recruited'){
                        $st = 'Recruited';
                    }else if($status=='resigned'){
                        $st = 'Resigned';
                    }else if($status=='internally_filled_by_employer'){
                        $st = 'Internally filled by employer';
                    }else if($status=='rejected'){
                        $st = 'Rejected';
                    }else{
                        $st = 'All';
                    }

                     ?>
                    
                    <h3>{{__('Shortlisted Candidates')}} ({{$st}})  (Company: {!!$company_link!!} - Job: {{$job->title}})</h3>

                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($users) && count($users))

                        @foreach($users as $user)

                        <li>

                            <div class="row">
                            
                                <div class="col-md-9 col-sm-9">

                                    <div class="jobimg">{{$user->printUserImage(150, 150)}}</div>

                                    <div class="jobinfo">

                                        <h3><a href="{{route('admin.view.public.profile', $user->id)}}">{{$user->getName()}}</a></h3>
										
                                        <div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> {{trim($user->getLocation(),',')}}</div>
                                        
                            @if(!empty($user->mobile_num))

                            <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{{$user->mobile_num}}">{{$user->mobile_num}}</a></div>

                            @endif

                            @if(!empty($user->email))

                            <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:{{$user->email}}">{{$user->email}}</a></div>

                            @endif
                            
                             <p>{{\Illuminate\Support\Str::limit($user->getProfileSummary('summary'),150,'...')}}</p>
                                    </div>

                                    <div class="clearfix"></div>

                                </div>

                                <div class="col-md-3 col-sm-3">
                                    <div class="listbtn"><a href="{{route('admin.view.public.profile',[ $user->id,'job='.Request::segment(3)])}}">{{__('View Profile')}}</a></div>
                                    <div class="notesbtn" role="group">
                                        <a href="javascript:;" class="btn" onclick="view_message({{$user->id}},{{$job_id}})">{{__('View Note')}}</a>
                                        <a href="javascript:;" class="btn" onclick="send_message({{$user->id}})">{{__('Add a Note')}}</a>
                                    </div>
                                    


                                </div>

                            </div>

                           

                        </li>

                        <!-- job end --> 

                        @endforeach

                        @endif

                    </ul>

                </div>

            </div>

        </div>

    </div>

</div>
<div class="modal fade" id="sendmessage" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="" id="send-form">
                @csrf
                <input type="hidden" name="seeker_id" id="seeker_id">
                <div class="modal-header">                    
                    <h4 class="modal-title">Add Note</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="formradio" style="margin-top: 25px;">
                            <label class="form-check-label" for="inlineCheckbox1">
                              <input class="" type="radio" id="inlineCheckbox1" value="called_for_interview" name="option">
                              Called for interview
                            </label>
                        </div>
                        <div class="formradio">
                            <label class="form-check-label" for="inlineCheckbox2">
                              <input class="" type="radio" id="inlineCheckbox2" value="interviewed" name="option">
                              Interviewed
                            </label>
                        </div>
                       

                     

                      

                    </div>
                    <input type="hidden" name="job_id" value="{{$job_id}}">
                    <div class="form-group">
                        <input type="text" name="date" placeholder="Please Select Date & Time" class="form-control datepicker">
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Add a Note" id="message" cols="10" rows="3">(Company: {{ strip_tags($company_link) }})-{{$job->title}}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="showmessage" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
                <div class="modal-header">                    
                    <h4 class="modal-title">All Notes</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="clearfix message-content">
                      <div class="message-details">
                        <h4> </h4>
                        <div id="append_messages"></div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>

    </div>
</div>



@endsection

@push('scripts')
<script type="text/javascript">
    function send_message(id) {
        $('#seeker_id').val(id);
        $('#sendmessage').modal('show');
    }
    if ($("#send-form").length > 0) {
        $("#send-form").validate({
            validateHiddenInputs: true,
            ignore: "",

            rules: {
                option: {
                    required: true,
                },
                date: {
                    required: true,
                },
                message: {
                    required: true,
                    maxlength: 5000
                },
            },
            messages: {

                option: {
                    required: "Please select an option",
                },
                date: {
                    required: "Date is required",
                },
                message: {
                    required: "Message is required",
                }

            },
            submitHandler: function(form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('admin.submit-action-seeker')}}",
                    type: "POST",
                    data: $('#send-form').serialize(),
                    success: function(response) {
                        $("#send-form").trigger("reset");
                        $('#sendmessage').modal('hide');
                        swal({
                            title: "Success",
                            text: response["msg"],
                            icon: "success",
                            button: "OK",
                        });
                    }
                });
            }
        })
    }
    $(".datepicker").datetimepicker();

    function view_message(id,job_id){
         $.ajax({
                url: "{{route('admin.view-action-seeker')}}?id="+id+"&job_id="+job_id,
                type: "GET",
                success: function(response) {
                    $('#append_messages').html(response);
                    $('#showmessage').modal('show');
                }
            });
    }

   /* $(".datepicker").datepicker({
            format: 'yyyy-m-d'
    });*/
</script>
@endpush