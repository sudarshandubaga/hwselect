@extends('admin.layouts.admin_layout')

@push('css')
<link rel="stylesheet" href="{{ asset('modules/menu/css/nestable.css') }}">
<link rel="stylesheet" href="{{ asset('modules/menu/css/menu.css') }}">
<style>
    .positon_tree{ border: 2px solid #dedede; padding: 15px; margin: 15px; }
    .positon_tree { max-width: 600px; font-family: sans-serif; color: #333; }
    .table-responsive {
    overflow-x: initial !important;
}
  </style>
@endpush
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
      <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Menus</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR --> 
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">Manage Menus <small>Menu</small> </h3>
<div class="content">
      @if(session()->has('message.added'))
       
        <div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
             {!! session('message.content') !!}
        </div>
       @endif
       @if(session()->has('message.updated'))
        <div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
             {!! session('message.content') !!}
        </div>
       @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                <div class="col-md-6">
                                    <h4 class="card-title">Manage Menus</h4>
                                </div>
                               <!-- <div class="col-md-6">
                                    <div class="add_btn">
                                        <a href="#newModal" class="add-modal btn btn-primary" data-toggle="modal">Add New Menu</a> 
                                    </div>
                                </div> -->
                                
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                     <div class="row">
            <div class="col-md-8">  
              <div class="well">
                
                <div class="dd" id="nestable">
                  {!!$menu!!}
                </div>

                <p id="success-indicator" style="display:none; margin-right: 10px;">
                  <span class="glyphicon glyphicon-ok"></span> Menu order has been saved
                </p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="well">
                <p>Drag items to move them in a different order</p>
              </div>
            </div>
          </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>

  <!-- Create new item Modal -->
   <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
       <div class="modal-content">
            <form method="POST" files="true" action="{{ asset('admin/menu/new')}}" enctype="multipart/form-data"> 
           {{csrf_field()}}
                <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Provide details of the new menu item</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label for="title" class="col-lg-2 control-label">Title</label>
                <div class="col-lg-10">
                     <input type="text" class="form-control" name="title" id="title" autofocus value="{{ old('title') }}" >
                </div>
            </div>
            <div class="form-group">
                <label for="label" class="col-lg-2 control-label">Label</label>
                <div class="col-lg-10">
                     <input type="text" class="form-control" name="label" id="label" autofocus value="{{ old('title') }}" >
                </div>
            </div>
            <div class="form-group">
                <label for="url" class="col-lg-2 control-label">URL</label>
                <div class="col-lg-10">
                     <input type="text" class="form-control" name="url" id="url" autofocus value="{{url('/')}}" >
                </div>
            </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
           <button type="submit" class="btn btn-primary">Create</button>
         </div>
            </form>
       </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
  <!-- Create new item Modal -->
   <div class="modal fade" id="editModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
       <div class="modal-content">
            <form method="POST" files="true" action="{{ asset('admin/menu/edit/')}}" enctype="multipart/form-data"> 
           {{csrf_field()}}
                <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Update menu item</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label for="title" class="col-lg-2 control-label">Title</label>
                <div class="col-lg-10">
                     <input type="text" class="form-control" name="title_edit" id="title_edit" autofocus value="{{ old('title') }}" >
                     <input type="hidden" class="form-control" name="id" id="id" autofocus value="{{ old('title') }}" >
                </div>
            </div>
            <div class="form-group">
                <label for="label" class="col-lg-2 control-label">Label</label>
                <div class="col-lg-10">
                     <input type="text" class="form-control" name="label_edit" id="label_edit" autofocus value="{{ old('title') }}" >
                </div>
            </div>
            <div class="form-group">
                <label for="url" class="col-lg-2 control-label">URL</label>
                <div class="col-lg-10">
                     <input type="text" class="form-control" name="url_edit" id="url_edit" autofocus value="{{ old('title') }}" >
                </div>
            </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
           <button type="submit" class="btn btn-primary">Update</button>
         </div>
            </form>
       </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
  
  <!-- Delete item Modal -->
   <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
           <form method="POST" files="true" action="{{ asset('admin/menu/delete')}}" enctype="multipart/form-data"> 
         {{csrf_field()}}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Provide details of the new menu item</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this menu item?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <input type="hidden" name="delete_id" id="postvalue" value="" />
            <input type="submit" class="btn btn-danger" value="Delete Item" />
          </div>
           </form>
       </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
@endsection
@push('scripts')
 <script type="text/javascript" src="{{ asset('toastr/toastr.min.js') }}"></script>
 <script src="{{ asset('modules/menu/js/jquery.nestable.js') }}"></script>
 <script src="{{ asset('modules/menu/js/menu.js') }}"></script>
@endpush






