@extends('admin.layouts.admin_layout')
@section('content')
<?php

$lang = config('default_lang');

if (isset($city))

    $lang = $city->lang;

$lang = MiscHelper::getLang($lang);

$direction = MiscHelper::getLangDirection($lang);

$queryString = MiscHelper::getLangQueryStr();

?>
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('list.cities') }}">Import / Cities</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Import Town / City</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->        
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Import Town / City Form</span> </div>
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                        </ul>
                        {!! Form::open(array('method' => 'post', 'route' => 'store.import.city', 'class' => 'form', 'files'=>true)) !!}
                        <div class="tab-content">              
                            <div class="tab-pane fade active in" id="Details"> <div class="form-body">        

    {!! Form::hidden('id', null) !!}

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}" id="lang_div">

        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}                    

        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang, array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value);')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}" id="country_id_div">
         <?php $arra = array(
            230=>'United Kingdom',
            231=>'United States of America',
        ); ?>  
        {!! Form::label('country_id', 'Country', ['class' => 'bold']) !!}

        {!! Form::select('country_id', ['' => 'Select Country']+$arra+$countries, old('country_id', (isset($city))? $city->getState()->getCountry('country_id'):$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}" id="state_id_div">

        {!! Form::label('state_id', 'County / State / Province / District', ['class' => 'bold']) !!}                    

        <span id="default_state_dd">

            {!! Form::select('state_id', ['' => 'Select County / State / Province / District'], null, array('class'=>'form-control', 'id'=>'state_id')) !!}

        </span>

        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city') !!}">

        {!! Form::label('city', 'Import Town / City', ['class' => 'bold']) !!}

        {!! Form::file('city', null, array('class'=>'form-control', 'id'=>'city', 'placeholder'=>'Town / City', 'dir'=>$direction)) !!}
		<p>Only .txt file allowed, which contains 1 entry per line.</p>
        {!! APFrmErrHelp::showErrors($errors, 'city') !!}
			
    </div>

    

    

      

    <div class="form-actions">

        {!! Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}

    </div>

</div> </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY --> 
</div>
@endsection
@push('scripts')

<script type="text/javascript">

    $(document).ready(function () {

        $('#country_id').on('change', function (e) {

            e.preventDefault();

            filterDefaultStates(0);

        });

        $(document).on('change', '#state_id', function (e) {

            e.preventDefault();

            filterDefaultCities(0);

        });

        filterDefaultStates(<?php echo old('state_id', (isset($city)) ? $city->getState('id') : 0); ?>);

        filterDefaultCities(<?php echo old('city_id', (isset($city)) ? $city->city_id : 0); ?>);

    });

    function setLang(lang) {

        window.location.href = "<?php echo url(Request::url()) . $queryString; ?>" + lang;

    }

    function showHideCityId() {

        $('#city_id_div').hide();

        var is_default = $("input[name='is_default']:checked").val();

        if (is_default == 0) {

            $('#city_id_div').show();

        }

    }

    showHideCityId();

    function filterDefaultStates(state_id)

    {

        var country_id = $('#country_id').val();

        if (country_id != '') {

            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_state_dd').html(response);

                    });

        }

    }

    function filterDefaultCities(city_id)

    {

        var state_id = $('#state_id').val();

        if (state_id != '') {

            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_cities_dd').html(response);

                    });

        }

    }

    if ($("form").length > 0) {
            $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    lang: {
                        required: true,
                    }, 

                    country_id: {
                        required: true,
                    }, 

                    state_id: {
                        required: true,
                    }, 

                    city: {
                        required: true,
                    },  


                    
                },
                messages: {

                    lang: {
                        required: "Please select language",
                    },
                    country_id: {
                        required: "Please select country",
                    },

                    state_id: {
                        required: "Please select state",
                    },


                  

                },
            
            })
        }        

</script>

@endpush