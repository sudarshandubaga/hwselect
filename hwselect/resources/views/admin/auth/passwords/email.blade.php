@extends('admin.layouts.login_layout')
@section('content') 
<!-- BEGIN LOGIN -->
<div class="content"> 
    <!-- BEGIN FORGOT PASSWORD FORM --> 
    @if (session('status'))
     <div class="alert alert-success">

                            {!! session('status') !!} 

                        </div>
    @else
    <form class="form-horizontal forget-form" role="form" method="POST" action="{{ route('admin.password.email') }}" style="display:block;">
        {{ csrf_field() }}
        <h3 class="font-green">Forgot Password ?</h3>
        <p> Enter your registered e-mail address to reset admin password. </p>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Invalid email address entered </span> </div>
        @if ($errors->has('email'))
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span>{{ $errors->first('email') }}</span> </div>
        @endif
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> 
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">E-Mail Address</label>
            <input class="form-control placeholder-no-fix" type="text" maxlength="30" autocomplete="off" placeholder="Email Address" name="email" value="{{old('email')}}" />
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase pull-right">Send Password Reset Link</button>
        </div>
    </form>
    @endif 
    <!-- END FORGOT PASSWORD FORM --> 
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    if ($("form").length > 0) {
            $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    email: {
                        required: true,
                        email: true,
                    }, 


                    
                },
                messages: {

                    email: {
                        required: "Please enter your registered email address",
                        email: "Please enter a valid email address",
                    },


                  

                },
            
            })
        }  

        function show_hidden_div(){
            location.reload();
        }          

</script>
<style type="text/css">
    .error{
        color: red !important;
    }
</style>
@endpush