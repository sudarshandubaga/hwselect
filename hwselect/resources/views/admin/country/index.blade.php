@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }	
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }

</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Countries</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage Countries</h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12"> 

                <!-- Begin: life time stats -->

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Countries List</span> </div>

                        <div class="actions"> <a href="{{ route('create.country') }}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i> Add New Country</a> </div>

                    </div>

                    <div class="portlet-body">

                        <div class="table-container">

                            <form method="post" role="form" id="country-search-form">

                                <table class="table table-striped table-bordered table-hover"  id="countryDatatableAjax">

                                    <thead>

                                        <tr role="row" class="filter"> 
                                            <td></td>

                                            <td>{!! Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('id'=>'lang', 'class'=>'form-control')) !!}</td>

                                            <td><input type="text" class="form-control" name="country" id="country" autocomplete="off" placeholder="Country"></td>                      

                                            <td><select name="is_active" id="is_active"  class="form-control">

                                                    <option value="-1">Is Active?</option>

                                                    <option value="1" selected="selected">Active</option>

                                                    <option value="0">In Active</option>

                                                </select></td></tr>

                                        <tr role="row" class="heading">                                            

                                            <th>Flag</th>

                                            <th>Language</th>

                                            <th>Country</th>

                                            <th>Actions</th>

                                        </tr>

                                    </thead>

                                    <tbody id="tablecontents">

                                    </tbody>

                                </table>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>

@endsection

@push('scripts') 
<script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>

<script>

    $(function () {

        var oTable = $('#countryDatatableAjax').DataTable({

            processing: true,

            serverSide: true,

            stateSave: false,

            searching: false,

            lengthMenu: [[-1], ['All']],
            
            pageLength: -1,

            pagingType: 'full_numbers',

            /*		

             "order": [[1, "asc"]],            

             paging: true,

             info: true,

             */

            ajax: {

                url: '{!! route('fetch.data.countries') !!}',

                data: function (d) {

                    d.lang = $('#lang').val();

                    d.country = $('input[name=country]').val();

                    d.is_active = $('#is_active').val();

                }

            }, columns: [

                {data: 'flag', name: 'flag'},

                {data: 'lang', name: 'lang'},

                {data: 'country', name: 'country'},

                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]

        });

        $( "#tablecontents" ).sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {
            // alert();
            var order = [];
            var id;
            var page;
            $('#tablecontents tr').each(function(index, element) {
                id = $(this).attr('id').replace("countryDtRow", "");
                page = oTable.page.info();
                // console.log(page.end, page.page, index);
                order.push({
                    id,
                    pos: parseInt(page.end) + parseInt(index) + 1
                });
            });
            
            $.ajax({
                type: "POST", 
                dataType: "json", 
                url: "{{ route('country.order.update') }}",
                data: {
                    order:order,
                    _token: '{{csrf_token()}}'
                },
                success: function(response) {
                    if (response.status == "success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });
        }

        $('#country-search-form').on('submit', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#lang').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#country').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#is_active').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

    });

    function deleteCountry(id, is_default) {

        var msg = 'Are you sure?';

        if (is_default == 1) {

            msg = 'Are you sure? You are going to delete default Country, all other non default Countries will be deleted too!';

        }

        if (confirm(msg)) {

            $.post("{{ route('delete.country') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        if (response == 'ok')

                        {

                            var table = $('#countryDatatableAjax').DataTable();

                            table.row('countryDtRow' + id).remove().draw(false);

                        } else

                        {

                            alert('Request Failed!');

                        }

                    });

        }

    }

    function makeActive(id) {

        $.post("{{ route('make.active.country') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#countryDatatableAjax').DataTable();

                        table.row('countryDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeNotActive(id) {

        $.post("{{ route('make.not.active.country') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#countryDatatableAjax').DataTable();

                        table.row('countryDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

</script> 

@endpush