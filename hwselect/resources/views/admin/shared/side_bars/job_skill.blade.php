<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-area-chart" aria-hidden="true"></i> <span class="title">Skill Acquired</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('list.job.skills') }}" class="nav-link "> <span class="title">List Skill Acquired</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('create.job.skill') }}" class="nav-link "> <span class="title">Add new Skill Acquired</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('sort.job.skills') }}" class="nav-link "> <span class="title">Sort Skill Acquired</span> </a> </li>

    </ul>

</li>