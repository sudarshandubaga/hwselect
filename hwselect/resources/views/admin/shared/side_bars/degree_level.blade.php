<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Qualification Types</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('list.degree.levels') }}" class="nav-link ">  <span class="title">List Qualification Types</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('create.degree.level') }}" class="nav-link ">  <span class="title">Add new Qualification Type</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('sort.degree.levels') }}" class="nav-link "> <span class="title">Sort Qualification Types</span> </a> </li>

    </ul>

</li>