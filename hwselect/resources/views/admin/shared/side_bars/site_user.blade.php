<li class="nav-item"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user"></i> <span class="title">Jobseekers</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">
        <li class="nav-item {{ Request::url() == route('list.users') ? 'active' : '' }}">
			<a href="{{ route('list.users') }}" class="nav-link "> <i class="icon-user"></i> <span class="title">List all Jobseekers</span> </a>
		</li>

        <li class="nav-item {{ Request::url() == route('create.user') ? 'active' : '' }}">
			<a href="{{ route('create.user') }}" class="nav-link "> <i class="icon-user"></i> <span class="title">Add new Jobseeker</span> </a>
		</li>

        <li class="nav-item {{ Request::url() == route('admin.search-cvs') ? 'active' : '' }}">
			<a href="{{ route('admin.search-cvs') }}" class="nav-link "> <i class="icon-user"></i> <span class="title">Search CVs</span> </a>
		</li>
    </ul>
</li>