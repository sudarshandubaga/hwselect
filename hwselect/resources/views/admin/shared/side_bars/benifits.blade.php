<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-venus-double" aria-hidden="true"></i> <span class="title">Benefits</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
        <li class="nav-item  "> <a href="{{ route('list.benifitss') }}" class="nav-link "> <span class="title">List Benefits</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('create.benifits') }}" class="nav-link "> <span class="title">Add new Benefit</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('sort.benifitss') }}" class="nav-link "> <span class="title">Sort Benefits</span> </a> </li>
    </ul>
</li>