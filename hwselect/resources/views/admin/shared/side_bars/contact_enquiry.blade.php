@if(APAuthHelp::check(['SUP_ADM']))
<li class="heading">
    <h3 class="uppercase">Contact Enquiries</h3>
</li>
<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-envelope"></i> <span class="title">Contact Enquiries</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <a href="{{ route('list.admin.contact', ['type' => 'Job Seeker']) }}" class="nav-link ">
                <i class="icon-envelope"></i>
                <span class="title">Job Seekers</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('list.admin.contact', ['type' => 'Company']) }}" class="nav-link ">
                <i class="icon-envelope"></i>
                <span class="title">Company</span>
            </a>
        </li>
    </ul>
</li>
@endif