<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-briefcase" aria-hidden="true"></i> <span class="title">Contractual Hours</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('list.job.types') }}" class="nav-link"> <span class="title">List Contractual Hours</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('create.job.type') }}" class="nav-link"> <span class="title">Add new Contractual Hours</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('sort.job.types') }}" class="nav-link"> <span class="title">Sort Contractual Hours</span> </a> </li>

    </ul>

</li>