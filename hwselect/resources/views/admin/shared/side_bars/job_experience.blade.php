<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-pie-chart" aria-hidden="true"></i> <span class="title">Length of Service</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('list.job.experiences') }}" class="nav-link ">  <span class="title">List Length of Service</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('create.job.experience') }}" class="nav-link ">  <span class="title">Add new Length of Service</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('sort.job.experiences') }}" class="nav-link ">  <span class="title">Sort Length of Service</span> </a> </li>

    </ul>

</li>