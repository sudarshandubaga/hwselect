<li class="nav-item"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-briefcase"></i> <span class="title">Jobs</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item"> <a href="{{ route('list.jobs') }}" class="nav-link "> <span class="title">List Jobs</span> </a> </li>
        <li class="nav-item"> <a href="{{ route('list.pending-for-approval.jobs') }}" class="nav-link "> <span class="title">Pending for Approval</span> </a> </li>
        <!-- <li class="nav-item"> <a href="{{ route('list.rejected.jobs') }}" class="nav-link "> <span class="title">Admin Refused</span> </a> </li> -->
        <li class="nav-item"> <a href="{{ route('create.job') }}" class="nav-link "> <span class="title">Add new Job</span> </a> </li>
        <li class="nav-item"> <a href="{{ route('list.request.jobs') }}" class="nav-link "> <span class="title">List Requests For Delete Jobs</span> </a> </li>
		<li class="nav-item"> <a href="{{ route('expired.jobs') }}" class="nav-link "> <span class="title">Archived Jobs</span> </a> </li>
		<li class="nav-item"> <a href="{{ route('list.expired.jobs') }}" class="nav-link "> <span class="title">Expired Jobs</span> </a> </li>
		<li class="nav-item"> <a href="{{ route('list.onhold.jobs') }}" class="nav-link "> <span class="title">Admin On-Hold Jobs</span> </a> </li>
		<li class="nav-item"> <a href="{{ route('list.abused.jobs') }}" class="nav-link "> <span class="title">Abuse Reported Jobs</span> </a> </li>
    </ul>

</li>