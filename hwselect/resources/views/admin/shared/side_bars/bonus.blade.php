<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-venus-double" aria-hidden="true"></i> <span class="title">Bonus</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
        <li class="nav-item  "> <a href="{{ route('list.bonuss') }}" class="nav-link "> <span class="title">List Bonus</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('create.bonus') }}" class="nav-link "> <span class="title">Add new Bonus</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('sort.bonuss') }}" class="nav-link "> <span class="title">Sort Bonus</span> </a> </li>
    </ul>
</li>