<li class="nav-item "><a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Manage Header Menu</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ url('admin/menu') }}" class="nav-link "> <span class="title">List Header Menu</span> </a> </li>

        

    </ul>

</li>


<li class="nav-item "><a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Manage HW Search Footer Menu</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('modules-data','hw-search-footer-menu') }}" class="nav-link "> <span class="title">List HW Search Footer Menu</span> </a> </li>

        

    </ul>

</li>





<li class="nav-item "><a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Manage Jobseeker Footer Menu</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('modules-data','jobseeker-footer-menu') }}" class="nav-link "> <span class="title">List Jobseeker Footer Menu</span> </a> </li>

        

    </ul>

</li>

<li class="nav-item "><a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Manage Employer Footer Menu</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('modules-data','employer-footer-menu') }}" class="nav-link "> <span class="title">List Employer Footer Menu</span> </a> </li>

        

    </ul>

</li>



