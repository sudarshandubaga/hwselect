{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort benifits</h3>
    {!! Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'refreshbenifitsSortData();')) !!}
    <div id="benifitsSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshbenifitsSortData();
    });
    function refreshbenifitsSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('benifits.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#benifitsSortDataDiv").html('');
                $("#benifitsSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var benifitsOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('benifits.sort.update') }}", {benifitsOrder: benifitsOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
