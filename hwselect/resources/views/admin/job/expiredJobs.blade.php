@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">
	
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }
    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }	
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .holder {
     padding: 0 0 30px;
     margin: 0 0 30px
 }

 .row {
     margin: 0 0 10px
 }

 h2 {
     font-weight: 500;
     font-size: 30px;
     margin: 0 0 20px
 }

 .customRadio input[type="radio"] {
     position: absolute;
     left: -9999px
 }

 .customRadio input[type="radio"]+label {
     position: relative;
     padding: 3px 0 0 40px;
     cursor: pointer
 }

 .customRadio input[type="radio"]+label:before {
     content: '';
     background: #fff;
     border: 2px solid #311B92;
     height: 25px;
     width: 25px;
     border-radius: 50%;
     position: absolute;
     top: 0;
     left: 0
 }

 .customRadio input[type="radio"]+label:after {
     content: '';
     background: #311B92;
     width: 15px;
     height: 15px;
     border-radius: 50%;
     position: absolute;
     top: 5px;
     left: 5px;
     opacity: 0;
     transform: scale(2);
     transition: transform 0.3s linear, opacity 0.3s linear
 }

 .customRadio input[type="radio"]:checked+label:after {
     opacity: 1;
     transform: scale(1)
 }

 .customCheckbox input[type="checkbox"] {
     position: absolute;
     left: -9999px
 }

 .customCheckbox input[type="checkbox"]+label {
     position: relative;
     padding: 3px 0 0 40px;
     cursor: pointer;
     color: rgb(120, 119, 121)
 }

 .customCheckbox input[type="checkbox"]+label:before {
     content: '';
     background: #fff;
     border: 2px solid #ccc;
     border-radius: 3px;
     height: 25px;
     width: 25px;
     position: absolute;
     top: 0;
     left: 0
 }

 .customCheckbox input[type="checkbox"]+label:after {
     content: '';
     border-style: solid;
     border-width: 0 0 2px 2px;
     border-color: transparent transparent #311B92 #311B92;
     width: 15px;
     height: 8px;
     position: absolute;
     top: 6px;
     left: 5px;
     opacity: 0;
     transform: scale(2) rotate(-45deg);
     transition: transform 0.3s linear, opacity 0.3s linear
 }

 .customCheckbox input[type="checkbox"]:checked+label:after {
     opacity: 1;
     transform: scale(1) rotate(-45deg);
     color: #311B92
 }

 .Advanced_setting {
     text-decoration: none;
     color: #1565C0;
     letter-spacing: 1px;
     font-weight: bolder
 }

 .modal-title {
     font-weight: bold !important
 }

 .modal-header,
 .modal-footer {
     border-bottom: 0;
     border-top: 0;
     max-width: 600px !important;
     position: relative
 }

 .my_checkbox {
     margin-left: 3%
 }

 .model-content {
     width: 840px;
     width: 100% !important
 }

 .modal-footer {
     max-width: 600px !important;
     position: relative
 }

 .modal-footer>:not(:last-child) {
     margin-right: 2rem
 }

 .modal-footer>:not(:first-child) {
     margin-left: 0.5rem
 }

 .modal-dialog {
     position: relative;
     width: auto;
     margin: 0 auto;
     max-width: 600px
 }

 .box-shadow--16dp {
     box-shadow: 0 16px 24px 2px rgba(0, 0, 0, .14), 0 6px 30px 5px rgba(0, 0, 0, .12), 0 8px 10px -5px rgba(0, 0, 0, .2)
 }

 @media only screen and (max-width: 780px) {
     .my_checkbox {
         margin-left: 7%
     }

     .modal-dialog {
         position: relative
     }
 }

 .container button focus {
     -moz-box-shadow: none !important;
     -webkit-box-shadow: none !important;
     box-shadow: none !important;
     border: none;
     outline-width: 0
 }

 @media only screen and (max-width: 580px) {
     .modal-dialog {
         position: relative
     }

     .my_checkbox {
         margin-left: 6%
     }
 }

 .btn-outline-light {
     color: #BDBDBD
 }

 #modal_footer {
     color: #BDBDBD;
     cursor: pointer;
     background: #fff
 }

 #modal_footer_support {
     color: #BDBDBD;
     width: 100%
 }

 .btn-success {
     background-color: #311B92 !important;
     border-radius: 8px;
     padding-right: 35px;
     padding-left: 35px
 }

</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Manage Expired Jobs</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage Expired Jobs </h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12"> 

                <!-- Begin: life time stats -->

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Jobs</span> </div>

                        <div class="actions"> <a href="{{ route('create.job') }}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i> Add New Job</a> </div>

                    </div>

                    <div class="portlet-body">

                        <div class="table-container">

                            <form method="post" role="form" id="job-search-form">

								 <div class="row filter">

                                            <div class="col-md-4">{!! Form::select('company_id', ['' => 'Select Company']+$companies, null, array('id'=>'company_id', 'class'=>'form-control')) !!} <br></div>

                                            <div class="col-md-4"><input type="text" class="form-control" name="title" id="title" autocomplete="off" placeholder="Job title"><br></div>

                                            <div class="col-md-4"><input type="text" class="form-control" name="description" id="description" autocomplete="off" placeholder="Job description"><br></div>

                                            <div class="col-md-4">

                                                <?php $default_country_id = Request::query('country_id', $siteSetting->default_country_id); ?>

                                                <?php /*?>{!! Form::select('country_id', ['' => 'Select Country']+$countries, $default_country_id, array('id'=>'country_id', 'class'=>'form-control')) !!}<?php */?>
<?php $arra = array(
            230=>'United Kingdom',
            231=>'United States of America',
        ); ?>                
        {!! Form::select('country_id', [''=>'Select Country']+$arra+$countries, old('country_id'), array('class'=>'form-control', 'id'=>'country_id')) !!}
												<br>
			 </div>									
												
												
								<div class="col-md-4">						
                                                <span id="default_state_dd">

                                                    {!! Form::select('state_id', ['' => 'Select State'], null, array('id'=>'state_id', 'class'=>'form-control')) !!}

                                                </span>
									<br>
									 </div>
							<div class="col-md-4">
                                                <span id="default_city_dd">

                                                    {!! Form::select('city_id', ['' => 'Select City'], null, array('id'=>'city_id', 'class'=>'form-control')) !!}

                                                </span>
								<br>
									 </div>
									
                                                
									 
                                           <div class="col-md-4">
												<select name="is_active" id="is_active" class="form-control">

                                                    <option value="-1">Is Active?</option>

                                                    <option value="1" >Active</option>

                                                    <option value="0">In Active</option>

                                                </select>
											   <br>
									 </div>
								<div class="col-md-4">
                                                <select name="is_featured" id="is_featured" class="form-control">

                                                    <option value="-1">Is Featured?</option>

                                                    <option value="1">Featured</option>

                                                    <option value="0">Not Featured</option>

                                                </select>
									<br>
									 </div>
									 <div class="col-md-4">
                                                <select name="admin_reviewed" id="admin_reviewed" class="form-control">
                                                    
                                                    <option value="-1">Is Reviewed?</option>

                                                    <option value="1">Reviewed</option>

                                                    <option value="0">Not Reviewed</option>

                                                </select>
										 <br>
									 </div>
                                                

                                        </div>
								
								
								
								
                                <table class="table table-striped table-bordered table-hover"  id="jobDatatableAjax">

                                    <thead>

                                       

                                        <tr role="row" class="heading">

                                            <th>Company</th>

                                            <th>Job title</th>

                                            <th>Job description</th>

                                            <th>City</th>

                                            <th>Date Posted</th>
                                            <th>Status</th>

                                            <th style="width: 75px;">Actions</th>
                                            <th class="text-center">
                                                <button class="btn btn-danger delete">Delete</button>
                                                <br>Select All: <br>
                                                <input type="checkbox" id="selectall" name="selectall" onclick="toggle(this)" autocomplete="off">
                                            </th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    </tbody>

                                </table>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>



    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> </div> <!-- Modal body --> 
                <form class="customRadio customCheckbox m-0 p-0">
                    <input type="hidden" name="job_id" id="job_id">
                <div class="modal-body mb-0 pb-0 mt-0">
                    <div class="container ">
                        <!-- custom radio button -->
                        <div class="holder">
                            <div class="row mb-1">
                                <div class="col">
                                    <h2>Choose Time Schedule</h2>
                                </div>
                            </div>
                           
                                <div class="row mb-0">
                                    <div class="row justify-content-start">
                                        <div class="col-12">
                                    <div class="row">
                                        <label class="radio-inline">
                                          <input type="radio" name="day_shudule"  value="+1 days" checked>Daily
                                        </label>
                                    </div>
                                    <div class="row">
                                        <label class="radio-inline">
                                          <input type="radio" name="day_shudule" value="+2 days">Every 2 Days
                                        </label>
                                    </div>
                                    
                                    <div class="row">
                                        <label class="radio-inline">
                                          <input type="radio" value="+3 days" name="day_shudule">Every 3 Days
                                        </label>
                                    </div> 
                                    <div class="row">
                                        <label class="radio-inline">
                                          <input type="radio" value="+4 days" name="day_shudule">Every 4 Days
                                        </label>
                                    </div>
                                    <div class="row">
                                        <label class="radio-inline">
                                          <input type="radio" value="+5 days" name="day_shudule">Every 5 Days
                                        </label>
                                    </div>
                                    <div class="row">
                                       <label class="radio-inline">
                                          <input type="radio" value="+6 days" name="day_shudule">Every 6 Days
                                        </label>
                                    </div>

                                    <div class="row">
                                       <label class="radio-inline">
                                          <input type="radio" value="+7 days" name="day_shudule">Every 7 Days
                                        </label>
                                    </div>
                                </div>
                                </div>
                                </div>
                               
                                

                                

                                 

                                 

                                

                                        
                               
                                
                                
                            
                        </div>
                    </div>
                </div> <!-- Modal footer -->
                <div class="modal-footer pt-0 mt-0 pb-5 pr-6 m-1 ">
                    <div class="col-2"> </div>
                    
                    
                    <div class="col-2 justify-content-start m-0 p-0"> <button type="button" class="btn btn-success box-shadow--16dp submit-form" data-dismiss="modal">Submit</button> </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="rejectedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="rejected_form">
        @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <input type="hidden" name="id" id="rejected_job_id">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Reason:</label>
            <textarea class="form-control" id="reason" name="reason"></textarea>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save_changes">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection

@push('scripts') 

<script>
    @if(request()->company_id)
    $('#company_id').val('{{request()->company_id}}');
    @endif
    $(function () {

        var oTable = $('#jobDatatableAjax').DataTable({

            processing: true,

            serverSide: true,

            stateSave: false,

            searching: false,
            

            pagingType: 'full_numbers',

            /*		

             "order": [[1, "asc"]],            

             paging: true,

             info: true,

             */

            ajax: {

                url: '{!! route('fetch.data.jobs') !!}',

                data: function (d) {

                    d.company_id = $('#company_id').val();

                    d.title = $('#title').val();

                    d.description = $('#description').val();

                    d.country_id = $('#country_id').val();

                    d.state_id = $('#state_id').val();

                    d.city_id = $('#city_id').val();
     
                    d.is_active = $('#is_active').val();

                    d.is_featured = $('#is_featured').val();
                    d.admin_reviewed = $('#admin_reviewed').val();
                    d.status = 'expired';

                }

            }, columns: [

                {data: 'company_id', name: 'company_id'},

                {data: 'title', name: 'title'},

                {data: 'description', name: 'description'},

                {data: 'city_id', name: 'city_id'},

                {data: 'created_at', name: 'created_at'},
                {data: 'status', name: 'status'},

                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},

            ]

        });

        $('#job-search-form').on('submit', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#company_id').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#title').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#description').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#country_id').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

            filterDefaultStates(0);

        });

        $(document).on('change', '#state_id', function (e) {

            oTable.draw();

            e.preventDefault();

            filterDefaultCities(0);

        });

        $(document).on('change', '#city_id', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#is_active').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#admin_reviewed').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#is_featured').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        filterDefaultStates(0);

    });
    $(document).ready(function() {
        toggle = function(source) {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
        checkboxes[i].checked = source.checked;
        }
        }
        $('.delete').on('click',function(){
            var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
                return this.value;
            }).get();
            var ids = checkedVals.join(",");
            $.ajax({
              method: "GET",
              url: "{{route('delete.jobs')}}",
              data: { ids: ids}
            })
            .done(function( msg ) {
                location.reload();
            });
        })
    });
    @if(isset(request()->is_active))
    $('#is_active').val({{request()->is_active}});
    @endif

    @if(isset(request()->admin_reviewed))
    $('#admin_reviewed').val({{request()->admin_reviewed}});
    @endif

    function deleteJob(id, is_default) {

        var msg = 'Please confirm you want to delete Job: '+$('#job-'+id).data('job');

        if (confirm(msg)) {

            $.post("{{ route('delete.job') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        if (response == 'ok')

                        {

                            var table = $('#jobDatatableAjax').DataTable();

                            table.row('jobDtRow' + id).remove().draw(false);

                        } else

                        {

                            alert('Request Failed!');

                        }

                    });

        }

    }

    function makeActive(id) {

        $.post("{{ route('make.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobDatatableAjax').DataTable();

                        table.row('jobDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeReviewed(id) {

        $.post("{{ route('make.reviewed.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobDatatableAjax').DataTable();

                        table.row('jobDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }
    $('.save_changes').on('click',function(){
    	$.post("{{ route('make.notreviewed.job') }}", {id: $('#rejected_job_id').val(),reason: $('#reason').val(), _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {
                    	$('#rejectedModal').modal('hide');
                        var table = $('#jobDatatableAjax').DataTable();

                        table.row('jobDtRow' + $('#rejected_job_id').val()).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });
    })
    function makeNotReviewed(id) {

        $('#rejected_job_id').val('');
        $('#rejected_job_id').val(id);
        $('#rejectedModal').modal('show');

    }




    function makeNotActive(id) {

        $.post("{{ route('make.not.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobDatatableAjax').DataTable();

                        table.row('jobDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeFeatured(id) {

        $.post("{{ route('make.featured.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobDatatableAjax').DataTable();

                        table.row('jobDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeNotFeatured(id) {

        $.post("{{ route('make.not.featured.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobDatatableAjax').DataTable();

                        table.row('jobDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function filterDefaultStates(state_id)

    {

        var country_id = $('#country_id').val();

        if (country_id != '') {

            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_state_dd').html(response);

                    });

        }

    }

    function filterDefaultCities(city_id)

    {

        var state_id = $('#state_id').val();

        if (state_id != '') {

            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_city_dd').html(response);

                    });

        }

    }

</script>

@endpush