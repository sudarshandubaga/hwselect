<?php
$lang = config('default_lang');
if (isset($jobSkill))
    $lang = $jobSkill->lang;
$lang = MiscHelper::getLang($lang);
$direction = MiscHelper::getLangDirection($lang);
$queryString = MiscHelper::getLangQueryStr();
?>
<?php $country = App\Country::where('lang','en')->get(); ?>
{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body" style="width: 100%">
    {!! Form::hidden('id', null) !!}

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company_id') !!}" id="company_id_div">
        <div class="row">
            <div class="col-md-8">
                {!! Form::label('company_id', 'Company', ['class' => 'bold']) !!}
                <span id="company_dd">
                    {!! Form::select('company_id', ['' => 'Select Company']+$companies, null,
                    array('class'=>'form-control', 'id'=>'company_id')) !!}
                </span>
                {!! APFrmErrHelp::showErrors($errors, 'company_id') !!}
            </div>
            <div class="col-md-2">
                <a style="margin-top: 24px;" href="#" id="btn-edit-company" class="btn btn-success">Edit</a>
            </div>
            <div class="col-md-2">
                <a style="margin-top: 24px;" href="javascript:;" id="btn-register" class="btn btn-primary">Add
                    Company</a>
            </div>
        </div>
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'title') !!}">
        {!! Form::label('title', 'Job title', ['class' => 'bold']) !!}
        {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'maxlength'=>'75',
        'placeholder'=>'Job title')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'title') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}">
        {!! Form::label('description', 'Job description', ['class' => 'bold']) !!}
        {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>'Job
        description')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'description') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company_id') !!}" id="company_id_div">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('skills', 'To select skillset from dropdown, enter the first few characters, or add
                skillsets required', ['class' => 'bold']) !!}
                <?php
		        	$skills = old('skills', $jobSkillIds);
		        ?>
                <span id="skills_dd">
                    {!! Form::select('skills[]', $jobSkills, $skills, array('class'=>'form-control select2-multiple',
                    'id'=>'skills', 'multiple'=>'multiple')) !!}
                </span>
                {!! APFrmErrHelp::showErrors($errors, 'skills') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-skill"
                    class="btn btn-primary">Add Skill</a></div>
        </div>

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}" id="country_id_div">
        {!! Form::label('country_id', 'Country', ['class' => 'bold']) !!}
        <?php $arra = array(
            // 230=>'United Kingdom',
            // 231=>'United States of America',
        ); ?>
        {!! Form::select('country_id', $arra+$countries, old('country_id', (isset($job))?
        $job->country_id:$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}" id="state_id_div">
        {!! Form::label('state_id', 'County / State / Province / District', ['class' => 'bold']) !!}
        <span id="default_state_dd">
            {!! Form::select('state_id', ['' => 'Select County / State / Province / District'], null,
            array('class'=>'form-control', 'id'=>'state_id')) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city_id') !!}" id="city_id_div">
        {!! Form::label('city_id', 'Town / City', ['class' => 'bold']) !!}
        <span id="default_city_dd">
            {!! Form::select('city_id', ['' => 'Select Town / City'], null, array('class'=>'form-control',
            'id'=>'city_id')) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'city_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'address') !!}" id="address">
        {!! Form::label('city_id', 'Address (A full address must be input into this field for google search and maps to
        work, Please add Full Address including Post/Zip Code)', ['class' => 'bold']) !!}
        {!! Form::text('address', null, array('class'=>'form-control', 'id'=>'current_location',
        'placeholder'=>__('Address'))) !!} {!! APFrmErrHelp::showErrors($errors, 'address') !!} </div>

    {!! Form::hidden('latitude', null, array('class'=>'form-control', 'id'=>'latitude', 'placeholder'=>__('Address')))
    !!} {!! APFrmErrHelp::showErrors($errors, 'latitude') !!}
    {!! Form::hidden('longitude', null, array('class'=>'form-control', 'id'=>'longitude', 'placeholder'=>__('Address')))
    !!} {!! APFrmErrHelp::showErrors($errors, 'longitude') !!}

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_freelance') !!}" style="display: none;">
        {!! Form::label('is_freelance', 'Is Freelance?', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_freelance_1 = '';
            $is_freelance_2 = 'checked="checked"';
            if (old('is_freelance', ((isset($job)) ? $job->is_freelance : 0)) == 1) {
                $is_freelance_1 = 'checked="checked"';
                $is_freelance_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="is_freelance_yes" name="is_freelance" type="radio" value="1" {{$is_freelance_1}}>
                Yes </label>
            <label class="radio-inline">
                <input id="is_freelance_no" name="is_freelance" type="radio" value="0" {{$is_freelance_2}}>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_freelance') !!}
    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company_id') !!}" id="company_id_div">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('career_level_id', 'Career level', ['class' => 'bold']) !!}
                <span id="career_level_dd">
                    {!! Form::select('career_level_id', ['' => 'Select Career level']+$careerLevels, null,
                    array('class'=>'form-control', 'id'=>'career_level_id')) !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-career-level"
                    class="btn btn-primary">Add Career level</a></div>
        </div>

    </div>


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company_id') !!}" id="company_id_div">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('salary_period_id', 'Salary Period', ['class' => 'bold']) !!}
                <span id="salary_period_dd">
                    {!! Form::select('salary_period_id', ['' => 'Select Salary Period']+$salaryPeriods, null,
                    array('class'=>'form-control', 'id'=>'salary_period_id')) !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'salary_period_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-salary_period"
                    class="btn btn-primary">Add Salary Period</a></div>
        </div>
    </div>
    <?php
     $select_opt = array(
        'single_salary' => 'Single Salary',
        'salary_in_range' => 'Salary in Range',
        'negotiable' => 'If Unspecified Salary, enter information below',
     );

     ?>

    <div id="salary_type_div" class="form-group {!! APFrmErrHelp::hasError($errors, 'site_user') !!}">
        {!! Form::label('salary_type', 'Salary Type', ['class' => 'bold']) !!}
        {!! Form::select('salary_type', [''=>'Select Salary Type']+$select_opt, null, array('class'=>'form-control',
        'id'=>'salary_type')) !!}
    </div><?php
    if(old('salary_type')=='negotiable'){
        $field_type = 'text';
    } else if(isset($job) && $job->salary_type=='negotiable'){
        $field_type = 'text';
    } else {
        $field_type = 'number';
    } ?>


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_from') !!}" id="salary_from_div">
        {!! Form::label('salary_from', 'Salary From', ['class' => 'bold']) !!} <span>Please enter number only (Example:
            25000)</span>
        {!! Form::$field_type('salary_from', null, array('class'=>'form-control',
        'id'=>'salary_from','maxlength'=>8,'oninput'=>"javascript: if (this.value.length > this.maxLength) this.value =
        this.value.slice(0, this.maxLength)")) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_from') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_to') !!}" id="salary_to_div">
        {!! Form::label('salary_to', 'Salary To', ['class' => 'bold']) !!} <span>Enter numbers only (Example: £35,500
            would be entered as 35500)</span>
        {!! Form::number('salary_to', null, array('class'=>'form-control',
        'id'=>'salary_to','maxlength'=>8,'oninput'=>"javascript: if (this.value.length > this.maxLength) this.value =
        this.value.slice(0, this.maxLength)")) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_to') !!}
    </div>

    <?php 
    $default_currecny = App\CountryDetail::where('code','like','%'.$siteSetting->default_currency_code.'%')->first();
    $d_cur = $default_currecny->symbol;
     ?>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}" id="salary_currency_div">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('salary_currency', 'Salary Currency', ['class' => 'bold']) !!}
                <span id="bonus_dd">
                {!! Form::select('salary_currency', ['' => 'Select Salary Currency']+$currencies, (isset($job))? 
                    $job->salary_currency:$d_cur, array('class'=>'form-control currency', 'id'=>'salary_currency')) !!}
                </span>
                {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!}
            </div>
            <!-- <div class="col-md-2">
                <a style="margin-top: 24px;" href="javascript:;" id="btn-add-currency" class="btn btn-primary">
                    Add Currency
                </a>
            </div> -->
        </div>

    </div>




    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'hide_salary') !!}" style="display: none;">
        {!! Form::label('hide_salary', 'Hide Salary?', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $hide_salary_1 = '';
            $hide_salary_2 = 'checked="checked"';
            if (old('hide_salary', ((isset($job)) ? $job->hide_salary : 0)) == 1) {
                $hide_salary_1 = 'checked="checked"';
                $hide_salary_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="hide_salary_yes" name="hide_salary" type="radio" value="1" {{$hide_salary_1}}>
                Yes </label>
            <label class="radio-inline">
                <input id="hide_salary_no" name="hide_salary" type="radio" value="0" {{$hide_salary_2}}>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'hide_salary') !!}
    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'bonus') !!}" id="company_id_div">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('bonus', 'Bonus', ['class' => 'bold']) !!}
                <?php
                    $bonuss = old('bonus', $bonusIds);
                ?>
                <span id="bonus_dd">
                    {!! Form::select('bonus[]', $bonus, $bonuss, array('class'=>'form-control select2-multiple-bonus',
                    'id'=>'bonus', 'multiple'=>'multiple')) !!}
                </span>
                {!! APFrmErrHelp::showErrors($errors, 'bonus') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-bonus"
                    class="btn btn-primary">Add Bonus</a></div>
        </div>

    </div>


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'benifits') !!}" id="company_id_div">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('benifits', 'Benefits', ['class' => 'bold']) !!}
                <?php
                    $benifitss = old('benifits', $benifitsIds);
                ?>
                <span id="benifits_dd">
                    {!! Form::select('benifits[]', $benifits, $benifitss, array('class'=>'form-control
                    select2-multiple-benifits', 'id'=>'benifits', 'multiple'=>'multiple')) !!}
                </span>
                {!! APFrmErrHelp::showErrors($errors, 'benifits') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-benifits"
                    class="btn btn-primary">Add Benifits</a></div>
        </div>

    </div>


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('functional_area_id', 'Functional Area', ['class' => 'bold']) !!}
                <span id="functional_area_dd">
                    {!! Form::select('functional_area_id', ['' => 'Select Functional Area']+$functionalAreas, null,
                    array('class'=>'form-control', 'id'=>'functional_area_id')) !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-functional_area"
                    class="btn btn-primary">Add Functional Area</a></div>
        </div>

    </div>



    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_type_id') !!}">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('job_type_id', 'Contractual Hours', ['class' => 'bold']) !!}
                <span id="job_type_dd">
                    {!! Form::select('job_type_id', ['' => 'Select Contractual Hours']+$jobTypes, null,
                    array('class'=>'form-control', 'id'=>'job_type_id')) !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'job_type_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-job_type"
                    class="btn btn-primary">Add Contractual Hours</a></div>
        </div>

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_shift_id') !!}">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('job_shift_id', 'Working Hours', ['class' => 'bold']) !!}
                <span id="job_shift_dd">
                    {!! Form::select('job_shift_id', ['' => 'Select Hours of Work']+$jobShifts, null,
                    array('class'=>'form-control', 'id'=>'job_shift_id')) !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'job_type_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-job_shift"
                    class="btn btn-primary">Add Working Hours</a></div>
        </div>

    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'num_of_positions') !!}" id="num_of_positions_div">
        {!! Form::label('num_of_positions', 'Positions Available', ['class' => 'bold']) !!}
        {!! Form::select('num_of_positions', ['' => 'Enter Positions Available']+MiscHelper::getNumPositions(), null,
        array('class'=>'form-control', 'id'=>'num_of_positions')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'num_of_positions') !!}
    </div>
    <?php
        if (isset($job)) {
            $expiry_date = old('expiry_date')?date('d-m-Y',strtotime(old('expiry_date'))):date('d-m-Y',strtotime($job->expiry_date));
         }else{
            $expiry_date = old('expiry_date')?date('d-m-Y',strtotime(old('expiry_date'))):date('d-m-Y');
         } 
            
         ?>
    <?php 
    $expiry_dates  = array(
                    '1 week' => '1 week',
                    '2 week' => '2 weeks',
                    '1 month' => '1 month',
                    '2 month' => '2 months',
                    '3 month' => '3 months',
                    '6 month' => '6 months',
                );
    ?>
    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'expiry_datee') !!}">
        {!! Form::label('expiry_datee', 'Job expiry duration', ['class' => 'bold']) !!}
        {!! Form::select('expiry_datee', ['' => __('Job expiry date')]+$expiry_dates + ['custom date' => 'custom date'], old('expiry_datee', (isset($job))?
        $job->expiry_datee:$siteSetting->default_months), array('class'=>'form-control', 'id'=>'expiry_datee')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'expiry_datee') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'expiry_date') !!}" id="expiry_date_group" @if(empty($job) || $job->expiry_datee != 'custom date') style="display: none" @endif>
        {!! Form::label('expiry_date', 'Job expiry date', ['class' => 'bold']) !!}
        {!! Form::date('expiry_date',  old('expiry_date', (isset($job))?
        date("Y-m-d", strtotime($job->expiry_date)) : date("Y-m-d", strtotime("+" . $siteSetting->default_months . "s", time()))), array('class'=>'form-control', 'id'=>'expiry_date')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'expiry_date') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'degree_level_id') !!}">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('degree_level_id', 'Required Qualifications', ['class' => 'bold']) !!}
                <span id="degree_level_dd">
                    {!! Form::select('degree_level_id', ['' => 'Select Required Qualifications']+$degreeLevels, null,
                    array('class'=>'form-control', 'id'=>'degree_level_id')) !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'degree_level_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-degree_level"
                    class="btn btn-primary">Add Qualification</a></div>
        </div>

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
        <div class="row">
            <div class="col-md-10">
                {!! Form::label('job_experience_id', 'Length of Service', ['class' => 'bold']) !!}
                <span id="job_experience_dd">
                    {!! Form::select('job_experience_id', ['' => 'Length of time served in this
                    sector']+$jobExperiences, null, array('class'=>'form-control', 'id'=>'job_experience_id'))
                    !!}</span>
                {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!}
            </div>
            <div class="col-md-2"><a style="margin-top: 24px;" href="javascript:;" id="btn-add-job_experience"
                    class="btn btn-primary">Add job experience</a></div>
        </div>

    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">
        {!! Form::label('is_active', 'Is Active?', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($job)) ? $job->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                Active </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                In-Active </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_featured') !!}">
        {!! Form::label('is_featured', 'Is Featured?', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_featured_1 = '';
            $is_featured_2 = 'checked="checked"';
            if (old('is_featured', ((isset($job)) ? $job->is_featured : 0)) == 1) {
                $is_featured_1 = 'checked="checked"';
                $is_featured_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="featured" name="is_featured" type="radio" value="1" {{$is_featured_1}}>
                Featured </label>
            <label class="radio-inline">
                <input id="not_featured" name="is_featured" type="radio" value="0" {{$is_featured_2}}>
                Not Featured </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_featured') !!} </div>
    <div class="form-actions">
        {!! Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn
        btn-large btn-primary', 'type'=>'submit')) !!}
    </div>
</div>

@push('css')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
</style>
@endpush
@push('scripts')
@include('admin.shared.tinyMCEFront')
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="employer_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="registerModalLabel">Register Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Add Company Name:</label>
                        <input type="text" name="name" class="form-control company_name employer_name" maxlength="50"
                            placeholder="{{__('Name')}}" value="{{old('name')}}">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Email:</label>
                                <input type="email" name="email" class="form-control employer_email" maxlength="50"
                                    placeholder="{{__('Email')}}" value="{{old('email')}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group compphone">
                                <select class="form-control vodiapicker_mobile_num" name="phone_code_mobile_num"
                                    id="phone_code_mobile_num">
                                    @if(null!==($country))
                                    @foreach($country as $count)
                                    <?php 
                $detail = DB::table('countries_details')->where('id',$count->id)->first();
                ?>
                                    @if(null!==($detail) && $detail->sort_name)
                                    <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?>
                                        value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}"
                                        data-thumbnail="{{asset('flags/')}}/{{$count->flag}}">{{$detail->sort_name}}
                                    </option>
                                    @endif
                                    @endforeach
                                    @endif


                                </select>
                                <div class="lang-select-mobile_num">

                                    <a class="btn-select-mobile_num cselect"
                                        value="{{old('mobile_num_code')?old('mobile_num_code'):null}}">
                                        <?php 

                    $phone_detail = DB::table('countries_details')->where('id',230)->first(); 
                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }
                    

                    ?>
                                        @if(null!==($phone_detail))
                                        <img src="{{asset('flags/')}}/{{$countr->flag}}" alt="">
                                        <span>{{$phone_detail->sort_name}}</span>
                                        @endif

                                        <i class="fa fa-caret-down"></i>


                                    </a>
                                    <input type="hidden" id="mobile_num_code" name="mobile_num_code"
                                        value="{{old('mobile_num_code')?old('mobile_num_code'):'+'.$phone_detail->phone_code}}">
                                    <div class="b-mobile_num">
                                        <input type="text" onkeyup="myFunction_emp()" name="search" id="search_emp"
                                            placeholder="Search" style="width: 100px;">
                                        <ul id="a-mobile_num"></ul>
                                    </div>
                                </div>
                                {!! Form::text('phone', '+'.$phone_detail->phone_code,
                                array('onchange'=>"staticDataMobile()"
                                ,'onkeyup'=>"staticDataMobile()",'class'=>'form-control', 'id'=>'mobile_num',
                                'maxlength'=>'18', 'placeholder'=>__('Phone'))) !!}




                            </div>

                        </div>
                    </div>



                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Password:</label>
                        <input type="password" name="password" id="password_emp" class="form-control password"
                            placeholder="{{__('Password')}}" value="">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Password Confirmation:</label>
                        <input type="password" name="password_confirmation" id="confirm-password" class="form-control"
                            placeholder="{{__('Password Confirmation')}}" value="">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-skill-model" tabindex="-1" role="dialog" aria-labelledby="skillModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="skill_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="skillModalLabel">Add Skill</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
                        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_skill') !!}">
                        {!! Form::label('job_skill', 'Enter Job Skill', ['class' => 'bold']) !!}
                        {!! Form::text('job_skill', null, array('class'=>'form-control job_skill', 'id'=>'job_skill',
                        'placeholder'=>'Enter Job Skill')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'job_skill') !!}
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;">
                        {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
	            $is_default_1 = 'checked="checked"';
	            $is_default_2 = '';
	            if (old('is_default', ((isset($jobSkill)) ? $jobSkill->is_default : 1)) == 0) {
	                $is_default_1 = '';
	                $is_default_2 = 'checked="checked"';
	            }
	            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideJobSkillId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideJobSkillId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!}
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($jobSkill)) ? $jobSkill->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-career-level-modal" tabindex="-1" role="dialog" aria-labelledby="CareerLevelModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="career_level_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="CareerLevelModalLabel">Add Career Level</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'career_level') !!}"> {!!
                        Form::label('career_level', 'Career Level', ['class' => 'bold']) !!}
                        {!! Form::text('career_level', null, array('class'=>'form-control career_level',
                        'id'=>'career_level', 'placeholder'=>'Career Level')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'career_level') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>




<div class="modal fade" id="add-salary-period-modal" tabindex="-1" role="dialog"
    aria-labelledby="salary_periodModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="salary_period_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="salary_periodModalLabel">Add Salary Period</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_period') !!}"> {!!
                        Form::label('salary_period', 'Salary Period', ['class' => 'bold']) !!}
                        {!! Form::text('salary_period', null, array('class'=>'form-control salary_period',
                        'id'=>'salary_period', 'placeholder'=>'Salary Period')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'salary_period') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-functional_area-modal" tabindex="-1" role="dialog"
    aria-labelledby="functional_areaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="functional_area_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="functional_areaModalLabel">Add Salary Period</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'functional_area') !!}"> {!!
                        Form::label('functional_area', 'Functional Area', ['class' => 'bold']) !!}
                        {!! Form::text('functional_area', null, array('class'=>'form-control functional_area',
                        'id'=>'functional_area', 'placeholder'=>'Functional Area')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'functional_area') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-btn-add-job_type-modal" tabindex="-1" role="dialog"
    aria-labelledby="btn-job_typeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="job_type_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="job_typeModalLabel">Add Contractual Hours</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_type') !!}"> {!!
                        Form::label('job_type', 'Contractual Hours', ['class' => 'bold']) !!}
                        {!! Form::text('job_type', null, array('class'=>'form-control job_type', 'id'=>'job_type',
                        'placeholder'=>'Contractual Hours')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'job_type') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-job_shift-modal" tabindex="-1" role="dialog" aria-labelledby="job_shiftModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="job_shift_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="job_shiftModalLabel">Add Contractual Hours</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_shift') !!}"> {!!
                        Form::label('job_shift', 'Contracted Hours of Work', ['class' => 'bold']) !!}
                        {!! Form::text('job_shift', null, array('class'=>'form-control job_shift', 'id'=>'job_shift',
                        'placeholder'=>'Add Contracted Hours of Work')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'job_shift') !!}
                        <span class="error_job_shift error"></span>
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="add-degree_level-modal" tabindex="-1" role="dialog" aria-labelledby="degree_levelModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="degree_level_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="degree_levelModalLabel">Add Qualification Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'degree_level') !!}"> {!!
                        Form::label('degree_level', 'Qualification Type', ['class' => 'bold']) !!}
                        {!! Form::text('degree_level', null, array('class'=>'form-control degree_level',
                        'id'=>'degree_level', 'placeholder'=>'Enter Qualification')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'degree_level') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-job_experience-modal" tabindex="-1" role="dialog"
    aria-labelledby="job_experienceModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="job_experience_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="degree_levelModalLabel">Add Job Experience</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}"> {!! Form::label('lang',
                        'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_experience') !!}"> {!!
                        Form::label('job_experience', 'Job Experience', ['class' => 'bold']) !!}
                        {!! Form::text('job_experience', null, array('class'=>'form-control job_experience',
                        'id'=>'job_experience', 'placeholder'=>'Job Experience')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'job_experience') !!} </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}"
                        style="display: none;"> {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($careerLevel)) ? $careerLevel->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}}
                                    onchange="showHideCareerLevelId();">
                                Yes </label>
                            <label class="radio-inline">
                                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}}
                                    onchange="showHideCareerLevelId();">
                                No </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!} </div>

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
                        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
                        <div class="radio-list">
                            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($careerLevel)) ? $careerLevel->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
                            <label class="radio-inline">
                                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                                Active </label>
                            <label class="radio-inline">
                                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                                In-Active </label>
                        </div>
                        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!} </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add-currency-model" tabindex="-1" role="dialog" aria-labelledby="currencyModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="currency_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="skillModalLabel">Add Currency</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
                        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'bonus') !!}">
                        {!! Form::label('bonus', 'Job Bonus', ['class' => 'bold']) !!}
                        {!! Form::text('bonus', null, array('class'=>'form-control bonus', 'placeholder'=>'Job Bonus'))
                        !!}
                        {!! APFrmErrHelp::showErrors($errors, 'bonus') !!}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add-bonus-model" tabindex="-1" role="dialog" aria-labelledby="bonusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="bonus_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="skillModalLabel">Add Bonus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
                        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'bonus') !!}">
                        {!! Form::label('bonus', 'Job Bonus', ['class' => 'bold']) !!}
                        {!! Form::text('bonus', null, array('class'=>'form-control bonus', 'placeholder'=>'Job Bonus'))
                        !!}
                        {!! APFrmErrHelp::showErrors($errors, 'bonus') !!}
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="add-benifits-model" tabindex="-1" role="dialog" aria-labelledby="benifitsModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="benifits_form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="benifitsModalLabel">Add benifits</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
                        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}
                        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang,
                        array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}
                    </div>
                    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'bonus') !!}">
                        {!! Form::label('benifits', 'Job benifits', ['class' => 'bold']) !!}
                        {!! Form::text('benifits', null, array('class'=>'form-control benifits', 'placeholder'=>'Job
                        benifits')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'benifits') !!}
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">
    change_period('');
    $(document).ready(function () {

        /**
         * @Rudraksha Tech
         */
        $('#expiry_datee').on('change', function () {
            if($(this).val() === 'custom date') {
                $('#expiry_date_group').show();
            } else {
                $('#expiry_date_group').hide();

            }
        });

        $('#salary_from, #salary_to').on('blur', function () {
            var from = $('#salary_from').val(),
                to   = $('#salary_to').val();

            from = parseFloat(from);
            to = parseFloat(to);
            
            if(from != '' && to != '' && from >= to) {
                // $('#salary_to').val(from + 1);
                swal('(Salary To) must be larger than (Salary From) Field')
                    .then(function() {
                        $('#salary_to').focus();
                    });
            }
        });

        // End @Rudraksha Tech

        $('.select2-multiple').select2({
            placeholder: "Select Required Skills",
            allowClear: true,
            maximumSelectionLength: 10,
            language: {
                maximumSelected: function (e) {
                    var t = "You can only select a maximum of " + e.maximum + " skill-sets";
                    return t;
                }
            }
        });
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'yyyy-m-d'
        });



        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterDefaultStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterDefaultCities(0);
        });
        filterDefaultStates( <?php echo old('state_id', (isset($job)) ? $job->state_id : 0); ?> );
    });


    $('#btn-register').on('click', function () {
        $('#register').modal('show');
    })

    $('#company_id').on('change', function () {
        var val = $(this).val();
        add_attr_btn(val);
    }); <?php
    if (isset($job)) {
        $val = old('company_id') ? old('company_id') : $job->company_id; ?>
        add_attr_btn("{{$val}}"); <?php
    } else {
        $val = old('company_id') ? old('company_id') : 0; ?>
        add_attr_btn("{{$val}}"); <?php
    } ?>
    function add_attr_btn(val) {
        if (!val) {
            $('#btn-edit-company').hide();
        } else {
            $('#btn-edit-company').show();
            $('#btn-edit-company').attr('href', "{{url('admin/edit-company/')}}/" + val);
        }

    }

    $('#btn-add-skill').on('click', function () {
        $('.skill_form').trigger('reset');
        $('#job_skill-error').remove();
        $('#add-skill-model').modal('show');
    })


    $('#btn-add-career-level').on('click', function () {
        $('.career_level_form').trigger('reset');
        $('#career_level-error').remove();
        $('#add-career-level-modal').modal('show');
    })

    $('#btn-add-salary_period').on('click', function () {
        $('.salary_period_form').trigger('reset');
        $('#salary_period-error').remove();
        $('#add-salary-period-modal').modal('show');
    })

    $('#btn-add-functional_area').on('click', function () {
        $('.functional_area_form').trigger('reset');
        $('#functional_area-error').remove();
        $('#add-functional_area-modal').modal('show');
    })


    $('#btn-add-job_type').on('click', function () {
        $('.job_type_form').trigger('reset');
        $('#job_type-error').remove();
        $('#add-btn-add-job_type-modal').modal('show');
    })

    $('#btn-add-job_shift').on('click', function () {
        $('.job_shift_form').trigger('reset');
        $('#job_shift-error').remove();
        $('#add-job_shift-modal').modal('show');
    })


    $('#btn-add-degree_level').on('click', function () {
        $('.degree_level_form').trigger('reset');
        $('#degree_level-error').remove();
        $('#add-degree_level-modal').modal('show');
    })


    $('#btn-add-job_experience').on('click', function () {
        $('.job_experience_form').trigger('reset');
        $('#job_experience-error').remove();
        $('#add-job_experience-modal').modal('show');
    })

    $('.select2-multiple-bonus').select2({

        placeholder: "{{__('Select Bonus')}}",

        allowClear: true

    });

    $('.select2-multiple-benifits').select2({

        placeholder: "{{__('Select Benefits')}}",

        allowClear: true

    });


    function filterDefaultStates(state_id) {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.default.states.dropdown') }}", {
                    country_id: country_id,
                    state_id: state_id,
                    _method: 'POST',
                    _token: '{{ csrf_token() }}'
                })
                .done(function (response) {
                    $('#default_state_dd').html(response);
                    filterDefaultCities( <?php echo old('city_id', (isset($job)) ? $job->city_id : 0); ?> );
                });
        }
    }

    function filterDefaultCities(city_id) {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.default.cities.dropdown') }}", {
                    state_id: state_id,
                    city_id: city_id,
                    _method: 'POST',
                    _token: '{{ csrf_token() }}'
                })
                .done(function (response) {
                    $('#default_city_dd').html(response);
                });
        }
    }
    $('.employer_name').on('keypress', function (event) {
        var regex = new RegExp(
            "^[a-zA-Z0-9,:\S/-:\S/&:\S/@:\S/£:\S/$:\S/€:\S/¥:\S/#:\S/.:\S/,:\S/::\S/;:\S/-]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    if ($(".form").length > 0) {
        $(".form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                title: {
                    required: true,
                },

                description: {
                    required: true,
                    minlength: 250,
                },

                'skills[]': {
                    required: true,
                },

                country_id: {
                    required: true,
                },

                state_id: {
                    required: true,
                },

                city_id: {
                    required: true,
                },

                address: {
                    required: true,
                },

                career_level_id: {
                    required: true,
                },

                functional_area_id: {
                    required: true,
                },

                salary_period_id: {
                    required: true,
                },

                salary_currency: {
                    required: true,
                },

                salary_from: {
                    required: true,
                },

                salary_type: {
                    required: true,
                },

                salary_to: {
                    required: function (element) {
                        if ($('#salary_type').val().toLowerCase() == "salary_in_range") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },

                job_type_id: {
                    required: true,
                },

                job_shift_id: {
                    required: true,
                },

                num_of_positions: {
                    required: true,
                },
            },
            messages: {

                title: {
                    required: "Please enter a job title for this role",
                },
                description: {
                    required: "Job Description Minimum Length Required 500 characters",
                    minlength: "Please enter at least 250 characters",
                },

                'skills[]': {
                    required: "Please Select Skills",
                },

                country_id: {
                    required: "Country is required",
                },

                state_id: {
                    required: "Please Select County/State",
                },

                city_id: {
                    required: "Please Select Town/City",
                },

                address: {
                    required: "Please enter Address",
                },

                career_level_id: {
                    required: "Please Select Career Level",
                },

                functional_area_id: {
                    required: "Please Select Functional Area",
                },

                salary_currency: {
                    required: "Please Select Salary Currency",
                },

                salary_type: {
                    required: "Please Select Salary type",
                },

                salary_from: {
                    required: function (element) {
                        if ($('#salary_type').val().toLowerCase() == "salary_in_range") {
                            return 'Please complete Salary in Range fields';
                        } else if ($('#salary_type').val().toLowerCase() == "single_salary") {
                            return 'Please complete Single Salary Field';
                        } else {
                            return 'Please enter Salary text information';
                        }
                    },
                },

                salary_to: {
                    required: "Please complete Salary in Range fields",
                },

                salary_period_id: {
                    required: "Please Select Salary Period",
                },

                job_shift_id: {
                    required: "Please select Working Hours",
                },

                job_type_id: {
                    required: "Please select Contractual Hours",
                },

                num_of_positions: {
                    required: "Please select Positions Available",
                },
            },

        })
    }

    if ($(".employer_form").length > 0) {
        $(".employer_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                name: {
                    required: true,
                    maxlength: 30,
                    minlength: 2,
                    remote: {
                        type: 'get',
                        url: "{{url('check-employer_name')}}",
                        data: {
                            employer_name: function () {
                                return $(".employer_name").val();
                            }
                        },
                        dataType: 'json'
                    },
                },

                phone: {
                    required: true,
                    minlength: 11,
                },

                email: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "{{url('check-employer_email')}}",
                        data: {
                            employer_email: function () {
                                return $(".employer_email").val();
                            }
                        },
                        dataType: 'json'
                    },
                },

                password: {
                    minlength: 8,
                    required: true,
                },


                password_confirmation: {
                    equalTo: "#password_emp"
                }
            },
            messages: {

                name: {
                    required: "Name Required",
                    remote: "Already available. Close & select from Company dropdown",
                },


                email: {
                    required: "Email Required",
                    remote: "Already available. Close & select from Company dropdown",
                },

                password: {
                    required: "Password Required",
                },

                phone: {
                    required: "Phone Number Required",
                    minlength: "Please Enter Valid Phone Number",
                },
                password_confirmation: "Password and Confirm Password Fields do not Match",
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('company.job.register')}}",
                    type: "POST",
                    data: $('.employer_form').serialize(),
                    success: function (res) {

                        $('#company_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Company (' + $(".employer_name").val() +
                                ') added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#register').modal('hide');

                    }
                });
            }

        })
    }



    if ($(".skill_form").length > 0) {
        var boxInput = $(".skill_form").find('input[name="job_skill"]').val();
        $(".skill_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                job_skill: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-jobskill",
                        data: {
                            job_skill: function () {
                                return $(".job_skill").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language Required",
                },
                job_skill: {
                    required: "Job Skill Title Required",
                    remote: boxInput + "Already available. Close & select from Job skill dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var skills = $('#skills').val();
                $.ajax({
                    url: "{{route('skill.job.add')}}",
                    type: "POST",
                    data: $('.skill_form').serialize() + '&skills=' + skills,
                    success: function (res) {

                        $('#skills_dd').html(res);
                        $('.select2-multiple').select2({
                            placeholder: "Select Required Skills",
                            allowClear: true,
                            maximumSelectionLength: 10,
                            language: {
                                maximumSelected: function (e) {
                                    var t = "You can only select a maximum of " + e.maximum + " skill-sets";
                                    return t;
                                }
                            }
                        });

                        swal({

                            title: "Success",

                            text: 'Job Skill (' + $(".job_skill").val() +
                                ') Added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $(".skill_form").trigger("reset");
                        $('#add-skill-model').modal('hide');

                    }
                });
            }

        })
    }

    if ($(".career_level_form").length > 0) {
        $(".career_level_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                career_level: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-career_level",
                        data: {
                            career_level: function () {
                                return $(".career_level").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language Required",
                },
                career_level: {
                    required: "Career Level Required",
                    remote: "Already available. Close & select from Career level dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('career_level.job.add')}}",
                    type: "POST",
                    data: $('.career_level_form').serialize(),
                    success: function (res) {
                        $('#career_level_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Career level (' + $(".career_level").val() +
                                ') added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-career-level-modal').modal('hide');

                    }
                });
            }

        })
    }


    if ($(".salary_period_form").length > 0) {
        $(".salary_period_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                salary_period: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-salary-period",
                        data: {
                            salary_period: function () {
                                return $(".salary_period").val();
                            }
                        },
                        dataType: 'json'
                    },
                },


            },
            messages: {

                lang: {
                    required: "Language required",
                },
                salary_period: {
                    required: "Enter New Salary Period",
                    remote: "Already available. Close & select from Salary Period dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('salary_period.job.add')}}",
                    type: "POST",
                    data: $('.salary_period_form').serialize(),
                    success: function (res) {
                        $('#salary_period_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Salary Period (' + $(".salary_period").val() +
                                ') added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-salary-period-modal').modal('hide');

                    }
                });
            }

        })
    }

    if ($(".functional_area_form").length > 0) {
        $(".functional_area_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                functional_area: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-functional_area",
                        data: {
                            functional_area: function () {
                                return $(".functional_area").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language required",
                },
                functional_area: {
                    required: "Functional area required",
                    remote: "Already available. Close & select from Functional Area dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('functional_area.job.add')}}",
                    type: "POST",
                    data: $('.functional_area_form').serialize(),
                    success: function (res) {
                        $('#functional_area_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Functional area (' + $(".functional_area").val() +
                                ') added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-functional_area-modal').modal('hide');

                    }
                });
            }

        })
    }


    if ($(".job_type_form").length > 0) {
        $(".job_type_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                job_type: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-job_type",
                        data: {
                            functional_area: function () {
                                return $(".job_type").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language required",
                },
                job_type: {
                    required: "Contractual Hours Required",
                    remote: "Already available. Close & select from Contractual Hours dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('job_type.job.add')}}",
                    type: "POST",
                    data: $('.job_type_form').serialize(),
                    success: function (res) {

                        $('#job_type_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Job Type (' + $(".job_type").val() +
                                ') added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-btn-add-job_type-modal').modal('hide');

                    }
                });
            }

        })
    }


    if ($(".job_shift_form").length > 0) {
        $(".job_shift_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                job_shift: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-job_shift",
                        data: {
                            job_shift: function () {
                                return $(".job_shift").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language Required",
                },
                job_shift: {
                    required: "Working Hours Required",
                    remote: "Already available. Close & select from Contracted Hours dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('job_shift.job.add')}}",
                    type: "POST",
                    data: $('.job_shift_form').serialize(),
                    success: function (res) {
                        $('#job_shift_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Contracted Hours (' + $(".job_shift").val() +
                                ') added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-job_shift-modal').modal('hide');

                    },
                    error: function (xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        $('.error_job_shift').html(err.errors.job_shift[0]);
                    }
                });
            }

        })
    }


    $('#btn-add-bonus').on('click', function () {
        $('.bonus').val('');
        $('#add-bonus-model').modal('show');
    });

    $('#btn-add-currency').on('click', function () {
        $('.currency').val('');
        $('#add-currency-model').modal('show');
    });

    $('#btn-add-benifits').on('click', function () {
        $('.benifits').val('');
        $('#add-benifits-model').modal('show');
    })

    if ($(".bonus_form").length > 0) {
        $(".bonus_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                bonus: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: '{{url("/check-bonus")}}',
                        data: {
                            bonus: function () {
                                return $(".bonus").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language Required",
                },
                bonus: {
                    required: "Bonus Required",
                    remote: "Already available. Close & select from Add Bonus dropdown."
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var bonus_m = $('#bonus').val();
                $.ajax({
                    url: "{{route('store-bonus')}}",
                    type: "POST",
                    data: $('.bonus_form').serialize() + '&bonus_m=' + bonus_m,
                    success: function (res) {

                        $('#bonus_dd').html(res);
                        $('.select2-multiple-bonus').select2({
                            placeholder: "Select Bonus",
                            allowClear: true
                        });

                        swal({

                            title: "Success",

                            text: 'Bonus (' + $(".bonus").val() +
                                ') Added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $(".bonus_form").trigger("reset");
                        $('#add-bonus-model').modal('hide');

                    }
                });
            }

        })
    }

    if ($(".benifits_form").length > 0) {
        $(".benifits_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                benifits: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: '{{url("/check-benifits")}}',
                        data: {
                            benifits: function () {
                                return $(".benifits").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language required",
                },
                benifits: {
                    required: "Benefit Required",
                    remote: "Already available. Close & select from Add Benefit dropdown."
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var benifits_m = $('#benifits').val();
                $.ajax({
                    url: "{{route('store-benifits')}}",
                    type: "POST",
                    data: $('.benifits_form').serialize() + '&benifits_m=' + benifits_m,
                    success: function (res) {

                        $('#benifits_dd').html(res);
                        $('.select2-multiple-benifits').select2({
                            placeholder: "Select benefits",
                            allowClear: true
                        });

                        swal({

                            title: "Success",

                            text: 'Benifits (' + $(".benifits").val() +
                                ') Added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $(".benifits_form").trigger("reset");
                        $('#add-benifits-model').modal('hide');

                    }
                });
            }

        })
    }

    @if(isset($job)) <?php $val = old('salary_type') ? old('salary_type') : $job->salary_type; ?>

    change_period("{{$val}}");
    @else
        <?php $val = old('salary_type') ? old('salary_type') : ''; ?>
    change_period("{{$val}}");
    @endif

    $('#salary_type').on('change', function () {
        var val = $(this).val();

        change_period(val);

    })



    function change_period(val) {
        //alert(val);
        if (val == 'single_salary') {
            $('#salary_from_div').show();
            $('#salary_to_div').hide();
            $('#salary_from_div').parent().removeClass('col-md-6');
            $('#salary_from_div').parent().addClass('col-md-12');
            $('#salary_from_div label').html('Salary Rate');
            $('#salary_from_div span').html('Enter numbers only (Example: £25,000 entered as 25000)');
            $('#salary_from').attr('type', 'number');
            $('#salary_from').attr('maxlength', '8');
        } else if (val == 'salary_in_range') {
            $('#salary_from_div').show();
            $('#salary_to_div').show();
            $('#salary_from_div').parent().removeClass('col-md-12');
            $('#salary_from_div').parent().addClass('col-md-6');
            $('#salary_from_div label').html('Salary From');
            $('#salary_from_div span').html('Enter numbers only (Example: £25,000 entered as 25000)');
            $('#salary_from').attr('type', 'number');
            $('#salary_from').attr('maxlength', '8');
        } else if (val == 'negotiable') {
            $('#salary_from_div').show();
            $('#salary_to_div').hide();
            $('#salary_from_div').parent().removeClass('col-md-6');
            $('#salary_from_div').parent().addClass('col-md-12');
            $('#salary_from_div label').html(
                'Enter Salary Text Information - Example: Negotiable, Unspecified, To Be Confirmed');
            $('#salary_from_div span').html('');
            $('#salary_from').attr('type', 'text');
            $('#salary_from').attr('placeholder', 'Enter Salary Text Information');
            $('#salary_from').attr('maxlength', '35');
        } else if (val == '') {
            $('#salary_from_div').hide();
            $('#salary_to_div').hide();
        }


        /* if(val2!=0)
            {
             $('#salary_to_div').show();
             $('#salary_from_div label').html('Salary From');
             $('#salary_in_range_div').show();
            } else if(val==6 || val==4 || val==8)
		    {
             $('#salary_in_range_div').show();
		     $('#salary_to_div').hide();
		     $('#salary_from_div label').html('Salary Rate');
		    }
            else if(val==7){
                $('#salary_to_div').hide();
                $('#salary_from_div').hide();
                $('#salary_in_range_div').hide();
            }
		    else{
             $('#salary_to_div').show();
             $('#salary_from_div label').html('Salary From');
             $('#salary_in_range_div').hide();
            }*/
    }
    if ($(".degree_level_form").length > 0) {
        $(".degree_level_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                degree_level: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-degree_level",
                        data: {
                            degree_level: function () {
                                return $(".degree_level").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language required",
                },
                degree_level: {
                    required: "Qualification Required",
                    remote: "Already available. Close & select from Qualification dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('degree_level.job.add')}}",
                    type: "POST",
                    data: $('.degree_level_form').serialize(),
                    success: function (res) {
                        $('#degree_level_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Qualification (' + $(".degree_level").val() +
                                ') Added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-degree_level-modal').modal('hide');

                    }
                });
            }

        })
    }



    if ($(".job_experience_form").length > 0) {
        $(".job_experience_form").validate({
            validateHiddenInputs: true,
            ignore: "",
            onkeyup: false,

            rules: {
                lang: {
                    required: true,
                },

                job_experience: {
                    required: true,
                    remote: {
                        type: 'get',
                        url: "/check-job_experience",
                        data: {
                            job_experience: function () {
                                return $(".job_experience").val();
                            }
                        },
                        dataType: 'json'
                    },
                },
            },
            messages: {

                lang: {
                    required: "Language required",
                },
                job_experience: {
                    required: "Job experience Required",
                    remote: "Already available. Close & select from Job experience dropdown",
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('job_experience.job.add')}}",
                    type: "POST",
                    data: $('.job_experience_form').serialize(),
                    success: function (res) {
                        $('#job_experience_dd').html(res);
                        swal({

                            title: "Success",

                            text: 'Job experience (' + $(".job_experience").val() +
                                ') Added Successfully',

                            icon: "success",

                            button: "OK",

                        });

                        $('#add-job_experience-modal').modal('hide');

                    }
                });
            }

        })
    }

    $('.password').on('change', function () {
        jQuery.validator.addMethod("passwordCheck",
            function (value, element, param) {
                if (this.optional(element)) {
                    return true;
                } else if (!/[A-Z]/.test(value)) {
                    return false;
                } else if (!/[a-z]/.test(value)) {
                    return false;
                } else if (!/[0-9]/.test(value)) {
                    return false;
                }

                return true;
            },
            "Password must include one uppercase one lowercase and one numeric number");
    })


    /*     $('.company_name').keypress(function (e) {
             var regex = new RegExp("^[a-zA-Z \s]+$");
             var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             if (regex.test(str)) {
                 return true;
             }
             else
             {
             return false;
             }
         });*/

    $('#mobile_num').keypress(function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });
    var langArray_mobile_num = [];
    $('.vodiapicker_mobile_num option').each(function () {
        var img = $(this).attr("data-thumbnail");
        var val = $(this).attr("data-val");
        var text = this.innerText;
        var value = $(this).val();
        var item = '<li><img src="' + img + '" alt="" value="' + value + '" val="' + val + '"/><span>' + text +
            '</span></li>';
        langArray_mobile_num.push(item);
    })

    $('#a-mobile_num').html(langArray_mobile_num);

    //Set the button value to the first el of the array

    //change button stuff on click
    $('#a-mobile_num li').click(function () {
        var img = $(this).find('img').attr("src");
        var value = $(this).find('img').attr('value');
        var val = $(this).find('img').attr('val');
        var text = this.innerText;
        var item = '<li><img src="' + img + '" alt="" /><span>' + text +
            '</span><i class="fa fa-caret-down"></i></li>';
        $('.btn-select-mobile_num').html(item);
        $('.btn-select-mobile_num').attr('value', value);
        $('#mobile_num').val(val);
        $('#mobile_num_code').val(val);
        $(".b-mobile_num").toggle();
        //console.log(value);
    });

    $(".btn-select-mobile_num").click(function () {
        $(".b-mobile_num").toggle();
    });

    //check local storage for the lang
    var sessionLang = localStorage.getItem('lang');
    if (sessionLang) {
        //find an item with value of sessionLang
        var langIndex = langArray.indexOf(sessionLang);
        $('.btn-select-mobile_num').html(langArray[langIndex]);
        $('.btn-select-mobile_num').attr('value', sessionLang);
    } else {
        //var langIndex = langArray.indexOf('ch');
        //console.log(langIndex);
        //$('.btn-select-mobile_num').html(langArray[langIndex]);
        //$('.btn-select').attr('value', 'en');
    }


    function myFunction() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("search");
        filter = input.value.toUpperCase();
        ul = document.getElementById("a");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("span")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    function myFunction_emp() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("search_emp");
        filter = input.value.toUpperCase();
        ul = document.getElementById("a-mobile_num");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("span")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    function staticData() {
        var data = $("#phone");

        var code = $('#phone_num_code').val();

        if (data.val().length < 3) {
            data.val(code);
            data.attr("readonly", true);
        } else {
            data.attr("readonly", false);
        }
    }

    $('.refresh-form').on('click', function () {
        $.ajax({
            type: "GET",
            url: "{{route('refresh-form-seeker')}}",
            data: $('#candidate_form').serialize(),
        }).done(function (data) {
            location.reload();
        });
    })


    $('.refresh-form-emp').on('click', function () {
        $.ajax({
            type: "GET",
            url: "{{route('refresh-form-emp')}}",
            data: $('#employer_form').serialize() + '&type=emp',
        }).done(function (data) {
            location.reload();
        });
    })

    function staticDataMobile() {
        var data = $("#mobile_num");
        var code = $('#mobile_num_code').val();
        if (data.val().length < 3) {
            data.val(code);
            data.attr("readonly", true);
        } else {
            data.attr("readonly", false);
        }
    }

    $('#salary_from').keypress(function (event) {
        if (event.which == 46 && $(this).attr('type') == 'number') {
            event.preventDefault();
            return false;
        } // prevent if already dot
    })

    $('#salary_to').keypress(function (event) {
        if (event.which == 46) {
            event.preventDefault();
            return false;
        } // prevent if already dot
    })
</script>
@endpush