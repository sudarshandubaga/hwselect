@extends('admin.layouts.admin_layout')
@push('css')
@endpush
@section('content')


@php

$company = $job->getCompany();

@endphp


  <?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
    if(null!==($job->salary_from)){
$salary = '<strong>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
} 


?>  






<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Detail</span> </li>

            </ul>

        </div>




<div class="listpgWraper">

    <div class="container"> 

        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet" style="margin-top: 30px;">
                  
                        <div class="row">
                            <div class="col-md-4">
                                <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Job Details</span> </div>
                            </div>
                            
                            <div class="col-md-8 text-right">
								<?php if ($job->status == 'active') {?>
                           

                                <span>{{__('Job Approved on')}} {{date('d-M-Y',strtotime($job->updated_at))}}</span>
                            
                        <?php } ?>
								
								
                                <?php if ($job->status == 'active') {

                                $activeTxt = 'Make InActive';

                                $activeHref = 'makeNotActive(' . $job->id . ');';

                                $activeIcon = 'square-o';

                            }else{
                                $activeTxt = 'Approve This Job';

                                $activeHref = 'makeActive(' . $job->id . ');';

                                $activeIcon = 'square-o';
                            } ?>
                            @if($job->status=='admin_review')
                        <a class="btn btn-success" href="javascript:void(0);" onClick="makeNotReviewed({{$job->id}})" id=""><i class="fa fa-square-o" aria-hidden="true"></i> On Hold</a>
                        @endif

                        @if($job->status=='request_to_delete')
                        <a class="btn btn-danger" href="javascript:void(0);" onClick="deleteJob({{$job->id}})" data-job="{{$job->title}}"  id="job-{{$job->id}}"><i class="fa fa-square-o" aria-hidden="true"></i> Delete</a>
                        @endif
                        <a class="btn btn-success" href="javascript:void(0);" onClick="{{$activeHref}}" id="onclickActive{{$job->id}}"><i class="fa fa-{{$activeIcon}}" aria-hidden="true"></i> {{$activeTxt}}</a>
                        
								
						<a class="btn btn-primary" href="{{route('edit.job', ['id' => $job->id])}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit Job</a>
								
                            </div>
                        </div>
                        
                    
                    <div class="portlet-body form">          
                       
                        {!! Form::model($job, array('method' => 'put', 'route' => array('update.job', $job->id), 'class' => 'form', 'files'=>true)) !!}
                        {!! Form::hidden('id', $job->id) !!}            
                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

       



        <!-- Job Detail start -->

        <div class="row">

            <div class="col-lg-7"> 

                

                 <!-- Job Header start -->

        <div class="job-header">

            <div class="jobinfo">

               

                        <h2>{{$job->title}}</h2>

                        <div class="ptext">{{__('Date Posted')}}: {{$job->created_at->format('d-M-Y')}}</div>

                        

                        

                        @if(!(bool)$job->hide_salary)

                        <div class="salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}}</div>

                        @endif

                    

            </div>

            

            <!-- Job Detail start -->

                <div class="jobmainreq">

                    <div class="jobdetail">

                       <h3><i class="fa fa-align-left" aria-hidden="true"></i> {{__('Job Details')}}</h3>

                        

                            

                             <ul class="jbdetail">

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Location(s)')}}:</div>
                                <?php 
                                      $locations = json_decode($job->multi_locations);
                                      $titles = array();
                                      if(null!==($locations)){
                                        foreach($locations as $lo){
                                            if($lo->multi_locations){
                                              $titles[] = $lo->multi_locations;
                                            }
                                        
                                        }
                                      }
                                      
                                  ?>
                                <div class="col-md-8 col-xs-7">

                                    @if((bool)$job->is_freelance)

                                    <span>Freelance</span>

                                    @else

                                    <span>{{$job->getLocation()}}</span>

                                    @endif

                                </div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Company')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{isset($company)?$company->name:null}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Type')}}:</div>

                                <div class="col-md-8 col-xs-7"><span class="permanent">{{$job->getJobType('job_type')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Shift')}}:</div>

                                <div class="col-md-8 col-xs-7"><span class="freelance">{{$job->getJobShift('job_shift')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Career Level')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{$job->getCareerLevel('career_level')}}</span></div>

                            </li>

                                <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Positions')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{$job->num_of_positions}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Experience')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{$job->getJobExperience('job_experience')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Gender')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{$job->getGender('gender')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Degree')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{$job->getDegreeLevel('degree_level')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-xs-5">{{__('Apply Before')}}:</div>

                                <div class="col-md-8 col-xs-7"><span>{{$job->expiry_date->format('d-M-Y')}}</span></div>

                            </li> 

                            

                        </ul>

                            

                            

                       

                    </div>

                </div>

            

            <hr>

          

        </div>

                

                

                

                <!-- Job Description start -->

                <div class="job-header">

                    <div class="contentbox">

                        <h3><i class="fa fa-file-text-o" aria-hidden="true"></i> {{__('Job Description')}}</h3>

                        <p>{!! $job->description !!}</p>                       

                    </div>

                </div>

                

                

                <div class="job-header bonus">

                    <div class="contentbox">

                        <h3>{{__('Bonus')}}</h3>

                        <?php 
                        $all_bouns = '';
                        if(null!==($job->bonus)){
                            $bon = json_decode($job->bonus);
                            if(null!==($bon)){
                                foreach ($bon as $key => $value) {
                                    $bonus = App\Bonus::findorFail($value);
                                    $all_bouns .=$bonus->bonus.', ';
                                }
                            }

                        }


                        $all_benifits = '';
                        if(null!==($job->benifits)){
                            $beni = json_decode($job->benifits);
                            if(null!==($beni)){
                                foreach ($beni as $key => $val) {
                                    $benifits = App\Benifits::findorFail($val);
                                    $all_benifits .=$benifits->benifits.', ';
                                }
                            }

                        }
                     ?> 
						
						<p style="margin-top: 5px;">
						@if(null!==($job->bonus)) <span>{{trim($all_bouns,', ')}}</span>
						@endif 
						</p> 	
						<h3>Benifits</h3>
						<p style="margin-top: 5px;">	
                        @if(null!==($job->benifits)) <span>{{trim($all_benifits,', ')}}</span>
						@endif
						</p> 

                    </div>

                </div>

                

                <div class="job-header">

                    <div class="contentbox">                        

                        <h3><i class="fa fa-puzzle-piece" aria-hidden="true"></i> {{__('Skills Required')}}</h3>

                        <ul class="skillslist">

                            {!!$job->getJobSkillsList()!!}

                        </ul>

                    </div>

                </div>

                

                

                <!-- Job Description end --> 



                

            </div>

            <!-- related jobs end -->



            <div class="col-lg-5"> 

                
                @if($job->isJobExpired())
				<div class="jobButtons applybox">
                <span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Expired')}} {{date('d-M-Y',strtotime($job->expiry_date))}}</span>
					</div>
                @endif
				

                <div class="companyinfo" style="padding:0 0 10px 0; margin-bottom: 0;">
                    <h3 style="margin-bottom: 0;"><i class="fa fa-check" aria-hidden="true"></i> 
                    	<?php if($job->status=='rejected'){?>
                          <strong style="color: red; font-weight: bold;">{{__('On-Hold')}}</strong>
                          <p><strong>Refused Reason:</strong> {{$job->rejected_reason}} </p>        
						<?php }else if($job->expiry_date <  \Carbon\Carbon::now()){ ?>
                    {{__('Job Expired')}}

                    <?php }else if($job->status == 'blocked'){ ?>

                           <b style="color:red"> {{__('Job InActive')}} - Deactivated by {{$job->blocked_by}}: {{date('d-M-Y',strtotime($job->updated_at))}}</b><br>
                           <p>{{$job->rejected_reason}}</p>
                        <?php }else if($job->status == 'active'){ ?>
                        <strong style="color:#00CD0D">{{__('Job Active')}}</strong> <span style="color: #00CD0D; font-size: 20px;">(Now Live On Website)</span> <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" style="font-size: 14px; font-weight: 700; text-decoration: underline;" target="_blank">Preview</a>
<?php } else if($job->status == 'admin_review'){ ?>
                            <b style="color:red">Job Awaiting Approval - Posted by {{$company->name}} On {{date('d-M-Y',strtotime($job->created_at))}}</b>
                        <?php } else if($job->status == 'request_to_delete'){ ?>
                            <b style="color:red">Awaiting admin review for deletion - Requested by {{$company->name}} On {{date('d-M-Y',strtotime($job->updated_at))}}</b>
                        <?php } ?>
					</h3>
                        </div>

                    @if(!empty($job->reason))
                    <div><strong>Reason:</strong> {{ $job->reason }}</div>
                    @endif

<hr>

                        <div class="companyinfo" style="padding:0 0 30px 0;">

                    <h3><i class="fa fa-building-o" aria-hidden="true"></i> {{__('Company Overview')}}</h3>

                            <div class="companylogo">{{isset($company)?$company->printCompanyImage():null}}</div>

                            <div class="title">{{isset($company)?$company->name:null}}</div>

                            <div class="ptext">{{isset($company)?$company->getLocation():null}}</div>
                            @if(isset($company))
                            <div class="opening">
                                <a href="{{route('public.company',$company->id)}}">
                                    {{App\Company::countNumJobs('company_id', $company->id)}} {{__('Current Job Opening(s)')}}
                                </a>

                            </div>
                            @endif

                            <div class="clearfix"></div>

                    <hr>

                <div class="companyoverview">
                    @if(isset($company))
                    <p>{{\Illuminate\Support\Str::limit(strip_tags($company->description), 250, '...')}} <a href="{{route('public.company',$company->id)}}">View More Company Details</a></p>
                    @endif

                    </div>

                        </div>

                

            

                

                

                <!-- Google Map start -->

                <div class="job-header">

                    <div class="jobdetail">

                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{__('Job Location on Map')}}</h3>

                        <div class="gmap">

                            <iframe src="https://maps.google.it/maps?q={{urlencode(strip_tags($job->getLocation()))}}&output=embed" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen=""></iframe>

                        </div>

                    </div>

                </div>

                <div class="job-header">

                    <div class="jobdetail">

                        <?php 

                            $list_candidates = App\JobApply::where('job_id', '=', $job->id)->count();
                             $short_listed_candidates = App\Unlocked_users::where('job_id', '=', $job->id)->count();
                             $rejected = App\Unlocked_users::where('job_id', '=', $job->id)->where('status','rejected')->count();
                             $interview = App\Unlocked_users::where('job_id', '=', $job->id)->where('status','called_for_interview')->count();
                             $interviewed = App\Unlocked_users::where('job_id', '=', $job->id)->where('status','interviewed')->count();
                             $recruited = App\Unlocked_users::where('job_id', '=', $job->id)->where('status','recruited')->count();
                             $resigned = App\Unlocked_users::where('job_id', '=', $job->id)->where('status','resigned')->count();
                             $title = '"'.$job->title.'"';
                         ?>

                        <h3> {{__('Actions')}}</h3>

                        <ul class="">
                            <li>
                            <a href="{{route('admin.list.applied.users', [$job->id])}}"><i class="fa fa-users" aria-hidden="true"></i>List Candidates <span class="badge badge-primary">{{ $list_candidates}}</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.company.unloced-users', [$job->id,'all'])}}"><i class="fa fa-file" aria-hidden="true"></i>Short Listed Candidates<span class="badge badge-primary">{{ $short_listed_candidates}}</span></a>
                        </li>
                       
                        <li>
                            <a href="{{route('admin.company.unloced-users', [$job->id,'called_for_interview'])}}"><i class="fa fa-lock" aria-hidden="true"></i>Called for Interview<span class="badge badge-primary">{{ $interview}}</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.company.unloced-users', [$job->id,'interviewed'])}}"><i class="fa fa-check" aria-hidden="true"></i>Interviewed Candidates<span class="badge badge-primary">{{ $interviewed}}</span></a>
                        </li>

                    </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</div>
</div>
<div class="modal fade" id="rejectedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="rejected_form">
        @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <input type="hidden" name="id" id="rejected_job_id">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Type the Reason:</label>
            <textarea class="form-control" id="reason" maxlength="300" name="reason" style="height: 200px"></textarea>
          </div>
        
      </div>
      <div class="modal-footer">
    
        <button type="button" class="btn btn-primary save_changes">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('styles')

<style type="text/css">

    .formrow iframe {

        height: 78px;

    }

</style>

@endpush

@push('scripts') 

<script type="text/javascript">
     function makeActive(id) {

        $.post("{{ route('make.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        $('#onclickActive{{$job->id}} i').removeClass('fa-square-o');
                        $('#onclickActive{{$job->id}} i').addClass('fa-check-square-o');
                        location.reload();

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }



    function makeNotReviewed(id) {

        $('#rejected_job_id').val('');
        $('#rejected_job_id').val(id);
        $('#rejectedModal').modal('show');

    }
    function makeNotActive(id) {

        $.post("{{ route('make.not.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        
                        $('#onclickActive{{$job->id}} i').removeClass('fa-check-square-o');
                        $('#onclickActive{{$job->id}} i').addClass('fa-square-o');

                        location.reload();
                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    $('.save_changes').on('click',function(){
        $.post("{{ route('make.notreviewed.job') }}", {id: $('#rejected_job_id').val(),reason: $('#reason').val(), _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {
                        $('#rejectedModal').modal('hide');
                        location.reload();

                    } else

                    {

                        alert('Request Failed!');

                    }

                });
    })

function deleteJob(id) {

        var msg = 'Please confirm you want to delete Job: '+$('#job-'+id).data('job');

        if (confirm(msg)) {

            $.post("{{ route('delete.job') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        if (response == 'ok')

                        {

                            // location.reload();
                            window.location = "{{ route('list.request.jobs') }}"

                        } else

                        {

                            alert('Request Failed!');

                        }

                    });

        }

    }

</script> 

@endpush