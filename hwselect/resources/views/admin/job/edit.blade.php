@extends('admin.layouts.admin_layout')
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('list.jobs') }}">Manage Jobs</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Edit Job</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->        
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Job Form</span> </div>

                                <!-- <div class="caption font-red-sunglo"> <a class="btn btn-primary" href="{{route('cron')}}">cron</a> </div> -->
                            </div>
                            <div class="col-md-5" align="right">
                                <?php if ((int) $job->is_active == 1) {

                                $activeTxt = 'Make InActive';

                                $activeHref = 'makeNotActive(' . $job->id . ');';

                                $activeIcon = 'fa-check-square-o';

                            }else{
                                $activeTxt = 'Approve Job';

                                $activeHref = 'makeActive(' . $job->id . ');';

                                $activeIcon = 'square-o';
                            } ?>
                        <a class="btn btn-success" href="javascript:void(0);" onClick="{{$activeHref}}" id="onclickActive{{$job->id}}"><i class="fa fa-{{$activeIcon}}" aria-hidden="true"></i> {{$activeTxt}}</a>
								
								 <a class="btn btn-primary" href="{{route('public.job', ['id' => $job->id])}}">Preview  Job</a>
								
                            </div>
                        </div>
                        
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                            <li> <a href="#metakeyword" data-toggle="tab" aria-expanded="false"> Advance SEO </a> </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="Details">   
                                {!! Form::model($job, array('method' => 'put', 'route' => array('update.job', $job->id), 'class' => 'form', 'files'=>true)) !!}
                                {!! Form::hidden('id', $job->id) !!}            
                                          
                                     @include('admin.job.forms.form') 
                                
                                {!! Form::close() !!}
                            </div>

                            <div class="tab-pane fade in" id="metakeyword">

                                {!! Form::model($job, array('method' => 'put', 'route' => array('updateMeta.job', $job->id), 'class' => 'forms', 'files'=>true)) !!}
                                
                                    @include('admin.job.forms.meta')
                                
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY --> 
    </div>
    @endsection
    @push('scripts')
    <script type="text/javascript">
        function makeActive(id) {
        @if($job->expiry_date <  \Carbon\Carbon::now())
        swal({

            title: "Error",

            text: 'You must first click update button at the bottom of the form, to enable access to Approve Job button',

            icon: "error",

            button: "OK",

        });
        @else
        $.post("{{ route('make.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {
                        location.reload();
                        $('#onclickActive{{$job->id}} i').removeClass('fa-square-o');
                        $('#onclickActive{{$job->id}} i').addClass('fa-check-square-o');

                    } else

                    {

                        alert('Request Failed!');

                    }

                });
        @endif    
        

    }




    function makeNotActive(id) {

        $.post("{{ route('make.not.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {
                        location.reload();
                        
                        $('#onclickActive'+id+' i').removeClass('fa-check-square-o');
                        $('#onclickActive'+id+' i').addClass('fa-square-o');
                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }
    </script>
    @endpush