@extends('admin.layouts.admin_layout')
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('list.users') }}">Manage Job Seeker</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Add Job Seeker</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR --> 
        <!-- BEGIN PAGE TITLE-->
        <!--<h3 class="page-title">Edit User <small>Users</small> </h3>-->
        <!-- END PAGE TITLE--> 
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Add Job Seeker Form</span> </div>
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                            <li><a href="#Summary"  aria-expanded="false" class="abc">Summary</a></li>
                            <li><a href="#CV"  aria-expanded="false" class="abc">C.V</a></li>
                            <li style="display: none;"><a href="#Projects"  aria-expanded="false" class="abc">Projects</a></li>
                            <li style="display: none;"><a href="#Experience"  aria-expanded="false" class="abc">Experience</a></li>
                            <li style="display: none;"><a href="#Education"  aria-expanded="false">Education</a></li>
                            <li ><a href="#Skills"  aria-expanded="false" class="abc">Skills</a></li>
                            <li><a href="#Languages"  aria-expanded="false" class="abc">Languages</a></li>
                        </ul>

                        <div class="tab-content">              
                            <div class="tab-pane fade active in" id="Details"> @include('admin.user.forms.form') </div>
                            @if(isset($user))
                            <div class="tab-pane fade" id="Summary"> @include('admin.user.forms.summary') </div>
                            <div class="tab-pane fade" id="CV"> @include('admin.user.forms.cv.cvs') </div>
                            <div class="tab-pane fade" id="Projects"> @include('admin.user.forms.project.projects') </div>
                            <div class="tab-pane fade" id="Experience"> @include('admin.user.forms.experience.experience') </div>
                            <div class="tab-pane fade" id="Education"> @include('admin.user.forms.education.education') </div>
                            <div class="tab-pane fade" id="Skills"> @include('admin.user.forms.skill.skills') </div>
                            <div class="tab-pane fade" id="Languages"> @include('admin.user.forms.language.languages') </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY --> 
</div>
@endsection
@push('scripts')
<script type="text/javascript">
        if ($("form").length > 0) {
    var msg = 'Please Enter a Valid Mobile Number and Click Verify to Continue';
    @if(!$siteSetting->verify_registration_phone_number)
        var msg = 'Please enter a valid contact number';
    @endif
     $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    first_name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                    },

                    last_name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                    }, 
                   
                    phone: {
                        required: true,
                        minlength: 11,
                    },
					eligible_uk: {
                        required: true,
                    },
                    eligible_eu: {
                        required: true,
                    },

                    email: {
                        required: true,
                        email:true,
                        remote: {
                            type: 'get',
                            url: "{{ url('check-email?type=seeker') }}",
                            data: {
                              email: function() {
                                return $( "#email" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                        
                    },
                    
                },
                messages: {

                    first_name: {
                        required: "First Name required",
                    },
                    last_name: {
                        required: "Last Name required",
                    },


                    email: {
                        required: "Email required",
                        remote: "This Email is already taken."
                    }, 

                    phone: {
                        required: "Phone Number required",
                        minlength: msg,
                    },

                },
                
                errorPlacement: function(error, element)  {
                    if ( element.is(":radio") ) 
                    {
                        error.appendTo( element.parents('.radio-list') );
                    }
                    else 
                    { // This is the default behavior 
                        error.insertAfter( element );
                    }
                }
            })
        }


        $('.abc').on('click',function(e){
            e.preventDefault();
	        swal({
	            title: "User Profile Required",
	            text: 'Please complete Jobseeker profile within the details tab first',
	            icon: "error",
	            button: "OK",
	        });

            $('.nav-tabs li').removeClass('active');
            $('.nav-tabs a[href="#Details"]').closest('li').addClass('active');
        })
</script>
@endpush