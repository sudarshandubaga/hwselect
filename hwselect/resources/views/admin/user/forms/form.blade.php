{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
@if(isset($user))
{!! Form::model($user, array('method' => 'put', 'route' => array('update.user', $user->id), 'class' => 'form', 'files'=>true)) !!}
{!! Form::hidden('id', $user->id) !!}
@else
{!! Form::open(array('method' => 'post', 'route' => 'store.user', 'class' => 'form', 'files'=>true)) !!}
@endif

<?php $country = App\Country::select('countries.flag', 'countries.id')->whereNotIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();
    
    $country2 = App\Country::select('countries.flag', 'countries.id')->whereIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();

 ?>
<div class="form-body">    
    <input type="hidden" name="front_or_admin" value="admin" />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'image') !!}">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><?php if(isset($user)){?> {{ ImgUploader::print_image("user_images/thumb/$user->image") }}<?php }else{ ?> <img src="{{ asset('/') }}admin_assets/no-image.png" alt="" /> <?php } ?> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select Profile Image </span> <span class="fileinput-exists"> Change </span><?php if(isset($user)){$img = $user->image;}else{$img = '';} ?> <input name="image" value="{{old('image')?old('image'):$img}}" type="file"> </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
					<p class="allowed_message" style="margin-top: 4px; color: #888;">Image Formats Allowed: .jpg .jpeg .png .gif</p>
                </div>
                {!! APFrmErrHelp::showErrors($errors, 'image') !!} </div>
        </div> 
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'first_name') !!}">
        {!! Form::label('first_name', 'First Name', ['class' => 'bold']) !!}                    
        {!! Form::text('first_name', null, array('class'=>'form-control', 'maxlength'=>'50', 'id'=>'first_name', 'placeholder'=>'First Name')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'first_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'middle_name') !!}" style="display: none;">
        {!! Form::label('middle_name', 'Middle Name', ['class' => 'bold']) !!}                    
        {!! Form::text('middle_name', null, array('class'=>'form-control', 'id'=>'middle_name', 'maxlength'=>'50', 'placeholder'=>'Middle Name')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'middle_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'last_name') !!}">
        {!! Form::label('last_name', 'Last Name', ['class' => 'bold']) !!}                    
        {!! Form::text('last_name', null, array('class'=>'form-control', 'id'=>'last_name', 'maxlength'=>'50', 'placeholder'=>'Last Name')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'last_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'email') !!}">
        {!! Form::label('email', 'Email Address', ['class' => 'bold']) !!}                    
        {!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Email','maxlength'=>50)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'email') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'password') !!}">
        {!! Form::label('password', 'Password', ['class' => 'bold']) !!}
        <input class="form-control" id="password" placeholder="Password" maxlength="30" name="password" type="text" value="{{isset($user)?$user->show_password:null}}">                  
        
        {!! APFrmErrHelp::showErrors($errors, 'password') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'father_name') !!}" style="display: none;">
        {!! Form::label('father_name', 'Father Name', ['class' => 'bold']) !!}                    
        {!! Form::text('father_name', 'Father Name', array('class'=>'form-control', 'id'=>'father_name', 'placeholder'=>'Father Name')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'father_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'date_of_birth') !!}">
        <?php
        $date_of_birth = '';
        if (isset($user)) {
        	if(!empty($user->date_of_birth)){
        		$d = $user->date_of_birth;
                // $date_of_birth = old('date_of_birth')?date('d-m-Y',strtotime(old('date_of_birth'))):date('d-m-Y',strtotime($d));
                $date_of_birth = $d;
        	}else{
        		// $d = date('d-m-Y', strtotime('-16 years'));
                $date_of_birth = '';
        	}
         }
         $age = !empty($user->age) ? $user->age : null;
         ?>
        {!! Form::label('age', 'Age', ['class' => 'bold']) !!}                    
        {!! Form::select('age', [
            '16 - 19'   => '16 - 19',
            '20 - 23'   => '20 - 23',
            '24 - 30'   => '24 - 30',
            '31 - 35'   => '31 - 35',
            '36 - 45'   => '36 - 45',
            '46 - 54'   => '46 - 54',
            '55+'       => '55+',
        ], $age, array('class'=>'form-control', 'id'=>'age', 'placeholder'=>'---Select an Age---', 'autocomplete'=>'off', 'required' => 'required')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'age') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'gender_id') !!}" style="display: none;">
        {!! Form::label('gender_id', 'Gender', ['class' => 'bold']) !!}                    
        {!! Form::select('gender_id', [''=>'Select Gender']+$genders, null, array('class'=>'form-control', 'id'=>'gender_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'gender_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'marital_status_id') !!}" style="display: none;">
        {!! Form::label('marital_status_id', 'Marital Status', ['class' => 'bold']) !!}                    
        {!! Form::select('marital_status_id', [''=>'Select Marital Status']+$maritalStatuses, null, array('class'=>'form-control', 'id'=>'marital_status_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'marital_status_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'nationality_id') !!}">
        {!! Form::label('nationality_id', 'Nationality', ['class' => 'bold']) !!}                    
        {!! Form::select('nationality_id', [''=>'Select Nationality']+$nationalities, null, array('class'=>'form-control', 'id'=>'nationality_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'nationality_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'national_id_card_number') !!}">
        {!! Form::label('national_id_card_number', 'National ID Card#', ['class' => 'bold']) !!}                    
        {!! Form::text('national_id_card_number', null, array('class'=>'form-control', 'id'=>'national_id_card_number', 'placeholder'=>'National ID Card#','maxlength'=>50)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'national_id_card_number') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">
         <?php $arra = array(
            230=>'United Kingdom',
            231=>'United States of America',
        ); ?>
        {!! Form::label('country_id', 'Country', ['class' => 'bold']) !!}                    
        {!! Form::select('country_id', $arra+$countries, old('country_id', (isset($user))? $user->country_id:$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">
        {!! Form::label('state_id', 'County / State / Province /District', ['class' => 'bold']) !!}                    
        <span id="default_state_dd">
            {!! Form::select('state_id', [''=>'Select County / State / Province /District'], null, array('class'=>'form-control', 'id'=>'state_id')) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">
        {!! Form::label('city_id', 'Town / City', ['class' => 'bold']) !!}                    
        <span id="default_city_dd">
            {!! Form::select('city_id', [''=>'Select Town / City'], null, array('class'=>'form-control', 'id'=>'city_id')) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'city_id') !!}                                       
    </div>
    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}"> 
                                {!! Form::label('phone', 'Mobile Number', ['class' => 'bold']) !!}                                   
                               <?php if(isset($user) &&$user->phone_code==''){
                                $phone_code = '+44';
                               }else{
                                $phone_code = isset($user)?$user->phone_code:'+44';
                               } ?>
                                <div class="step_1 seekphone">
                                    
                               
                                    <select class="form-control vodiapicker" value="{{null !==(old('phone_code'))?old('phone_code'):$phone_code}}" name="phone_code" id="phone_code">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                if(old('phone_code')){
                    $phone_code =  old('phone_code');
                }else{
                    $phone_code =  isset($phone_code)?$phone_code:null;
                }
                

                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if($phone_code==$detail->sort_name ){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <div class="lang-select">
                <?php  
                if(old('phone_num_code')){
                    $phone_num_code =  old('phone_num_code');
                }else{
                    $phone_num_code =  isset($user)?$user->phone_num_code:'+44';
                }?>

                <a  class="btn-select cselect" value="{{old('phone_num_code')?old('phone_num_code'):$phone_num_code}}">
                    
                   <?php 

                    if($phone_num_code!=''){
                        $phone_detail = DB::table('countries_details')->where('phone_code',$phone_num_code)->first();

                    }else{
                       $phone_detail = DB::table('countries_details')->where('id',230)->first();  
                    }


                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>

                    
                    @endif
                    
                    <i class="fa fa-caret-down"></i>

                </a>
<input type="hidden" id="phone_num_code" name="phone_num_code" value="{{old('phone_num_code')?old('phone_num_code'):'+'.$phone_detail->phone_code}}">
                
                <div class="b"> 
                    <input type="text" onkeyup="myFunction()" name="search" id="search" placeholder="Search"  style="width: 100px;">
                <ul id="a">
                    
                </ul>

                </div>
                </div>
              {!! Form::text('phone',null, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('Mobile Number'), 'maxlength'=>18)) !!}
                           
                                  
                                
                             
                              </div>
                                
                                  
                                   
                                     @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif                      
                                                                      
                                </div>
    <!-- <div class="form-group {!! APFrmErrHelp::hasError($errors, 'phone') !!}">
        {!! Form::label('phone', 'Phone Number', ['class' => 'bold']) !!}                    
        {!! Form::text('phone', null, array('class'=>'form-control', 'id'=>'phone', 'placeholder'=>'Phone Number', 'maxlength'=>18)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'phone') !!}                                       
    </div> -->
    <!-- <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mobile_num') !!}">
        {!! Form::label('mobile_num', 'Mobile Number', ['class' => 'bold']) !!}                    
        {!! Form::text('mobile_num', null, array('class'=>'form-control', 'id'=>'mobile_num', 'placeholder'=>'Mobile Number', 'maxlength'=>18)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'mobile_num') !!}                                       
    </div> -->
	
	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">
        {!! Form::label('industry_id', 'Industry', ['class' => 'bold']) !!}                    
        {!! Form::select('industry_id', [''=>'Select Industry']+$industries, null, array('class'=>'form-control', 'id'=>'industry_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!}                                       
    </div>
	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}">
        {!! Form::label('functional_area_id', 'Functional Area (Job Description Type)', ['class' => 'bold']) !!}                    
        {!! Form::select('functional_area_id', [''=>'Select Functional Area']+$functionalAreas, null, array('class'=>'form-control', 'id'=>'functional_area_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!}                                       
    </div>
	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'career_level_id') !!}">
        {!! Form::label('career_level_id', 'Career Level', ['class' => 'bold']) !!}                    
        {!! Form::select('career_level_id', [''=>'Select Career Level']+$careerLevels, null, array('class'=>'form-control', 'id'=>'career_level_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!}                                       
    </div>
	
	
	
	
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
        {!! Form::label('job_experience_id', 'Years of Experience', ['class' => 'bold']) !!}                    
        {!! Form::select('job_experience_id', [''=>'Select Years of Experience']+$jobExperiences, null, array('class'=>'form-control', 'id'=>'job_experience_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!}                                       
    </div>
    
    
    
	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}">
        <?php 
    $default_currecny = App\CountryDetail::where('code','like','%'.$siteSetting->default_currency_code.'%')->first();
    $d_cur = $default_currecny->symbol;
     ?>
        {!! Form::label('salary_currency', 'Salary Currency', ['class' => 'bold']) !!}                    
        {!! Form::select('salary_currency', [''=>'Select Currency']+$currencies, (isset($user))? $user->salary_currency:$d_cur, array('class'=>'form-control', 'id'=>'salary_currency')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'current_salary') !!}">
        {!! Form::label('current_salary', 'Current Salary', ['class' => 'bold']) !!}                    
        {!! Form::text('current_salary', null, array('class'=>'form-control', 'id'=>'current_salary', 'placeholder'=>'Current Salary','maxlength'=>10,"oninput"=>"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');")) !!}
        {!! APFrmErrHelp::showErrors($errors, 'current_salary') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'expected_salary') !!}">
        {!! Form::label('expected_salary', 'Expected Salary', ['class' => 'bold']) !!}                    
        {!! Form::text('expected_salary', null, array('class'=>'form-control', 'id'=>'expected_salary', 'placeholder'=>'Minimum Required Salary','maxlength'=>10,"oninput"=>"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');")) !!}
        {!! APFrmErrHelp::showErrors($errors, 'expected_salary') !!}                                       
    </div>
    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'street_address') !!}">
        {!! Form::label('street_address', 'Street Address', ['class' => 'bold']) !!}
        {!! Form::textarea('street_address', null, array('class'=>'form-control', 'id'=>'current_location', 'placeholder'=>'Street Address')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'street_address') !!}                
    </div>

    @if((bool)config('jobseeker.is_jobseeker_package_active'))
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_seeker_package_id') !!}"> {!! Form::label('job_seeker_package_id', 'Package', ['class' => 'bold']) !!}  
        {!! Form::select('job_seeker_package_id', ['' => 'Select Package']+$packages, null, array('class'=>'form-control', 'id'=>'job_seeker_package_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_seeker_package_id') !!} </div>

    @if(isset($user) && $user->package_id > 0)
    <div class="form-group">
        {!! Form::label('package', 'Package : ', ['class' => 'bold']) !!}         
        <strong>{{$user->getPackage('package_title')}}</strong>
    </div>
    <div class="form-group">
        {!! Form::label('package_Duration', 'Package Duration : ', ['class' => 'bold']) !!}
        <strong>{{$user->package_start_date->format('d-M-Y')}}</strong> - <strong>{{$user->package_end_date->format('d-M-Y')}}</strong>
    </div>
    <div class="form-group">
        {!! Form::label('package_quota', 'Availed quota : ', ['class' => 'bold']) !!}
        <strong>{{$user->availed_jobs_quota}}</strong> / <strong>{{$user->jobs_quota}}</strong>
    </div>
    <hr/>
    @endif
    @endif


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_immediate_available') !!}">
        {!! Form::label('is_immediate_available', 'Available for Work?', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_immediate_available_1 = 'checked="checked"';
            $is_immediate_available_2 = '';
            if (old('is_immediate_available', ((isset($user)) ? $user->is_immediate_available : 1)) == 0) {
                $is_immediate_available_1 = '';
                $is_immediate_available_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="immediate_available" name="is_immediate_available" type="radio" value="1" {{$is_immediate_available_1}}>
                Available Immediately </label>
            <label class="radio-inline">
                <input id="not_immediate_available" name="is_immediate_available" type="radio" value="0" {{$is_immediate_available_2}}>
                Available on (Select for Calendar) </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_immediate_available') !!}
    </div>
    <?php if(isset($user)){?>
        <div id="immediate_form" <?php  if((bool)$user->is_immediate_available){echo 'style="display:none"';}else{} ?>>
            <div class="row">
            <input type="hidden" name="old_status" value="1">
              <div class="form-group col-md-3" style="text-align: left">
                <label for="date_from" style="margin-bottom: 5px;">Date From</label>
                <input type="date" class="form-control" id="date_from" name="date_from" aria-describedby="emailHelp" value="{{null!==($user->immediate_date_from)?date('d-m-Y',strtotime($user->immediate_date_from)):date('d-m-Y')}}" placeholder="Date From">
              </div>
              <div class="col-md-6"></div>
             <!--  <div class="form-group" style="text-align: left">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==($user->immediate_date_to)?date('Y-m-d',strtotime($user->immediate_date_to)):date('Y-m-d')}}" placeholder="Date To">
              </div> -->
        </div>
    </div>
    <?php }else{ ?>
        <div id="immediate_form" style="display:none">
            <div class="row">
            <input type="hidden" name="old_status" value="1">
              <div class="form-group col-md-3" style="text-align: left">
                <label for="date_from" style="margin-bottom: 5px;">Date From</label>
                <input type="date" class="form-control" id="date_from" name="date_from" aria-describedby="emailHelp" value="{{null!==(old('immediate_date_from'))?date('Y-m-d',old('immediate_date_from')):date('Y-m-d')}}" placeholder="Date From">
              </div>
              <div class="col-md-6"></div>
              <!-- <div class="form-group" style="text-align: left">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==(old('immediate_date_to'))?date('Y-m-d',strtotime(old('immediate_date_to'))):date('Y-m-d')}}" placeholder="Date To">
              </div> -->
          </div>
        </div>
    <?php } ?>
    


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">
        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($user)) ? $user->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                Active </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                In-Active </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
    </div>


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'eligible_uk') !!}">
        {!! Form::label('eligible_uk', 'Eligible to work in the UK:', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = '';
            $is_active_2 = '';
            if (old('eligible_uk', ((isset($user)) ? $user->eligible_uk : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }else if(old('eligible_uk', ((isset($user)) ? $user->eligible_uk : 0)) == 1){
                $is_active_1 = 'checked="checked"';
                $is_active_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="eligible_uk" type="radio" value="1" {{$is_active_1}}>
                Yes </label>
            <label class="radio-inline">
                <input id="not_active" name="eligible_uk" type="radio" value="0" {{$is_active_2}}>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eligible_uk') !!}
    </div>  




    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'eligible_eu') !!}">
        {!! Form::label('eligible_eu', 'Eligible to work in the EU:', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = '';
            $is_active_2 = '';
            if (old('eligible_eu', ((isset($user)) ? $user->eligible_eu : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }else if(old('eligible_eu', ((isset($user)) ? $user->eligible_eu : 0)) == 1){
                $is_active_1 = 'checked="checked"';
                $is_active_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="eligible_eu" type="radio" value="1" {{$is_active_1}}>
                Yes </label>
            <label class="radio-inline">
                <input id="not_active" name="eligible_eu" type="radio" value="0" {{$is_active_2}}>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eligible_eu') !!}
    </div>
	
	<h3>{{__('Car & Driving Licence')}}</h3>
	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'eu_licence') !!}">
        {!! Form::label('eu_licence', 'Do you have a driving license:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">           
            <label class="radio-inline">
                {!! Form::radio('driving_license', '1' , false, ['required' => 'required']) !!}
                Yes </label>
            <label class="radio-inline">
                {!! Form::radio('driving_license', '0' , false, ['required' => 'required']) !!}
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eu_licence') !!}
    </div>

	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'eu_relicate') !!}">
        {!! Form::label('eu_relicate', 'Are you willing to relocate:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">           
            <label class="radio-inline">
                {!! Form::radio('willing_to_relocate', '1' , false, ['required' => 'required']) !!}
                Yes </label>
            <label class="radio-inline">
                {!! Form::radio('willing_to_relocate', '0' , false, ['required' => 'required']) !!}
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eu_relicate') !!}
    </div>

    @php
        $attrArr = ['required' => 'required'];
        $is_active_1 = '';
        $is_active_2 = 'checked = "checked"';
        if(isset($user) && $user->unit_type === 'kms') {
            $is_active_1 = 'checked = "checked"';
            $is_active_2 = '';
        } else if(isset($user) && $user->unit_type === 'miles') {
            $is_active_1 = '';
            $is_active_2 = 'checked = "checked"';
        }
    @endphp
    
    <div class="form-group">
        {!! Form::label('unit_type', 'Distance Unit:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">           
            <label class="radio-inline">
            <input type="radio" name="unit_type" value="kms" {{ $is_active_1 }} required>
            Kms </label>
            <label class="radio-inline">
                <input type="radio" name="unit_type" value="miles" {{ $is_active_2 }} required>
                Miles </label>
        </div>
    </div>
		
		<div class="form-group {!! APFrmErrHelp::hasError($errors, 'eu_distance') !!}">
        {!! Form::label('eu_distance', 'Maximum distance prepared to travel from Home:', ['class' => 'bold mb-2']) !!}
        {!! Form::select('eu_distance',[
            '5'     => '5',
            '10'    => '10',
            '20'    => '20',
            '30'    => '30',
            '40'    => '40',
            '50'    => '50',
            '60+'   => '60+'
        ], null, array('class'=>'form-control', 'id'=>'eu_distance', 'placeholder'=>__('---Select Distance---'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'eu_distance') !!}
    </div>
	
	
	
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'verified') !!}">
        {!! Form::label('verified', 'Verified', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $verified_1 = 'checked="checked"';
            $verified_2 = '';
            if (old('verified', ((isset($user)) ? $user->verified : 1)) == 0) {
                $verified_1 = '';
                $verified_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="verified" name="verified" type="radio" value="1" {{$verified_1}}>
                Verified </label>
            <label class="radio-inline">
                <input id="not_verified" name="verified" type="radio" value="0" {{$verified_2}}>
                Not Verified </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'verified') !!}
    </div> 
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'two_step_verification') !!}">
        {!! Form::label('two_step_verification', '2 step verification:', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = '';
            $is_active_2 = '';
            if (old('two_step_verification', ((isset($user)) ? $user->two_step_verification : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }else if(old('two_step_verification', ((isset($user)) ? $user->two_step_verification : 0)) == 1){
                $is_active_1 = 'checked="checked"';
                $is_active_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="two_step_verification" type="radio" value="1" {{$is_active_1}}>
                Yes </label>
            <label class="radio-inline">
                <input id="not_active" name="two_step_verification" type="radio" value="0" {{$is_active_2}}>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'two_step_verification') !!}
    </div>
    {!! Form::button('Update Personal Information <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}   
</div>
{!! Form::close() !!}
@push('css')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
</style>
@endpush
@push('scripts')
<script type="text/javascript">
     $('input[name="is_immediate_available"]').on('change',function(){
        if($('input[name="is_immediate_available"]:checked').val() == '1'){
            $('#immediate_form').hide();
        }else{
            $('#immediate_form').show();
        }
        
    })
    $(document).ready(function () {
        initdatepicker();
        $('#salary_currency').typeahead({
            source: function (query, process) {
                return $.get("{{ route('typeahead.currency_codes') }}", {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });

        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterDefaultStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterDefaultCities(0);
        });
        filterDefaultStates(<?php echo old('state_id', (isset($user)) ? $user->state_id : 0); ?>);
    });
    function filterDefaultStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterDefaultCities(<?php echo old('city_id', (isset($user)) ? $user->city_id : 0); ?>);
                    });
        }
    }
    function filterDefaultCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                    });
        }
    }
    function initdatepicker() {
        var d = new Date();
        var pastYear = d.getFullYear() - 16;
        d.setFullYear(pastYear);
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'd-m-yyyy',
            endDate: d,
            maxDate: new Date(d)
        });
    }

    (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {

                 

                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                const size =  
                   (this.files[0].size / 1024).toFixed(2); 
      
                if (size > 100) { 
                    alert("A maximum image size of 100kb allowed, please reduce image size and try again"); 
                    options.error();
                    $(this).focus();
                }else{
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }
                }    

                

            });

        });
    };

})(jQuery);

$(function() {
    $("input[type='file']").checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            $('.allowed_message').css("color", "#888");
        },
        error: function() {
            $('.allowed_message').css("color", "red");
            $("input[type='file']").reset();
        }
    });

});

 $('#phone').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
var langArray = [];

$('.vodiapicker option').each(function(){
  var img = $(this).attr("data-thumbnail");
  var val = $(this).attr("data-val");
  var text = this.innerText;
  var value = $(this).val();
  var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
  langArray.push(item);
})

$('#a').html(langArray);

//Set the button value to the first el of the array


//change button stuff on click
$('#a li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fa fa-caret-down"></i></li>';
  $('.btn-select').html(item);
  $('.btn-select').attr('value', value);
  $('#phone').val(val);
  $('#phone_num_code').val(val);
  $(".b").toggle();
  //console.log(value);
});

$(".btn-select").click(function(){
        $(".b").toggle();
    });



 $('#mobile_num').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    ul = document.getElementById("a");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}


function staticData(){
  var data=$("#phone");
  
    var code = $('#phone_num_code').val();

  if(data.val().length<3)
    {
     data.val(code);
     data.attr("readonly",true);
    }
    else
    {
     data.attr("readonly",false);
    }
}

</script>
@endpush