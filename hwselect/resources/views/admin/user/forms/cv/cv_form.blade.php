<div class="modal-body">
    <div class="form-body">
        <div class="form-group" id="div_title">
            <label for="name" class="bold">Enter a title for this CV</label>
            <input class="form-control" id="title" placeholder="Example: Laura Doon -28thAugust 2020" name="title" type="text" value="{{(isset($profileCv)? $profileCv->title:'')}}">
            <span class="help-block title-error"></span> </div>

        @if(isset($profileCv))
        <div class="form-group">
            {{ImgUploader::print_doc("cvs/$profileCv->cv_file", $profileCv->title, $profileCv->title)}}
        </div>
        @endif

        <div class="form-group" id="div_cv_file">
            <label for="cv_file" class="bold">CV File</label>
            <input name="cv_file" id="cv_file" type="file" />
			<p>File Formats Allowed .doc .docx .pdf</p>
            <!-- <span class="help-block cv_file-error error">Only PDF and DOC files can be uploaded</span> </div> -->

        
    </div>