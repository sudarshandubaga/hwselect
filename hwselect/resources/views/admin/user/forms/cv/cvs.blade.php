{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body"> 
    <div class="form-group"> <button class="btn purple btn-outline sbold" onclick="showProfileCvModal();"> Add CV </button> </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"> <i class=" icon-layers font-green"></i> <span class="caption-subject font-blue bold">Click Add CV to attach a CV to this profile</span> </div>
                </div>
                <div class="portlet-body">
                    <div class="mt-element-card mt-element-overlay">
                        <div class="row" id="cvs_div"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="cv-loading" style="display: none">
    <div class="loading-bg" style="position: fixed; top: 0; left: 0;right: 0; bottom: 0; z-index: 999999; background: rgba(0, 0,0, 0.5);"></div>
    <div class="loading-body" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 999999; background: #fff; padding: 15px; border-radius: 20px; text-align: center;">
        <img src="{{ url('loading.gif') }}" alt="" style="width: 48px">
    </div>
</div>

<div class="modal fade bs-modal-lg" id="add_cv_modal" tabindex="-1" role="dialog" aria-hidden="true"></div>
@push('css')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
</style>
@endpush
@push('scripts') 
<script type="text/javascript">
    $(document).ready(function(){
    showCvs();
    });
    /**************************************************/
    function showProfileCvModal(){
    $("#add_cv_modal").modal();
    loadProfileCvForm();
    }
    function loadProfileCvForm(){
    $.ajax({
    type: "POST",
            url: "{{ route('get.profile.cv.form', $user->id) }}",
            data: {"_token": "{{ csrf_token() }}"},
            datatype: 'json',
            success: function (json) {
                $("#add_cv_modal").html(json.html);
            }
    });
    }

    function uploadCV(isDefault) {
        $('#cv-loading').show();

        var form = $('#add_edit_profile_cv');
        var formData = new FormData();
        formData.append("id", $('#id').val());
        formData.append("_token", $('input[name=_token]').val());
        formData.append("title", $('#title').val());
        formData.append("is_default", isDefault); // $('input[name=is_default]:checked').val());
        if (document.getElementById("cv_file").value != "") {
            formData.append("cv_file", $('#cv_file')[0].files[0]);
        }
        //form.attr('method'),
        $.ajax({
            url     : form.attr('action'),
            type    : 'POST',
            data    : formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success : function (json){
                $('#cv-loading').hide();
                $ ("#add_cv_modal").html(json.html);
                showCvs();
                swal('Success!', 'CV has been added successfully.');
            },
            error: function(json){
                $('#cv-loading').hide();
                console.log(json.status, json.responseJSON.errors);
                if (json.status == 422) {
                    swal('Error!', 'Only PDF and DOC files can be uploaded.', 'error');
                } else {
                    // Error
                    // Incorrect credentials
                    // alert('Incorrect credentials. Please try again.')
                }
            }
        });
    }

    function submitProfileCvForm() {

        let ext = '';

        if($('#cv_file').val() != '') {
            let myFile = $('#cv_file').val();
            ext = myFile.split('.').pop();
        }

        if($('#title').val() == '') {
            // swal('Warning!', 'Please enter title.', 'warning');
            alert('Please add CV Title Name - Example: David’s CV 28 March 2021');
        } else if($('#cv_file').val() == '') {
            // swal('Warning!', 'Please select CV file.', 'warning');
            alert('Please select CV file.');
        } else if(ext != 'pdf' && ext != 'docx' && ext != 'doc') {
            alert('Only PDF and DOC files can be uploaded.');
        } else {
            $("#add_cv_modal").modal('hide');

            if($('#totalCvCount').length) {
                swal({
                    title: "Set CV as default priority",
                    text: "",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    closeOnClickOutside: false
                })
                .then((confirmed) => {
                    var isDefault;
                    if (confirmed) {
                        isDefault = 1;
                    } else {
                        isDefault = 0;
                        // swal("Your imaginary file is safe!");
                    }
                    uploadCV(isDefault);
                });
            } else {
                uploadCV(1);
            }
        }


    }
    /*****************************************/
    function showProfileCvEditModal(cv_id){
    $("#add_cv_modal").modal();
    loadProfileCvEditForm(cv_id);
    }
    function loadProfileCvEditForm(cv_id){
    $.ajax({
    type: "POST",
            url: "{{ route('get.profile.cv.edit.form', $user->id) }}",
            data: {"cv_id": cv_id, "_token": "{{ csrf_token() }}"},
            datatype: 'json',
            success: function (json) {
            $("#add_cv_modal").html(json.html);
            }
    });
    }
    /*****************************************/
    function showCvs()
    {
    $.post("{{ route('show.profile.cvs', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
            .done(function (response) {
            $('#cvs_div').html(response);
            });
    }
    function delete_profile_cv(id,title) {
    if (confirm('Are you sure you want to delete  ('+title+')')) {
    $.post("{{ route('delete.profile.cv') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
            .done(function (response) {
            if (response == 'ok')
            {
            $('#cv_' + id).remove();
            } else
            {
            alert('Request Failed!');
            }
            });
    }
    }
</script>
@endpush
