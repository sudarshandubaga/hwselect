<form class="form" id="add_edit_profile_cv" method="POST" action="{{ route('store.profile.cv', [$user->id]) }}" target="cv_iframe">{{ csrf_field() }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add CV</h4>
    </div>
    <div class="modal-body">
        <div class="form-body">
            <div class="form-group" id="div_name">
                <label for="name" class="bold">Enter a title for this CV</label>
                <input class="form-control" id="title" placeholder="Example: Laura Doon -28thAugust 2020" name="title" type="text" value="{{(isset($profileCv)? $profileCv->title:'')}}" required>
                <span class="help-block title-error"></span> </div>

            @if(isset($profileCv))
            <div class="form-group">
                {{ImgUploader::print_doc("cvs/$profileCv->cv_file")}}
            </div>
            @endif


            <div class="form-group" id="div_cv_file">
                <label for="cv_file" class="bold">CV File</label>
                <input class="form-control" id="cv_file" name="cv_file" type="file">
				<p>File Formats Allowed .doc .docx .pdf</p>				
                <span class="help-block cv_file-error"></span>
			
			</div>


            
        </div>

        <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-large btn-primary">Add ADD CV <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
        </div>
</form>
