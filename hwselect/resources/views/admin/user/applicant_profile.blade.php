@extends('admin.layouts.admin_layout')
@push('css')
@endpush
@section('content')



<?php $true = FALSE; 

$profileCv = $user->getDefaultCv();


if(Auth::guard('company')->user()){

$package = Auth::guard('company')->user();

if(null!==($package)){

    $array_ids = explode(',',$package->availed_cvs_ids);

    if(in_array($user->id, $array_ids)){

        $true = TRUE;

    }

}

}

$profile_skills = $user->profileSkills;
$skills_arr = array();
if(null!==($profile_skills)){
    foreach ($profile_skills as $key => $value) {
        $skills_arr[] = $value->job_skill_id;
    }
}

?><!-- Inner Page Title end -->

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Profile</span> </li>

            </ul>

        </div>

        @include('flash::message')  
        @if(!request()->job && !request()->job_id)
		<div class="assignboxad">
		
        @if(Auth::guard('admin')->user())
        <div class="form-group">
            <?php
                    $skills = old('skills',$skills_arr);
                ?>
                <span id="skills_dd">                 
                    {!! Form::select('skills[]', $jobSkills, $skills, array('class'=>'form-control select2-multiple', 'id'=>'skills', 'multiple'=>'multiple')) !!}
                </span>

            </div>
            <h3>Select Companies to shortlist Candidate against</h3>
        <div class="form-group">
        	<span id="default_companies_dd">
            <select class="form-control select2-multiple" id="company_id" name="company_id[]" multiple="multiple">

                @if(null!==($companies))

                @foreach($companies as $company)

                <option value="{{$company->id}}" @if(isset($company_id) && $company_id== $company->id) selected="selected" @endif data-ids={{$company->availed_cvs_ids}}>{{$company->name}}</option>

                @endforeach

                @endif

            </select>
            </span>

            </div>

        <div class="form-group">

                <span id="default_jobs_dd"></span>

                <input type="hidden" name="all_ids" id="all_ids">

            </div>

        @else 
        @if(Auth::guard('company')->user())
        <div class="form-group">
            <select class="form-control" id="company_id" name="company_id">
                <option value="" selected="selected">Select Company</option>
                <option value="{{Auth::guard('company')->user()->id}}" data-ids={{Auth::guard('company')->user()->availed_cvs_ids}}>{{Auth::guard('company')->user()->name}}</option>
            </select>
            </div>
		
            <div class="form-group">

                <span id="default_jobs_dd">

                    {!! Form::select('job_id', ['' => 'Select Job'], null, array('id'=>'job_id', 'class'=>'form-control')) !!}

                </span>

            </div>   

        @endif
        @endif
		</div>
        @else
        <?php
            if(request()->job_id){
                $job = App\Job::findorFail(request()->job_id);
            }else{
                $job = App\Job::findorFail(request()->job);
            } 
            
            $company_link = null!==($job->getCompany('id'))?'<a target="_blank" href="'.route('public.company',$job->getCompany('id')).'">'.$job->getCompany('name').'</a>':'';
         ?>
        
		<div class="job-header mt-3">
			 <div class="jobinfo">
             <h3 style="font-weight: bold; margin-top: 0;">Jobs Applied for:</h3>
			 <p>Job: {{$job->title}}</p>
			 <p>Job posted by: {!!$company_link!!}</p>
         </div>
		</div>
		
		
        @endif


        <!-- Job Detail start -->

        <div class="row">

            <div class="col-md-8"> 

                

                <!-- Job Header start -->

        <div class="job-header">

            <div class="jobinfo">
                        <!-- Candidate Info -->

                        <div class="candidateinfo">
							
							<div class="row">
								<div class="col-md-3"><div class="userPic">{{$user->printUserImage()}}</div></div>
								<div class="col-md-9">
								
								<div class="title">{{__('Jobs Applied for by ')}} ({{$user->getName()}})</div>
                            <div class="desi">{{$user->address}}</div>
                            <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> {{__('Member Since')}}, {{$user->created_at->format('d-M-Y')}}</div>
								</div>
							</div>
                            

                            

                            

                            <div class="clearfix"></div>

                        </div>

                    

                    

                                 

                   

                

            </div>

            <!-- Buttons -->

            <div class="jobButtons">
                @if(isset($job) && isset($company))
                @if(Auth::guard('company')->check() && Auth::guard('company')->user()->isFavouriteApplicant($user->id, $job->id, $company->id))
                @else
                @endif
                @endif
                @if(Auth::guard('company')->user())
                @if(null !== $profileCv)@endif @endif 
				
                @if(Auth::guard('company')->user() || Auth::guard('admin')->user())
                @if(count($user->profileCvs)<=0)
                <a class="btn btn-default disabled">{{__('No CV Available')}}</a>
                @else
                <a href="{{asset('cvs/'.$profileCv->cv_file)}}" target="_blank" class="btn btn-default"> {{__('View Candidates CV')}}</a>
                @endif

               <a href="javascript:;" class="btn btn-default admin-short lock"><i class="fa fa-lock" aria-hidden="true"></i> {{__('Shortlist Candidate')}}</a>
             
                @endif 

                @if(null !== $profileCv && !empty($profileCv->cv_file) && file_exists( public_path() . 'cvs/' .$profileCv->cv_file))<a target="_blank" href="{{asset('cvs/'.$profileCv->cv_file)}}" class="btn btn-default"><i class="fa fa-download" aria-hidden="true"></i> {{__('Download CV')}}</a>@endif
                
              @if(request()->job)
				<a href="javascript:;" class="btn btn-default" onclick="view_messagee({{$user->id}},{{request()->job}})">{{__('View Note')}}</a>
				<a href="javascript:;" class="btn btn-default" onclick="send_messagee({{$user->id}})">{{__('Add a Note')}}</a>
                                    
                @endif

                @if(request()->job_id)
                <a href="{{route('reject.candidate',[$user->id,'job_id='.request()->job_id])}}" class="btn btn-default admin-short lock"><i class="fa fa-lock" aria-hidden="true"></i> {{__('Reject Candidate')}}</a>
                @endif
            </div>

        </div>

                

                

                

                

                <!-- About Employee start -->

                <div class="job-header">

                    <div class="contentbox">

                        <h3>About Me (Summary)</h3>

                        <p>{{$user->getProfileSummary('summary')}}</p>

                    </div>

                </div>



               




                <?php 
                	$job_ids = App\JobApply::where('user_id',$user->id)->orderBy('id', 'DESC')->get();


                 ?>
                  <div class="job-header">

                <h3 style="    font-size: 24px;
    font-weight: 700;
    color: #009eff;
    margin-bottom: 10px;
    margin-top: 0px;">{{__('Jobs Applied for by ')}} ({{$user->getName()}})</h3>
</div>
                 <table class="table table-striped table-bordered table-hover" id="user_datatable_ajax">
                <thead>
                <tr role="row" class="heading">
                <th>Name</th>
                <th>Job Title</th>
                <th>Company Name</th>
                <th>Applied Date</th>
                </tr>
                </thead>
                <tbody>
                    @if(null!==($job_ids))
                    @foreach($job_ids as $jo)
                    <?php 
                        $jobb = App\Job::where('id',$jo->job_id)->first();
                        $userr = App\User::findorFail($jo->user_id);
                        if(null!==($jobb)){
                        	$companyy = $jobb->getCompany();
                        }
                        
                     ?>
                     @if(isset($companyy) && null!==$companyy && null!==($jobb))
                    <tr id="user_dt_row_{{$jo->id}}" role="row" class="odd">
                    <td>{{$userr->name}}</td>

                    <td><a target="_blank" href="{{route('public.job', ['id' => $jo->job_id])}}">{{$jobb->title}}</a></td>
                    <td>{{$companyy->name}}</td>
                    <td>{{date('d-M-Y', strtotime($jo->created_at))}}</td>
                    
</tr>
@endif
@endforeach
@endif
                </tbody>
</table>

    
<hr>
    <?php 
        $job_idss = App\Unlocked_users::where('unlocked_users_ids',$user->id)->orderBy('id', 'DESC')->get();

    ?>
                  <div class="job-header">

                <h3 style="    font-size: 24px;
    font-weight: 700;
    color: #009eff;
    margin-bottom: 10px;
    margin-top: 0px;">{{__('Jobs Shortlisted for this Candidate')}}</h3>
</div>
                 <table class="table table-striped table-bordered table-hover" id="user_datatable_ajaxs">
                <thead>
                <tr role="row" class="heading">
                <th>Name</th>
                <th>Job Title</th>
                <th>Company Name</th>
                <th>Shortlisted Date</th>
                </tr>
                </thead>
                <tbody>
                    @if(null!==($job_idss))
                        @foreach($job_idss as $joo)
                            @php 
                                $jobbb = App\Job::find($joo->job_id);
                            @endphp
                            @if($jobbb != null)
                                @php 
                                    $userrr = App\User::findorFail($joo->unlocked_users_ids);
                                    $companyyy = $jobbb->getCompany(); 
                                @endphp
                                @if(null!==$companyyy)
                                <tr id="user_dt_row_{{$joo->id}}" role="row" class="odd">
                                    <td>{{$userrr->name}}</td>

                                    <td><a target="_blank" href="{{route('public.job', ['id' => $joo->job_id])}}">{{$jobbb->title}}</a></td>
                                    <td>{{$companyyy->name}}</td>
                                    <td>{{date('d-M-Y', strtotime($joo->created_at))}}</td>

                                </tr>
                                @endif
                            @endif
                        @endforeach
                    @endif
                </tbody>
</table>


            </div>

            <div class="col-md-4"> 

                

                 <!-- Candidate Contact -->

                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Candidate Contact Details')}}</h3>

                        <div class="candidateinfo">            

                            @if(!empty($user->phone))

                            <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{{$user->phone}}">{{$user->phone}}</a></div>

                            @endif

                            @if(!empty($user->mobile_num))

                            <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{{$user->mobile_num}}">{{$user->mobile_num}}</a></div>

                            @endif

                            @if(!empty($user->email))

                            <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:{{$user->email}}">{{$user->email}}</a></div>

                            @endif

                            <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$user->getlocation()}}</div>

                        </div>  

                    </div>

                </div>

                

                

                <!-- Candidate Detail start -->

                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Candidate Details')}}</h3>

                        <ul class="jbdetail">
                         

                            <li class="row">
                                <div class="col-md-6 col-xs-6">{{__('Available Immediately')}}</div>
                                <div class="col-md-6 col-xs-6"><span>{{((bool)$user->is_immediate_available)? 'Yes':'No'}}</span></div>
                            </li>
							@If(!(bool)$user->is_immediate_available)
							<li class="row">
                                <div class="col-md-6 col-xs-6">Available from</div>
                                <div class="col-md-6 col-xs-6"><span>({{null!==($user->immediate_date_from)?date('d-M-Y',strtotime($user->immediate_date_from)):date('d-M-Y')}})</span></div>
                            </li>
@endif

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Eligible to work in the UK:')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{((bool)$user->eligible_uk)? 'Yes':'No'}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Eligible to work in the EU:')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{((bool)$user->eligible_eu)? 'Yes':'No'}}</span></div>

                            </li>
							
							


                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Age')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getAge()}} Years</span></div>

                            </li>
                            @if(null!==($user->getGender('gender')))
                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Gender')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getGender('gender')}}</span></div>

                            </li>
                            @endif
							
							@if(null!==($user->getMaritalStatus('marital_status')))

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Marital Status')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getMaritalStatus('marital_status')}}</span></div>

                            </li>
                            @endif
						
							
                             @if(null!==($user->education_id))
                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Education')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->education->degree_level}}</span></div>

                            </li>

                            @endif

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Experience')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getJobExperience('job_experience')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Career Level')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getCareerLevel('career_level')}}</span></div>

                            </li>             

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Current Salary')}}</div>

                                <div class="col-md-6 col-xs-6"><span class="permanent">{{$user->salary_currency}}{{$user->current_salary}} </span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Expected Salary')}}</div>

                                <div class="col-md-6 col-xs-6"><span class="freelance">{{$user->salary_currency}}{{$user->expected_salary}} </span></div>

                            </li>              

                        </ul>

                    </div>

                </div>



                <!-- Google Map start -->
                @if(count($user->profileCvs)<=0)
                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('CV Added')}} - <strong style="color: #E70509">No</strong></h3>          

                    </div>

                </div>
                @else
                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('CV Added')}} - <strong style="color: #2DC507">Yes</strong></h3>          

                    </div>

                </div>
                @endif
			
                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Skills')}}</h3>

                        <div id="skill_div"></div>            
						<div class="clearfix"></div>
                    </div>

                </div>



                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Languages')}}</h3>

                        <div id="language_div"></div>            

                    </div>

                </div>

               

            </div>

        </div>

    </div>

</div>

<div class="modal fade" id="sendmessage" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

            <form action="" id="send-form">

                @csrf

                <input type="hidden" name="seeker_id" id="seeker_id" value="{{$user->id}}">

                <div class="modal-header">                    

                    <h4 class="modal-title">Send Message</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                <div class="modal-body">

                    <div class="form-group">

                        <textarea class="form-control" name="message" id="message" cols="10" rows="7"></textarea>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="submit" class="btn btn-primary">Submit</button>

                </div>

            </form>

        </div>



    </div>

</div>


<div class="modal fade" id="sendmessagee" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="" id="send-formm">
                @csrf
                <input type="hidden" name="seeker_id" class="seeker_id" id="seeker_id">
                <div class="modal-header">                    
                    <h4 class="modal-title">Add Note</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="formradio" style="margin-top: 25px;">
                          <input class="" type="radio" id="inlineCheckbox1" value="called_for_interview" name="option">
                          <label class="form-check-label" for="inlineCheckbox1">Called for interview</label>
                        </div>
                        <div class="formradio">
                          <input class="" type="radio" id="inlineCheckbox2" value="interviewed" name="option">
                          <label class="form-check-label" for="inlineCheckbox2">Interviewed</label>
                        </div>
                       

                     

                      

                    </div>
                    <input type="hidden" name="job_id" value="{{request()->job}}">
                    <div class="form-group">
                        <input type="text" name="date" class="form-control datepicker">
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Add a Note" id="message" cols="10" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>

    </div>
</div>


<div class="modal fade" id="showmessage" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

                <div class="modal-header">                    

                    <h4 class="modal-title">All Notes</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                <div class="modal-body">

                    <div class="clearfix message-content">

                      <div class="message-details">

                      <h4> </h4>

                        <div id="append_messages"></div>

                      </div>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

        </div>



    </div>

</div>

<div class="modal fade" id="sendmessage" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="" id="send-form">
                @csrf
                <input type="hidden" name="seeker_id" id="seeker_id">
                <div class="modal-header">                    
                    <h4 class="modal-title">Add Note</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="formradio" style="margin-top: 25px;">
                          <input class="" type="radio" id="inlineCheckbox1" value="called_for_interview" name="option">
                          <label class="form-check-label" for="inlineCheckbox1">Called for interview</label>
                        </div>
                        <div class="formradio">
                          <input class="" type="radio" id="inlineCheckbox2" value="interviewed" name="option">
                          <label class="form-check-label" for="inlineCheckbox2">Interviewed</label>
                        </div>
                       

                     

                      

                    </div>
                    <input type="hidden" name="job_id" value="{{request()->job}}">
                    <div class="form-group">
                        <input type="text" name="date" class="form-control datepicker">
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Add a Note" id="message" cols="10" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="modal fade" id="showmessagee" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
                <div class="modal-header">                    
                    <h4 class="modal-title">All Notes</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="clearfix message-content">
                      <div class="message-details">
                        <h4> </h4>
                        <div id="append_messagess"></div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>

    </div>
</div>

@endsection

@push('styles')

<style type="text/css">

    .formrow iframe {

        height: 78px;

    }

</style>

@endpush

@push('scripts') 

<script type="text/javascript">

     function send_messagee(id) {
        $('.seeker_id').val(id);
        $('#sendmessagee').modal('show');
    }
    if ($("#send-formm").length > 0) {
        $("#send-formm").validate({
            validateHiddenInputs: true,
            ignore: "",

            rules: {
                option: {
                    required: true,
                },
                date: {
                    required: true,
                },
                message: {
                    required: true,
                    maxlength: 5000
                },
            },
            messages: {

                option: {
                    required: "Please select an option",
                },
                date: {
                    required: "Date is required",
                },
                message: {
                    required: "Message is required",
                }

            },
            submitHandler: function(form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('admin.submit-action-seeker')}}",
                    type: "POST",
                    data: $('#send-formm').serialize(),
                    success: function(response) {
                        $("#send-formm").trigger("reset");
                        $('#sendmessagee').modal('hide');
                        swal({
                            title: "Success",
                            text: response["msg"],
                            icon: "success",
                            button: "OK",
                        });
                    }
                });
            }
        })
    }
    $(".datepicker").datetimepicker();
    function view_messagee(id,job_id){
         $.ajax({
                url: "{{route('admin.view-action-seeker')}}?id="+id+"&job_id="+job_id,
                type: "GET",
                success: function(response) {
                    $('#append_messagess').html(response);
                    $('#showmessagee').modal('show');
                }
            });
    }
function view_message(id){

         $.ajax({

                url: "{{route('admin.view-notes')}}?id="+id,

                type: "GET",

                success: function(response) {

                    $('#append_messages').html(response);

                    $('#showmessage').modal('show');

                }

            });

    }
 function myfunction(id){
    var ab = $('#all_ids').val();
    if($("#input_chcked_"+id).is(":checked") == true){
        var toString = ab+','+id;
        toString = toString.replace(/,+$/,'');
        $('#all_ids').val(toString);
    }else{
        var ary = ab.split(',');
        for(var i = 0 ; i < ary.length ; i++) {
          if(ary[i] == id) {
             ary.splice(i, id);
             ary.join(",");
          }
       }
        $('#all_ids').val(ary);
    }
    
 } 



  
$('#skills').select2({
            placeholder: "{{__('Select Skills')}}",
            allowClear: true
});

$('#company_id').select2({
            placeholder: "{{__('Select Company')}}",
            allowClear: true
});


$(document).on('change', '.job_id', function (e) {

    var job_id = $(this).val();
    var company_id = $('#company_id').val();
    $.ajax({

          type: "GET", 

          url: '{{route("check-lock-unlock")}}?job_id='+job_id+'&company_id='+company_id,   

          success: function(data){
                var arr = data.toString().split(",");
                var item = "{{$user->id}}";
                for (i = 0; i < arr.length; i++) { 
                   if($.trim(arr[i]) == item){
                    $('.admin-short i').addClass('fa-unlock');
                    $('.admin-short i').removeClass('fa-lock');
                    $('.admin-short').addClass('unlock');
                    $('.admin-short').removeClass('lock');
                   }else{
                    $('.admin-short i').removeClass('fa-unlock');
                    $('.admin-short i').addClass('fa-lock');
                    $('.admin-short').addClass('lock');
                    $('.admin-short').removeClass('unlock');
                   }
                }
          },

      });
})

    $(document).ready(function () {

    $(document).on('click', '#send_applicant_message', function () {

    var postData = $('#send-applicant-message-form').serialize();

    $.ajax({

    type: 'POST',

            url: "{{ route('contact.applicant.message.send') }}",

            data: postData,

            //dataType: 'json',

            success: function (data)

            {

            response = JSON.parse(data);

            var res = response.success;

            if (res == 'success')

            {

            var errorString = '<div role="alert" class="alert alert-success">' + response.message + '</div>';

            $('#alert_messages').html(errorString);

            $('#send-applicant-message-form').hide('slow');

            $(document).scrollTo('.alert', 2000);

            } else

            {

            var errorString = '<div class="alert alert-danger" role="alert"><ul>';

            response = JSON.parse(data);

            $.each(response, function (index, value)

            {

            errorString += '<li>' + value + '</li>';

            });

            errorString += '</ul></div>';

            $('#alert_messages').html(errorString);

            $(document).scrollTo('.alert', 2000);

            }

            },

    });

    });

    showEducation();

    showProjects();

    showExperience();

    showSkills();

    showLanguages();

    });

    function showProjects()

    {

    $.post("{{ route('show.applicant.profile.projects', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#projects_div').html(response);

            });

    }

    function showExperience()

    {

    $.post("{{ route('show.applicant.profile.experience', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#experience_div').html(response);

            });

    }

    function showEducation()

    {

    $.post("{{ route('show.applicant.profile.education', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#education_div').html(response);

            });

    }

    function showLanguages()

    {

    $.post("{{ route('show.applicant.profile.languages', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#language_div').html(response);

            });

    }

    function showSkills()

    {

    $.post("{{ route('show.applicant.profile.skills', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#skill_div').html(response);

            });

    }



    function send_message() {


        $('#sendmessage').modal('show');

       

    }

    if ($("#send-form").length > 0) {

        $("#send-form").validate({

            validateHiddenInputs: true,

            ignore: "",



            rules: {

                message: {

                    required: true,

                    maxlength: 5000

                },

            },

            messages: {



                message: {

                    required: "Message is required",

                }



            },

            submitHandler: function(form) {

                $.ajaxSetup({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });



                $.ajax({

                    url: "{{route('submit-message-seeker')}}",

                    type: "POST",

                    data: $('#send-form').serialize(),

                    success: function(response) {

                        $("#send-form").trigger("reset");

                        $('#sendmessage').modal('hide');

                        swal({

                            title: "Success",

                            text: response["msg"],

                            icon: "success",

                            button: "OK",

                        });

                    }

                });



            }

        })

    }

    $(document).ready(function(){

        $('#skills').on('change', function (e) {

            e.preventDefault();



            filterCompanies(0);



        });


@if(null!==($skills_arr) && !request()->job)
    filterCompanies(0);

    @endif



        /*******************************/



    function filterCompanies(job_id)



    {



        var user_id = "{!! collect(request()->segments())->last() !!}";

        var skills = $('#skills').val();

        var companies = $('#company_id').val();

        if (skills != '') {



            $.post("{{ route('filter.default.companies.dropdown.all') }}", {skills: skills, companies: companies, user_id:user_id, _method: 'POST', _token: '{{ csrf_token() }}'})



                    .done(function (response) {
                        
                            $('#default_companies_dd').html(response);
                       $('#company_id').select2({
                                    placeholder: "{{__('Select Company')}}",
                                    allowClear: true
                        });
                        
                        
                       


                    });






        }



    }


    
    

    })




     $(document).ready(function(){

        $(document).on('change', '#company_id', function (e) {

            $('.admin-short').show();

            e.preventDefault();



            filterJobs(0);



        });
        
        @if(isset($job_id))
        filterJobs({{$job_id}});
        @endif



       

        @if(null!==(old('job_id')))

        filterJobs(<?php echo old('job_id'); ?>);

        @endif







        /*******************************/



         function filterJobs(job_id)



    {


        var user_id = "{!! collect(request()->segments())->last() !!}";
        var company_ids = $('#company_id').val();

        var ab = $('#all_ids').val();

        if (company_id != '') {



            $.post("{{ route('filter.default.jobs.dropdown.all') }}", {company_id: company_ids, job_id: job_id,all_ids:ab , user_id:user_id, _method: 'POST', _token: '{{ csrf_token() }}'})



                    .done(function (response) {


                        $('#default_jobs_dd').html(response);


                        
                            $('table').DataTable();
                        



                    });






        }



    }


    
    

    })

    @if(Auth::guard('admin')->user() || Auth::guard('company')->user())

    $(".admin-short.lock").on('click',function() {
        if($('#company_id').val() ==''){
            alert('Select from list of Companies that match this candidates skillsets')
            return false;
        }

    var contact_detail = 'hide';
    if ($('#contact_detail').is(":checked"))
    {
      contact_detail = 'show';
    }

    var array = [];
    $("input:checkbox:checked").each(function() { 
        array.push($(this).val()); 
    });
    if(array == ''){
        alert('Please select at least one job to shortlist candidate against');
        return false;
    }
    else{
        var user_id='{{$user->id}}';

        var company_id="{{request()->company_id}}";

        var job_id="{{request()->job_id}}";
        var job_ids=$('#all_ids').val();

        //alert(user_id);

       var id='';

       var url  = '';
       @if(Auth::guard('admin')->user())
        url = '{{route("admin.company.unlock")}}?user_id='+user_id+'&company_id='+company_id+'&job_ids='+job_ids+'&contact_detail='+contact_detail;
       @else
       url = '{{route("company.unlock")}}?user_id='+user_id+'&company_id='+company_id+'&job_id='+job_id
       @endif
       



                    

                        $.ajax({

                          type: "GET", 

                          url: url,   

                          success: function(data){

                            var obj = JSON.parse(data);
                               swal({

                                    title: "Success",

                                    text: obj.msg,

                                    icon: "success",

                                    button: "OK",

                                });
                               if(obj.flag=='unlocked'){
                                $('.admin-short i').removeClass('fa-lock');

                               $('.admin-short i').addClass('fa-unlock');
                               $('.admin-short').addClass('unlock');
                               $('.admin-short').removeClass('lock');
                           }else{
                               
                                $('.admin-short i').addClass('fa-lock');

                                $('.admin-short i').removeClass('fa-unlock');
                                $('.admin-short').removeClass('unlock');
                                $('.admin-short').addClass('lock');
                            }
                            location.reload();
                           }

                      });

                

        
        }
        

});

$(".admin-short.unlock").on('click',function() {

        var user_id='{{$user->id}}';

        var company_id="{{request()->company_id}}";

        var job_id="{{request()->job_id}}";

        //alert(user_id);

       var id='';

       var durl  = '';
       @if(Auth::guard('admin')->user())
        durl = '{{route("admin.company.lock")}}?user_id='+user_id+'&company_id='+company_id+'&job_id='+job_id;
       @else
       durl = '{{route("company.lock")}}?user_id='+user_id+'&company_id='+company_id+'&job_id='+job_id
       @endif
       alert(durl);
        if(company_id!='' && company_id!='Select Employer' && job_id!='' && job_id!='Select Job'){



                    

                        $.ajax({

                          type: "GET", 

                          url: durl,   

                          success: function(data){

                               swal({

                                    title: "Success",

                                    text: 'You have successully locked this profile',

                                    icon: "success",

                                    button: "OK",

                                });

                               $('.admin-short i').removeClass('fa-unlock');

                               $('.admin-short i').addClass('fa-lock');
                               $('.admin-short').addClass('lock');
                               $('.admin-short').removeClass('unlock');
                               location.reload();
                          },

                      });

                

        }else{

            alert('Please Select Employer')

            $('.customCheck:checked').prop('checked', false);

            $('.checker span').removeClass( 'checked');

        }

        

        

});

@endif

$("#contact_detail").change(function() {

       var id='';

       var user_id='{{$user->id}}';

       var contact_detail = 'hide';

                if($(this).is(':checked')){

                    contact_detail = 'show';

                    $.ajax({

                          type: "GET", 

                          url: '{{route("admin.company.show_hide")}}?user_id='+user_id+'&contact_detail='+contact_detail,   

                          success: function(data){

                            



                          },

                      });

                } else {

                        $.ajax({

                          type: "GET", 

                          url: '{{route("admin.company.show_hide")}}?user_id='+user_id+'&contact_detail='+contact_detail,   

                          success: function(data){

                            



                          },

                      });

                }

        

}); 


    $('table').DataTable({"orderable": false});

</script> 
<style type="text/css">

    
.select2-selection--multiple:before {
    content: "";
    position: absolute;
    right: 7px;
    top: 42%;
    border-top: 5px solid #888;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
}
</style>
@endpush