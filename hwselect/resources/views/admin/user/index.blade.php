@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;


    }	
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }
</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Jobseekers</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage Jobseekers </h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12"> 

                <!-- Begin: life time stats -->

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Job seekers</span> </div>

                        <div class="actions">

                            <a href="{{ route('create.user') }}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i> Add New User</a>

                        </div>

                    </div>

                    <div class="portlet-body">

                        <div class="table-container">

                            <form method="post" role="form" id="user-search-form">

                                <table class="table table-striped table-bordered table-hover"  id="user_datatable_ajax">

<thead>

<tr role="row" class="filter">                  

<td><input type="text" class="form-control" name="id" id="id" placeholder="Search By ID" autocomplete="off"></td>                    

<td><input type="text" class="form-control" name="name" placeholder="Search By Name" id="name" autocomplete="off"></td>

<td><input type="text" class="form-control" name="email" placeholder="Search By Email" maxlength="50" id="email" autocomplete="off"></td>

<td>
<input type="date" class="form-control" name="date_from" id="date_from" autocomplete="off" placeholder="Date From">

<input type="date" class="form-control" name="date_to" id="date_to" autocomplete="off" placeholder="Date To">
</td>

<td width="150">
<select name="cv_title" id="cv_title" class="form-control">

<option>Select Status</option>

<option value="yes">Yes</option>

<option value="no">No</option>

</select>
</td>

<td width="150"><select name="is_active" id="is_active" class="form-control">

<option value="-1">Is Active?</option>

<option value="1" selected="selected">Active</option>

<option value="0">In Active</option>

</select>

<select name="is_verified" id="is_verified" class="form-control">

<option value="-1">Is Verified?</option>

<option value="1" selected="selected">Verfied</option>

<option value="0">Not Verified</option>

</select></td>
<td></td>
</tr>

<tr role="row" class="heading"> 

<th>Id</th>                                        

<th>Name</th>

<th>Email</th>  

<th>Registration Date</th>
<th>CV Added</th>

              

<th>Actions</th>
<th class="text-center">
<button class="btn btn-danger delete">Delete</button>
<br>
Select All<br><input type="checkbox" id="selectall" name="selectall" onclick="toggle(this)" autocomplete="off"></th>

</tr>

</thead>

                                    <tbody>

                                    </tbody>

                                </table></form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>

@endsection

@push('scripts') 

<script>

    $(function () {

        var oTable = $('#user_datatable_ajax').DataTable({

            processing: true,

            serverSide: true,

            stateSave: false,

            searching: false,

            pagingType: 'full_numbers',

            "language": {                
                "infoFiltered": ""
            },

            "order": [[0, "desc"]],
            "ordering": false,
            
            /*		

             paging: true,

             info: true,

             */

            ajax: {

                url: '{!! route('fetch.data.users') !!}',

                data: function (d) {

                    d.id = $('input[name=id]').val();

                    d.name = $('input[name=name]').val();

                    d.email = $('input[name=email]').val();

                    d.cv_title = $('#cv_title').val();

                     d.date_from = $('#date_from').val();

                     d.is_active = $('#is_active').val();

                     d.is_verified = $('#is_verified').val();

                    d.date_to = $('#date_to').val();

                }

            }, columns: [

                /*{data: 'id_checkbox', name: 'id_checkbox', orderable: false, searchable: false},*/

                {data: 'id', name: 'id'},

                {data: 'name', name: 'name'},

                {data: 'email', name: 'email'},



                 {data: 'created_at', name: 'created_at'},
                 {data: 'cv_added', name: 'cv_added'},

                {data: 'action', name: 'action', orderable: false, searchable: false},

                {data: 'checkbox', name: 'checkbox'},

            ]

        });

        $('#user-search-form').on('submit', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#id').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#name').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#email').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#cv_title').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

         $('#date_from').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#date_to').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

          $('#is_active').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });


          $('#is_verified').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

    });

    function delete_user(id,title) {

        if (confirm('Are you sure! you want to delete ('+title+')')) {

            $.post("{{ route('delete.user') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        

                            var table = $('#user_datatable_ajax').DataTable();

                            table.row('user_dt_row_' + id).remove().draw(false);

                        

                    });

        }

    }
    @if(isset(request()->is_active))
    $('#is_active').val({{request()->is_active}});
    @endif


    @if(isset(request()->is_verified))
    $('#is_verified').val({{request()->is_verified}});
    @endif
    function make_active(id) {

        $.post("{{ route('make.active.user') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        $('#onclick_active_' + id).attr("onclick", "make_not_active(" + id + ")");

                        $('#onclick_active_' + id).html("<i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>Make InActive");

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function make_not_active(id) {

        $.post("{{ route('make.not.active.user') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        $('#onclick_active_' + id).attr("onclick", "make_active(" + id + ")");

                        $('#onclick_active_' + id).html("<i class=\"fa fa-square-o\" aria-hidden=\"true\"></i>Make Active");

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function make_verified(id) {

        $.post("{{ route('make.verified.user') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        $('#onclick_verified_' + id).attr("onclick", "make_not_verified(" + id + ")");

                        $('#onclick_verified_' + id).html("<i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>Verified");

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function make_not_verified(id) {

        $.post("{{ route('make.not.verified.user') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        $('#onclick_verified_' + id).attr("onclick", "make_verified(" + id + ")");

                        $('#onclick_verified_' + id).html("<i class=\"fa fa-square-o\" aria-hidden=\"true\"></i>Not Verified");

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }
 toggle = function(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i] != source)
    checkboxes[i].checked = source.checked;
    }
    }
    $('.delete').on('click',function(){
        var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
            return this.value;
        }).get();
        var ids = checkedVals.join(",");
        $.ajax({
          method: "GET",
          url: "{{route('delete.users')}}",
          data: { ids: ids}
        })
        .done(function( msg ) {
            location.reload();
        });
    })

</script> 
<style type="text/css">
    table td:last-child {text-align: center;}
    .checkboxes{
        
    }
</style>
@endpush