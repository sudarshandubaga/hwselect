@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }   
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }

</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>All Modules</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage All Modules <small>All Modules</small> </h3>


<!-- end page title end breadcrumb -->
<div class="row">
    <div class="col-md-12">
        <div class="card m-b-30">
            <div class="card-body">
                @if(session()->has('message.added'))
                <div class="alert alert-success alert-dismissible fade show d-flex align-items-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! session('message.content') !!}.
                </div>
                @endif
                @if ($message = Session::get('warning'))
                <div class="alert alert-danger alert-dismissible fade show d-flex align-items-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! $message !!}.
                </div>
                @endif
                <table id="datatable" style="text-align: center;" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Module Name</th>
                            <th>Module Term</th>
                            <th>Link</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($modules)
                        @foreach($modules as $module)
                        <tr>
                            <td>{{$module->module_name}}</td>
                            <td>{{$module->module_term}}</td>
                            <td><a href="{{ route('modules-data',$module->slug)}}">Link</a></td>
                            <td>
                                <a class="btn btn-success" href="{{route('edit-module',$module->id)}}" role="button">Edit</a>
                                <a class="btn btn-danger delete" href="{{route('delete-modules',$module->id)}}" role="button">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div>
</div>
</div>


@endsection