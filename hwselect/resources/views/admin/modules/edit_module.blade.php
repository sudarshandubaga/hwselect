@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h4 class="page-title">Create Module</h4>
        </div>
    </div>
</div>
<!-- end page title end breadcrumb -->
<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="card m-b-30">
            <div class="card-body">
                <div class="">

                    @if(session()->has('message.added'))
                    <div class="alert alert-success alert-dismissible fade show d-flex align-items-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! session('message.content') !!}.
                    </div>
                    @endif
                    <div class="tab-content">
                        <div class="tab-pane active show" id="settings">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="">

                                        <form class="form-horizontal form-material" action="{{ route('post-module') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Module Name</label>
                                                        <input type="text" name="module_name" id="module_name" placeholder="Module Name" required class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Module Term</label>
                                                        <input type="text" name="module_term" id="module_term" placeholder="Module Term" required class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Thumbnail Height</label>
                                                        <input type="text" name="thumbnail_height" id="thumbnail_height" placeholder="Thumbnail Height" required class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Thumbnail Width</label>
                                                        <input type="text" name="thumbnail_width" id="thumbnail_width" placeholder="Module Term" required class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="card-body card-body-internal">
                                                        <div class="row">
                                                            <div class="col-md-4 checkbox my-2">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="page_slug" id="page_slug" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                                    <label class="custom-control-label" for="page_slug">Page Slug</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 checkbox my-2">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="menu" id="menu" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                                    <label class="custom-control-label" for="menu">TopBar Menu</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 checkbox my-2">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="page_description" id="page_description" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                                    <label class="custom-control-label" for="page_description">Page Description</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 checkbox my-2">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="featured_image" id="featured_image" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                                    <label class="custom-control-label" for="featured_image">Featured Image</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 checkbox my-2">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="seo" id="seo" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                                    <label class="custom-control-label" for="seo">Search Engine Optimization</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="text-right m-t-15">
                                                        <a href="{{route('modules')}}" class="btn btn-warning waves-effect waves-light btn-form"><i class="fas fa-arrow-alt-circle-left"></i>Back</a>
                                                        <button type="Submit" class="btn btn-primary waves-effect waves-light btn-form">Submit<i class="fas fa-arrow-alt-circle-right"></i></button>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection