@extends('layouts.app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.paginate.css')}}">
@endpush
@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Company Posted Jobs')])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        <div class="row">

          <div class="col-lg-3 col-md-4">
				
			 @include('includes.company_dashboard_menu')	

			</div>



            <div class="col-lg-9 col-md-8"> 

                <div class="myads">
					<div class="all_jobs display-jobs">
					 <h3>{{__('Refused Jobs')}} <span style="font-size: 16px; color: red;">A member of our team will contact you shortly</span></h3>
					
					
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                         <?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
    if(null!==($job->salary_from)){
$salary = '<strong>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
} 


?>   

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)

						
						
						@if($job->isJobExpired())
						<li id="job_li_{{$job->id}}" class="jbexpired">
						@else
						<li id="job_li_{{$job->id}}">
						@endif
							
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
											
                      @if($job->is_rejected==1)
                      - <span class="expire">Refused</span>
                      @endif
										</h3>
										<div class="row">
											<div class="col-lg-6">
											<div class="jobitem itemsalary" title="Salary">{!!$salary!!}
         <?php 
                      $all_bouns = '';
                      if(null!==($job->bonus)){
                        $bon = json_decode($job->bonus);
                        if(null!==($bon)){
                          foreach ($bon as $key => $value) {
                            $bonus = App\Bonus::where('id',$value)->first();
                                    if(null!==($bonus)){
                                       $all_bouns .=$bonus->bonus.', '; 
                                    }
                          }
                        }

                      }


                      $all_benifits = '';
                      if(null!==($job->benifits)){
                        $beni = json_decode($job->benifits);
                        if(null!==($beni)){
                          foreach ($beni as $key => $val) {
                            $benifits = App\Benifits::where('id',$val)->first();
                                    if(null!==($benifits)){
                                       $all_benifits .=$benifits->benifits.', ';
                                    }
                          }
                        }

                      }
                     ?>


           @if($job->salary_type != 'negotiable') {{$job->getSalaryPeriod('salary_period')}} @endif @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif
                                            </div>
												<div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
										<div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
											</div>
											<div class="col-lg-6">
											<div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
										<div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>
											
											{{$job->created_at->format('d-M-Y')}}
											
											</div>
											</div>
										</div>                                       

									<p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
									</div>
									 </div>
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
									  <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
									  <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit- Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

									   <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
						
									   @if($job->is_active==1)
									<a href="javascript:;" onclick="makeNotActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Make Inactive')}}</a>   
									   @endif
                                 
                                </div>
                            </div>
							</div>
                        </li>
						
						
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
						<li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>



                    <div class="expired_jobs display-jobs">
           <h3>{{__('Expired Jobs')}}</h3>
          
          
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                        <?php 

if($job->salary_type == 'single_salary'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else if($job->salary_type == 'salary_in_range'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - '.number_format($job->salary_to).'</strong>';

}else{
    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
} 


?>

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)

            
            
            @if($job->isJobExpired())
            <li id="job_li_{{$job->id}}" class="jbexpired">
            
              
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                      @if($job->isJobExpired())
                       - <span class="expire">Job Expired ({{date('d-M-Y',strtotime($job->expiry_date))}})</span>
                      @endif
                    </h3>
                    <div class="row">
                      <div class="col-lg-6">
                      <div class="jobitem itemsalary" title="Salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}} @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif 
                                            </div>
                        <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                    <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                      </div>
                      <div class="col-lg-6">
                      <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                    <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>
                      
                      {{$job->created_at->format('d-M-Y')}}
                      
                      </div>
                      </div>
                    </div>                                       

                  <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                  </div>
                   </div>
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                    <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit - Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                     <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
 
                                 
                                </div>
                            </div>
              </div>
                        </li>
            @endif
            
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
            <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>


                    <div class="active_jobs display-jobs">
           <h3>{{__('Active Jobs')}}</h3>
          
          
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                        <?php 

if($job->salary_type == 'single_salary'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else if($job->salary_type == 'salary_in_range'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - '.number_format($job->salary_to).'</strong>';

}else{
    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
} 


?>

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)

            
            
            @if($job->is_active==1 && !$job->isJobExpired())
            <li id="job_li_{{$job->id}}">
            
              
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                      @if($job->isJobExpired())
                       - <span class="expire">Job Inactive</span>
                      @endif
                    </h3>
                    <div class="row">
                      <div class="col-lg-6">
                      <div class="jobitem itemsalary" title="Salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}} @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif 
                                            </div>
                        <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                    <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                      </div>
                      <div class="col-lg-6">
                      <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                    <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>
                      
                      {{$job->created_at->format('d-M-Y')}}
                      
                      </div>
                      </div>
                    </div>                                       

                  <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                  </div>
                   </div>
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                    <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit - Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                     <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
<a href="javascript:;" onclick="makeNotActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Make Inactive')}}</a>  
                                 
                                </div>
                            </div>
              </div>
                        </li>
            @endif
            
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
            <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>


                    <div class="inactive_jobs display-jobs">
           <h3>{{__('Inactive Jobs')}}</h3>
          
          
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                        <?php 

if($job->salary_type == 'single_salary'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else if($job->salary_type == 'salary_in_range'){

     $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - <span class="symbol">'.$job->salary_currency.'</span> '.number_format($job->salary_to).'</strong>';

}else{
    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
} 


?>

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)

            
            
            @if($job->is_active!==1)


            <li id="job_li_{{$job->id}}" class="jbexpired">
            
              
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                      @if($job->isJobExpired())
                       - <span class="expire">Job Inactive</span>
                      @endif
                       
                       @if($job->request_to_delete=='yes')
                       -<span class="expire">Awaiting admin review for deletion</span>
                      
                      @else
                      @if($job->admin_reviewed!==1)
                      - <span class="expire">Job Awaiting Approval</span> 
                      @endif
                      @endif
                    </h3>
                    <div class="row">
                      <div class="col-lg-6">
                      <div class="jobitem itemsalary" title="Salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}} @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif 
                                            </div>
                        <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                    <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                      </div>
                      <div class="col-lg-6">
                      <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                    <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>
                      
                      {{$job->created_at->format('d-M-Y')}}
                      
                      </div>
                      </div>
                    </div>                                       

                  <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                  </div>
                   </div>
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                    <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit - Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                     <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif

                     <a href="javascript:;" onclick="makeActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Reactivate')}}</a>

                                 
                                </div>
                            </div>
              </div>
                        </li>
            @endif
            
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
            <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('js/jquery.paginate.js')}}"></script>
<script type="text/javascript">

    function deleteJob(id,title) {

        $('#job_del_id').val(id);
        $('.job-title-lable').html(title);
        $('#requesttodeletejob').modal('show');
  /*  var msg = 'Are you sure?';

    if (confirm(msg)) {

    $.post("{{ route('delete.front.job') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            if (response == 'ok')

            {

            $('#job_li_' + id).remove();

            } else

            {

            alert('Request Failed!');

            }

            });

    }*/

    }


    $('.submitActiontoDelete').on('click',function(){

    
  $.ajax({

      url: "{{route('delete.front.job')}}",

      type: "POST",

      data: $('#requestdelete').serialize(),

      success: function(response) {

          $("#requestdelete").trigger("reset");

          $('#requesttodeletejob').modal('hide');

          swal({

              title: "Success",

              text: response,

              icon: "success",

              button: "OK",

          });


      }

  });
})
    displayjobs('all');
    function displayjobs(type){
      $('.display-jobs').hide();
      $('.'+type+'_jobs').show();
      $('.btn-show-jobs').removeClass('active');
      $('.'+type+'-jobs').addClass('active');

    }

    function makeNotActive(id,title) {
       const el = document.createElement('div')
          el.innerHTML ='<p>Click <strong>OK</strong> to make Job Inactive or Cancel to Exit</p> <p>Job can be reactivated at any time.</p> <p>To reactivate a Job, Click View Inactive Jobs,</p> <p>Select <strong>Edit-Extend</strong> and <strong>UPDATE JOB</strong></p>';
swal({
  title: "Job: "+title,
  content: el,
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {

    $.post("{{ route('front.make.not.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        swal("Thank You", "Job Successfully Inactivated", "success");
                        location.reload();

                    } else

                    {

                        alert('Request Failed!');

                    }

                }); 
  } else {
    return false;
  }
});







      

        

    }

        function makeActive(id,title) {
       const el = document.createElement('div')
          el.innerHTML ='Click OK to make Job Active or Cancel to Exit';
swal({
  title: "Job: "+title,
  content: el,
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {

    $.post("{{ route('front.make.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        swal("Thank You", "Job Successfully Activated", "success");
                        location.reload();

                    } else

                    {

                        alert('Request Failed!');

                    }

                }); 
  } else {
    return false;
  }
});







      

        

    }

    $(document).on('click','.swal-button--confirm',function(){
                location.reload();
          });

    $('.searchList').paginate({
       perPage:10,
    });

</script>

@endpush