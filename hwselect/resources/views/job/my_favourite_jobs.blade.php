@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Saved Jobs')])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        <div class="row">
            @if(Auth::user())
            @include('includes.user_dashboard_menu')
            @endif

            <div class="col-md-9 col-sm-8">

                <div class="myads">
                    <div class="row mb-4">
                      <div class="col-md-4 col-12"><h3><i class="far fa-star"></i> {{__('Saved Jobs')}}</h3></div>
						
						<div class="col-md-5 col-7"><p>To remove Job, click saved button</p></div>
						
                      <div class="col-md-3 col-5">						  
						  <div class="delsvedjobs"><a href="javascript:;" onclick="remove_all_jobs()"><i class="fas fa-trash"></i> Delete all saved jobs</a></div></div>
                    </div>
                    

						@if(!Auth::user() && !Auth::guard('company')->user())
              <p>
                  You can save jobs on any device, but you'll need to
                      
                  <a href="{{route('login')}}">log&nbsp;in</a>
                  or
                  <a href="{{route('register')}}">register</a>
                      
                  to see them between devices.
              </p>
					  @endif
					
					
					
                    <div class="saved-jobs-container no-savedjobs" style="display: none;">
                      <p>
                          <b>
                              To save a job, click the <button type="button" class="btn btn-default btn-savejob"><i class="far fa-star" aria-hidden="true"></i> Save</button>
                              button on a job you like.
                          </b>
                      </p>
                      <p>The jobs you save will be stored here.</p>
                  </div>

                    <ul class="searchList mt-3">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                         <?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
    if(null!==($job->salary_from)){
$salary = '<strong>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}
?>   

                        @php $company = $job->getCompany(); @endphp

                       


						<li id="li_{{$job->id}}">

                            <div class="row">
								<div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
										 <?php
                                                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $job->created_at);
                                                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', date('Y-m-d H:i:s'));
                                                    $diff_in_hours = $to->diffInHours($from);
                                                    ?>
                                                    <?php if (null !== ($diff_in_hours) && $diff_in_hours < 24) { ?>
                                                    <span class="jbtype featured">New</span>
                                                    <?php } ?>
										</h3>
										<div class="row">
											<div class="col-lg-6">
											<div class="jobitem itemsalary" title="Salary">{!!$salary!!}
         <?php 
                      $all_bouns = '';
                      if(null!==($job->bonus)){
                        $bon = json_decode($job->bonus);
                        if(null!==($bon)){
                          foreach ($bon as $key => $value) {
                            $bonus = App\Bonus::where('id',$value)->first();
                                    if(null!==($bonus)){
                                       $all_bouns .=$bonus->bonus.', '; 
                                    }
                          }
                        }

                      }


                      $all_benifits = '';
                      if(null!==($job->benifits)){
                        $beni = json_decode($job->benifits);
                        if(null!==($beni)){
                          foreach ($beni as $key => $val) {
                            $benifits = App\Benifits::where('id',$val)->first();
                                    if(null!==($benifits)){
                                       $all_benifits .=$benifits->benifits.', ';
                                    }
                          }
                        }

                      }
                     ?>


           @if($job->salary_type != 'negotiable') {{$job->getSalaryPeriod('salary_period')}} @endif @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif
                                            </div>
												<div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
										<div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
											</div>

											<div class="col-lg-6">
											<div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>	
										<div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i> {{$job->created_at->format('d-M-Y')}}</div>
											</div>
										</div>
									<p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                                </div>
									 </div>

                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn">
									<a href="javascript:void(0)" id="job_{{$job->id}}" data-id="{{$job->id}}" class="btn save_job"><i class="far fa-star" aria-hidden="true"></i> {{__('Save')}}</a>
									   
									   
									   
									   
									  @if(Auth::check() && Auth::user()->isAppliedOnJob($job->id))
									    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="btn apply"><i class="fas fa-paper-plane" aria-hidden="true"></i> {{__('View Details')}}</a>
									  @else
									    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="btn apply"><i class="fas fa-paper-plane" aria-hidden="true"></i> {{__('Apply')}}</a>
									  @endif


									  <button class="btn btn-danger mt-3" onclick="deleteFromFav({{ $job->id }})"><i class="fas fa-trash"></i> Delete</button>
									   
									   
									</div>
                                </div>
                            </div>
                        </li>
						

                        <!-- job end --> 


                        @endforeach

                        @endif

                    </ul>
                    @if(Auth::user())
                    <div class="pagiWrap">



                        <div class="row">



                            <div class="col-lg-5">



                                <div class="showreslt">



                                    {{__('Displaying Results')}} : {{ $jobs->firstItem() }} - {{ $jobs->lastItem() }} {{__('Total')}} {{ $jobs->total() }}



                                </div>



                            </div>





                            <div class="col-lg-7">



                                @if(isset($jobs) && count($jobs))



                                {{ $jobs->appends(request()->query())->links() }}

                             

                                @endif



                            </div>



                        </div>



                    </div>
                    @endif

                </div>

            </div>
            <div class="col-lg-1"></div>

        </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('scripts')

@include('includes.immediate_available_btn')
<script src="{{asset('/')}}js/jquery.cookie.js"></script>
<script>


        //This is not production quality, its just demo code.
      var cookieList = function(cookieName) {
      //When the cookie is saved the items will be a comma seperated string
      //So we will split the cookie by comma to get the original array
      var cookie = $.cookie(cookieName);
      //Load the items or a new array if null.
      var items = cookie ? cookie.split(/,/) : new Array();

      //Return a object that we can use to access the array.
      //while hiding direct access to the declared items array
      //this is called closures see http://www.jibbering.com/faq/faq_notes/closures.html
      return {
          "add": function(val) {
              //Add to the items.
              items.push(val);
              //Save the items to a cookie.
              //EDIT: Modified from linked answer by Nick see 
              //      http://stackoverflow.com/questions/3387251/how-to-store-array-in-jquery-cookie
              $.cookie(cookieName, items.join(','));
          },
          "remove": function (val) { 
              //EDIT: Thx to Assef and luke for remove.
              //indx = items.indexOf(val);
              items = jQuery.grep(items, function(value) {
                  return value != val;
                });
              $.cookie(cookieName, items);        },
          "clear": function() {
              items = null;
              //clear the cookie.
              $.cookie(cookieName, null);
          },
          "items": function() {
              //Get all the items.
              return items;
          }
        }
      }


    var list = new cookieList("saved_jobs");  
    //list.clear();

    $('.listbtn').on('click', '.save_job', function() {
      var id = $(this).data('id');
      list.add(id);
      @if(Auth::user())
      $.ajax({
        type: "GET",
        url: "{{route('add.to.favourite')}}",
        data: {id : id},
      });
      @endif
      $( "#job_" + id ).removeClass('save_job');
        $( "#job_" + id ).addClass('remove_job');
        $( "#job_" + id ).attr('title','Remove from Saved Job');
        $( "#job_" + id+' i' ).removeAttr('class');
        $( "#job_" + id ).html('<i class="fas fa-star"></i> Saved'); 
      })


     $( function() {
        var array = list.items();
        jQuery.each( array, function( i, val ) {
        $( "#job_" + val ).removeClass('save_job');
        $( "#job_" + val ).addClass('remove_job');
        $( "#job_" + val ).attr('title','Remove from Saved Job');
        $( "#job_" + val ).html('<i class="fas fa-star"></i> Saved');
        }); 
     })
     $('.listbtn').on('click', '.remove_job', function() {
      var id = $(this).data('id');
      list.remove(id);
      @if(Auth::user())
      $.ajax({
        type: "GET",
        url: "{{route('remove.from.favourite')}}",
        data: {id : id},
      });
      @endif
      var count = list.items();
      
      if(count.length>0){
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span>');
        $('.no-savedjobs').hide();
      }else{
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="far fa-star"></i></a>');

          $('.no-savedjobs').show();




      }
        $( "#li_" + id ).remove();

        location.reload();
        
      });

      function deleteFromFav(id) {
        list.remove(id);
        $.cookie("saved_job", list);

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this saved job!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              type: "GET",
              url: "{{ route('remove.from.favourite') }}",
              data: {
                id
              },
              success: function(data) {
                  location.reload();
              }
            });
          }
        });
      }
      
     function remove_all_jobs(){
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover saved job!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          list.clear();
          $.cookie("saved_job", "", { expires: -1 });
          $.removeCookie('saved_jobs', { path: '/' });
          @if(Auth::user())
            $.ajax({
              type: "GET",
              url: "{{route('remove.all.from.favourite')}}",
              success: function(data) {
                location.reload();
            }
          });
          @else
            location.reload();
          @endif
        }
      });

      
     }



</script>
<style type="text/css">
    .remove_job svg{
        color: "#ffeb3b";
    }
</style>

@endpush