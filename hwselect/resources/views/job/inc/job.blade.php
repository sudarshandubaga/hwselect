
<?php 
$lang = config('default_lang');
if (isset($jobSkill))
    $lang = $jobSkill->lang;

 ?>

<div class="mandatoryt">All fields marked with <strong>*</strong> are mandatory</div>

<h5>{{__('Job Details')}}</h5>

@if(isset($job))

{!! Form::model($job, array('method' => 'put', 'route' => array('update.front.job', $job->id), 'class' => 'form')) !!}

{!! Form::hidden('id', $job->id) !!}

@else

{!! Form::open(array('method' => 'post', 'route' => array('store.front.job'), 'class' => 'form')) !!}

@endif

<div class="row">  

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'title') !!}">
			<label for="">Job Title <strong>*</strong></label>
			{!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'maxlength'=>'75', 'placeholder'=>__('Job title'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'title') !!} </div>

    </div>

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'description') !!}">
			<label for="">Description <strong>*</strong></label>
			{!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'maxlength'=>'6000', 'minlength'=>'6000',   'placeholder'=>__('Job description'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'description') !!} </div>

    </div>

	


	

	

    <div class="col-md-12">
        
            <div class="row">
                <div class="col-md-10">
                   <div class="formrow {!! APFrmErrHelp::hasError($errors, 'company_id') !!}" id="company_id_div">
					<label for="">To select skillset from dropdown, enter the first few characters, or add skillsets required <strong>*</strong></label>
					
                    <?php
                        $skills = old('skills', $jobSkillIds);
                    ?>
                    <span id="skills_dd">                 
                        {!! Form::select('skills[]', $jobSkills, $skills, array('class'=>'form-control select2-multiple', 'id'=>'skills', 'multiple'=>'multiple', 'data-maximum-selection-length' => 10)) !!}
                    </span> 
                    @if($errors->has('skills.*'))
                        <span id="skills-error" class="help-block help-block-error">{{ $errors->first('skills.*') }} </span>
                        @endif  
                    </div>
					</div>
                <div class="col-md-2"><a style="font-size: 10px;margin-top: 20px;" href="javascript:;" id="btn-add-skill" class="btn btn-primary">Add New Skillset</a></div>
            </div>
                                            
        </div><!-- 
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'skills') !!}">
			<label for="">Skill Required</label>
            <?php

            $skills// = old('skills', $jobSkillIds);

            ?>

            {!! Form::select('skills[]', $jobSkills, $skills, array('class'=>'form-control select2-multiple', 'id'=>'skills', 'multiple'=>'multiple')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'skills') !!} </div> -->

   

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'country_id') !!}" id="country_id_div">
			<label for="">Country <strong>*</strong></label>
         <?php $arra = array(
            230=>'United Kingdom',
            231=>'United States of America',
        ); ?>  {!! Form::select('country_id', $arra+$countries, old('country_id', (isset($job))? $job->country_id:$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'state_id') !!}" id="state_id_div">
			<label for="">County / State / Province / District <strong>*</strong></label>
			<span id="default_state_dd"> {!! Form::select('state_id', ['' => __('Select County / State / Province / District')], null, array('class'=>'form-control', 'id'=>'state_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'city_id') !!}" id="city_id_div">
			<label for="">Town / City <strong>*</strong></label>
			<span id="default_city_dd"> {!! Form::select('city_id', ['' => __('Select Town / City')], null, array('class'=>'form-control', 'id'=>'city_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>

    </div>

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'address') !!}" id="address"> 
			<label for="">Address <strong>*</strong></label>
         {!! Form::text('address', null, array('class'=>'form-control', 'id'=>'current_location', 'placeholder'=>__('Address'))) !!} {!! APFrmErrHelp::showErrors($errors, 'address') !!} </div>

    </div>

    {!! Form::hidden('latitude', null, array('class'=>'form-control', 'id'=>'latitude', 'placeholder'=>__('Latitude'))) !!} {!! APFrmErrHelp::showErrors($errors, 'latitude') !!}


    {!! Form::hidden('longitude', null, array('class'=>'form-control', 'id'=>'longitude', 'placeholder'=>__('Latitude'))) !!} {!! APFrmErrHelp::showErrors($errors, 'latitude') !!}
    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_period_id') !!}" id="salary_period_id_div">
            <label for="">Salary Period <strong>*</strong></label>
            {!! Form::select('salary_period_id', ['' => __('Select Salary Period')]+$salaryPeriods, null, array('class'=>'form-control', 'id'=>'salary_period_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'salary_period_id') !!} </div>

    </div>

	
    <div class="col-md-12">
    <?php
     $select_opt = array(
        'single_salary' => 'Enter Salary Amount',
        'salary_in_range' => 'Salary in Range',
        'negotiable' => 'If Unspecified Salary, enter information below',
     );

     ?>

    <div id="salary_type_div" class="formrow {!! APFrmErrHelp::hasError($errors, 'site_user') !!}">
                {!! Form::label('salary_type', 'Salary Type', ['class' => 'bold']) !!}
                {!! Form::select('salary_type', [''=>'Select Salary Type']+$select_opt, null, array('class'=>'form-control', 'id'=>'salary_type')) !!}

    </div>

    <?php if(old('salary_type')=='negotiable'){
        $field_type = 'text';
    }else if(isset($job) && $job->salary_type=='negotiable'){
        $field_type = 'text';
    }else{
        $field_type = 'number';
    } ?>
</div>
    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_from') !!}" id="salary_from_div">
			<label for="">Salary From <span></span></label>
	

			{!! Form::$field_type('salary_from', null, array('class'=>'form-control', 'id'=>'salary_from','maxlength'=>8, 'placeholder'=>__('Salary from'), 'oninput'=>"javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength)")) !!}
			
			
			
            {!! APFrmErrHelp::showErrors($errors, 'salary_from') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_to') !!}" id="salary_to_div">
			<label for="">Salary To</label>
            {!! Form::number('salary_to', null, array('class'=>'form-control', 'id'=>'salary_to', 'onKeyPress'=>'if(this.value.length==8) return false', 'maxlength'=>'8', 'placeholder'=>__('Enter Salary To'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'salary_to') !!} </div>

    </div>

    
    <div class="col-md-12">
        <?php 
            $default_currecny = App\CountryDetail::where('code','like','%'.$siteSetting->default_currency_code.'%')->first();
            $d_cur = $default_currecny->symbol;
        ?>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}" id="salary_currency_div">
            <label for="">Salary Currency <strong>*</strong></label>
            @php

            $salary_currency = Request::get('salary_currency', (isset($job))? $job->salary_currency:$d_cur);

            @endphp



            {!! Form::select('salary_currency', ['' => __('Select Salary Currency')]+$currencies, $salary_currency, array('class'=>'form-control', 'id'=>'salary_currency')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!} </div>

    </div>
   

    <div class="col-md-4" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'hide_salary') !!}"> {!! Form::label('hide_salary', __('Hide Salary?'), ['class' => 'bold']) !!}

            <div class="radio-list">

                <?php

                $hide_salary_1 = '';

                $hide_salary_2 = 'checked="checked"';

                if (old('hide_salary', ((isset($job)) ? $job->hide_salary : 0)) == 1) {

                    $hide_salary_1 = 'checked="checked"';

                    $hide_salary_2 = '';

                }

                ?>

                <label class="radio-inline">

                    <input id="hide_salary_yes" name="hide_salary" type="radio" value="1" {{$hide_salary_1}}>

                    {{__('Yes')}} </label>

                <label class="radio-inline">

                    <input id="hide_salary_no" name="hide_salary" type="radio" value="0" {{$hide_salary_2}}>

                    {{__('No')}} </label>

            </div>

            {!! APFrmErrHelp::showErrors($errors, 'hide_salary') !!} </div>

    </div>



    <div class="col-md-12">

    	<div class="formrow {!! APFrmErrHelp::hasError($errors, 'bonus') !!}" id="company_id_div">
    	<div class="row">
    		<div class="col-md-10">
    			{!! Form::label('bonus', 'Bonus') !!}  
    			<?php
		        	$bonuss = old('bonus', $bonusIds);
		        ?>
    			<span id="bonus_dd">                 
			        {!! Form::select('bonus[]', $bonus, $bonuss, array('class'=>'form-control select2-multiple-bonus', 'id'=>'bonus', 'multiple'=>'multiple')) !!}
			    </span> 
		        {!! APFrmErrHelp::showErrors($errors, 'bonus') !!}   
		    </div>
    		<div class="col-md-2"><a style="margin-top: 9px; font-size: 12px" href="javascript:;" id="btn-add-bonus" class="btn btn-primary">Add</a></div>
    	</div>
                                            
    </div>

    </div>

    <div class="col-md-12">

    	<div class="formrow {!! APFrmErrHelp::hasError($errors, 'benifits') !!}" id="company_id_div">
    	<div class="row">
    		<div class="col-md-10">
    			{!! Form::label('benifits', 'Benefits') !!}  
    			<?php
		        	$benifitss = old('benifits', $benifitsIds);
		        ?>
    			<span id="benifits_dd">                 
			        {!! Form::select('benifits[]', $benifits, $benifitss, array('class'=>'form-control select2-multiple-benifits', 'id'=>'benifits', 'multiple'=>'multiple')) !!}
			    </span> 
		        {!! APFrmErrHelp::showErrors($errors, 'benifits') !!}   
		    </div>
    		<div class="col-md-2"><a style="margin-top: 9px; font-size: 12px" href="javascript:;" id="btn-add-benifits" class="btn btn-primary">Add</a></div>
    	</div>
                                            
    </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'career_level_id') !!}" id="career_level_id_div">
			<label for="">Career Level</label>
			{!! Form::select('career_level_id', ['' => __('Select Career level')]+$careerLevels, null, array('class'=>'form-control', 'id'=>'career_level_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!} </div>

    </div>



    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}" id="functional_area_id_div">
			<label for="">Functional Area</label>
			{!! Form::select('functional_area_id', ['' => __('Select Functional Area')]+$functionalAreas, null, array('class'=>'form-control', 'id'=>'functional_area_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_type_id') !!}" id="job_type_id_div">
			<label for="">Contractual Hours <strong>*</strong></label>
			{!! Form::select('job_type_id', ['' => __('Select Contractual Hours')]+$jobTypes, null, array('class'=>'form-control', 'id'=>'job_type_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'job_type_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_shift_id') !!}" id="job_shift_id_div">
			<label for="">Working Hours <strong>*</strong></label>
			{!! Form::select('job_shift_id', ['' => __('Select Working Hours')]+$jobShifts, null, array('class'=>'form-control', 'id'=>'job_shift_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'job_shift_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'num_of_positions') !!}" id="num_of_positions_div">
			<label for="">Positions Available <strong>*</strong></label>
			{!! Form::select('num_of_positions', ['' => __('Select Positions Available')]+MiscHelper::getNumPositions(), null, array('class'=>'form-control', 'id'=>'num_of_positions')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'num_of_positions') !!} </div>

    </div>

   

    <div class="col-md-6">
        <?php 
            if(isset($job)){
                $expiry_date = old('expiry_date')?old('expiry_date'):date('Y-m-d',strtotime($job->expiry_date));
            }else{
                $expiry_date = old('expiry_date')?old('expiry_date'):null;
            }
            
        $expiry_dates  = array(
                    '1 week' => '1 week',
                    '2 week' => '2 weeks',
                    '1 month' => '1 month',
                    '2 month' => '2 months',
                    '3 month' => '3 months',
                    '6 month' => '6 months',
                );
    ?>
    <div class="formrow {!! APFrmErrHelp::hasError($errors, 'expiry_datee') !!}">
         {!! Form::label('expiry_datee', 'Job expiry date') !!} 
        {!! Form::select('expiry_datee', ['' => __('Job expiry date')]+$expiry_dates, old('expiry_datee', (isset($job))? $job->expiry_datee:$siteSetting->default_months), array('class'=>'form-control', 'id'=>'expiry_datee')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'expiry_datee') !!}
    </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'degree_level_id') !!}" id="degree_level_id_div">
			<label for="">Required Qualifications</label>
			{!! Form::select('degree_level_id', ['' =>__('Select Required Qualifications')]+$degreeLevels, null, array('class'=>'form-control', 'id'=>'degree_level_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'degree_level_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}" id="job_experience_id_div">
			<label for="">Length of Service</label>
			{!! Form::select('job_experience_id', ['' => __('Experience in a similar position')]+$jobExperiences, null, array('class'=>'form-control', 'id'=>'job_experience_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_freelance') !!}" style="display: none;"> {!! Form::label('is_freelance', __('Is Freelance?'), ['class' => 'bold']) !!}

            <div class="radio-list">

                <?php

                $is_freelance_1 = '';

                $is_freelance_2 = 'checked="checked"';

                if (old('is_freelance', ((isset($job)) ? $job->is_freelance : 0)) == 1) {

                    $is_freelance_1 = 'checked="checked"';

                    $is_freelance_2 = '';

                }

                ?>

                <label class="radio-inline">

                    <input id="is_freelance_yes" name="is_freelance" type="radio" value="1" {{$is_freelance_1}}>

                    {{__('Yes')}} </label>

                <label class="radio-inline">

                    <input id="is_freelance_no" name="is_freelance" type="radio" value="0" {{$is_freelance_2}}>

                    {{__('No')}} </label>

            </div>

            {!! APFrmErrHelp::showErrors($errors, 'is_freelance') !!} </div>

    </div>

    <div class="col-md-12">

        <div class="formrow">

            <button type="submit" class="btn">{{__('Update Job')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>

        </div>

    </div>

</div>

<input type="file" name="image" id="image" style="display:none;" accept="image/*"/>

{!! Form::close() !!}

<hr>

<div class="modal fade" id="add-skill-model" tabindex="-1" role="dialog" aria-labelledby="skillModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form class="skill_form">
          @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="skillModalLabel">Add Skill</h5>
        <button style="margin-top: -16px !important; " type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
            {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}                    
            {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang, array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'lang') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_skill') !!}">
            {!! Form::label('job_skill', 'Enter Job Skill', ['class' => 'bold']) !!}                    
            {!! Form::text('job_skill', null, array('class'=>'form-control job_skill', 'id'=>'job_skill', 'placeholder'=>'Enter Job Skill')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'job_skill') !!}                                       
        </div>
          <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}" style="display: none;">
            {!! Form::label('is_default', 'Is Default?', ['class' => 'bold']) !!}
            <div class="radio-list">
                <?php
                $is_default_1 = 'checked="checked"';
                $is_default_2 = '';
                if (old('is_default', ((isset($jobSkill)) ? $jobSkill->is_default : 1)) == 0) {
                    $is_default_1 = '';
                    $is_default_2 = 'checked="checked"';
                }
                ?>
                <label class="radio-inline">
                    <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}} onchange="showHideJobSkillId();">
                    Yes </label>
                <label class="radio-inline">
                    <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}} onchange="showHideJobSkillId();">
                    No </label>
            </div>
            {!! APFrmErrHelp::showErrors($errors, 'is_default') !!}
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}" style="display: none;">
        {!! Form::label('is_active', 'Active', ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($jobSkill)) ? $jobSkill->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                Active </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                In-Active </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </form>
    </div>
  </div>
</div>

<div class="modal fade" id="add-bonus-model" tabindex="-1" role="dialog" aria-labelledby="bonusModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form class="bonus_form">
          @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="skillModalLabel">Add Bonus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
	        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}                    
	        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang, array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
	        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}                                       
	    </div>
	    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'bonus') !!}">
	        {!! Form::label('bonus', 'Job Bonus', ['class' => 'bold']) !!}                    
	        {!! Form::text('bonus', null, array('class'=>'form-control bonus', 'placeholder'=>'Job Bonus')) !!}
	        {!! APFrmErrHelp::showErrors($errors, 'bonus') !!}                                       
	    </div>
          
	    
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </form>
    </div>
  </div>
</div>


<div class="modal fade" id="add-benifits-model" tabindex="-1" role="dialog" aria-labelledby="benifitsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form class="benifits_form">
          @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="benifitsModalLabel">Add benifits</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
	        {!! Form::label('lang', 'Language', ['class' => 'bold']) !!}                    
	        {!! Form::select('lang', ['' => 'Select Language']+$languages, $lang, array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value)')) !!}
	        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}                                       
	    </div>
	    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'bonus') !!}">
	        {!! Form::label('benifits', 'Job benifits', ['class' => 'bold']) !!}                    
	        {!! Form::text('benifits', null, array('class'=>'form-control benifits', 'placeholder'=>'Job benifits')) !!}
	        {!! APFrmErrHelp::showErrors($errors, 'benifits') !!}                                       
	    </div>
          
	    
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </form>
    </div>
  </div>
</div>

@push('styles')

<style type="text/css">

    .datepicker>div {

        display: block;

    }

</style>

@endpush

@push('scripts')

@include('includes.tinyMCEFront')
<script type="text/javascript">
    var base_url = "{{url('/')}}"
</script>
<script src="{{asset('js/index1.js')}}"></script>
<script type="text/javascript">

    function maxlength(element, maxvalue){
        var q = element.value.split(/[\s]+/).length;
        if(q > maxvalue){
            var r = q - maxvalue;
            alert("Sorry, you have input "+q+" words into the "+
            "text area box you just completed. It can return no more than "+
            maxvalue+" words to be processed. Please abbreviate "+
            "your text by at least "+r+" words");
            return false;
        }
    }
	if ($(".form").length > 0) {
            $(".form").validate({
                validateHiddenInputs: true,
                ignore: "",
                onkeyup: false,

                rules: { 
                    title: {
                        required: true,
                    }, 

                    description: {
                        required: true,
                        minlength: 250,
                    },

                    'skills[]': {
                        required: true,
                    },

                    country_id: {
                        required: true,
                    },

                    state_id: {
                        required: true,
                    },

                    city_id: {
                        required: true,
                    },

                    address: {
                        required: true,
                    },
					
					career_level_id: {
                        required: true,
                    },
					
					functional_area_id: {
                        required: true,
                    },

                    salary_period_id: {
                        required: true,
                    },

                    salary_currency: {
                        required: true,
                    },

                    salary_from: {
                        required: true,
                    }, 

                    salary_type: {
                        required: true,
                    }, 

                    salary_to: {
                        required: function(element) {
		                    if ($('#salary_type').val().toLowerCase() == "salary_in_range") {
		                        return true;
		                    } else {
		                        return false;
		                    }
		                }
                    },

                    job_type_id: {
                        required: true,
                    },

                    job_shift_id: {
                        required: true,
                    },

                    num_of_positions: {
                        required: true,
                    },
                },
                messages: {

                    title: {
                        required: "Please enter a job title for this role",
                    },
                    description: {
                        required: "Job Description Minimum Length Required 500 characters",
                        minlength: "Please enter at least 250 characters",
                    }, 

                    'skills[]': {
                        required: "Please Select Skills",
                    },

                    country_id: {
                        required: "Country is required",
                    },

                    state_id: {
                        required: "Please Select County/State",
                    }, 

                    city_id: {
                        required: "Please Select Town/City",
                    }, 

                    address: {
                        required: "Please enter Address",
                    },
					
					career_level_id: {
                        required: "Please Select Career Level",
                    },
					
					functional_area_id: {
                        required: "Please Select Functional Area",
                    },

                    salary_currency: {
                        required: "Please Select Salary Currency",
                    },

                    salary_type: {
                        required: "Please Select Salary type",
                    },

                    salary_from: {
                    	required: function(element) {
		                    if ($('#salary_type').val().toLowerCase() == "salary_in_range") {
		                        return 'Please Enter Salary From';
		                    } else if($('#salary_type').val().toLowerCase() == "single_salary") {
		                        return 'Please Enter Salary Amount';
		                    }else{
		                    	return 'Please Enter Salary Text Information';
		                    }
		                },
                    },

                    salary_to: {
                        required: "Please Enter Salary To",
                    },

                    salary_period_id: {
                        required: "Please Select Salary Period",
                    }, 

                    job_shift_id: {
                        required: "Please select Working Hours",
                    },

                    job_type_id: {
                        required: "Please select Contractual Hours",
                    },

                    num_of_positions: {
                        required: "Please select Positions Available",
                    },  
                },
            
            })
        }



    $('#btn-add-skill').on('click',function(){
        $('#add-skill-model').modal('show');
    })

     if ($(".skill_form").length > 0) {
            $(".skill_form").validate({
                validateHiddenInputs: true,
                ignore: "",
                onkeyup: false,

                rules: {
                    lang: {
                        required: true,
                    }, 
                   
                    job_skill: {
                        required: true,
                        remote: {
                            type: 'get',
                            url: "{{url('check-jobskill')}}",
                            data: {
                              job_skill: function() {
                                return $( ".job_skill" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                    },
                },
                messages: {

                    lang: {
                        required: "Language is required",
                    },
                    job_skill: {
                        required: "Job Skill is required",
                        remote: "Job Skill is already exist."
                    },  
                },
                 submitHandler: function(form) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var skills = $('#skills').val();
                    $.ajax({
                        url: "{{route('skill.front.job.add')}}",
                        type: "POST",
                        data: $('.skill_form').serialize()+'&skills='+skills,
                        success: function(res) {
                            $('#add-skill-model').modal('hide');
                             $('#skills_dd').html(res);
                             $('.select2-multiple').select2({
                                placeholder: "Select Required Skills",
                                allowClear: true,
                                language: {
                                    maximumSelected: function (e) {
                                        var t = "You can only select a maximum of " + e.maximum + " skill-sets";
                                        return t;
                                    }
                                }
                            });

                             $('.select2-multiple-bonus').select2({
                                placeholder: "Select Bonus",
                                allowClear: true
                            });

                             $('.select2-multiple-benifits').select2({
                                placeholder: "Select Benefits",
                                allowClear: true
                            });
                             swal({

                                title: "Success",

                                text: 'Job Skill ('+$( ".job_skill" ).val()+') Added Successfully',

                                icon: "success",

                                button: "OK",

                            });

                             $('#skill_form').reset();
                             $('#add-skill-model').modal('hide');
                           
                        }
                    });
                }
            
            })
        }
</script>
<script type="text/javascript">
	change_period();
    $(document).ready(function () {

        $('.select2-multiple').select2({

            placeholder: "{{__('Select Required Skills')}}",

            allowClear: true,
            language: {
                maximumSelected: function (e) {
                    var t = "You can only select a maximum of " + e.maximum + " skill-sets";
                    return t;
                }
            }

        });

        $('.select2-multiple-bonus').select2({

            placeholder: "{{__('Select Bonus')}}",

            allowClear: true

        });

        $('.select2-multiple-benifits').select2({

            placeholder: "{{__('Select Benefits')}}",

            allowClear: true

        });

        $(".datepicker").datepicker({

            autoclose: true,

            format: 'yyyy-m-d'

        });

        $('#country_id').on('change', function (e) {

            e.preventDefault();

            filterLangStates(0);

        });

        $(document).on('change', '#state_id', function (e) {

            e.preventDefault();

            filterLangCities(0);

        });

        filterLangStates(<?php echo old('state_id', (isset($job)) ? $job->state_id : 0); ?>);

    });

    function filterLangStates(state_id)

    {

        var country_id = $('#country_id').val();

        if (country_id != '') {

            $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_state_dd').html(response);

                        filterLangCities(<?php echo old('city_id', (isset($job)) ? $job->city_id : 0); ?>);

                    });

        }

    }

    function filterLangCities(city_id)

    {

        var state_id = $('#state_id').val();

        if (state_id != '') {

            $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#default_city_dd').html(response);

                    });

        }

    }

       @if(isset($job))
        <?php $val = old('salary_type')?old('salary_type'):$job->salary_type; ?>
        
        change_period("{{$val}}");
        @else
        <?php $val = old('salary_type')?old('salary_type'):''; ?>
        change_period("{{$val}}");
        @endif

        $('#salary_type').on('change',function(){
            var val = $(this).val();
            
                change_period(val);
            
        })

        $('#salary_from, #salary_to').on('blur', function () {
            var from = $('#salary_from').val(),
                to   = $('#salary_to').val();

            from = parseFloat(from);
            to = parseFloat(to);
            
            if(from != '' && to != '' && from >= to) {
                // $('#salary_to').val(from + 1);
                swal('(Salary To) must be larger than (Salary From) Field')
                    .then(function() {
                        $('#salary_to').focus();
                    });
            }
        });

       

        function change_period(val){
            //alert(val);
            if(val == 'single_salary'){
                $('#salary_from_div').show();
                $('#salary_to_div').hide();
                $('#salary_from_div').parent().removeClass('col-md-6');
                $('#salary_from_div').parent().addClass('col-md-12');
                $('#salary_from_div label').html('Salary Amount');
                $('#salary_from_div span').html('Enter numbers only (Example: £25,000 entered as 25000)');
                $('#salary_from').attr('type','number');
				$('#salary_from').attr('maxlength','8');
                $('#salary_from').attr('placeholder','Enter Salary Amount');

            }else if(val == 'salary_in_range'){
                $('#salary_from_div').show();
                $('#salary_to_div').show();
                $('#salary_from_div').parent().removeClass('col-md-12');
                $('#salary_from_div').parent().addClass('col-md-6');
                $('#salary_from_div label').html('Salary From');
                $('#salary_from_div span').html('Enter numbers only (Example: £25,000 entered as 25000)');
                $('#salary_from').attr('type','number');
				$('#salary_from').attr('maxlength','8');
                $('#salary_from').attr('placeholder','Enter Salary From');
            }else if(val == 'negotiable'){
                $('#salary_from_div').show();
                $('#salary_to_div').hide();
                $('#salary_from_div').parent().removeClass('col-md-6');
                $('#salary_from_div').parent().addClass('col-md-12');
                $('#salary_from_div label').html('Enter Salary Text Information - Example: Negotiable, Unspecified, To Be Confirmed');
                $('#salary_from_div span').html('');
                $('#salary_from').attr('type','text');
                $('#salary_from').attr('placeholder','Enter Salary Text Information');
				$('#salary_from').attr('maxlength','35');
            }else if(val == ''){
                $('#salary_from_div').hide();
                $('#salary_to_div').hide();
            }   

         /* if(val2!=0)
            {
             $('#salary_to_div').show();
             $('#salary_from_div label').html('Salary From');
             $('#salary_in_range_div').show();
            } else if(val==6 || val==4 || val==8)
            {
             $('#salary_in_range_div').show();
             $('#salary_to_div').hide();
             $('#salary_from_div label').html('Salary Rate');
            }
            else if(val==7){
                $('#salary_to_div').hide();
                $('#salary_from_div').hide();
                $('#salary_in_range_div').hide();
            }
            else{
             $('#salary_to_div').show();
             $('#salary_from_div label').html('Salary From');
             $('#salary_in_range_div').hide();
            }*/
        }

     $('input[type="checkbox"]').each(function() {

       this.value =  1;

     });

    $('input[type="checkbox"]').change(function(){

        this.value = this.checked ? 1 : 0;

    });

    $('#btn-add-bonus').on('click',function(){
    	$('#add-bonus-model').modal('show');
    })

    $('#btn-add-benifits').on('click',function(){
    	$('#add-benifits-model').modal('show');
    })

    if ($(".bonus_form").length > 0) {
            $(".bonus_form").validate({
                validateHiddenInputs: true,
                ignore: "",
                onkeyup: false,

                rules: {
                    lang: {
                        required: true,
                    }, 
                   
                    bonus: {
                        required: true,
                        remote: {
                            type: 'get',
                            url: '{{url("/check-bonus")}}',
                            data: {
                              bonus: function() {
                                return $( ".bonus" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                    },
                },
                messages: {

                    lang: {
                        required: "Language is required",
                    },
                    bonus: {
                        required: "Bonus is required",
                        remote: "Bonus is already exist."
                    },  
                },
                 submitHandler: function(form) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var bonus_m = $('#bonus').val();
                    $.ajax({
                        url: "{{route('store-bonus')}}",
                        type: "POST",
                        data: $('.bonus_form').serialize()+'&bonus_m='+bonus_m,
                        success: function(res) {

                        	 $('#bonus_dd').html(res);
                        	 $('.select2-multiple-bonus').select2({
					            placeholder: "Select Bonus",
					            allowClear: true
					        });
                            
                        	 swal({

				                title: "Success",

				                text: 'bonus ('+$( ".bonus" ).val()+') Added Successfully',

				                icon: "success",

				                button: "OK",

				            });

                             $(".bonus_form").trigger("reset");
                        	 $('#add-bonus-model').modal('hide');
                           
                        }
                    });
                }
            
            })
        }

        if ($(".benifits_form").length > 0) {
            $(".benifits_form").validate({
                validateHiddenInputs: true,
                ignore: "",
                onkeyup: false,

                rules: {
                    lang: {
                        required: true,
                    }, 
                   
                    benifits: {
                        required: true,
                        remote: {
                            type: 'get',
                            url: '{{url("/check-benifits")}}',
                            data: {
                              benifits: function() {
                                return $( ".benifits" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                    },
                },
                messages: {

                    lang: {
                        required: "Language is required",
                    },
                    benifits: {
                        required: "Benefit is required",
                        remote: "Benefit is already exist."
                    },  
                },
                 submitHandler: function(form) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var benifits_m = $('#benifits').val();
                    $.ajax({
                        url: "{{route('store-benifits')}}",
                        type: "POST",
                        data: $('.benifits_form').serialize()+'&benifits_m='+benifits_m,
                        success: function(res) {

                        	 $('#benifits_dd').html(res);
                        	 $('.select2-multiple-benifits').select2({
					            placeholder: "Select Benefits",
					            allowClear: true
					        });
                            
                        	 swal({

				                title: "Success",

				                text: 'bonus ('+$( ".benifits" ).val()+') Added Successfully',

				                icon: "success",

				                button: "OK",

				            });

                             $(".benifits_form").trigger("reset");
                        	 $('#add-benifits-model').modal('hide');
                           
                        }
                    });
                }
            
            })
        }

        @if(session()->has('success'))
           swal({
                title: "Success",
                text: '{!! session('success') !!}',
                icon: "success",
                button: "OK",
            });

        @endif
    (function($) {
        $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {},
            size_error: function() {}
        };
        options = $.extend(defaults, options);
        return this.each(function() {
            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                if(this.files[0].size > 1048576){
                    options.size_error();
                    $(this).focus();
                }    
                else if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
})(jQuery);
$(function() {
    $('#job_detail').checkFileType({
        allowedExtensions: ['pdf', 'doc', 'docx'],
        error: function() {
            $('#job_detail').val('');
            alert('File Formats Allowed: .pdf .doc .docx');

        },
        size_error: function(){
            $('#job_detail').val('');
            alert('File must be less then 1MB');
        }
    });
});

 
$('#salary_from').keypress(function(event) {
    if(event.which == 46 && $(this).attr('type')=='number') {
        event.preventDefault();
        return false;
    } // prevent if already dot
})  

$('#salary_to').keypress(function(event) {
    if(event.which == 46) {
        event.preventDefault();
        return false;
    } // prevent if already dot
})  
</script>

<style>
    .tag-ctn {
        position: relative;
        padding: 0;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 20px;
        color: #555555;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        background-color: #ffffff;
        border: 1px solid #cccccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
        -moz-transition: border linear 0.2s, box-shadow linear 0.2s;
        -o-transition: border linear 0.2s, box-shadow linear 0.2s;
        transition: border linear 0.2s, box-shadow linear 0.2s;
        cursor: default;
        display: block;
    }

    .tag-ctn-invalid {
        border: 1px solid #CC0000;
    }

    .tag-ctn-readonly {
        cursor: pointer;
    }

    .tag-ctn-disabled {
        cursor: not-allowed;
        background-color: #eeeeee;
    }

    .tag-ctn-bootstrap-focus,
    .tag-ctn-bootstrap-focus .tag-res-ctn {
        border-color: rgba(82, 168, 236, 0.8) !important;
        /* IE6-9 */
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(82, 168, 236, 0.6) !important;
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(82, 168, 236, 0.6) !important;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(82, 168, 236, 0.6) !important;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    .tag-ctn input {
        border: 0;
        box-shadow: none;
        -webkit-transition: none;
        outline: none;
        display: block;
        padding: 4px 6px;
        line-height: normal;
        overflow: hidden;
        height: auto;
        border-radius: 0;
        float: left;
        margin: 2px 0 2px 2px;
    }

    .tag-ctn-disabled input {
        cursor: not-allowed;
        background-color: #eeeeee;
    }

    .tag-ctn .tag-input-readonly {
        cursor: pointer;
    }

    .tag-ctn .tag-empty-text {
        color: #DDD;
    }

    .tag-ctn input:focus {
        border: 0;
        box-shadow: none;
        -webkit-transition: none;
        background: #FFF;
    }

    .tag-ctn .tag-trigger {
        float: right;
        width: 27px;
        height: 100%;
        position: absolute;
        right: 0;
        border-left: 1px solid #CCC;
        background: #EEE;
        cursor: pointer;
    }

    .tag-ctn .tag-trigger .tag-trigger-ico {
        display: inline-block;
        width: 0;
        height: 0;
        vertical-align: top;
        border-top: 4px solid gray;
        border-right: 4px solid transparent;
        border-left: 4px solid transparent;
        content: "";
        margin-left: 9px;
        margin-top: 13px;
    }

    .tag-ctn .tag-trigger:hover {
        background: -moz-linear-gradient(100% 100% 90deg, #e3e3e3, #f1f1f1);
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#f1f1f1), to(#e3e3e3));
    }

    .tag-ctn .tag-trigger:hover .tag-trigger-ico {
        background-position: 0 -4px;
    }

    .tag-ctn-disabled .tag-trigger {
        cursor: not-allowed;
        background-color: #eeeeee;
    }

    .tag-ctn-bootstrap-focus {
        border-bottom: 1px solid #CCC;
    }

    .tag-res-ctn {
        position: relative;
        background: #FFF;
        overflow-y: auto;
        z-index: 9999;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        border: 1px solid #CCC;
        left: -1px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
        -moz-transition: border linear 0.2s, box-shadow linear 0.2s;
        -o-transition: border linear 0.2s, box-shadow linear 0.2s;
        transition: border linear 0.2s, box-shadow linear 0.2s;
        border-top: 0;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .tag-res-ctn .tag-res-group {
        line-height: 23px;
        text-align: left;
        padding: 2px 5px;
        font-weight: bold;
        border-bottom: 1px dotted #CCC;
        border-top: 1px solid #CCC;
        background: #f3edff;
        color: #333;
    }

    .tag-res-ctn .tag-res-item {
        line-height: 25px;
        text-align: left;
        padding: 2px 5px;
        color: #666;
        cursor: pointer;
    }

    .tag-res-ctn .tag-res-item-grouped {
        padding-left: 15px;
    }

    .tag-res-ctn .tag-res-odd {
        background: #F3F3F3;
    }

    .tag-res-ctn .tag-res-item-active {
        background-color: #3875D7;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3875D7', endColorstr='#2A62BC', GradientType=0);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, color-stop(20%, #3875D7), color-stop(90%, #2A62BC));
        background-image: -webkit-linear-gradient(top, #3875D7 20%, #2A62BC 90%);
        background-image: -moz-linear-gradient(top, #3875D7 20%, #2A62BC 90%);
        background-image: -o-linear-gradient(top, #3875D7 20%, #2A62BC 90%);
        background-image: linear-gradient(#3875D7 20%, #2A62BC 90%);
        color: #fff;
    }

    .tag-sel-ctn {
        overflow: auto;
        line-height: 22px;
        padding-right: 27px;
    }

    .tag-sel-ctn .tag-sel-item {
        background: #555;
        color: #EEE;
        float: left;
        font-size: 12px;
        padding: 0 5px;
        border-radius: 3px;
        margin-left: 5px;
        margin-top: 4px;
    }

    .tag-sel-ctn .tag-sel-text {
        background: #FFF;
        color: #666;
        padding-right: 0;
        margin-left: 0;
        font-size: 14px;
        font-weight: normal;
    }

    .tag-res-ctn .tag-res-item em {
        font-style: normal;
        background: #565656;
        color: #FFF;
    }

    .tag-sel-ctn .tag-sel-item:hover {
        background: #565656;
    }

    .tag-sel-ctn .tag-sel-text:hover {
        background: #FFF;
    }

    .tag-sel-ctn .tag-sel-item-active {
        border: 1px solid red;
        background: #757575;
    }

    .tag-ctn .tag-sel-ctn .tag-sel-item {
        margin-top: 3px;
    }

    .tag-stacked .tag-sel-item {
        float: inherit;
    }

    .tag-sel-ctn .tag-sel-item .tag-close-btn {
        width: 7px;
        cursor: pointer;
        height: 7px;
        float: right;
        margin: 8px 2px 0 10px;
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAOCAYAAADjXQYbAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAEZ0FNQQAAsY58+1GTAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABSSURBVHjahI7BCQAwCAOTzpThHMHh3Kl9CVos9XckFwQAuPtGuWTWwMwaczKzyHsqg6+5JqMJr28BABHRwmTWQFJjTmYWOU1L4tdck9GE17dnALGAS+kAR/u2AAAAAElFTkSuQmCC);

    }

    .tag-sel-ctn .tag-sel-item .tag-close-btn:hover {
        background-position: 0 -7px;
    }

    .tag-helper {
        color: #AAA;
        font-size: 10px;
        position: absolute;
        top: -17px;
        right: 0;
    }

    .tag-ctn {
        width: 100% !important;
    }
    .button-group a{  
        font-size: 12px !important;
        margin-top: 15px;
        width: 41% !important;
    }
    .btn-danger{
        background-color: #c82333 !important;
        border-color: #bd2130;
    }
    .select2-selection__clear{
        display: none;
    }
</style>

@endpush