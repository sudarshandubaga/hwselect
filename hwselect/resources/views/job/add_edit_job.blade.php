@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end -->

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Job Details')])

<!-- Inner Page Title end -->
@php
    $user = auth()->guard('company')->user();
@endphp

<div class="listpgWraper">

    <div class="container">

        

        <div class="row">

            <div class="col-lg-3 col-md-4">

                @include('includes.company_dashboard_menu')

            </div>



            <div class="col-lg-9 col-md-8">
                @if(!empty($user->ceo) && !empty($user->phone))

                <div class="row">

                    <div class="col-md-12">

                        <div class="userccount">

                            <div class="testitoptxt">
                                {!!get_widget(23)->content!!}
                            </div>

                            <div class="uploadjobinfo2">
                                <p>Don't have time to post a job? Don't worry just upload your specifications and we
                                    will take care of it.</p>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#uploadjobdetail">Upload
                                    Job Details</a>
                            </div>



                            <div class="formpanel jobpostfrm"> @include('flash::message')

                                <!-- Personal Information -->

                                @include('job.inc.job')

                            </div>

                        </div>

                    </div>

                </div>
        @else 
            <div class="userccount searchform">
                <div class="alert alert-danger">Please fill <strong style="font-weight: bold;">Company Contact Name 1</strong> and <strong style="font-weight: bold;">1st Point of Contact Phone Number</strong> to post a job</div>
                <a href="{{ route('company.profile') }}" class="btn">Edit Profile</a>
            </div>
        @endif 

            </div>

        </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('styles')

<style type="text/css">
    .userccount p {
        text-align: left !important;
    }
</style>

@endpush