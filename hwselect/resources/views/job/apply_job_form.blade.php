@extends('layouts.app')

@section('content') 

<!-- Header start --> 

@include('includes.header') 

<!-- Header end --> 

<!-- Inner Page Title start --> 

@include('includes.inner_page_title', ['page_title'=>__('Apply for this Job Vacancy')]) 

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container"> @include('flash::message')

        <div class="row">
			<div class="col-lg-2"></div>
            <div class="col-lg-8">

                <div class="userccount">

                    <div class="formpanel applyjobpage"> {!! Form::open(array('method' => 'post', 'route' => ['post.apply.job', $job_slug])) !!} 

                        <!-- Job Information -->

                        <h5 class="text-center">You are applying for the following Job</h5>
							
                        
                        		
 <?php 

                                    if($job->salary_type == 'single_salary'){

                                        $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
                                    }else if($job->salary_type == 'salary_in_range'){

                                        $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - <span class="symbol">'.$job->salary_currency.'</span> '.number_format($job->salary_to).'</strong>';

                                    }else{
                                        $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
                                    } 


                                    ?> 
									<div class="job-header">
                         <div class="jobinfo text-center">
				<h2><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" target="_blank">{{$job->title}}</a></h2>
				<div class="ptext">{{__('Date Posted')}}: {{$job->created_at->format('d-M-Y')}}</div>
				<div class="salary">{!!$salary!!} @if(!$job->salary_type != 'negotiable') {{$job->getSalaryPeriod('salary_period')}} @endif @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif  </div>
							 
							 
							  <div class="switchbox border-0 p-0 pt-3">

        <div class="txtlbl">{{__('Available for Work Immediately')}} <i class="fa fa-info-circle" title="{{__('Set to YES if you are available for work immediately')}}?"></i>

        </div>

        <div class="">

            <label class="switch switch-green"> @php

                $checked = ((bool)Auth::user()->is_immediate_available)? 'checked="checked"':'';

                @endphp

                <input type="checkbox" name="is_immediate_available" id="is_immediate_available" class="switch-input" {{$checked}} onchange="changeImmediateAvailableStatus({{Auth::user()->id}}, {{Auth::user()->is_immediate_available}});">

                <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span> </label>

        </div>

        <div class="clearfix"></div>
        <div id="immediate_form" <?php if((bool)Auth::user()->is_immediate_available){echo 'style="display:none"';}else{} ?>>
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        	<input type="hidden" name="old_status" value="1">
              <div class="form-group mt-3" style="text-align: left;">
                <label for="date_from" style="margin-bottom: 5px;">Date Available from</label>
                <input type="date" class="form-control" data-date="" data-date-format="DD MMMM YYYY" id="date_from" name="date_from" value="{{null!==(Auth::user()->immediate_date_from)?date('Y-m-d',strtotime(Auth::user()->immediate_date_from)):date('Y-m-d')}}" placeholder="Date From">
              </div>
              <div class="form-group" style="text-align: left; display: none;">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==(Auth::user()->immediate_date_to)?date('Y-m-d',strtotime(Auth::user()->immediate_date_to)):date('Y-m-d')}}" placeholder="Date To">
              </div>							
			</div>							
										
										

    </div>
							 
							 
            </div>
			</div>	


                                   

                          			
						 <div class="jobmainreq">
                    <div class="jobdetail">
						<h3><i class="fas fa-align-left" aria-hidden="true"></i> {{__('Job Details')}}</h3>
							 <ul class="jbdetail">
                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Location')}}:</div>

                                <div class="col-md-8 col-7">

                                    @if((bool)$job->is_freelance)

                                    <span>Freelance</span>

                                    @else

                                    <span>{{$job->getLocation()}}</span>

                                    @endif

                                </div>

                            </li>
                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Contractual Hours')}}:</div>

                                <div class="col-md-8 col-7"><span class="permanent">{{$job->getJobType('job_type')}}</span></div>

                            </li>
                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Working Hours')}}:</div>

                                <div class="col-md-8 col-7"><span class="freelance">{{$job->getJobShift('job_shift')}}</span></div>

                            </li>
						</ul>
						</div>	
</div>
                       

                        <br>
						
						<?php $default = App\ProfileCv::where('user_id',Auth::user()->id)->where('is_default',1)->first(); 

                        		 $default_id = null!==($default)?$default->id:null;
                        		?>
                           

                                <div class="formrow{{ $errors->has('cv_id') ? ' has-error' : '' }}"> {!! Form::select('cv_id', [''=>__('Select CV')]+$myCvs, $default_id, array('class'=>'form-control', 'id'=>'cv_id')) !!}

                                    @if ($errors->has('cv_id')) <span class="help-block"> <strong>{{ $errors->first('cv_id') }}</strong> </span> @endif </div>
						
						

                        <input type="submit" class="btn" value="{{__('Apply For This Job Vacancy')}}">

                        {!! Form::close() !!} </div>

                </div>

            </div>

        </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('scripts') 

<script>


	
	
	$('form').on('submit',function(){
		var cv = $('#cv_id').val();
		if(cv==''){
            swal({
                title: "No CV Selected",
                text: "You must select a CV to apply for this vacancy",
                type: "error"
            }).then(function() {
                $('select').focus();
            });
			return false;
		}
	})
       $('#is_immediate_available').on('change',function(){
        if($('input[name="is_immediate_available"]:checked').val() == 'on'){
            $('#immediate_form').hide();
        }else{
            $('#immediate_form').show();
        }
        
    })

    function changeImmediateAvailableStatus(user_id, old_status) {

        

        $.post("{{ route('update.immediate.available.status') }}", {user_id: user_id, old_status: old_status, _method: 'POST', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (responce == 'ok') {

                        if (old_status == 0)

                            $('#is_immediate_available').attr('checked', 'checked');

                        else

                            $('#is_immediate_available').removeAttr('checked');

                    }

                });



    }
    $(document).ready(function () {

        $('#salary_currency').typeahead({

            source: function (query, process) {

                return $.get("{{ route('typeahead.currency_codes') }}", {query: query}, function (data) {

                    console.log(data);

                    data = $.parseJSON(data);

                    return process(data);

                });

            }

        });



    });
   

</script> 

@endpush