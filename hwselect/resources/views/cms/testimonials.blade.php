@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>'Testimonials'])

<!-- Inner Page Title end -->

<div class="testimonialwraper">

    <div class="container">

		<div class="testitoptxt">
		{!!get_widget(22)->content!!}
		</div>
		
		
               <ul class="testimonialsList row">
            @if(isset($testimonials) && count($testimonials))
            @foreach($testimonials as $testimonial)
            <li class="col-lg-6">   
				<div class="testibox">
                <div class="ratinguser" >
					<?php if($testimonial->rating == 5){?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                <?php }else if($testimonial->rating == 4){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating == 3){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating==2){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating==1){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating==0){ ?>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php } ?>
				</div>
                <div class="clientname">{{$testimonial->testimonial_by}}</div>
				 <div class="clientinfo">{{$testimonial->company}}</div>
                <p>"{{$testimonial->testimonial}}"</p>
               </div>
            </li>
            @endforeach
            @endif
        </ul>    

    </div>  

</div>

@include('includes.footer')

@endsection