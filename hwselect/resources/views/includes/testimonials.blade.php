<div class="section testimonialwrap">
    <div class="container"> 
        <!-- title start -->
        <div class="titleTop">
            <h3>{{__('Testimonials')}}</h3>
        </div>
        <!-- title end -->

        <ul class="testimonialsList hmtesti owl-carousel">
            @if(isset($testimonials) && count($testimonials))
            @foreach($testimonials as $testimonial)
            <li class="item">   
				<div class="testibox">
                <div class="ratinguser" >
					<?php if($testimonial->rating == 5){?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                <?php }else if($testimonial->rating == 4){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating == 3){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating==2){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating==1){ ?>
                                    <i class="fa fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php }else if($testimonial->rating==0){ ?>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                <?php } ?>
				</div>
                <div class="clientname">{{$testimonial->testimonial_by}}</div>
				 <div class="clientinfo">{{$testimonial->company}}</div>
                <p>"{!! \Illuminate\Support\Str::limit($testimonial->testimonial, $limit = 125, $end = '...') !!}<a href="{{url('/testimonials')}}">read more</a>"</p>
               </div>
            </li>
            @endforeach
            @endif
        </ul>
    </div>
</div>