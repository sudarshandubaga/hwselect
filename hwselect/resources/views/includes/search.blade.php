@if((bool)$siteSetting->is_slider_active)

<!-- Revolution slider start -->

<div class="tp-banner-container">

    <div class="tp-banner" >
        <ul>

        @if(isset($sliders) && count($sliders))

            @foreach($sliders as $slide)

            <!--Slide-->

            <li data-slotamount="7" data-transition="slotzoom-horizontal" data-masterspeed="1000" data-saveperformance="on"> <img alt="{{$slide->slider_heading}}" src="{{asset('/')}}images/slider/dummy.png" data-lazyload="{{ ImgUploader::print_image_src('/slider_images/'.$slide->slider_image) }}">

                <div class="caption lft large-title tp-resizeme slidertext1" data-x="left" data-y="150" data-speed="600" data-start="1600">{{$slide->slider_heading}}</div>

                <div class="caption lfb large-title tp-resizeme sliderpara" data-x="left" data-y="200" data-speed="600" data-start="2800">{!!$slide->slider_description!!}</div>

                <div class="caption lfb large-title tp-resizeme slidertext5" data-x="left" data-y="280" data-speed="600" data-start="3500"><a href="{{$slide->slider_link}}">{{$slide->slider_link_text}}</a></div>

            </li>

            <!--Slide end--> 

            @endforeach

            @endif

        </ul>

    </div>

</div>

<!-- Revolution slider end --> 

<!--Search Bar start-->

<div class="searchbar searchblack">

    <div class="container">

        @include('includes.search_form')

    </div>

</div>

<!-- Search End --> 

@else

<div class="searchwrap">

    <div class="container">
		<div class="row">
			<div class="col-lg-4">
				
				
				<div class="hiretalentbox">
					<h4>{{get_widget(24)->heading}}</h4>
					<p>{{get_widget(24)->content}}</p>
					
					@if(Auth::guard('company')->user())
					<a href="{{route('post.job')}}" class="reqbtn">Post Job</a>
					@else
					@if(Auth::user())
					<a href="javascript:void(0)" data-toggle="modal" data-target="#reqstaff" class="reqbtn">Request Staff</a>
					
					
						<div class="modal fade" id="reqstaff" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered">
							<div class="modal-content">      
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="fas fa-times"></i>
								</button>

								<div class="invitereval">
									<h3>{{__('You need to Register as a Company')}}</h3>

									<p> {{__('Your are currently logged in as a Jobseeker. To be able to request staff to fill job vacancies you must have a company account setup and verified. You must logout of your Jobseeker account and register a Company account.')}}</p>
									<div class="btn2s">
									<a href="{{route('logout')}}">{{__('Logout')}}</a>
									<a href="{{route('register')}}">{{__('Register')}}</a>
									</div>
								</div>
							</div>

							</div>
						</div>
						</div>
					
					
					@else
					<a href="javascript:void(0)" data-toggle="modal" data-target="#reqstaff" class="reqbtn">{{get_widget(24)->button_1}}</a>
					
					<div class="modal fade" id="reqstaff" tabindex="-1" role="dialog" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-centered">
						<div class="modal-content">      
						  <div class="modal-body">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <i class="fas fa-times"></i>
							</button>

							  <div class="invitereval">
								<h3>{{get_widget(25)->heading}}</h3>
								 
								  <h5>HW Select can help.</h5>
								<p>{!!get_widget(25)->content!!}</p>
								  
								  <div class="btn2s">
								  <a href="{{route('login')}}?type=emp">{{__('Login')}}</a>
								  <a href="{{route('register')}}?type=emp">{{__('Register')}}</a>
								</div>
							  </div>

						  </div>

						</div>
					  </div>
					</div>
					
					
					@endif
					
					
					@endif
					
					<a href="{{url('/learn-more')}}" class="morelink">{{get_widget(24)->button_2}}</a> about our hiring and consulting options
				</div>
				
				
			</div>
			<div class="col-lg-8">
				<div class="tpsrchbox">
				@if(Auth::guard('company')->check())
				@include('flash::message')
				<h3 style="margin-top: 35px;">{{get_widget(10)->heading}}</h3>
				@else
				<h3>{{get_widget(10)->heading}}</h3>
				@endif
				
				
        		@include('includes.search_form')
				</div>
			</div>
			
		</div>
    </div>

</div>

@endif
@push('scripts')
<script type="text/javascript">
	@if(session()->has('success'))
           swal({
                title: "Success",
                text: '{!! session('success') !!}',
                icon: "success",
                button: "OK",
            });

        @endif

    
</script>
@endpush