<div class="col-lg-3">

	<div class="usernavwrap">

    <div class="switchbox">

        <div class="txtlbl">{{__('Available for Work Immediately')}} <i class="fa fa-info-circle" title="{{__('Set to YES if you are available for work immediately')}}?"></i>

        </div>

        <div class="">

            <label class="switch switch-green"> @php

                $checked = ((bool)Auth::user()->is_immediate_available)? 'checked="checked"':'';

                @endphp

                <input type="checkbox" name="is_immediate_available" id="is_immediate_available" class="switch-input" {{$checked}} onchange="changeImmediateAvailableStatus({{Auth::user()->id}}, {{Auth::user()->is_immediate_available}});">

                <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span> </label>
        </div>

        <div class="clearfix"></div>
        <form id="immediate_form" action="{{ route('update.immediate.available.status') }}" method="post" <?php if((bool)Auth::user()->is_immediate_available){echo 'style="display:none"';}else{} ?>>
        	@csrf
        	<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        	<input type="hidden" name="old_status" value="1">
              <div class="form-group mt-3" style="text-align: left;">
                <label for="date_from" style="margin-bottom: 5px;">Date Available from</label>
                <input type="date" class="form-control" data-date="" data-date-format="DD MMMM YYYY" id="date_from" name="date_from" value="{{null!==(Auth::user()->immediate_date_from)?date('Y-m-d',strtotime(Auth::user()->immediate_date_from)):date('Y-m-d')}}" placeholder="Date From">
              </div>
              <div class="form-group" style="text-align: left; display: none;">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==(Auth::user()->immediate_date_to)?date('Y-m-d',strtotime(Auth::user()->immediate_date_to)):date('Y-m-d')}}" placeholder="Date To">
              </div>
              <button type="submit" class="btn btn-success">Save Date</button>
        </form>

    </div>
    

    <ul class="usernavdash">

        <li class="{{ Request::url() == route('home') ? 'active' : '' }}"><a href="{{route('home')}}"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a>
        </li>
		
		<li class=""><a href="{{url('/jobs')}}"><i class="fas fa-search"></i> {{__('Search Jobs')}}</a>
        </li>
		
        <li class="{{ Request::url() == route('my.profile') ? 'active' : '' }}"><a href="{{ route('my.profile') }}"><i class="fas fa-user-edit"></i> {{__('Edit Profile')}}</a>
        </li>


        <li class="{{ Request::url() == route('change-passwords') ? 'active' : '' }}"><a href="{{ route('change-passwords') }}"><i class="fas fa-user-edit"></i> {{__('Change Password')}}</a>
        </li>
		
		<li class="{{ Request::url() == url('my-profile#cvs') ? 'active' : '' }}"><a href="{{url('my-profile#cvs')}}"><i class="fas fa-file-alt"></i> {{__('Manage CV / Resume')}}</a>@if(count(Auth::user()->profileCvs)<=0)<span style="color: red;font-size: 13px;margin-left: 25px;">No CV uploaded</span>@endif
        </li>
		
        <li class="{{ Request::url() == route('view.public.profile', Auth::user()->id) ? 'active' : '' }}"><a href="{{ route('view.public.profile', Auth::user()->id) }}"><i class="fas fa-eye"></i> {{__('View  Profile')}}</a>
        </li>

        <li class="{{ Request::url() == route('my.job.applications') ? 'active' : '' }}"><a href="{{ route('my.job.applications') }}"><i class="fas fa-desktop"></i> {{__('My Job Applications')}}</a>
        </li>

        <li class="{{ Request::url() == route('my.favourite.jobs') ? 'active' : '' }}"><a href="{{ route('my.favourite.jobs') }}"><i class="fas fa-heart"></i> {{__('My Saved Jobs')}}</a>
        </li>

        <li class="{{ Request::url() == route('my-alerts') ? 'active' : '' }}"><a href="{{ route('my-alerts') }}"><i class="fas fa-bullhorn"></i> {{__('My Job Alerts')}}</a>
        </li>
        
		
		<li><a href="javascript:;" onclick="view_message_request()"><i class="fas fa-times"></i> {{__('Request to Close Account')}}</a></li>
		

        <?php /*?><li><a href="{{route('my.messages')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{__('My Messages')}}</a>

        </li>

        <li><a href="{{route('my.followings')}}"><i class="fa fa-user-o" aria-hidden="true"></i> {{__('My Followings')}}</a>

        </li><?php */?>

        <li><a id="logout-with-cookie" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                {{ csrf_field() }}

            </form>

        </li>

    </ul>
	
		
	<hr>
		
    <div class="switchbox log2part">
        <div class="txtlbl"><i class="fas fa-lock"></i> {{__('Security Settings')}}
        </div>
		
		<p>Turn ON or OFF 2 Step Verification for login to make your account more secure.</p>
		
        <div class="">
            <label class="switch switch-green"> @php
                $checked = ((bool)Auth::user()->two_step_verification)? 'checked="checked"':'';
                @endphp
                <input type="checkbox" name="two_step_verification" id="two_step_verification" class="switch-input" {{$checked}} onchange="changeTwoStepVerificationStatus({{Auth::user()->id}}, {{Auth::user()->two_step_verification}});">
                <span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
        </div>
        <div class="clearfix"></div>
    </div>
    @if($siteSetting->is_newsletter_active==1)
	<div class="switchbox log2part">
        <div class="txtlbl"><i class="fas fa-envelope"></i> {{__('Newsletter')}}
        </div>
		
		<p>Here you can Subscribe or Unsubscribe to our newsletter.</p>
		
        <div class="">
            <div class="">
                        <label class="switch switch-green"> @php
                            $checked = ((bool)Auth::user()->is_subscribed)? 'checked="checked"':'';
                            @endphp
                            <input type="checkbox" name="is_subscribed" id="is_subscribed" class="switch-input" {{$checked}} onchange="changeSubscribedStatus({{Auth::user()->id}}, {{Auth::user()->is_subscribed}});">
                            <span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
                    </div>
			
			
			
			
			
        </div>
        <div class="clearfix"></div>
    </div>
    @endif	
		
		
	
		
		</div>

    

		

</div>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title" id="exampleModalLabel">
			@if(Auth::user()->two_step_verification)
			To Disable 2 Part Verification 
			@else
			
			To Enable 2 Part Verification 
			<br>please enter your account password
			@endif
			
		  
		  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>		
      <form method="POST" action="{{ route('update.user.twoStep.verification.status') }}" id="cnfirm_pass">
        @csrf
        <input type="hidden" name="user_id" id="user_id">
        <input type="hidden" name="old_status" id="old_status">
        <input type="hidden" name="otpVerified" id="otpVerified" value="0">
        <div class="modal-body">
			<p class="mb-3">
				@if(Auth::user()->two_step_verification)
				Please enter your account password
				@else

				To enable 2 step verification, please make sure you have a valid contactable mobile number associated with your account profile, before entering your login account password.
				@endif				
			</p>
			
			 <?php 
            if(Auth::user() && Auth::user()->phone !=''){
              $phone = Auth::user()->phone;
            }else{
              $phone = '+';
            }
          ?>
			
            <div class="form-group">
                <label for="">
                  <h5>Your Phone number is: ********{{substr($phone, -4)}}</h5>
            </label>
            <input type="password" class="form-control" maxlength="20" id="password1" name="password" placeholder="Password" required>
          </div>
          <div class="form-group">
            <input type="number" required class="form-control" maxlength="6" id="otp1" name="otp" placeholder="OTP" required>
          </div>
        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-center">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-auth.js"></script>
<script type="text/javascript">
  var config = {
      apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
      authDomain: "hw-select-uk.firebaseapp.com",
      projectId: "hw-select-uk",
      storageBucket: "hw-select-uk.appspot.com",
      messagingSenderId: "269417794687",
      appId: "1:269417794687:web:b8aee097fea11a0895434a"
  };
  firebase.initializeApp(config);
  function sendSms(uphone) {
      window.uphone = $.trim(uphone);
      // console.log('phone number: ', window.uphone);
      if(true) {
          var appVerifier = new firebase.auth.RecaptchaVerifier('two_step_verification', { 'size': 'invisible' });

          // console.log('app verifier: ', appVerifier);
          // $(this).attr("disabled", "disabled");
          firebase.auth().signInWithPhoneNumber(uphone, appVerifier)
              .then(function (confirmationResult) {
                  // console.log('firebase response: ',confirmationResult);
                  window.confirmationResult = confirmationResult;

                  // return confirmationResult;
              //console.log("verification code sent");
              }).catch(function (error) {
                  console.error('Error during signInWithPhoneNumber', error);
              });
      }
  }
        $('#is_immediate_available').on('change',function(){
        if($('input[name="is_immediate_available"]:checked').val() == 'on'){
            $('#immediate_form').hide();
        }else{
            $('#immediate_form').show();
        }
        
    })

    function changeImmediateAvailableStatus(user_id, old_status) {

        

        $.post("{{ route('update.immediate.available.status') }}", {user_id: user_id, old_status: old_status, _method: 'POST', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (responce == 'ok') {

                        if (old_status == 0)

                            $('#is_immediate_available').attr('checked', 'checked');

                        else

                            $('#is_immediate_available').removeAttr('checked');

                    }

                });



    }
      function changeSubscribedStatus(user_id, old_status) {

        

        $.post("{{ route('update.subscribtion.status') }}", {user_id: user_id, old_status: old_status, _method: 'POST', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (responce == 'ok') {

                        if (old_status == 0){
                            $('#is_subscribed').attr('checked', 'checked');

                            swal({

                              title: "Success",

                              text: 'You have Subscribed successfully for our newsletter',

                              icon: "success",

                              button: "OK",

                          });
                        }

                            

                        else{
                            $('#is_subscribed').removeAttr('checked');
                            swal({

                              title: "Success",

                              text: 'You have Unsubscribed successfully for our newsletter',

                              icon: "success",

                              button: "OK",

                          });
                        }

                            

                    }

                });



    }

    $('.submitAction').on('click', function() {
    var val = $('input[name="option"]').val();
    var reason = $('#reason').val();

    if(!reason || (reason === 'other' && !$('#message').val())) {
        // alert();
        swal('Warning!', 'Please enter reason.', 'warning');
        return false;
    }

    var r = confirm("Are you sure you want to close your account?");
    if (r == true && reason) {
        $.ajax({
            url: "{{route('submit-action-request-user')}}",
            type: "POST",
            data: $('#send-form-account-request').serialize(),
            success: function(response) {

                $("#send-form-account-request").trigger("reset");
                $('#showmodalrequest').modal('hide');

                swal({
                    title: "Success",
                    text: response["msg"],
                    icon: "success",
                    button: "OK",
                });

                function show_popup(){
                    location.reload();
                }

                window.setTimeout( show_popup, 2000 );
            }

  });
      } else {
        return false;
      }
        
  
})

    
    function changeTwoStepVerificationStatus(user_id, old_status) {
        $('#old_status').val(old_status);
        $('#user_id').val(user_id);
        $('#form').modal('show');
        
        sendSms('{{ auth()->user()->phone }}');

    }

    $(document).on('submit', '#cnfirm_pass', function (e) {
      var otpVerified = $('#otpVerified').val();

      if(otpVerified == 0) {
        e.preventDefault();
        var code = $.trim($("#otp1").val());
        if ( code.length > 3 ) {
            window.confirmationResult
                .confirm(code)
                .then(result => {
                    // console.log('result OTP: ', result);
                    error = false;
                    $('#otpVerified').val(1);
                    $('#cnfirm_pass').trigger('submit');
                })
                .catch(err => {
                  console.log('OTP Error:', err);
                  alert("One Time Password Entered Incorrectly.");
                });
        } else { alert("A valid SMS verification code must be entered"); }
      }
    });

    $(document).ready(function() {
        if ($("#cnfirm_pass").length > 0) {
            $("#cnfirm_pass").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    password: {
                        required: true,
                    },
                },
                messages: {

                    password: {
                        required: "Please enter account password",
                    }

                },
                
            })
        }
    })
    @if(session()->has('message.added'))
    <?php if(session('message.status')==1){
        $status = 'Enabled';
        $description = 'Once signed out, future logins will require you to enter a 4 digit code sent to your registered mobile number to access your account.';
    }else{
        $status = 'Disabled';
        $description = 'Security settings switched off, you may login without entering a 4 digit code.';
    } ?>
       swal({
            title: "2 Part Verification {{$status}}",
            text: '{!! $description !!}',
            icon: "success",
            button: "OK",
        });

      

    @endif

    @if(session()->has('message.error'))
       swal({
            title: "Error",
            text: '{!! session('message.error') !!}',
            icon: "error",
            button: "OK",
        });

      

    @endif

    $('#form').on('hidden.bs.modal', function () {
        location.reload();
    });
</script>
@endpush