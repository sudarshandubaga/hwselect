@if(!Auth::user() && !Auth::guard('company')->user())
<div class="emploginbox">

	<div class="container">		

		<div class="titleTop">

			<div class="subtitle">{{get_widget(5)->heading}}</div>

           <h3>{{get_widget(6)->heading}}  </h3>

			<h4>{!!get_widget(6)->content!!}</h4>

        </div>

		<p>{!!get_widget(7)->content!!}</p>

		<div class="viewallbtn"><a href="{{route('register')}}">{{__('Post a Job')}}</a></div>

	</div>

</div>
@endif