<?php

use App\Http\Controllers\Controller;
$mainCategories = Controller::JobCount();
?>

<div class="header" id="stickyheader">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-12 col-12"> <a href="{{url('/')}}" class="logo"><img src="{{ asset('/') }}sitesetting_images/thumb/{{ $siteSetting->site_logo }}" alt="{{ $siteSetting->site_name }}" /></a>
                <div class="navbar-header navbar-light">
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav-main" aria-controls="nav-main" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-10 col-md-12 col-12"> 

                <!-- Nav start -->
                <nav class="navbar navbar-expand-lg navbar-light">
					
                    <div class="navbar-collapse collapse" id="nav-main">
                        <ul class="navbar-nav mr-auto">
							
							
							{!!get_menus()!!}					
							
							<li class="nav-item"><a href="{{url('/jobs')}}" class="nav-link">Jobs</a></li>
							<li class="nav-item"><a href="{{ url('/testimonials') }}" class="nav-link">Testimonials</a></li>
							<li class="nav-item"><a href="{{ route('contact.us') }}" class="nav-link">Contact Us</a></li>							
                        </ul>						
						
						<ul class="navbar-nav usernav ml-auto">
							<li class="nav-item phonetop">
								<span><i class="fas fa-phone-alt"></i> <a href="tel:{{ $siteSetting->site_phone_primary }}">{{ $siteSetting->site_phone_primary }}</a></span>
							</li>
							
							@if(!Auth::user() && !Auth::guard('company')->user())
							<li class="nav-item notifications"><a title="Save Job Alerts" href="javascript:void(0)" data-toggle="modal" data-target="#notifyalert" class="nav-link"><i class="far fa-bell"></i></a></li>							
							
							@else
                            <li class="nav-item notifications dropdown"><a title="Save Job Alerts" href="#" id="notification" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-bell"></i></a>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="notification">
									<div class="noentry">
									    <ul class="notification">
                                </ul>
                                </div>
								</div>
							</li>
							@endif

							@if(!Auth::user())
	                            <li class="nav-item addsaved" title="Saved Jobs"><a href="{{ route('my.favourite.jobs') }}" class="nav-link"><i class="far fa-star"></i></a> <span class="badge badge-light"></span>
								</li>
							@else
								<li class="nav-item addsaved" title="Saved Jobs"><a href="{{ route('my.favourite.jobs') }}" class="nav-link"><i class="far fa-star"></i></a> <span class="badge badge-light"></span>
								</li>
							@endif
							
							@if(!Auth::user() && !Auth::guard('company')->user())
                            <li class="nav-item dropdown login"><a href="javascript:void()" class="nav-link dropdown-toggle" id="userlogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Sign In')}}</a>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="userlogin">
									<li class="nav-item"><a href="{{route('loginCompany')}}" class="nav-link">{{__('Company')}}</a></li>
									<li class="nav-item"><a href="{{route('loginJobseeker')}}" class="nav-link">{{__('Jobseeker')}}</a></li>
								</ul>
							
							</li>
							
							
				<!-- <li class="nav-item register"><a href="{{route('registerJobseeker')}}" class="nav-link">{{__('Register as a Jobseeker')}}</a></li>			
				<li class="nav-item subjob"><a href="{{route('registerCompany')}}" class="nav-link">{{__('Recruiting - Submit Job')}}</a></li> -->

				
				<li class="nav-item register"><a href="{{route('register')}}?type=seeker" class="nav-link">{{__('Register as a Jobseeker')}}</a></li>			
				<li class="nav-item subjob"><a href="{{route('register')}}?type=emp" class="nav-link">{{__('Recruiting - Submit Job')}}</a></li> 
							
							
							
                            @endif
							
							@if(Auth::user() || Auth::guard('company')->user())
							
						
							
                            <li class="nav-item usertoplinks dropdown"><a href="#" class="nav-link dropdown-toggle" id="userdata" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user"></i>
								<span>@if(Auth::check()) {{\Illuminate\Support\Str::limit(Auth::user()->first_name, 10, $end='')}} @else {{\Illuminate\Support\Str::limit(Auth::guard('company')->user()->name, 10, $end='')}} @endif</span>
								<i class="fas fa-caret-down"></i>
								</a>
							
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="userdata">									
							@if(Auth::check())
							@if(Auth::user()->two_step_verification==1)
							@if(Auth::user()->phone_verify=='yes')
                            <li class="userdet">
								{{Auth::user()->printUserImage()}}
								<span>{{Auth::user()->getName()}}</span>

								<a href="{{ route('my.profile') }}"><i class="fas fa-user-edit"></i> {{__('Edit Profile')}}</a>
							</li>
									
							
								
							<li class="nav-item"><a href="{{route('home')}}" class="nav-link"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a></li>
								
								
						<li class="nav-item"><a href="{{url('/jobs')}}" class="nav-link"><i class="fas fa-search"></i> {{__('Search Jobs')}}</a>
        </li>

        <li class="nav-item"><a href="{{ route('my.profile') }}" class="nav-link"><i class="fas fa-user-edit"></i> {{__('Edit Profile')}}</a></li>
		
		<li class="nav-item"><a href="{{url('my-profile#cvs')}}" class="nav-link"><i class="fas fa-file-alt"></i> {{__('Manage CV / Resume')}}</a>@if(count(Auth::user()->profileCvs)<=0)<span style="color: red;font-size: 13px;margin-left: 25px;">No CV uploaded</span>@endif</li>
		
        <li class="nav-item"><a href="{{ route('view.public.profile', Auth::user()->id) }}" class="nav-link"><i class="fas fa-eye"></i> {{__('View  Profile')}}</a></li>

        <li class="nav-item"><a href="{{ route('my.job.applications') }}" class="nav-link"><i class="fas fa-desktop"></i> {{__('My Job Applications')}}</a></li>

        <li class="nav-item"><a href="{{ route('my.favourite.jobs') }}" class="nav-link"><i class="fas fa-heart"></i> {{__('My Saved Jobs')}}</a></li>

        <li class="nav-item"><a href="{{ route('my-alerts') }}" class="nav-link"><i class="fas fa-bullhorn"></i> {{__('My Job Alerts')}}</a></li>
		<li class="nav-item"><a href="javascript:;" onclick="view_message_request()" class="nav-link"><i class="fas fa-times"></i> {{__('Request to Close Account')}}</a></li>
		<li class="nav-item"><a id="logout-with-cookie" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a> </li>
		<form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>  
							@else
							<li class="nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a> </li>
							<form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>   
				@endif
				@else

						 <li class="userdet">
								{{Auth::user()->printUserImage()}}
								<span>{{Auth::user()->getName()}}</span>

								<a href="{{ route('my.profile') }}"><i class="fas fa-user-edit"></i> {{__('Edit Profile')}}</a>
							</li>
									
							
								
							<li class="nav-item"><a href="{{route('home')}}" class="nav-link"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a></li>
								
								
						<li class="nav-item"><a  href="{{url('/jobs')}}" class="nav-link"><i class="fas fa-search"></i> {{__('Search Jobs')}}</a>
        </li>

        <li class="nav-item"><a href="{{ route('my.profile') }}" class="nav-link"><i class="fas fa-user-edit"></i> {{__('Edit Profile')}}</a></li>
		
		<li class="nav-item"><a href="{{url('my-profile#cvs')}}" class="nav-link"><i class="fas fa-file-alt"></i> {{__('Manage CV / Resume')}}</a>@if(count(Auth::user()->profileCvs)<=0)<span style="color: red;font-size: 13px;margin-left: 25px;">No CV uploaded</span>@endif</li>
		
        <li class="nav-item"><a href="{{ route('view.public.profile', Auth::user()->id) }}" class="nav-link"><i class="fas fa-eye"></i> {{__('View  Profile')}}</a></li>

        <li class="nav-item"><a href="{{ route('my.job.applications') }}" class="nav-link"><i class="fas fa-desktop"></i> {{__('My Job Applications')}}</a></li>

        <li class="nav-item"><a href="{{ route('my.favourite.jobs') }}" class="nav-link"><i class="fas fa-heart"></i> {{__('My Saved Jobs')}}</a></li>

        <li class="nav-item"><a href="{{ route('my-alerts') }}" class="nav-link"><i class="fas fa-bullhorn"></i> {{__('My Job Alerts')}}</a></li>
        <li class="nav-item"><a href="{{ route('change-passwords') }}" class="nav-link"><i class="fab fa-black-tie"></i> {{__('Change Password')}}</a></li>
		<li class="nav-item"><a href="javascript:;" onclick="view_message_request()" class="nav-link"><i class="fas fa-times"></i> {{__('Request to Close Account')}}</a></li>
		<li class="nav-item"><a id="logout-with-cookie" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a> </li>
							<form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>  		
							                         
                            @endif 
                            @endif 
									
									
									
							@if(Auth::guard('company')->check())
							@if(Auth::guard('company')->user()->two_step_verification==1)
							@if(Auth::guard('company')->user()->phone_verify=='yes')
                            <li class="nav-item"><a href="{{route('company.home')}}" class="nav-link"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a> </li>	
							 <li class="nav-item"><a href="{{ route('company.profile') }}" class="nav-link"><i class="fas fa-edit"></i> {{__('Edit Profile')}}</a></li>
							<li class="nav-item"><a href="{{ route('company.detail', Auth::guard('company')->user()->slug) }}" class="nav-link"><i class="fas fa-user"></i> {{__('Company Profile')}}</a></li>	
							<li class="nav-item"><a href="{{route('post.job')}}" class="nav-link"><i class="fas fa-file-alt"></i> {{__('Post a job')}}</a> </li>							
							
							
							<li class="nav-item"><a href="{{ route('posted.jobs') }}" class="nav-link"><i class="fab fa-black-tie"></i> {{__('Company Jobs')}}</a></li>

							<li class="nav-item"><a href="{{ route('company.change-passwords') }}" class="nav-link"><i class="fab fa-black-tie"></i> {{__('Change Password')}}</a></li>

							<li class="nav-item"><a href="{{ route('rejected.jobs') }}" class="nav-link"><i class="fab fa-black-tie"></i> {{__('Refused Jobs')}}</a></li>

							<li class="nav-item"><a href="javascript:;" onclick="view_message_request()" class="nav-link"><i class="fab fa-black-tie"></i> {{__('Request to Close Account')}}</a></li>


							<li class="nav-item"><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a> </li>
							<form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>        
							@else
							<li class="nav-item"><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a> </li>
							<form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>                   	
							@endif
							@else
							<li class="nav-item"><a href="{{route('company.home')}}" class="nav-link"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a> </li>	
							 <li class="nav-item"><a href="{{ route('company.profile') }}" class="nav-link"><i class="fas fa-edit"></i> {{__('Edit Profile')}}</a></li>
							<li class="nav-item"><a href="{{ route('company.detail', Auth::guard('company')->user()->slug) }}" class="nav-link"><i class="fas fa-user"></i> {{__('Company Profile')}}</a></li>	
							<li class="nav-item"><a href="{{route('post.job')}}" class="nav-link"><i class="fas fa-file-alt"></i> {{__('Post a job')}}</a> </li>							
							
							
							<li class="nav-item"><a href="{{ route('posted.jobs') }}" class="nav-link"><i class="fab fa-black-tie"></i> {{__('Company Jobs')}}</a></li>
							<li class="nav-item"><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();" class="nav-link"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a> </li>
							<form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>  
							@endif
							             
                            @endif 	
									
                             </ul>							
							
							</li>
							@endif
							
							
							<li class="nav-item dropdown">
								<a href="{{url('/')}}" class="langinfo dropdown-toggle" title="Choose Your Language" id="selectlang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-globe"></i> <span>Lang</span></a>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="selectlang">
									<li class="langlink">
										<div id="google_translate_element"></div>
									</li>
								</ul>
                            </li>
						</ul>
                        <!-- Nav collapes end --> 

                    </div>
                    <div class="clearfix"></div>
                </nav>

                <!-- Nav end --> 

            </div>
        </div>

        <!-- row end --> 

    </div>

    <!-- Header container end --> 

</div>


<div class="modal fade" id="notifyalert" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
	<div class="modal-content">      
	  <div class="modal-body">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <i class="fas fa-times"></i>
		</button>

		  <div class="invitereval">
			<h3>{{__('Job Alert Notifications')}}</h3>

<p>{{__('Currently you do not have any Job alerts Saved, You need to be Registered and Signed into your account to set up Job Alerts.')}}</p>
			  <div class="btn2s">
			  <a href="{{route('login')}}">{{__('Login')}}</a>
			  <a href="{{route('register')}}">{{__('Register')}}</a>
			</div>
		  </div>

	  </div>

	</div>
  </div>
</div>

@push('scripts')
<script type="text/javascript">
	$('#logout-with-cookie').on('click',function(){
		list.clear();
		$.removeCookie('saved_jobs', { path: '/' });
	});

	$(document).ready(function() {
		var test = '{{ $mainCategories }}';
		if(test != 0){
			var link = "{{ route('my.favourite.jobs') }}";
        	$('.addsaved').html('<a style="color:yellow"  href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+test+'</span>');
		}
        
	});
</script>
@endpush



