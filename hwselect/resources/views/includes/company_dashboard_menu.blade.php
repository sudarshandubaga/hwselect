@php
$slug = $two_step_verification = $is_subscribed = $id = "";
if(Auth::guard('company')->check() && Auth::guard('company')->user()->slug) {
  $user = Auth::guard('company')->user();
  $slug = $user->slug;
  $two_step_verification = $user->two_step_verification;
  $is_subscribed = $user->is_subscribed;
  $id = $user->id;
}
@endphp
<div class="recruternav">

  <ul class="dashnav">

    <li class="{{ Request::url() == route('company.home') ? 'active' : '' }}"><a href="{{route('company.home')}}"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a></li>

    <li class="{{ Request::url() == route('company.profile') ? 'active' : '' }}"><a href="{{ route('company.profile') }}"><i class="fas fa-edit"></i> {{__('Edit Profile')}}</a></li>

    <li class="{{ Request::url() == route('company.detail', $slug) ? 'active' : '' }}"><a href="{{ route('company.detail', $slug) }}"><i class="fas fa-user"></i> {{__('Company Profile')}}</a></li>

    <li class="{{ Request::url() == route('company.change-passwords') ? 'active' : '' }}"><a href="{{ route('company.change-passwords') }}"><i class="fas fa-user-edit"></i> {{__('Change Password')}}</a>
        </li>

    <li class="{{ Request::url() == route('post.job') ? 'active' : '' }}"><a href="{{ route('post.job') }}"><i class="fas fa-desktop"></i> {{__('Post New Job')}}</a>  <i class="fas fa-arrow-right rightarrow2"></i></li>

    <li class="{{ Request::url() == route('posted.jobs') ? 'active' : '' }}"><a href="{{ route('posted.jobs') }}"><i class="fab fa-black-tie"></i> {{__('Company Jobs Posted')}}</a></li>

    <li class="{{ Request::url() == route('rejected.jobs') ? 'active' : '' }}"><a href="{{ route('rejected.jobs') }}"><i class="fab fa-black-tie"></i> {{__('Refused Jobs')}}</a></li>

    <li><a href="javascript:;" onclick="view_message_request()"><i class="fas fa-times"></i> {{__('Request to Close Account')}}</a></li>

    <li><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a>

      <form id="logout-form" action="{{ route('company.logout') }}" method="POST" style="display: none;">

        {{ csrf_field() }}

      </form>

    </li>

  </ul>

	<div class="securitybox">
	<div class="switchbox log2part">
		<div class="txtlbl"><i class="fas fa-lock"></i> {{__('Security Settings')}}
        </div>
		<p>Turn ON or OFF 2 Step Verification for login to make your account more secure.</p>
        <div class="">
            <label class="switch switch-green"> @php
                $checked = ((bool)$two_step_verification)? 'checked="checked"':'';
                @endphp
                <input type="checkbox" name="two_step_verification" id="two_step_verification" class="switch-input" {{$checked}} onchange="changeTwoStepVerificationStatus({{$id}}, {{$two_step_verification}});">
                <span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
	@if($siteSetting->is_newsletter_active==1)
	<div class="securitybox">
	<div class="switchbox ">
            <div class="txtlbl"><i class="fas fa-envelope"></i> {{__('Newsletter')}}
            </div>
            
            <p>Here you can Subscribe or Unsubscribe to our newsletter.</p>
            
            <div class="">
                <div class="">
                            <label class="switch switch-green"> @php
                                $checked = ((bool)$is_subscribed)? 'checked="checked"':'';
                                @endphp
                                <input type="checkbox" name="is_subscribed" id="is_subscribed" class="switch-input" {{$checked}} onchange="changeSubscribedStatus({{$id}}, {{$is_subscribed}});">
                                <span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
                        </div>
                
                
                
                
                
            </div>
            <div class="clearfix"></div>
        </div>
	</div>
  @endif
</div>


<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title" id="exampleModalLabel">
			    To {{ $two_step_verification ? 'Disable' : 'Enable'}} 2 Part Verification
        </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <form method="POST" action="{{ route('update.twoStep.verification.status') }}" id="cnfirm_pass">
    @csrf
    <input type="hidden" name="user_id" id="user_id">
    <input type="hidden" name="old_status" id="old_status">
    <input type="hidden" name="otpVerified" id="otpVerified" value="0">
    <div class="modal-body">
          @if(!$two_step_verification)
          <p class="mb-3">
                To enable 2 step verification, please make sure you have a valid contactable mobile number associated with your account profile, before entering your login account password.
              </p>
          @endif

          <?php 
            if(Auth::guard('company')->check() && Auth::guard('company')->user()->phone !=''){
              $phone = Auth::guard('company')->user()->phone;
            }else{
              $phone = '+';
            }
            ?>
			
			
      <div class="form-group">
        <h5>Please enter account password</h5>
        <input type="password" required class="form-control" maxlength="20" id="password1" name="password" placeholder="Password">
        
      </div>
      <div class="form-group">
            <h5>One Time Password (OTP) sent to ********{{substr($phone, -4)}} Mobile</h5>
            <input type="text" required class="form-control" maxlength="6" id="otp1" name="otp" placeholder="OTP">
            
          </div>
        </div>

        <p class="text-center otp-counter-text" ></p>
        <div class="modal-footer border-top-0 d-flex justify-content-center">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-auth.js"></script>
<script type="text/javascript">
  var config = {
      apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
      authDomain: "hw-select-uk.firebaseapp.com",
      projectId: "hw-select-uk",
      storageBucket: "hw-select-uk.appspot.com",
      messagingSenderId: "269417794687",
      appId: "1:269417794687:web:b8aee097fea11a0895434a"
  };
  firebase.initializeApp(config);
  function sendSms(uphone) {
      window.uphone = $.trim(uphone);
      // console.log('phone number: ', window.uphone);
      if(true) {
          var appVerifier = new firebase.auth.RecaptchaVerifier('two_step_verification', { 'size': 'invisible' });

          // console.log('app verifier: ', appVerifier);
          // $(this).attr("disabled", "disabled");
          firebase.auth().signInWithPhoneNumber(uphone, appVerifier)
              .then(function (confirmationResult) {
                  // console.log('firebase response: ',confirmationResult);
                  window.confirmationResult = confirmationResult;

                  var s = 60 * 5;
                    var min, sec;
                    var otpInterval = setInterval(() => {
                        if(s > 1) {
                            s--;
                            min = Math.floor(s / 60);
                            sec = s - min * 60;
                            if(sec < 10) sec = '0' + sec;

                            $('.otp-counter-text').text('OTP Expires in '+min+':'+sec+' minutes');
                        } else {
                            $('.otp-counter-text').text('OTP Expired');
                            clearInterval(otpInterval);
                        }
                    }, 1000);

                  // return confirmationResult;
              //console.log("verification code sent");
              }).catch(function (error) {
                  console.error('Error during signInWithPhoneNumber', error);
              });
      }
  }
     function changeSubscribedStatus(user_id, old_status) {

        

        $.post("{{ route('update.company.subscribtion.status') }}", {user_id: user_id, old_status: old_status, _method: 'POST', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (responce == 'ok') {

                        if (old_status == 0)

                            $('#is_subscribed').attr('checked', 'checked');
                         swal({

                              title: "Success",

                              text: 'You have Subscribed successfully for our newsletter',

                              icon: "success",

                              button: "OK",

                          });

                        }else{

                            $('#is_subscribed').removeAttr('checked');
                         swal({

                              title: "Success",

                              text: 'You have Unsubscribed successfully for our newsletter',

                              icon: "success",

                              button: "OK",

                          });

                    }

                });



    }
    function changeTwoStepVerificationStatus(user_id, old_status) {
        $('#old_status').val(old_status);
        $('#user_id').val(user_id);
        
        sendSms('{{ auth()->guard('company')->user()->phone }}');

        $('#form').modal('show');

    }

    $(document).on('submit', '#cnfirm_pass', function (e) {
      var otpVerified = $('#otpVerified').val();

      if(otpVerified == 0) {
        e.preventDefault();
        var code = $.trim($("#otp1").val());
        if ( code.length > 3 ) {
            window.confirmationResult
                .confirm(code)
                .then(result => {
                    console.log('result OTP: ', result);
                    error = false;
                    $('#otpVerified').val(1);
                    $('#cnfirm_pass').trigger('submit');
                })
                .catch(err => {
                  console.log('OTP Error:', err);
                  alert("One Time Password Entered Incorrectly.");
                });
        } else { alert("A valid SMS verification code must be entered"); }
      }
    });

    $('.submitAction').on('click',function(){
  var val = $('input[name="option"]').val();
     var r = confirm("Are you sure you want to close your account?");
      if (r == true) {
        $.ajax({

      url: "{{route('submit-action-request-company')}}",

      type: "POST",

      data: $('#send-form-account-request').serialize(),

      success: function(response) {

          $("#send-form-account-request").trigger("reset");

          $('#showmodalrequest').modal('hide');

          swal({

              title: "Success",

              text: response["msg"],

              icon: "success",

              button: "OK",

          });

          function show_popup(){
          location.reload();
         }
         window.setTimeout( show_popup, 2000 );

      }

  });
      } else {
        return false;
      }
    
  
})

    $(document).ready(function() {
        if ($("#cnfirm_pass").length > 0) {
            $("#cnfirm_pass").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    password: {
                        required: true,
                    },
                },
                messages: {

                    password: {
                        required: "Please enter account password",
                    }

                },
                
            })
        }
    })

    @if(session()->has('message.added'))
    <?php if(session('message.status')==1){
        $status = 'Enabled';
        $description = 'Once signed out, future logins will require you to enter a 4 digit code sent to your registered mobile number to access your account.';
    }else{
        $status = 'Disabled';
        $description = 'Security settings switched off, you may login without entering a 4 digit code.';
    } ?>
       swal({
            title: "2 Part Verification {{$status}}",
            text: '{!! $description !!}',
            icon: "success",
            button: "OK",
        });

      

    @endif

    @if(session()->has('message.error'))
       swal({
            title: "Error",
            text: '{!! session('message.error') !!}',
            icon: "error",
            button: "OK",
        });

      

    @endif

    $('#form').on('hidden.bs.modal', function () {
        location.reload();
    });
</script>
@endpush

