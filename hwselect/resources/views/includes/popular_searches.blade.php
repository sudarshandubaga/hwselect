<div class="section popularwrap">

    <div class="container">

        <div class="maintext">

        <h3>{{get_widget(8)->heading}}</h3>

        <p>{!!get_widget(8)->content!!}</p>

        </div>

        

        <div class="topsearchwrap">

            <div class="tabswrap">

                <div class="row">

                    <div class="col-md-4"><h3>{{__('Browse Jobs By')}}</h3></div>

                    <div class="col-md-8">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                          <li class="nav-item"><a data-toggle="tab" href="#byfunctional" class="nav-link active" aria-expanded="true">{{__('Jobs by sector')}}</a></li>

                          <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#bycities" aria-expanded="false">{{__('Jobs by location')}}</a></li>

                          <li class="nav-item"><a data-toggle="tab" href="#byindustries" class="nav-link" aria-expanded="false">{{__('Jobs by Skills')}}</a></li>


                          <li class="nav-item"><a data-toggle="tab" href="#byrole" class="nav-link" aria-expanded="false">{{__('Jobs by Role')}}</a></li>

                        </ul>

                    </div>

                </div>

            

            </div>

                

            <div class="tab-content" id="jobsby">               

                <div class="tab-pane fade show active" id="byfunctional" role="tabpanel" aria-labelledby="byfunctional-tab">

                    <div class="srchbx">                

                <!--Categories start-->

               

                <div class="srchint">

                    <ul class="row catelist">

                         @if(isset($topFunctionalAreaIds) && count($topFunctionalAreaIds)) @foreach($topFunctionalAreaIds as $functional_area_id_num_jobs)
                        <?php
                        $functionalArea = App\FunctionalArea::where('functional_area_id', '=', $functional_area_id_num_jobs->functional_area_id)->lang()->active()->first();
                        ?> @if(null !== $functionalArea)

                        <li class="col-md-4 col-sm-6"><a href="{{route('job.category',['jobsector-'.\Str::slug($functionalArea->functional_area),$functionalArea->functional_area_id])}}" title="{{$functionalArea->functional_area}}">{{$functionalArea->functional_area}} </a><span>({{$functional_area_id_num_jobs->num_jobs}})</span>
                        </li>

                        @endif @endforeach @endif

                    </ul>

                    <!--Categories end-->

                </div>

            </div>

                </div>

                

                    <div class="tab-pane fade show" id="bycities" role="tabpanel" aria-labelledby="bycities-tab">

                    <div class="srchbx">

                <!--Cities start-->

                

                 <div class="srchint">
                    <ul class="row catelist">
                            @if(isset($topCityIds) && count($topCityIds)) @foreach($topCityIds as $city_id_num_jobs)
                            <?php
                            $city = App\City::getCityById($city_id_num_jobs->city_id);
                            ?> @if(null !== $city)

                            <li class="col-md-3 col-sm-4 col-xs-6"><a href="{{route('job.category',['joblocation-'.\Str::slug($city->city),$city->id])}}" title="{{$city->city}}">{{$city->city}} </a><span>({{$city_id_num_jobs->num_jobs}})</span>
                            </li>

                            @endif @endforeach @endif
                    </ul>
                    <!--Categories end-->
                </div>

            </div>

                </div>

                

                    <div class="tab-pane fade show" id="byindustries" role="tabpanel" aria-labelledby="byindustries-tab">

                    <div class="srchbx">

                <!--Categories start-->

               

                <div class="srchint">

                    <ul class="row catelist">                   

                      

                        <?php
                        $job_skills_ids = DB::table('manage_job_skills')
                        ->select('job_skill_id', DB::raw('COUNT(manage_job_skills.job_id) AS num_jobs'))->join('jobs','jobs.id','=','manage_job_skills.job_id')->where('jobs.expiry_date', '>' ,Carbon\Carbon::now())
                        ->where('jobs.is_active',1)
                        ->where('jobs.admin_reviewed',1)->groupBy('job_skill_id')
                        ->limit(21)
                        ->get();


                        
                        ?> 

                        @if(null !== $job_skills_ids)

                        @foreach($job_skills_ids as $job_skill_id)

                        <?php 

                        
                            $jobs_count = $job_skill_id->num_jobs;

                            $job_skill = App\JobSkill::where('id',$job_skill_id->job_skill_id)->first();
                            // echo "<pre>";print_r($job_skill->id.'-'.Str::slug($job_skill->job_skill));exit;

                         ?>

                        <li class="col-md-4 col-sm-6"><a href="{{route('job.category',['jobskills-'. \Str::slug($job_skill->job_skill),$job_skill->id])}}" title="{{$job_skill->job_skill}}">{{$job_skill->job_skill}} </a><span>({{$jobs_count}})</span>

                        </li>

                        @endforeach 

                        @endif

                    </ul>

                    <!--Categories end-->

                </div>

            </div>              

                </div>



                <div class="tab-pane fade show" id="byrole" role="tabpanel" aria-labelledby="byrole-tab">

                    <div class="srchbx">

                <!--Categories start-->

               

                <div class="srchint">

                    <ul class="row catelist">                   

                      

                        <?php
                        $job_role_ids = DB::table('jobs')
                        ->select('career_level_id', DB::raw('COUNT(jobs.career_level_id) AS num_jobs'))
                        ->where('expiry_date', '>' ,Carbon\Carbon::now())
                        ->where('is_active',1)
                        ->where('admin_reviewed',1)
                        ->groupBy('career_level_id')
                        ->orderBy('num_jobs', 'DESC')
                        ->limit(21)
                        ->get();
                        
                        ?> 

                        @if(null !== $job_role_ids)

                        @foreach($job_role_ids as $job_role_id)
                        @if($job_role_id->num_jobs>0)
                        <?php 

                            $job_role = App\CareerLevel::where('id',$job_role_id->career_level_id)->first();

                         ?>

                        <li class="col-md-4 col-sm-6"><a href="{{route('job.category',['jobrole-'.\Str::slug($job_role->career_level),$job_role->id])}}" title="{{$job_role->career_level}}">{{$job_role->career_level}} </a><span>({{$job_role_id->num_jobs}})</span>

                        </li>
@endif
                        @endforeach 

                        @endif

                    </ul>

                    <!--Categories end-->

                </div>

            </div>              

                </div>

            </div>

            



            



            



            

        </div>

    </div>

</div>