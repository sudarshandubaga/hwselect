<form action="{{route('job.list')}}" method="get">

	<!-- Page Title start -->

	<div class="pageSearch">

<div class="container">

				<div class="searchform">

					<div class="row">

						<div class="col-lg-5 col-md-5">

							<label for=""> {{__('Search Job Type')}}</label>

							
							
							<div class="clearable-input">
				<input type="text" name="search" id="jbsearch" value="{{Request::get('search', '')}}" class="form-control" placeholder="{{__('e.g. Project Manager')}}" />
				<span data-clear-input><i class="fas fa-times"></i></span>
				</div>
							
							

						</div>

						

						<div class="col-lg-5 col-md-4">
						<div class="location-input">	
						<label for=""> {{__('Where')}}</label>
						<input type="text"  name="address" id="current_location" value="{{Request::get('address', '')}}" class="form-control" placeholder="{{__('Town, City or Postcode')}}" autocomplete="off" />
						</div>	
							
						<div class="radius">
						<label for=""> {{__('Radius')}}</label>
						<?php 
							$sel = (request()->radius==''?'selected':'');
						 ?>
						<select name="radius" class="form-control search-radius search-locationsearchtype" id="Radius">
							<option <?php echo (request()->radius==0?'selected':''); ?> value="0">0 miles</option>
							<option <?php echo (request()->radius==5?'selected':''); ?> value="5">5 miles</option>
							<option <?php echo (request()->radius==10?'selected':''); ?> value="10">10 miles</option>
							<option <?php echo (request()->radius==20?'selected':''); echo $sel; ?>  value="20">20 miles</option>
							<option <?php echo (request()->radius==30?'selected':''); ?> value="30">30 miles</option>
						</select>
					</div>
					<div class="clearfix"></div>

						</div>

						<input type="hidden" name="longitude" id="longitude" value="{{Request::get('longitude', '')}}">
						<input type="hidden" name="latitude" id="latitude" value="{{Request::get('latitude', '')}}">



						<div class="col-lg-2 col-md-3">

							<label for="">&nbsp;</label>

							<button type="submit" class="btn">{{__('Search Jobs')}}</button>

						</div>



					</div>

				</div>

</div>

	</div>

	<!-- Page Title end -->

</form>

