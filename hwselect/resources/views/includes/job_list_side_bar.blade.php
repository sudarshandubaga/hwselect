<div class="col-lg-3 col-sm-6"> 

<div class="mobsfltr">
    <a href="javascript:void();" data-toggle="offcanvas" data-target="#sidebarfilter">Job Search Filter</a> 
</div>
    
    <div class="sidebarmob" id="sidebarfilter">
        <button class="close-toggler" type="button" data-toggle="offcanvas"> <span><i class="fas fa-times-circle" aria-hidden="true"></i></span> </button>
        
    <div class="jobreqbtn"> 

    @if(Auth::guard('company')->check())

    <a href="{{ route('post.job') }}" class="btn"><i class="fas fa-file-alt"></i> {{__('Post Job')}}</a>

    @else
    
    @if(!Auth::user() && !Auth::guard('company')->user())
    <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#uploadresume">Upload CV</a>
    @else
    <a href="{{url('my-profile#cvs')}}" class="btn"><i class="fas fa-upload"></i> {{__('Upload Your Resume')}}</a>
    @endif
    @endif

    </div>

    <!-- Side Bar start -->

    <div class="sidebar">

        <input type="hidden" name="search" value="{{Request::get('search', '')}}"/>

        <!-- Jobs By City -->

        <div class="widget">

            <h4 class="widget-title">{{__('Jobs By Location')}}</h4>
            
            
            
            
            
            <ul class="optionlist view_more_ul">

                @if(isset($cityIdsArray) && count($cityIdsArray))

                @foreach($cityIdsArray as $key=>$city_id)

                @php

                $city = App\City::where('city_id','=',$city_id)->lang()->active()->first();           

                @endphp

                @if(null !== $city)

                @php

                $checked = (in_array($city->city_id, Request::get('city_id', array())))? 'checked="checked"':'';

                @endphp

                <li>

                    <a href="{{route('job.category',['joblocation-'.\Str::slug($city->city),$city->id])}}" title="{{$city->city}}">{{$city->city}}</a> <span>{{App\Job::countNumJobs('city_id', $city->city_id)}}</span> </li>

                @endif

                @endforeach

                @endif

            </ul>

            <span class="text text-primary view_more hide_vm">{{__('View More')}}</span> </div>

        <!-- Jobs By City end--> 





        <!-- Jobs By Career Level -->

        <div class="widget">

            <h4 class="widget-title">{{__('Jobs by Role')}}</h4>

            <ul class="optionlist view_more_ul">

                @if(isset($careerLevelIdsArray) && count($careerLevelIdsArray))

                @foreach($careerLevelIdsArray as $key=>$career_level_id)

                @php

                $careerLevel = App\CareerLevel::where('career_level_id','=',$career_level_id)->lang()->active()->first();

                @endphp

                @if(null !== $careerLevel)

                @php

                $checked = (in_array($careerLevel->career_level_id, Request::get('career_level_id', array())))? 'checked="checked"':'';

                @endphp

                <li>
                    <a href="{{route('job.category',['jobrole-'.\Str::slug($careerLevel->career_level),$careerLevel->id])}}" title="{{$careerLevel->career_level}}">

                    {{$careerLevel->career_level}} </a><span>{{App\Job::countNumJobs('career_level_id', $careerLevel->career_level_id)}}</span> </li>

                @endif

                @endforeach

                @endif

            </ul>

            <span class="text text-primary view_more hide_vm">{{__('View More')}}</span> </div>

        <!-- Jobs By Career Level end --> 






        <!-- Jobs By Gender -->

        

        <!-- Jobs By Gender end --> 










        <!-- Jobs By Skill -->

        <div class="widget">

            <h4 class="widget-title">{{__('Jobs By Skill')}}</h4>

            <ul class="optionlist view_more_ul">

                @if(isset($skillIdsArray) && count($skillIdsArray))

                @foreach($skillIdsArray as $key=>$job_skill_id)

                @php

                $jobSkill = App\JobSkill::where('job_skill_id','=',$job_skill_id)->lang()->active()->first();

                @endphp

                @if(null !== $jobSkill)



                @php

                $checked = (in_array($jobSkill->job_skill_id, Request::get('job_skill_id', array())))? 'checked="checked"':'';

                @endphp

                <li>
                    <a href="{{route('job.category',['jobskills-'.\Str::slug($jobSkill->job_skill),$jobSkill->id])}}" title="{{$jobSkill->job_skill}}">

                    {{$jobSkill->job_skill}}</a> <span>{{App\Job::countNumJobs('job_skill_id', $jobSkill->job_skill_id)}}</span> </li>

                @endif

                @endforeach

                @endif

            </ul>

            <span class="text text-primary view_more hide_vm">{{__('View More')}}</span> </div>

        <!-- Jobs By Industry end --> 





        <div class="widget">

            <h4 class="widget-title">{{__('Jobs By Functional Areas')}}</h4>

            <ul class="optionlist view_more_ul">

                @if(isset($functionalAreaIdsArray) && count($functionalAreaIdsArray))

                @foreach($functionalAreaIdsArray as $key=>$functional_area_id)

                @php

                $functionalArea = App\FunctionalArea::where('functional_area_id','=',$functional_area_id)->lang()->active()->first();

                @endphp

                @if(null !== $functionalArea)

                @php

                $checked = (in_array($functionalArea->functional_area_id, Request::get('functional_area_id', array())))? 'checked="checked"':'';

                @endphp

                <li>

                    <a href="{{route('job.category',['jobsector-'.\Str::slug($functionalArea->functional_area),$functionalArea->functional_area_id])}}" title="{{$functionalArea->functional_area}}">

                    {{$functionalArea->functional_area}}</a> <span>{{App\Job::countNumJobs('functional_area_id', $functionalArea->functional_area_id)}}</span>

                </li>                

                @endif

                @endforeach

                @endif



            </ul>

            <!-- title end --> 

            <span class="text text-primary view_more hide_vm">{{__('View More')}}</span> </div>





  

        <!-- Salary -->

        <div class="widget">

            <h4 class="widget-title">{{__('Salary Range')}}</h4>

            <div class="form-group">

                {!! Form::number('salary_from', Request::get('salary_from', null), array('class'=>'form-control',  'id'=>'salary_from',  'placeholder'=>__('Salary From'))) !!}

            </div>

            <div class="form-group">

                {!! Form::number('salary_to', Request::get('salary_to', null), array('class'=>'form-control', 'id'=>'salary_to', 'placeholder'=>__('Salary To'))) !!}

            </div>

            <div class="form-group">

                {!! Form::select('salary_currency', ['' =>__('Select Salary Currency')]+$currencies, Request::get('salary_currency'), array('class'=>'form-control', 'id'=>'salary_currency')) !!}

            </div>

            <!-- Salary end --> 



            <!-- button -->

            <div class="searchnt">

                <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i> {{__('Search Jobs')}}</button>

            </div>

            <!-- button end--> 

        </div>

        <!-- Side Bar end --> 

    </div>
    </div>
</div>