

{!! Form::model($user, array('method' => 'put', 'route' => array('my.profile'), 'id'=> 'profile-form', 'class' => 'form ', 'files'=>true)) !!}


<?php $country = App\Country::select('countries.flag', 'countries.id')->whereNotIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();
    
    $country2 = App\Country::select('countries.flag', 'countries.id')->whereIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();

 ?>
<h5>{{__('Account Information')}}</h5>

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'email') !!}">

			<label for="">{{__('Email')}}</label>

			{!! Form::text('email', null, array('class'=>'form-control', 'readonly'=>'readonly','id'=>'email', 'placeholder'=>__('Email'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'email') !!} </div>

<p class="mb-0">To change email address, <a href="{{ route('contact.us') }}">Contact Support</a></p>


<hr>


<div class="accounguide">
<h6>Adding personal information allows us to carefully match your profile with suitable Job Vacancies. Please complete as many fields as possible, including adding an up to date CV. All information is kept confidential and will not be shared without your prior permission.</h6>
<p>To Apply for any Job vacancy advertised on this website,  you must have a current CV uploaded to your account profile. <a href="#cvs" onclick="showProfileCvModal();" class=" btn btn-success">Add a CV to your Profile </a></p>
</div>



<h5 class="mb-1">{{__('Personal Information')}}</h5>
<p class="mt-0 mb-2">On adding or updating Profile Image, Click <strong>Save</strong> Button</p>
<div class="row">
<div class="col-md-6">
<div class="formrow ">
<div class="fileinput fileinput-new" data-provides="fileinput">
<div class="fileinput-preview thumbnail" style="max-width: 200px; max-height: 150px;">{{ ImgUploader::print_image("user_images/thumb/$user->image", 200, 150) }} </div>
<div> <span style="width: auto;" class="btn default btn-file"> <span class="fileinput-new"> Add Profile Image </span> <span class="fileinput-exists"> Change </span> <input id="image" name="image" value="" type="file"> </span> <a style="width: auto; @if(!$user->image) display:none @endif;" href="javascript:; " class="btn red " onclick="remvoe_iamge()" data-dismiss="fileinput"> Remove </a> <a style="width: auto; display:none" href="javascript:; " class="btn btn-success save-image"> Save Image </a></div>
<p class="allowed_message" style="margin-top: 4px; color: #888;">Image Formats Allowed: .jpg .jpeg .png .gif</p>
<!-- Modal -->
<div class="modal fade" id="imageHelpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="d-flex align-items-center">
    <p class="m-0">Max Image Size 100 KB</p>
    <a href="#imageHelpModal" data-toggle="modal" class="ml-auto" >
        <i class="fa fa-question-circle-o"></i> Help
    </a>
</div>
</div>
</div>
</div>
</div>
<!-- <div class="row">

    <div class="col-md-6">

        <div class="formrow">

            <div id="thumbnail">{{ ImgUploader::print_image("user_images/thumb/$user->image", 100, 100) }}</div>

            <label class="btn btn-default"> {{__('Select Profile Image')}}

                <input type="file" name="image" id="image" style="display: none;">

            </label>
			
			
			

            {!! APFrmErrHelp::showErrors($errors, 'image') !!} </div>

    </div>
	
	<div class="col-md-6">
		<div class="alertimg">
				Image formats allowed: .jpg .jpeg .gif<br>
				Maximum image size allowed – 1meg
			</div>
	</div>
	
</div> -->

<hr>

<div class="row">

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'first_name') !!}">

			<label for="">{{__('First Name')}}</label>

			{!! Form::text('first_name', null, array('class'=>'form-control', 'maxlength'=>'20', 'id'=>'first_name', 'placeholder'=>__('First Name'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'first_name') !!} </div>

    </div>

    <div class="col-md-6" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'middle_name') !!}">

			<label for="">{{__('Midlle Name')}}</label>

			{!! Form::text('middle_name', null, array('class'=>'form-control', 'maxlength'=>'20','id'=>'middle_name', 'placeholder'=>__('Middle Name'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'middle_name') !!}</div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'last_name') !!}">

			<label for="">{{__('Last Name')}}</label>

			{!! Form::text('last_name', null, array('class'=>'form-control', 'maxlength'=>'20','id'=>'last_name', 'placeholder'=>__('Last Name'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'last_name') !!}</div>

    </div>

    <div class="col-md-6" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'father_name') !!}">

			<label for="">{{__("Father's Name")}}</label>

			{!! Form::text('father_name', "Father's Name", array('class'=>'form-control', 'maxlength'=>'30', 'id'=>'father_name', 'placeholder'=>__('Father Name'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'father_name') !!} </div>

    </div>

    

    <div class="col-md-6" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'gender_id') !!}">

			<label for="">{{__('Gender')}}</label>

			{!! Form::select('gender_id', [''=>__('Select Gender')]+$genders, null, array('class'=>'form-control', 'id'=>'gender_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'gender_id') !!} </div>

    </div>

    <div class="col-md-6" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'marital_status_id') !!}">

			<label for="">{{__('Martial Status')}}</label>

			{!! Form::select('marital_status_id', [''=>__('Select Marital Status')]+$maritalStatuses, null, array('class'=>'form-control', 'id'=>'marital_status_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'marital_status_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">

			<label for="">{{__('Country')}}</label>

            <?php $country_id = old('country_id', (isset($user) && (int) $user->country_id > 0) ? $user->country_id : $siteSetting->default_country_id); ?>

            <?php $arra = array(
            230=>'United Kingdom',
            231=>'United States of America',
        ); ?>

            {!! Form::select('country_id', $arra+$countries, $country_id, array('class'=>'form-control', 'id'=>'country_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">

			<label for="">{{__('County / State / Province / District')}}</label>

			<span id="state_dd"> {!! Form::select('state_id', [''=>__('Select County / State / Province / District')], null, array('class'=>'form-control', 'id'=>'state_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">

			<label for="">{{__('Town/City')}}</label>

			<span id="city_dd"> {!! Form::select('city_id', [''=>__('Select Town/City')], null, array('class'=>'form-control', 'id'=>'city_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>

    </div>

    <div class="col-md-6" >

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'nationality_id') !!}">

			<label for="">{{__('Nationality')}}</label>

			{!! Form::select('nationality_id', [''=>__('Select Nationality')]+$nationalities, null, array('class'=>'form-control', 'id'=>'nationality_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'nationality_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'age') !!}">

			<label for="">{{__('Age')}}</label>
            <?php 
            if(!empty($user->date_of_birth)){

                $d = date('Y-m-d', strtotime($user->date_of_birth));
                // $dob = old('date_of_birth')?date('Y-m-d',strtotime(old('date_of_birth'))):$d;
                $dob = $d;
            }else{
                // $d = date('Y-m-d', strtotime('-16 years'));
                $dob = '';
            }

            $age = !empty($user->age) ? $user->age : null;

            //dd($dob);
            ?>
			{!! Form::select('age', [
                '16 - 19'   => '16 - 19',
                '20 - 23'   => '20 - 23',
                '24 - 30'   => '24 - 30',
                '31 - 35'   => '31 - 35',
                '36 - 45'   => '36 - 45',
                '46 - 54'   => '46 - 54',
                '55+'       => '55+',
            ], $age, array('class'=>'form-control', 'id'=>'age', 'placeholder'=>__('---Select an Age---'), 'max' => date('Y-m-d', strtotime('-16 years')), 'autocomplete'=>'off', 'required')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'age') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'national_id_card_number') !!}">

			<label for="">{{__('National ID')}}</label>

			{!! Form::text('national_id_card_number', null, array('class'=>'form-control', 'maxlength'=>'25', 'id'=>'national_id_card_number', 'placeholder'=>__('National ID Card#'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'national_id_card_number') !!} </div>

    </div>

    <div class="col-md-6">

    	<div class="formrow {{ $errors->has('phone') ? ' has-error' : '' }}"> 
                                {!! Form::label('phone', 'Mobile Number', ['class' => 'bold']) !!}                                   
                               <?php if($user->phone_code==''){
                               	$phone_code = '+44';
                               }else{
                               	$phone_code = $user->phone_code;
                               } ?>
                                <div class="step_1">
									
                                <div class="input-group">
                                    <select class="form-control vodiapicker" value="{{null !==(old('phone_code'))?old('phone_code'):$phone_code}}" name="phone_code" id="phone_code">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('id',$key)->first();
                if(old('phone_code')){
                    $phone_code =  old('phone_code');
                }else{
                    $phone_code =  $phone_code;
                }
                

                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if($phone_code==$detail->sort_name ){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <div class="lang-select">
                <?php  
                if(old('phone_num_code')){
                    $phone_num_code =  old('phone_num_code');
                }else{
                    $phone_num_code =  $user->phone_num_code;
                }?>

                <a  class="btn-select cselect" value="{{old('phone_num_code')?old('phone_num_code'):$user->phone_num_code}}">
                    
                   <?php 

                    if($phone_num_code!=''){
                        $phone_detail = DB::table('countries_details')->where('phone_code',$phone_num_code)->first();

                    }else{
                       $phone_detail = DB::table('countries_details')->where('id',230)->first();  
                    }


                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('id',$phone_detail->country_id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>

                    
                    @endif
                    
                    <i class="fa fa-caret-down"></i>

                </a>
<input type="hidden" id="phone_num_code" name="phone_num_code" value="{{old('phone_num_code')?old('phone_num_code'):'+'.$phone_detail->phone_code}}">
                
                <div class="b"> 
                    <input type="text" onkeyup="myFunction()" name="search" id="search" placeholder="Search"  style="width: 100px;">
                <ul id="a">
                    
                </ul>

                </div>
                </div>
              {!! Form::text('phone',null, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('Mobile Number'), 'maxlength'=>18)) !!}
                           
                                  
                                  </div>
                             
                              </div>
                                
                                  
                                   
                                     @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif                      
                                                                      
                                </div>

    </div>

    <div class="col-md-6" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'mobile_num') !!}">

			<label for="">{{__('Landline: Phone Number')}}</label>

			{!! Form::text('mobile_num', null, array('class'=>'form-control', 'maxlength'=>'18', 'id'=>'mobile_num', 'placeholder'=>__('Phone Number'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'mobile_num') !!} </div>

    </div>   

    <div class="col-md-12">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'street_address') !!}">

			<label for="">{{__('Street Address')}}</label>

			{!! Form::textarea('street_address', null, array('class'=>'form-control', 'id'=>'current_location', 'autocomplete' => 'off', 'maxlength'=>'100', 'placeholder'=>__('Street Address'))) !!}

            {!! APFrmErrHelp::showErrors($errors, 'street_address') !!} </div>

    </div>

	

</div>



<hr>



<h5>{{__('Career Information')}}</h5>



<div class="row">

 <div class="col-md-6" style="display: none;">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">

			<label for="">{{__('Job Experience')}}</label>

			{!! Form::select('job_experience_id', [''=>__('Select Experience')]+$jobExperiences, null, array('class'=>'form-control', 'id'=>'job_experience_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'career_level_id') !!}">

			<label for="">{{__('Career Level')}}</label>

			{!! Form::select('career_level_id', [''=>__('Select Career Level')]+$careerLevels, null, array('class'=>'form-control', 'id'=>'career_level_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">

			<label for="">{{__('Select Industry')}}</label>

			{!! Form::select('industry_id', [''=>__('Select Industry')]+$industries, null, array('class'=>'form-control', 'id'=>'industry_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!} </div>

    </div>

    <div class="col-md-6">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}">

			<label for="">{{__('Functional Area (Job Description Type)')}}</label>

			{!! Form::select('functional_area_id', [''=>__('Select Functional Area')]+$functionalAreas, null, array('class'=>'form-control', 'id'=>'functional_area_id')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!} </div>

    </div>
	
	<div class="col-md-6">
		<div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
			<label for="">{{__('Years of Experience')}}</label>                   
        {!! Form::select('job_experience_id', [''=>'Select Years of Experience']+$jobExperiences, null, array('class'=>'form-control', 'id'=>'job_experience_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!}                                       
    </div>
        

    </div>
	
	

    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'current_salary') !!}">

			<label for="">{{__('Current Salary')}}</label>

			{!! Form::text('current_salary', null, array('class'=>'form-control', 'maxlength'=>'12', 'id'=>'current_salary', 'required' => 'required', 'placeholder'=>__('Current Salary'),'maxlength'=>12,"oninput"=>"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');")) !!}

            {!! APFrmErrHelp::showErrors($errors, 'current_salary') !!} </div>

    </div>

    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'expected_salary') !!}">

			<label for="">{{__('Expected Salary')}}</label>

			{!! Form::text('expected_salary', null, array('class'=>'form-control', 'maxlength'=>'12', 'id'=>'expected_salary', 'required' => 'required', 'placeholder'=>__('Expected Salary'),'maxlength'=>12,"oninput"=>"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');")) !!}

            {!! APFrmErrHelp::showErrors($errors, 'expected_salary') !!} </div>

    </div>

    <div class="col-md-4">

        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}">
            <?php 
            $default_currecny = App\CountryDetail::where('code','like','%'.$siteSetting->default_currency_code.'%')->first();
            $d_cur = $default_currecny->symbol;
        ?>

			<label for="">{{__('Salary Currency')}}</label>			
            @php

            $salary_currency = Request::get('salary_currency', (isset($user) && !empty($user->salary_currency))? $user->salary_currency:$d_cur);
            @endphp



            {!! Form::select('salary_currency', ['' => __('Select Salary Currency')]+$currencies, $salary_currency, array('class'=>'form-control', 'id'=>'salary_currency')) !!}

            {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!} </div>

    </div>

</div>

	
    <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_immediate_available') !!}">
        {!! Form::label('is_immediate_available', 'Available for Work?', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">
            <?php
            $is_immediate_available_1 = 'checked="checked"';
            $is_immediate_available_2 = '';
            if (old('is_immediate_available', ((isset($user)) ? $user->is_immediate_available : 1)) == 0) {
                $is_immediate_available_1 = '';
                $is_immediate_available_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="immediate_available" name="is_immediate_available" type="radio" value="1" {{$is_immediate_available_1}}>
                Available Immediately </label>
			
            <label class="radio-inline ml-3">
                <input id="not_immediate_available" name="is_immediate_available" type="radio" value="0" {{$is_immediate_available_2}}>
                Available on (Select for Calendar) </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_immediate_available') !!}
    </div>
    <?php if(isset($user)){?>
        <div id="immediate_form_in_form" <?php  if((bool)$user->is_immediate_available){echo 'style="display:none"';}else{} ?>>
            <div class="row">
            <input type="hidden" name="old_status" value="1">
              <div class="formrow col-md-4" style="text-align: left">
                <label for="date_from" style="margin-bottom: 5px;">Date From</label>
                <input type="date" class="form-control" id="date_from" name="date_from" aria-describedby="emailHelp" value="{{null!==($user->immediate_date_from)?date('Y-m-d',strtotime($user->immediate_date_from)):date('Y-m-d')}}" placeholder="Date From">
              </div>
              <div class="col-md-6"></div>
             <!--  <div class="formrow" style="text-align: left">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==($user->immediate_date_to)?date('Y-m-d',strtotime($user->immediate_date_to)):date('Y-m-d')}}" placeholder="Date To">
              </div> -->
        </div>
    </div>
    <?php }else{ ?>
        <div id="immediate_form_in_form" style="display:none">
            <div class="row">
            <input type="hidden" name="old_status" value="1">
              <div class="formrow col-md-4" style="text-align: left">
                <label for="date_from" style="margin-bottom: 5px;">Date From</label>
                <input type="date" class="form-control" id="date_from" name="date_from" aria-describedby="emailHelp" value="{{null!==(old('immediate_date_from'))?date('Y-m-d',old('immediate_date_from')):date('Y-m-d')}}" placeholder="Date From">
              </div>
              <div class="col-md-6"></div>
              <!-- <div class="formrow" style="text-align: left">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==(old('immediate_date_to'))?date('Y-m-d',strtotime(old('immediate_date_to'))):date('Y-m-d')}}" placeholder="Date To">
              </div> -->
          </div>
        </div>
    <?php } ?>

     <div class="formrow {!! APFrmErrHelp::hasError($errors, 'eligible_uk') !!}">
        {!! Form::label('eligible_uk', 'Eligible to work in the UK:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = '';
            $is_active_2 = '';
            
            if(isset($user) && $user->eligible_uk === 0) {
                $is_active_2 = 'checked="checked"';
            } else if(isset($user) && $user->eligible_uk === 1) {
                $is_active_1 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="eligible_uk" type="radio" value="1" {{$is_active_1}} required>
                Yes </label>
            <label class="radio-inline">
                <input id="not_active" name="eligible_uk" type="radio" value="0" {{$is_active_2}} required>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eligible_uk') !!}
    </div> 

    <div class="formrow {!! APFrmErrHelp::hasError($errors, 'eligible_eu') !!}">
        {!! Form::label('eligible_eu', 'Eligible to work in the EU:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = '';
            $is_active_2 = '';
            // if (old('eligible_eu', ((isset($user)) ? $user->eligible_eu : 1)) == 0) {
            //     $is_active_1 = '';
            //     $is_active_2 = 'checked="checked"';
            // }else if(old('eligible_eu', ((isset($user)) ? $user->eligible_eu : 0)) == 1){
            //     $is_active_1 = 'checked="checked"';
            //     $is_active_2 = '';
            // }
            if(isset( $user ) && $user->eligible_eu === 0) {
                $is_active_2 = 'checked="checked"';
            } else if(isset( $user ) && $user->eligible_eu === 1) {
                $is_active_1 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="eligible_eu" type="radio" value="1" {{$is_active_1}} required>
                Yes </label>
            <label class="radio-inline">
                <input id="not_active" name="eligible_eu" type="radio" value="0" {{$is_active_2}} required>
                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eligible_eu') !!}
    </div>
	
	<hr>

<h5>{{__('Car & Driving Licence')}}</h5>
	<div class="formrow {!! APFrmErrHelp::hasError($errors, 'eu_licence') !!}">
        {!! Form::label('driving_license', 'Do you have a driving license:', ['class' => 'bold mb-2']) !!}
        @php
            $attrArr = ['required' => 'required'];
            $is_active_1 = $is_active_2 = '';
            if(isset($user) && $user->driving_license === 1) {
                $is_active_1 = 'checked = "checked"';
            } else if(isset($user) && $user->driving_license === 0) {
                $is_active_2 = 'checked = "checked"';
            }
        @endphp
        <div class="radio-list">           
            <label class="radio-inline">
                <input type="radio" name="driving_license" value="1" {{ $is_active_1 }} required>
                Yes </label>
                <label class="radio-inline">
                    <input type="radio" name="driving_license" value="0" {{ $is_active_2 }} required>

                No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'eu_licence') !!}
    </div>

	<div class="formrow {!! APFrmErrHelp::hasError($errors, 'eu_relicate') !!}">
        @php
            $attrArr = ['required' => 'required'];
            $is_active_1 = $is_active_2 = '';
            if(isset($user) && $user->willing_to_relocate === 1) {
                $is_active_1 = 'checked = "checked"';
            } else if(isset($user) && $user->willing_to_relocate === 0) {
                $is_active_2 = 'checked = "checked"';
            }
        @endphp
        {!! Form::label('willing_to_relocate', 'Are you willing to relocate:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">           
            <label class="radio-inline">
            <input type="radio" name="willing_to_relocate" value="1" {{ $is_active_1 }} required>
            Yes </label>
            <label class="radio-inline">
                <input type="radio" name="willing_to_relocate" value="0" {{ $is_active_2 }} required>

            No </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'willing_to_relocate') !!}
    </div>

    @php
        $attrArr = ['required' => 'required'];
        $is_active_1 = $is_active_2 = '';
        if(isset($user) && $user->unit_type === 'kms') {
            $is_active_1 = 'checked = "checked"';
        } else if(isset($user) && $user->unit_type === 'miles') {
            $is_active_2 = 'checked = "checked"';
        }
    @endphp

    <div class="formrow">
        {!! Form::label('unit_type', 'Distance Unit:', ['class' => 'bold mb-2']) !!}
        <div class="radio-list">           
            <label class="radio-inline">
            <input type="radio" name="unit_type" value="kms" {{ $is_active_1 }} required>
            Kms </label>
            <label class="radio-inline">
                <input type="radio" name="unit_type" value="miles" {{ $is_active_2 }} required>
                Miles </label>
        </div>
    </div>
		
	<div class="formrow {!! APFrmErrHelp::hasError($errors, 'eu_distance') !!}">
        {!! Form::label('eu_distance', 'Maximum distance prepared to travel from Home:', ['class' => 'bold mb-2']) !!}
        {!! Form::select('eu_distance', [
            '5'     => '5',
            '10'    => '10',
            '20'    => '20',
            '30'    => '30',
            '40'    => '40',
            '50'    => '50',
            '60+'   => '60+'
        ], null, array('class'=>'form-control', 'id'=>'eu_distance', 'placeholder'=>__('---Select Distance---'), 'maxlength' => 5)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'eu_distance') !!}
    </div>
		
		
		
	<div class="row">	

    <div class="col-md-12">

    <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_subscribed') !!}">

    <?php

	$is_checked = 'checked="checked"';	

	if (old('is_subscribed', ((isset($user)) ? $user->is_subscribed : 1)) == 0) {

		$is_checked = '';

	}

	?>

      <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />

      {{__('Subscribe to newsletter')}}

      {!! APFrmErrHelp::showErrors($errors, 'is_subscribed') !!}

      </div>

  </div>


  

    <div class="col-md-12"><h5>{{__('Benefits of Adding a summary to your profile')}}</h5>

<p>A summary is a statement of between two or three sentences, giving a professional introduction highlighting your most valuable skills, experiences and assets.</p>
<p>A CV summary can help employers quickly learn whether you have the skills and background they require.</p>
<p id="limit_message">A maximum of 1000 characters including spaces can be input, approximately 170 words</p>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'summary') !!}">
                    <textarea name="summary" class="form-control" id="summary" placeholder="{{__('Profile Summary')}}" maxlength="1000">{{ old('summary', $user->getProfileSummary('summary')) }}</textarea>
                    <span class="help-block summary-error"></span> </div>
              

    </div>


    <div class="col-md-12">

        <div class="formrow"><button type="submit" class="btn">{{__('Update Profile and Save')}}  <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button></div>

    </div>

</div>





{!! Form::close() !!}

<hr>

@push('styles')

<style type="text/css">

    .datepicker>div {

        display: block;

    }

</style>

@endpush

@push('scripts') 

<script type="text/javascript">
     $('#image').on('change',function(){
        if($(this).val()){
            $('.red').show();
        }else{
          $('.red').hide();  
        }
    })
    if ($("#profile-form").length > 0) {
    var msg = 'Please Enter a Valid Mobile Number and Click Verify to Continue';
    @if(!$siteSetting->verify_registration_phone_number)
        var msg = 'Please enter a valid phone number';
    @endif
     $("#profile-form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    first_name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                    },

                    last_name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                    }, 
                   
                    phone: {
                        required: true,
                        minlength: 11,
                    },

                    email: {
                        required: true,
                        email:true,
                    },
                    eligible_uk: {
                        required: true,
                    },
                    eligible_eu: {
                        required: true,
                    },
                    
                },
                messages: {

                    first_name: {
                        required: "First Name required",
                    },
                    last_name: {
                        required: "Last Name required",
                    },


                    email: {
                        required: "Email required",
                    }, 

                    phone: {
                        required: "Phone Number required",
                        minlength: msg,
                    },
                    image: {
                        extension: "Please upload file in these format only (jpg, jpeg, png, gif)."
                    }

                },
                errorPlacement: function(error, element)  {
                    if ( element.is(":radio") ) 
                    {
                        error.appendTo( element.parents('.radio-list') );
                    }
                    else 
                    { // This is the default behavior 
                        error.insertAfter( element );
                    }
                }
            })
        }
    
    (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);
        return this.each(function() {
            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
})(jQuery);
$(function() {
    $('#image').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'bmp' , 'gif' , 'png' ],
        error: function() {
            if($('#image').val()!=''){
                alert('Image Formats Allowed: .jpg .jpeg .png .gif');
                $('#image').val('').trigger('change');
            }
            
            // $('#image').val('').trigger('change');
            $('.save-image').hide();
        }
    });
});
    $(document).ready(function () {





        initdatepicker();

        $('#salary_currency').typeahead({

            source: function (query, process) {

                return $.get("{{ route('typeahead.currency_codes') }}", {query: query}, function (data) {

                    console.log(data);

                    data = $.parseJSON(data);

                    return process(data);

                });

            }

        });

    $('input[name="is_immediate_available"]').on('change',function(){
        if($('input[name="is_immediate_available"]:checked').val() == '1'){
            $('#immediate_form_in_form').hide();
        }else{
            $('#immediate_form_in_form').show();
        }
        
    })

        $('#country_id').on('change', function (e) {

            e.preventDefault();

            filterStates(0);

        });

        $(document).on('change', '#state_id', function (e) {

            e.preventDefault();

            filterCities(0);

        });

        filterStates(<?php echo old('state_id', $user->state_id); ?>);



        /*******************************/

        var fileInput = document.getElementById("image");

        fileInput.addEventListener("change", function (e) {

            var files = this.files

            console.log(files);

            const size =  
               (this.files[0].size / 1024).toFixed(2); 
  
            if (size > 100) { 
                document.getElementById("image").value = null;
                $('#image').trigger('change');
                alert("A maximum image size of 100kb allowed, please reduce image size and try again"); 
                return false;
            } 

            showThumbnail(files)

        }, false)

        function showThumbnail(files) {

            $('#thumbnail').html('');

            for (var i = 0; i < files.length; i++) {

                var file = files[i]

                var imageType = /image.*/

                if (!file.type.match(imageType)) {

                    console.log("Not an Image");

                    continue;

                }

                var reader = new FileReader()

                reader.onload = (function (theFile) {

                    return function (e) {

                        $('#thumbnail').append('<div class="fileattached"><img height="100px" src="' + e.target.result + '" > <div>' + theFile.name + '</div><div class="clearfix"></div></div>');

                    };

                }(file))

                var ret = reader.readAsDataURL(file);

            }

        }

    });



    function filterStates(state_id)

    {

        var country_id = $('#country_id').val();

        if (country_id != '') {

            $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#state_dd').html(response);

                        filterCities(<?php echo old('city_id', $user->city_id); ?>);

                    });

        }

    }

    function filterCities(city_id)

    {

        var state_id = $('#state_id').val();

        if (state_id != '') {

            $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('#city_dd').html(response);

                    });

        }

    }

    function initdatepicker() {

        var d = new Date();
        var pastYear = d.getFullYear() - 16;
        d.setFullYear(pastYear);
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'd-m-yyyy',
            endDate: d,
        });

    }

    $('#first_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z \s]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });

        $('#middle_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z \s]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });

        $('#last_name').keypress(function (e) {
            var regex = new RegExp("^[a-z A-Z]*\-?[a-zA-Z]*$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });

        $('#father_name').keypress(function (e) {
            var regex = new RegExp("^[a-z A-Z]*\-?[a-zA-Z]*$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });


        $('#phone').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $('#mobile_num').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

         $('#phone').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
var langArray = [];

$('.vodiapicker option').each(function(){
  var img = $(this).attr("data-thumbnail");
  var val = $(this).attr("data-val");
  var text = this.innerText;
  var value = $(this).val();
  var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
  langArray.push(item);
})

$('#a').html(langArray);

//Set the button value to the first el of the array


//change button stuff on click
$('#a li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fas fa-caret-down"></i></li>';
  $('.btn-select').html(item);
  $('.btn-select').attr('value', value);
  $('#phone').val(val);
  $('#phone_num_code').val(val);
  $(".b").toggle();
  //console.log(value);
});

$(".btn-select").click(function(e){
    e.stopPropagation();
    $(".b").toggle();
});

$('#search').click(function (e) {
    e.stopPropagation();
});

$(document).click(function () {
    $('.b').hide();
});

 $('#mobile_num').keypress(function (evt) {
    var val = $(this).val();
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode != 32 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});
function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    ul = document.getElementById("a");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}


function staticData(){
  var data=$("#phone");
  
    var code = $('#phone_num_code').val();

  if(data.val().length<3)
    {
     data.val(code);
     data.attr("readonly",true);
    }
    else
    {
     data.attr("readonly",false);
    }
}
    (function($) {
    // $.fn.checkFileType = function(options) {
    //     var defaults = {
    //         allowedExtensions: [],
    //         success: function() {},
    //         error: function() {}
    //     };
    //     options = $.extend(defaults, options);

    //     return this.each(function() {

    //         $(this).on('change', function() {

                 

    //             var value = $(this).val(),
    //                 file = value.toLowerCase(),
    //                 extension = file.substring(file.lastIndexOf('.') + 1);
    //             const size =  
    //                (this.files[0].size / 1024).toFixed(2); 
      
    //             if (size > 100) { 
    //                 document.getElementById("image").value = null;
    //                 $('#image').trigger('change');
    //                 alert("A maximum image size of 100kb allowed, please reduce image size and try again"); 
    //                 options.error();
    //                 $(this).focus();
    //             }else{
    //                 if ($.inArray(extension, options.allowedExtensions) == -1) {
    //                 options.error();
    //                 $(this).focus();
    //             } else {
    //                 options.success();

    //             }
    //             }    

                

    //         });

    //     });
    // };

})(jQuery);

// $(function() {
//     $("input[type='file']").checkFileType({
//         allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
//         success: function() {
//             $('.allowed_message').css("color", "#888");
//         },
//         error: function() {
//             $('.allowed_message').css("color", "red");
//             $("input[type='file']").val(null);
//         }
//     });

// });

function remvoe_iamge(id) {

        if (confirm('Are you sure you want to remove your profile image?')) {

            $.post("{{ route('remove.image') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        $('.save-image').hide();


                        

                    });

        }

    }
    // $('#image').on('change',function(){
    // })
    $('#image').on('change',function(){
        if($(this).val() && $(this).val != ''){
            $('.save-image').show();
            $('.red').show();

            // $('.save-image').trigger('click');
        }else{
            $('.save-image').hide();

          $('.red').hide();  
        }
    })
    $( '.save-image' ).on('click', function ( evt ) {
        evt.preventDefault();
        let myForm = document.getElementById('profile-form');
        var formData = new FormData(myForm);

        $.ajax( {
            type: 'POST',
            url: "{{route('user.save-image')}}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function ( data ) {
                $( '.save-image' ).hide();
                swal("Done", "Image Successfully Saved", "success");
            },
            error: function ( data ) {
                $( '#imagedisplay' ).html( "<h2>this file type is not supported</h2>" );
            }
        } );
    } );


</script> 

@endpush            