<h5 onclick="showCvs();">{{__('CV - Curriculum vitae')}}</h5>

<div class="row">

    <div class="col-md-12">

        <div class="" id="cvs_div"></div>

    </div>

</div>

<a href="javascript:;" class="uscvbtn" onclick="showProfileCvModal();"> {{__('Add a CV to your Profile')}} </a>

<hr>

<div class="modal" id="add_cv_modal" role="dialog"></div>

<div id="cv-loading" style="display: none">
    <div class="loading-bg" style="position: fixed; top: 0; left: 0;right: 0; bottom: 0; z-index: 999999; background: rgba(0, 0,0, 0.5);"></div>
    <div class="loading-body" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 999999; background: #fff; padding: 15px; border-radius: 20px; text-align: center;">
        <img src="{{ url('loading.gif') }}" alt="" style="width: 48px">
    </div>
</div>

@push('css')

<style type="text/css">

    .datepicker>div {

        display: block;

    }

</style>

@endpush

@push('scripts') 

<script type="text/javascript">

    $(document).ready(function(){

    showCvs();

    });

    /**************************************************/

    function showProfileCvModal(){

    $("#add_cv_modal").modal();

    loadProfileCvForm();

    }

    function loadProfileCvForm(){

    $.ajax({

    type: "POST",

            url: "{{ route('get.front.profile.cv.form', $user->id) }}",

            data: {"_token": "{{ csrf_token() }}"},

            datatype: 'json',

            success: function (json) {

            $("#add_cv_modal").html(json.html);

            }

    });

    }

    function uploadCv(isDefault) {
        var form = $('#add_edit_profile_cv');

        var formData = new FormData();

        formData.append("id", $('#id').val());

        formData.append("_token", $('input[name=_token]').val());

        formData.append("title", $('#title').val());

        formData.append("is_default", isDefault);

        if (document.getElementById("cv_file").value != "") {

            formData.append("cv_file", $('#cv_file')[0].files[0]);

        }

        //form.attr('method'),

        $('#cv-loading').show();

        $.ajax({

            url     : form.attr('action'),

            type    : 'POST',

            data    : formData,

            dataType: 'json',

            contentType: false,

            processData: false,

            success : function (json){

                // $ ("#add_cv_modal").html(json.html);
                $('#cv-loading').hide();

                swal('Success!', 'CV has been uploaded successfully.', 'success');

                showCvs();

                @if(isset($_COOKIE['set_session']))
                // window.location.href = "{{$_COOKIE['set_session']}}";
                @endif
            },

            error: function(json){

                if (json.status === 422) {

                    var resJSON = json.responseJSON;

                    $('.help-block').html('');

                    $.each(resJSON.errors, function (key, value) {

                    $('.' + key + '-error').html('<strong>' + value + '</strong>');

                    $('#div_' + key).addClass('has-error');

                    $('form .btn-primary').removeAttr('disabled');
                    $('form .btn-primary').text('Add Cv');

                    });

                } else {

                    // Error

                    // Incorrect credentials

                    // alert('Incorrect credentials. Please try again.')

                }

            }

        });
    }

    function submitProfileCvForm() {
        // if($('#title').val()!==''){
        // }

        let ext = '';

        if($('#cv_file').val() != '') {
            let myFile = $('#cv_file').val();
            ext = myFile.split('.').pop();
        }

        if($('#title').val() == '') {
            // swal('Warning!', 'Please enter title.', 'warning');
            alert('Please enter title');
        } else if($('#cv_file').val() == '') {
            // swal('Warning!', 'Please choose CV file.', 'warning');
            alert('Please choose CV file.');
        } else if(ext != 'pdf' && ext != 'docx' && ext != 'doc') {
            alert('Only PDF and DOC files can be uploaded.');
        } else {
            $('#add_cv_modal').modal('hide');
            // $('form .btn-primary').text('Uploading...');
            // $('form .btn-primary'). prop('disabled', true);
            if($('#totalCvCount').length) {
                swal({
                    title: "Set CV as default priority",
                    text: "",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    closeOnClickOutside: false
                    // dangerMode: true,
                })
                .then((confirmed) => {
                    var isDefault;
                    if(confirmed) {
                        isDefault = 1;
                    } else {
                        isDefault = 0;
                    }
                    
                    uploadCv(isDefault);

                });
            } else {
                uploadCv(1);
            }
        }
		
    }

    /*****************************************/

    function showProfileCvEditModal(cv_id){

    $("#add_cv_modal").modal();

    loadProfileCvEditForm(cv_id);

    }

    function loadProfileCvEditForm(cv_id){

    $.ajax({

    type: "POST",

            url: "{{ route('get.front.profile.cv.edit.form', $user->id) }}",

            data: {"cv_id": cv_id, "_token": "{{ csrf_token() }}"},

            datatype: 'json',

            success: function (json) {

            $("#add_cv_modal").html(json.html);

            }

    });

    }

    /*****************************************/

    function showCvs()

    {

    $.post("{{ route('show.front.profile.cvs', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#cvs_div').html(response);

            });

    }

    function delete_profile_cv(id,name) {

    var msg = "{{__('Are you sure you want to delete CV ')}}"+name;

    if (confirm(msg)) {

    $.post("{{ route('delete.front.profile.cv') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            if (response == 'ok')

            {

            $('#cv_' + id).remove();

            location.reload();

            } else

            {

            alert('Request Failed!');

            }

            });

    }

    }

</script>

@endpush

