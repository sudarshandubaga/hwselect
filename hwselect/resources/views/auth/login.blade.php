@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Login')])

@php
$c_or_e = 'candidate';
if(request()->get('type') === 'emp') {
    $c_or_e = 'employer';
}
@endphp

@include('auth.loginForm', ['c_or_e' => $c_or_e])

@include('includes.footer')

@endsection
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-auth.js"></script>
<script type="text/javascript">
     @if(Session::get('verified'))
        swal({

                title: "Thank you for Registering",

                text: 'Account Successfully Verified & Activated. Click OK to Login to your Account',

                icon: "success",

                button: "OK",

            });
       @endif  


       @if(Session::get('alread_verified'))
        swal({

                title: "Account Already Verified",

                text: '',

                icon: "success",

                button: "OK",

            });
       @endif  

     @if(Session::get('not_verified'))
         swal({

                title: "Incomplete Verification",

                text: 'On registration you will have been sent an email, this must be verified in order to login. If unable to verify, contact support to resend email',

                icon: "error",

                button: "OK",

            });
       @endif 

        @if(Session::get('message.added'))
            const el = document.createElement('div')
            var registerd_mail = '{{session()->get("email")}}';
            el.innerHTML = 'Thank you for registering an Account with {{$siteSetting->site_name}}.<br><br> Email sent to <a href="mailto:'+registerd_mail+'">'+registerd_mail+'</a><br><br>  <strong>Please verify to activate your account</strong>';
            swal({

                title: "Jobseeker",

                text: "Registration Confirmation",

                content: el,

                icon: "success",

                button: "OK",

            });
        @endif

        @if(Session::get('message.added1'))
            const el = document.createElement('div')
            var registerd_mail = '{{session()->get("email")}}';
            el.innerHTML = 'Thank you for registering an Account with {{$siteSetting->site_name}}.<br><br> Email sent to <a href="mailto:'+registerd_mail+'">'+registerd_mail+'</a><br><br>  <strong>Please verify to activate your account</strong>';
            swal({

                title: "Company",

                text: "Registration Confirmation",

                content: el,

                icon: "success",

                button: "OK",

            });
        @endif


        function set_seeker(seeker){
            var type = seeker;
            //alert(type);
            $.ajax({
              type: 'POST',
              url: "{{route('set_session')}}",
              data: {
                '_token': $('input[name=_token]').val(),
                'type': type
              }
            });
        }
        function set_employer(employer){
            var type = employer;
            //alert(type);
            $.ajax({
              type: 'POST',
              url: "{{route('set_session')}}",
              data: {
                '_token': $('input[name=_token]').val(),
                'type': type
              }
            });
    } 

    var config = {
        // apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
        // authDomain: "hwselect-62f4f.firebaseapp.com",
        // databaseURL: "https://hwselect-62f4f.firebaseio.com",
        // projectId: "hw-select-uk",
        // storageBucket: "hwselect-62f4f.appspot.com",
        // messagingSenderId: "269417794687",
        // appId: "1:401245972574:web:207e49cc038d66ee976ccf",
        // measurementId: "G-TSVHYD80QW"
        apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
        authDomain: "hw-select-uk.firebaseapp.com",
        projectId: "hw-select-uk",
        storageBucket: "hw-select-uk.appspot.com",
        messagingSenderId: "269417794687",
        appId: "1:269417794687:web:b8aee097fea11a0895434a"
    };
    firebase.initializeApp(config);
    function sendSms(uphone, container = 'phone_verify') {
        window.uphone = $.trim(uphone);
        // console.log('phone number: ', window.uphone);
        if(true) {
            var appVerifier = new firebase.auth.RecaptchaVerifier(container, { 'size': 'invisible' });
            window.recaptchaVerifier = appVerifier;

            // console.log('app verifier: ', appVerifier);
            // $(this).attr("disabled", "disabled");
            sendOTP(uphone);
        }
    }

    function sendOTP(uphone) {
        firebase.auth().signInWithPhoneNumber(uphone, window.recaptchaVerifier)
                .then(function (confirmationResult) {
                    // console.log('firebase response: ',confirmationResult);
                    window.confirmationResult = confirmationResult;

                    var s = 60 * 5;
                    var min, sec;
                    var otpInterval = setInterval(() => {
                        if(s > 1) {
                            s--;
                            min = Math.floor(s / 60);
                            sec = s - min * 60;
                            if(sec < 10) sec = '0' + sec;
                            
                            $('.otp-counter-text').text('OTP Expires in '+min+':'+sec+' minutes');
                        } else {
                            $('.otp-counter-text').text('OTP Expired');
                            clearInterval(otpInterval);
                        }
                    }, 1000);

                    // return confirmationResult;
                //console.log("verification code sent");
                }).catch(function (error) {
                    console.error('Error during signInWithPhoneNumber', error);
                });
    }

    $(function() {

        $(window).on('load', function() {
            $('#loadingBox').hide();
        });

        $(document).on('submit', '#loginForm', function (e) {
            e.preventDefault();

            var is_verified = $('#loginVerified').val();
            
            $.ajax({
                type: 'POST',
                url: "{{ route('login') }}",
                data: $(this).serialize(),
                success: function (res) {
                    if(res.success) {
                        if(res.user.two_step_verification == 1) {
                            sendSms(res.user.phone);

                            $('#lastPhoneNumber').html( res.user.phone.slice(-4) );

                            $('#otpForm').show();
                            $('#loginForm').hide();

                            // window.confirmationResult = confirmationResult;
                        } else {
                            location.reload();
                        }
                    } else {
                        swal('Error!', res.msg, 'warning');
                    }
                }
            });
        });

        $(document).on('click', '#resendOTPBtn', function (e) {
            e.preventDefault();

            sendSms(window.uphone, 'resendOTPCaptcha');

            alert("OTP sent to mobile number xxxxxx" + window.uphone.slice(-4));
        });

        $(document).on('click', '#companyResendOTPBtn', function (e) {
            e.preventDefault();

            sendSms(window.uphone, 'companyResendOTPCaptcha');

            alert("OTP sent to mobile number xxxxxx" + window.uphone.slice(-4));
        });

        $(document).on('submit', '#companyLoginForm', function (e) {
            e.preventDefault();

            var is_verified = $('#loginCompanyVerified').val();
            
            $.ajax({
                type: 'POST',
                url: "{{ route('company.login') }}",
                data: $(this).serialize(),
                success: function (res) {
                    if(res.success) {
                        if(res.user.two_step_verification == 1) {
                            sendSms(res.user.phone);
                            
                            $('#companyLastPhoneNumber').html( res.user.phone.slice(-4) );
                            $('#otpCompanyForm').show();
                            $('#companyLoginForm').hide();

                            // window.confirmationResult = confirmationResult;
                        } else {
                            // location.reload();
                            window.location = "{{ url('company-home') }}";
                        }
                    } else {
                        swal('Error!', res.msg, 'warning');
                    }
                }
            });
        });

        $(document).on('click', '#otpVerifyBtn', function (e) {
            e.preventDefault();

            var code = $.trim($("#otp_code").val());
            if ( code.length > 3 ) {
                window.confirmationResult
                    .confirm(code)
                    .then(result => {
                        console.log('result OTP: ', result);

                        $.ajax({
                            url: "{{ route('verify_otp') }}",
                            method: 'POST',
                            data: {
                                _token: "{{ csrf_token() }}",
                                phoneNumber: result.user.phoneNumber,
                                email: $('#email').val()
                            },
                            success: function (res) {
                                location.reload();
                            }
                        });
                    });
            } else { alert("A valid SMS verification code must be entered"); }
        });


        $(document).on('click', '#companyOtpVerifyBtn', function (e) {
            e.preventDefault();

            var code = $.trim($("#company_otp_code").val());
            if ( code.length > 3 ) {
                window.confirmationResult
                    .confirm(code)
                    .then(result => {
                        console.log('result OTP: ', result);

                        $.ajax({
                            url: "{{ route('company.verify_otp') }}",
                            method: 'POST',
                            data: {
                                _token: "{{ csrf_token() }}",
                                phoneNumber: result.user.phoneNumber,
                                email: $('#emailCompany').val()
                            },
                            success: function (res) {
                                // location.reload();
                                window.location = "{{ url('company-home') }}";
                            }
                        });
                    });
            } else { alert("A valid SMS verification code must be entered"); }
        });
    });
</script>
@endpush

