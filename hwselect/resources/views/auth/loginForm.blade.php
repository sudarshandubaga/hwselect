<div class="listpgWraper" style="position: relative">

    <div id="loadingBox" style="position: absolute;top: 0;bottom: 0;left: 0;right: 0;display: flex;z-index: 1;justify-content: center;align-items: center;background: #f0f0f0;">
        <img src="{{ url('loading.gif') }}" alt="" style="width: 48px">
    </div>

    <div class="container">

        @include('flash::message')

            <div class="useraccountwrap">

				<div class="userbtns">

                        <ul class="nav nav-tabs">

                             <?php
                            // if(request()->type=='emp' || session()->has('form_type')){
                            //     $c_or_e = 'employer';
                            // }else{
                            //     $c_or_e = old('candidate_or_employer', 'candidate');
                            // }
                            

                            ?>
                            <li class="nav-item"><a class="nav-link {{($c_or_e == 'candidate')? 'active':''}}" data-toggle="tab" href="#candidate" aria-expanded="true">{{__('Jobseeker Sign In')}}</a></li>

                            <li class="nav-item"><a class="nav-link {{($c_or_e == 'employer')? 'active':''}}" data-toggle="tab" href="#employer" aria-expanded="false">{{__('Company Sign In')}}</a></li>

                        </ul>

                    </div>

				

                <div class="userccount">

					

                    <div class="tab-content">

                        <div id="candidate" class="formpanel tab-pane {{($c_or_e == 'candidate')? 'active':''}}">

							

							<div class="row">

								<div class="col-lg-7">

									<div class="loginboxint">

									 <div class="socialLogin">

                                        <h5>{{__('Login with Social')}}</h5>

                                        <a  onclick="set_seeker('jobseeker')" href="{{ url('login/jobseeker/linkedin')}}" class="linkedin">Login With Social Media <span><i class="fab fa-linkedin-in"></i></span></a>
                                    </div>

                            <div id="otpForm" style="display: none">
                                <div class="formpanel">
                                    <div class="formrow font-weight-bold">
                                        Please enter the One Time Password(OTP) in the field below. This would have been sent to your registered mobile number ******<span id="lastPhoneNumber"></span>
                                    </div>
                                    <div class="formrow">
                                        <span id="resendOTPCaptcha"></span>
                                        <a href="#" class="float-right" id="resendOTPBtn">Resend OTP?</a>
                                        <label for="otp_code">{{ __('Enter OTP') }}</label>
                                        <input type="number" name="otp_code" id="otp_code" class="form-control" required>
                                    </div>
                                            <p class="text-center otp-counter-text" style="margin: 10px 0;">OTP Expires in 60 seconds</p>
                                    <input type="button" class="btn" id="otpVerifyBtn" value="{{__('Verify')}}">
                                </div>
                            </div>

                            <form class="form-horizontal" method="POST" action="{{ route('login') }}" id="loginForm">

                                {{ csrf_field() }}

                                <input type="hidden" name="candidate_or_employer" value="candidate" />
                                <input type="hidden" name="verified" value="{{ $setting->verify_registration_phone_number ? 0 : 1 }}" id="loginVerified" />

                                <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                                <div class="formpanel">

                                    <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">

										<label for="">{{__('Username')}}</label>

                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="{{__('Email Address')}}">

                                        @if ($errors->has('email'))

                                        <span class="help-block">

                                            <strong>{{ $errors->first('email') }}</strong>

                                        </span>

                                        @endif

                                    </div>

                                    <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">

										<label for="">{{__('Password')}}</label>

                                        <input id="password" type="password" class="form-control" name="password" value="" placeholder="{{__('Password')}}">

                                        @if ($errors->has('password'))

                                        <span class="help-block">

                                            <strong>Password Required </strong>

                                        </span>

                                        @endif

                                    </div> 

									

									<div class="forgotpass"><i class="fa fa-user" aria-hidden="true"></i> {{__('Forgot Your Password')}}? <a href="{{ route('password.request') }}">{{__('Click here')}}</a></div>

									<!-- <div id="`phone_verify`"></div> -->

                                    <input type="submit" class="btn" id="phone_verify" value="{{__('Login')}}">

                                </div>

                                <!-- login form  end--> 

                            </form>

                            <!-- sign up form -->

							

							

							<!-- sign up form end-->

								</div>

								</div>

								<div class="col-lg-5">

									<div class="newuser">

									<h3>{{get_widget(17)->heading}}</h3>									

									{!!get_widget(17)->content!!}								

								<a href="{{route('register')}}?type=seeker" class="btn">{{__('Register Here')}}</a></div>

									

									

								</div>

							</div>
                        </div>

                        <div id="employer" class="formpanel tab-pane fade {{($c_or_e == 'employer')? 'active':''}}">

							

							<div class="row">

								<div class="col-lg-7">

									<div class="loginboxint">

                                        <div class="socialLogin">

                                        <h5>{{__('Login with Social')}}</h5>

                                        <a onclick="set_employer('employer')" href="{{ url('login/employer/linkedin')}}" class="linkedin">Login With Social Media <span><i class="fab fa-linkedin-in"></i></span></a> </div>
                                        
                                        <div id="otpCompanyForm" style="display: none">
                                            <div class="formrow font-weight-bold">
                                                Please enter the One Time Password(OTP) in the field below. This would have been sent to your registered mobile number ******<span id="companyLastPhoneNumber"></span>
                                            </div>
                                            <div class="formrow">
                                                <span id="companyResendOTPCaptcha"></span>
                                                <a href="#" class="float-right" id="companyResendOTPBtn">Resend OTP?</a>
                                                <label for="otp_code">{{ __('Enter OTP') }}</label>
                                                <input type="number" name="otp_code" id="company_otp_code" class="form-control" required>
                                            </div>
                                            <p class="text-center otp-counter-text" style="margin: 10px 0;"></p>
                                            <input type="button" class="btn" id="companyOtpVerifyBtn" value="{{__('Verify')}}">
                                        </div>
                            <form class="form-horizontal" method="POST" action="{{ route('company.login') }}" id="companyLoginForm">

                                {{ csrf_field() }}

                                <input type="hidden" name="candidate_or_employer" value="employer" />
                                <input type="hidden" name="verified" value="{{ $setting->verify_registration_phone_number ? 0 : 1 }}" id="loginCompanyVerified" />

                                <div class="formpanel">

                                    <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="">{{__('Username')}}</label>
                                        <input id="emailCompany" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="{{__('Email Address')}}">

                                        @if ($errors->has('email'))

                                        <span class="help-block">

                                            <strong>{{ $errors->first('email') }}</strong>

                                        </span>

                                        @endif

                                    </div>

                                    <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
			                            <label for="">{{__('Password')}}</label>
                                        <input id="passwordCompany" type="password" class="form-control" name="password" value="" placeholder="{{__('Password')}}">

                                        @if ($errors->has('password'))

                                        <span class="help-block">

                                            <strong>Password Required </strong>

                                        </span>

                                        @endif

                                    </div>    

									<div class="forgotpass"><i class="fa fa-user" aria-hidden="true"></i> {{__('Forgot Your Password')}}? <a href="{{ route('company.password.request') }}">{{__('Click here')}}</a></div>

									

                                    <input type="submit" class="btn" value="{{__('Login')}}">

                                </div>

                                <!-- login form  end--> 

                            </form>

                            <!-- sign up form -->

                    

                    

                    <!-- sign up form end-->

								

							</div>

								</div>

								<div class="col-lg-5">

								

								<div class="newuser">

									<h3>{{get_widget(18)->heading}}</h3>                                   

                                    {!!get_widget(18)->content!!}								

								<a href="{{route('register')}}?type=emp" class="btn">{{__('Register Here')}}</a></div>

								</div>

							</div>

							

							

                        </div>

                    </div>

                    <!-- login form -->



                     



                </div>

            </div>

        

    </div>

</div>