@extends('layouts.app')
@push('styles')
<link href="{{asset('admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin_assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
<style type="text/css">
    .error{color: red !important}
</style>
@endpush
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('Change Password')]) 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row">
            @include('includes.user_dashboard_menu')

            <div class="col-md-9 col-sm-8"> 
              
                        
                        <div class="userccount">
                            <div class="formpanel mt0">
                               <h5>{{__('Change Password')}}</h5>
                               <form class="form-horizontal" method="POST" action="{{ route('changePassword') }}">
                                {{ csrf_field() }}
                                <div class="row">

                                <div class="col-md-12">

                                        <div class="formrow">

                                            <label for="">{{__('Current Password')}}</label>

                                            <input id="current_password" type="password" maxlength="40" class="form-control" name="current_password" required oninvalid="this.setCustomValidity('Enter Current Password')" oninput="setCustomValidity('')">

                                            @if ($errors->has('current_password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('current_password') }}</strong>
                                                </span>
                                            @endif 
                                    </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="formrow">

                                            <label for="">{{__('New Password')}}</label>

                                            <input id="password" type="password" maxlength="40" class="form-control" name="password" required oninvalid="this.setCustomValidity('Enter New Password')" oninput="setCustomValidity('')">

                                            @if ($errors->has('password'))
                                                <span class="help-block error">
                                                   {{ $errors->first('password') }}
                                                </span>
                                            @endif 
                                    </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="formrow">

                                            <label for="">{{__('Confirm New Password')}}</label>

                                            <input id="password_confirmation" maxlength="40" type="password" class="form-control" name="password_confirmation" required oninvalid="this.setCustomValidity('Confirm New Password')" oninput="setCustomValidity('')">

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block error">
                                                   {{ $errors->first('password_confirmation') }}
                                                </span>
                                            @endif 
                                    </div>

                                    </div>

                                    <div class="col-md-12">

                                    <div class="formrow"><button type="submit" class="btn">{{__('Change Password')}}  <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button></div>

                                </div>

                                    

                                </div>  
                                </form>                           
                            </div>
                        </div>
                        
                         
                        
                        
                        
                        
                        
                        
                        
                        
            
            </div>
        </div>
    </div>  
</div>


@include('includes.footer')
@endsection
@push('scripts')
@include('includes.immediate_available_btn')
<script type="text/javascript">
    @if(Session::get('message.added'))
            const el = document.createElement('div')
            el.innerHTML = 'Password Changed Successfully';
            swal({

                title: "Password Changed",

                content: el,

                icon: "success",

                button: "OK",

            });
        @endif
</script>

@endpush