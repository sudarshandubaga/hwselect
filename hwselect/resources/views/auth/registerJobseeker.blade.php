@extends('layouts.app')



@section('content') 

@push('styles')
<style type="text/css">
    .select2-container .select2-selection--single {
    height: 45px !important;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 9px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 43px !important;
}

.form-control.error{
    border-color: red !important;
    color: #495057 !important;
}
.error {
    color: red !important;
}

.disabled {
    pointer-events: none;
}
</style>
@endpush



<!-- Header start --> 



@include('includes.header') 



<!-- Header end --> 



<!-- Inner Page Title start --> 



@include('includes.inner_page_title', ['page_title'=>__('Register')]) 



<!-- Inner Page Title end -->



<div class="listpgWraper">



    <div class="container">



        @include('flash::message')



        

<?php $country = App\Country::select('countries.flag', 'countries.id')->whereNotIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();
    
    $country2 = App\Country::select('countries.flag', 'countries.id')->whereIn('countries.id', [230,231])->isDefault()->active()->sorted()->pluck('countries.flag', 'countries.id')->toArray();

 ?>

           <div class="useraccountwrap" style="max-width: 800px;">

				<div class="userbtns">

                        <ul class="nav nav-tabs">

                             <?php
                            if(request()->type=='emp'){
                                $c_or_e = 'employer';
                            }else{
                                $c_or_e = old('candidate_or_employer', 'candidate');
                            }
                            

                            ?>

                            <li class="nav-item"><a class="nav-link {{($c_or_e == 'candidate')? 'active':''}}" data-toggle="tab" href="#candidate" aria-expanded="true">{{__('Register as a Jobseeker')}}</a></li>

                            <li class="nav-item"><a class="nav-link {{($c_or_e == 'employer')? 'active':''}}" data-toggle="tab" href="#employer" aria-expanded="false">{{__('Register as a Company')}}</a></li>

                        </ul>

                    </div>

			   

                <div class="userccount">

                    <div class="tab-content">

                        <div id="candidate" class="formpanel tab-pane {{($c_or_e == 'candidate')? 'active':''}}">
							
							<div class="regtxt">
								{!!get_widget(20)->content!!}	
							</div>
							
							<div class="reqflds">All fields are required <strong>*</strong></div>
							
							
                            <form class="form-horizontal candidate_form" id="candidate_form" method="POST" action="{{ route('register') }}">

                                {{ csrf_field() }}

                                <input type="hidden" name="candidate_or_employer" value="candidate" />

                                <input type="hidden" name="auth_time" id="auth_time" />
                                <input type="hidden" name="user_id" id="user_id" />



                                <div class="formrow{{ $errors->has('first_name') ? ' has-error' : '' }}">
								<label for="">First Name <strong>*</strong></label>	
                                    <input type="text" data-validation="alpha"  maxlength="20"  minlength="2" name="first_name" class="form-control first_name"  placeholder="{{__('First Name')}}" value="{{isset(session()->get('seeker_form_data')['first_name'])?session()->get('seeker_form_data')['first_name']:old('first_name')}}">
                                    @if ($errors->has('first_name')) <span class="help-block"> <strong>{{ $errors->first('first_name') }}</strong> </span> @endif </div>
								
                                


                                <div class="formrow{{ $errors->has('last_name') ? ' has-error' : '' }}">
									<label for="">Last Name <strong>*</strong></label>	
                                    <input type="text"  name="last_name" maxlength="20" minlength="2" class="form-control last_name"  placeholder="{{__('Last Name')}}" value="{{isset(session()->get('seeker_form_data')['last_name'])?session()->get('seeker_form_data')['last_name']:old('last_name')}}">
                                    @if ($errors->has('last_name')) <span class="help-block"> <strong>{{ $errors->first('last_name') }}</strong> </span> @endif </div>

                                 

                                <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
									<label for="">Enter Email Address <strong>*</strong></label>	

                                    <input type="email" name="email" id="email" class="form-control email_check_seeker"  placeholder="{{__('Enter Email Address')}}" value="{{isset(session()->get('seeker_form_data')['email'])?session()->get('seeker_form_data')['email']:old('email')}}" maxlength="50">
                                    <span class="messages"></span>
                                    @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>

                                    
                                     <div class="formrow{{ $errors->has('email_confirmation') ? ' has-error' : '' }}">
									<label for="">Confirm Email Address <strong>*</strong></label>
                                    <input type="email" name="email_confirmation" id="confirm-email" class="form-control"  placeholder="{{__('Confirm Email Address')}}" value="{{isset(session()->get('seeker_form_data')['email_confirmation'])?session()->get('seeker_form_data')['email_confirmation']:old('email_confirmation')}}" maxlength="50">
                                    <span class="messages"></span>
                                    @if ($errors->has('email_confirmation')) <span class="help-block"> <strong>{{ $errors->first('email_confirmation') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
									<label for="">Password <strong>*</strong></label>	
                                    <input type="password" name="password" id="password" class="form-control password"  placeholder="{{__('Password')}}" value="{{isset(session()->get('seeker_form_data')['password'])?session()->get('seeker_form_data')['password']:old('password')}}" passwordCheck="passwordCheck" maxlength="40">
                                    <span class="messages"></span>
                                    @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>



                                <div class="formrow{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
									<label for="">Confirm Password <strong>*</strong></label>	

                                    <input type="password" name="password_confirmation" id="confirm-password" class="form-control"  placeholder="{{__('Password Confirmation')}}" value="{{isset(session()->get('seeker_form_data')['password_confirmation'])?session()->get('seeker_form_data')['password_confirmation']:old('password_confirmation')}}" maxlength="40">
                                    <span class="messages"></span>
                                    @if ($errors->has('password_confirmation')) <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span> @endif </div>

                                    <div class="formrow{{ $errors->has('phone') ? ' has-error' : '' }}"> 
                                <label for="">Phone <strong>*</strong></label>									  
                               
                                <div class="step_1">
								<div class="input-group">
                                    <select class="form-control vodiapicker" value="{{isset(session()->get('seeker_form_data')['last_name'])?session()->get('seeker_form_data')['last_name']:old('last_name')}}" name="phone_code" id="phone_code">

                @if(null!==($country2))
                @foreach($country2 as $key => $count2)

                <?php 
                $detail = DB::table('countries_details')->where('id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
				@endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)

                <?php 
                $detail = DB::table('countries_details')->where('country_id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>

              <input type="hidden" name="phone_verified" id="phone_verified" value="<?php if($siteSetting->verify_registration_phone_number){echo 'no';}else{echo 'yes';} ?>">
              <div class="lang-select">
                
                <a  class="btn-select cselect" value="{{old('phone_num_code')?old('phone_num_code'):null}}">
                    
                	<?php 

                    if(old('phone_num_code')!=''){
                        $phone_detail = DB::table('countries_details')->where('phone_code',old('phone_num_code'))->first();

                    }else{
                       $phone_detail = DB::table('countries_details')->where('id',230)->first();  
                    }


                  
                   /*if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }*/
                    

                    ?>


                   <?php 

                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>
                    @endif
					
					<i class="fas fa-caret-down"></i>

                </a>
                <input type="hidden" id="phone_num_code" name="phone_num_code" value="{{old('phone_num_code')?old('phone_num_code'):'+'.$phone_detail->phone_code}}">
                <div class="b"> 
                    <input type="text" onkeyup="myFunction()" name="search" id="search" placeholder="Search"  style="width: 100px;">
                <ul id="a">
                    
                </ul>
                </div>
                </div>
              {!! Form::text('phone', '+'.$phone_detail->phone_code, array('onchange'=>"staticData()" ,'onkeyup'=>"staticData()",'class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('Phone'), 'maxlength'=>18)) !!}
              @if($siteSetting->verify_registration_phone_number) 
								<div class="input-group-append">
   <a href="javascript:;" class="phone_verify_btn verify btn disabled" id="phone_verify" >Verify</a>
   <span id="refresh-form"><a href="javascript:;" class="refresh-form btn" title="If you have entered your mobile number incorrectly, change the number and click refresh button"><i class="fas fa-sync-alt"></i></a></span>
  </div>
  @endif
                                  
								  </div>
                              </div>
                              
							    <div class="step_2" style="display: none;">
									<div class="form-wrap">
									  <label for="title">SMS Code</label>
										<div class="input-group">
									  <input placeholder="Enter SMS code" type="text" class="phone_code_input form-control" maxlength="12">
									                 
								<div class="input-group-append send">
									<a href="javascript:;" class="phone_code_verify_btn btn">Verify Code</a>
									
								</div>
								</div>
							</div>
										</div>
                                  
                                   
                                	 @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif 
                                	
                               
                                				<p id="phone_text" style="text-align: left;"> @if(!$siteSetting->verify_registration_phone_number)
                                                    Please enter a valid contact phone number
                                                    @else
                                                    Please make sure you enter your mobile number correctly, this number is used to send a verification code when logging into your account.
                                                    @endif </p>					  
                                									  
                                </div>
								
@if($siteSetting->is_newsletter_active==1)
                                    <div class="formrow{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">



    <?php



	$is_checked = '';



	if (old('is_subscribed', 1)) {



		$is_checked = 'checked="checked"';



	}

	if(isset(session()->get('seeker_form_data')['is_subscribed']) && session()->get('seeker_form_data')['is_subscribed']==1){
		$is_checked = 'checked="checked"';
	}



	?>



                                    



                                    <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} /> 
                                    {{__('Subscribe to Newsletter')}}



                                    @if ($errors->has('is_subscribed')) <span class="help-block"> <strong>{{ $errors->first('is_subscribed') }}</strong> </span> @endif </div>
                                    @endif


                                <div class="formrow accepttxt {{ $errors->has('terms_of_use') ? ' has-error' : '' }}">
                                    <input type="checkbox" value="1" name="terms_of_use" />
                                    I accept
                                    <a href="{{url('terms-and-conditions')}}" target="_blank">{{__(' Terms and Conditions')}}</a> and <a href="{{url('privacy-policy')}}" target="_blank">{{__('Privacy Policy')}}</a>

                                    @if ($errors->has('terms_of_use')) <span class="help-block"> <strong>{{ $errors->first('terms_of_use') }}</strong> </span> @endif </div>

                                    <input type="hidden" id="g-recaptcha-response1" name="g-recaptcha-response">

                                <input type="submit" class="btn form_submit" value="{{__('Register')}}">
                            </form>
                        </div>



                        <div id="employer" class="formpanel tab-pane fade {{($c_or_e == 'employer')? 'active':''}}">
							
							<div class="regtxt">
								{!!get_widget(21)->content!!}	
							</div>
							
							<div class="reqflds">All fields are required <strong>*</strong></div>
							
                            <form class="form-horizontal employer_form" method="POST" id="employer_form" action="{{ route('company.register') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="candidate_or_employer" value="employer" />
                                <input type="hidden" name="auth_time" id="auth_time_emp" />
                                <input type="hidden" name="user_id" id="user_id_emp" />
                                <div class="formrow{{ $errors->has('name') ? ' has-error' : '' }}">
									<label for="">Company Name <strong>*</strong></label>
                                    <input type="text" name="name" class="form-control" id="company_name" placeholder="{{__('Company Name')}}" value="{{isset(session()->get('emp_form_data')['name'])?session()->get('emp_form_data')['name']:old('name')}}" maxlength=50>
                                    @if ($errors->has('name')) <span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span> @endif </div>

                            <div class="formrow{{ $errors->has('ceo') ? ' has-error' : '' }}">
                                    <label for="">Contact Name <strong>*</strong></label>
                                    {!! Form::text('ceo', null, array('class'=>'form-control', 'id'=>'ceo', 'placeholder'=>__('Primary Point of Contact'),'maxlength'=>25)) !!}
                                    @if ($errors->has('ceo')) <span class="help-block"> <strong>{{ $errors->first('ceo') }}</strong> </span> @endif </div>

                               

                                <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
									<label for="">Enter Email Address <strong>*</strong></label>	

                                    <input type="email" name="email" id="email_emp" class="form-control email_check"  placeholder="{{__('Enter Email Address')}}" value="{{isset(session()->get('emp_form_data')['email'])?session()->get('emp_form_data')['email']:old('email')}}" maxlength="50">
                                    <span class="messages"></span>
                                    @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>

                                    
                                     <div class="formrow{{ $errors->has('email_confirmation') ? ' has-error' : '' }}">
										 <label for="">Confirm Email Address <strong>*</strong></label>
                                    <input type="email" name="email_confirmation" id="confirm-email-emp" class="form-control"  placeholder="{{__('Confirm Email Address')}}" value="{{isset(session()->get('emp_form_data')['email_confirmation'])?session()->get('emp_form_data')['email_confirmation']:old('email_confirmation')}}" maxlength="50">
                                    <span class="messages"></span>
                                    @if ($errors->has('email_confirmation')) <span class="help-block"> <strong>{{ $errors->first('email_confirmation') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
									<label for="">Password <strong>*</strong></label>	
                                    <input type="password" name="password" id="password_emp" class="form-control password"  placeholder="{{__('Password')}}" value="{{isset(session()->get('emp_form_data')['password'])?session()->get('emp_form_data')['password']:old('password')}}" passwordCheck="passwordCheck" maxlength="40">
                                    <span class="messages"></span>
                                    @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>



                                <div class="formrow{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
									<label for="">Confirm Password <strong>*</strong></label>	

                                    <input type="password" name="password_confirmation" id="confirm-password" class="form-control"  placeholder="{{__('Password Confirmation')}}" value="{{isset(session()->get('emp_form_data')['password_confirmation'])?session()->get('emp_form_data')['password_confirmation']:old('password_confirmation')}}" maxlength="40">
                                    <span class="messages"></span>
                                    @if ($errors->has('password_confirmation')) <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span> @endif </div>

<div class="formrow{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                                  
                                <label for="">Phone <strong>*</strong></label>									  
                                
                                    <div class="step_1_emp">
										<div class="input-group">
                                    <select class="form-control vodiapicker_mobile_num" name="phone_code_mobile_num" id="phone_code_mobile_num">
                 @if(null!==($country2))
                @foreach($country2 as $key => $count2)
                <?php 
                $detail = DB::table('countries_details')->where('id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count2}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


                @if(null!==($country))
                @foreach($country as $key => $count)
                <?php 
                $detail = DB::table('countries_details')->where('id',$key)->first();
                ?>
                @if(null!==($detail) && $detail->sort_name)
                <option <?php if(old('phone_num_code')==$detail->sort_name){echo 'selected';} ?> value="{{$detail->sort_name}}" data-val="+{{$detail->phone_code}}" data-thumbnail="{{asset('flags/')}}/{{$count}}">{{$detail->sort_name}}</option>
                @endif
                @endforeach
                @endif


              </select>
              <div class="lang-select-mobile_num">
               
                <a  class="btn-select-mobile_num cselect" value="{{old('mobile_num_code')?old('mobile_num_code'):null}}">
                     <?php 

                    if(old('mobile_num_code')!=''){
                        $phone_detail = DB::table('countries_details')->where('phone_code',old('mobile_num_code'))->first();

                    }else{
                       $phone_detail = DB::table('countries_details')->where('id',230)->first();  
                    }

                    //$phone_detail = DB::table('countries_details')->where('id',230)->first(); 
                  
                   if(null!==($phone_detail)){
                        $countr = DB::table('countries')->where('country_id',$phone_detail->id)->first();
                    }
                    

                    ?>
                    @if(null!==($phone_detail))
                    <img src="{{asset('flags/')}}/{{$countr->flag}}" alt=""> <span>{{$phone_detail->sort_name}}</span>
                    @endif
                   
					<i class="fas fa-caret-down"></i>


                </a>
                 <input type="hidden" id="mobile_num_code" name="mobile_num_code" value="{{old('mobile_num_code')?old('mobile_num_code'):'+'.$phone_detail->phone_code}}">
                <div class="b-mobile_num">
                    <input type="text" onkeyup="myFunction_emp()" name="search" id="search_emp" placeholder="Search"  style="width: 100px;">
                <ul id="a-mobile_num"></ul>
                </div>
                </div>
              {!! Form::text('phone', '+'.$phone_detail->phone_code, array('onchange'=>"staticDataMobile()" ,'onkeyup'=>"staticDataMobile()",'class'=>'form-control', 'id'=>'mobile_num', 'placeholder'=>__('Phone') , 'maxlength'=>18)) !!}
                         
                         <input type="hidden" name="mobile_verified" id="mobile_verified" value="<?php if($siteSetting->verify_registration_phone_number){echo 'no';}else{echo 'yes';} ?>">  
@if($siteSetting->verify_registration_phone_number)                              
<div class="input-group-append">
   <a href="javascript:;" class="phone_verify_btn_emp verify btn disabled" id="phone_verify" >Verify</a>
   <span id="refresh-form-emp"><a href="javascript:;" class="refresh-form-emp btn" title="If you have entered your mobile number incorrectly, change the number and click refresh button"><i class="fas fa-sync-alt"></i></a></span>
  </div>
  @endif	
										
										
                                  </div> 
                              </div>
									
									
									
									
                                  <div class="step_2_emp" style="display: none;">
         
               
                    <div class="form-wrap">
                      <label for="title">SMS Code</label>
						<div class="input-group">
                      <input placeholder="Enter SMS code" type="text" class="phone_code_input_emp form-control" maxlength="12">
                    
                 
                <div class="send input-group-append">
                    <a href="javascript:;" class="phone_code_verify_btn_emp btn sbutn">Verify Code</a>
                </div>
					</div>	
				</div>	
									  
								  
									  
									  
									  
									  
									  
            </div>
                                  
                                   
                                	 @if ($errors->has('phone')) <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span> @endif 
                                	
                               
                                				<p id="mobile_code_text" style="text-align: left;"> 
                                                    @if(!$siteSetting->verify_registration_phone_number)
                                                    Please enter a valid contact phone number
                                                    @else
                                                    Please make sure you enter your mobile number correctly, this number is used to send a verification code when logging into your account.
                                                    @endif 
        </p>					  
                                									  
                                </div>
                                @if($siteSetting->is_newsletter_active==1)
                                    <div class="formrow{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">



    <?php



	$is_checked = '';



	if (old('is_subscribed', 1)) {



		$is_checked = 'checked="checked"';



	}



	?>



                                    



                                    <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} /> 
                                    {{__('Subscribe to Newsletter')}}



                                    @if ($errors->has('is_subscribed')) <span class="help-block"> <strong>{{ $errors->first('is_subscribed') }}</strong> </span> @endif </div>

                                    @endif



                                <div class="formrow accepttxt{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">



                                    <input type="checkbox" value="1" name="terms_of_use" />




I accept  <a href="{{url('terms-and-conditions')}}" target="_blank">{{__(' Terms and Conditions')}}</a> and <a href="{{url('privacy-policy')}}" target="_blank">{{__('Privacy Policy')}}</a>





                                    @if ($errors->has('terms_of_use')) <span class="help-block"> <strong>{{ $errors->first('terms_of_use') }}</strong> </span> @endif </div>



                                <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">



                                <input type="submit" class="btn" value="{{__('Register')}}">



                            </form>



                        </div>



                    </div>



                    <!-- sign up form -->



                    <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('Have an Account')}}? <a href="{{route('loginJobseeker')}}">{{__('Sign In')}}</a></div>



                    <!-- sign up form end--> 







                </div>



            </div>



        



    </div>



</div>



@include('includes.footer')



@endsection 

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-auth.js"></script>
<script src='https://www.google.com/recaptcha/api.js?render=6LdwGMkdAAAAAG8pRmWUj4iiFtK30Ik_uIGzOQC8'></script>

<script type="text/javascript">
grecaptcha.ready(function()
{
grecaptcha.execute('6Lc5oYAcAAAAAG0O0XFb96mYC7u_3HFkGnMw0N-D', {action: 'action_name'})
.then(function(token)
{
$("#g-recaptcha-response1").val(token);
console.log(token);

});
});
</script>

<script type="text/javascript">
grecaptcha.ready(function()
{
grecaptcha.execute('6Lc5oYAcAAAAAG0O0XFb96mYC7u_3HFkGnMw0N-D', {action: 'action_name'})
.then(function(token)
{
$("#g-recaptcha-response").val(token);
console.log(token);

});
});
</script>

<script type="text/javascript">

        $('#phone').on('keyup',function(){
          var val = $(this).val();
          if(val.charAt(0)!='+'){
            swal("Error", "You must put the + sign at the beginning of each phone number, including country code and your number", "error");
            $(this).val('');
          }
       }) 

        $('#mobile_num').on('keyup',function(){
          var val = $(this).val();
          if(val.charAt(0)!='+'){
            swal("Error", "You must put the + sign at the beginning of each phone number, including country code and your number", "error");
            $(this).val('');
          }
       })

        $('#phone_code_seeker').select2();



        if ($(".candidate_form").length > 0) {
            var msg = 'Please Enter a Valid Mobile Number and Click Verify to Continue';
            @if(!$siteSetting->verify_registration_phone_number)
                var msg = 'Please enter a valid Mobile Number and click register to continue';
            @endif
            $(".candidate_form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    first_name: {
                        required: true,
                        maxlength: 30,
                        minlength: 2,
                        digits: false,
                    }, 
                   
                    last_name: {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    },
                    phone: {
                        required: true,
                        minlength: 11,
                    },

                    email: {
                        required: true,
                        email:true,
                        remote: {
                            type: 'get',
                            url: "/check-email?type=seeker",
                            data: {
                              email: function() {
                                return $( "#email" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                    },

                    terms_of_use: {
                        required: true,
                    },

                    email_confirmation: {
                        equalTo: "#email"
                    },
                    password: {
                    	minlength: 8,
                        required: true,
                    }, 

                   
                    password_confirmation: {
                        equalTo: "#password"
                    }
                },
                messages: {

                    first_name: {
                        required: "First Name Required",
                    },

                   

                    last_name: {
                        required: "Last Name Required",
                    },

                    email: {
                        required: "Email Required",
                        remote: "Email Already Taken"
                    }, 


                    terms_of_use: {
                        required: "You must tick the box to agree you have read and accept our terms of use",
                    }, 

                    password: {
                        required: "Password Required",
                    },

                    phone: {
                        required: "Phone Number Required",
                        minlength: msg,
                    },
                    password_confirmation: "Password & Confirm Password Fields Do Not Match",
                    email_confirmation: "Email & Confirm Email Do Not Match"

                },
            
            })
        }


        $('.candidate_form').on('submit',function(){
        	var phone_verified = $('#phone_verified').val();
            if ($(".step_2").is(':visible'))
            {
                
                 swal({

                title: "Error",

                text: 'Please verify sms code',

                icon: "error",

                button: "OK",

            });
                return false; 
            }
        	if(phone_verified!='yes'){
        		
        		 swal({

                title: "Error",

                text: 'Phone Number verification is incomplete. Please Verify your phone number to continue.',

                icon: "error",

                button: "OK",

            });
        		return false;
        	}
        })


        $('.employer_form').on('submit',function(){
        	var mobile_verified = $('#mobile_verified').val();
        	
        	if ($(".step_2_emp").is(':visible'))
            {
                
                
                 swal({

                title: "Error",

                text: 'Please verify sms code',

                icon: "error",

                button: "OK",

            });
                return false; 
            }
            if(mobile_verified!='yes'){
        		 swal({

                title: "Error",

                text: 'Phone Number verification is incomplete. Please Verify your phone number to continue.',

                icon: "error",

                button: "OK",

            });
        		return false;
        	}
        })


        if ($(".employer_form").length > 0) {
            var msg = 'Please Enter a Valid Mobile Number and Click Verify to Continue';
            @if(!$siteSetting->verify_registration_phone_number)
                var msg = 'Please enter a valid Mobile Number and click register to continue';
            @endif
            $(".employer_form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    name: {
                        required: true,
                        maxlength: 50,
                        minlength: 2,
                    }, 

                    ceo: {
                        required: true,
                        maxlength: 50,
                        minlength: 2,
                    }, 
                   
                    phone: {
                        required: true,
                        minlength: 11,
                    },

                    email: {
                        required: true,
                        email:true,
                        remote: {
                            type: 'get',
                            url: "/check-email?type=emp",
                            data: {
                              email: function() {
                                return $( "#email_emp" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                        
                    },

                    terms_of_use: {
                        required: true,
                    },

                    contact_details: {
                        required: true,
                    },

                    email_confirmation: {
                        equalTo: "#email_emp"
                    },
                    password: {
                    	minlength: 8,
                        required: true,
                    }, 

                   
                    password_confirmation: {
                        equalTo: "#password_emp"
                    }
                },
                messages: {

                    name: {
                        required: "Company Name Required",
                    }, 

                    ceo: {
                        required: "Contact Name Required",
                    },


                    email: {
                        required: "Email Required",
                        remote: "Email Already Taken"
                    }, 

                     terms_of_use: {
                        required: "You must tick the box to agree you have read and accept our terms of use",
                    }, 

                    password: {
                        required: "Password Required",
                    },

                    contact_details: {
                        required: "Contact Details Required",
                    },

                    phone: {
                        required: "Phone Number Required",
                        minlength: msg,
                    },
                    password_confirmation: "Password & Confirm Password Fields Do Not Match",
                    email_confirmation: "Email & Confirm Email Do Not Match"

                },
            
            })
        }

        $('.password').on('change',function(){
        	jQuery.validator.addMethod("passwordCheck",
        function(value, element, param) {
            if (this.optional(element)) {
                return true;
            } else if (!/[A-Z]/.test(value)) {
                return false;
            } else if (!/[a-z]/.test(value)) {
                return false;
            } else if (!/[0-9]/.test(value)) {
                return false;
            }

            return true;
        },
        "Password must include one uppercase one lowercase and one numeric number");
        })

        

        $('.first_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z \s]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });

        

        $('.last_name').keypress(function (e) {
            var regex = new RegExp("^[a-z A-Z]*\-?[a-zA-Z]*$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });


        $('#phone').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
		        return false;
		    return true;
        });

        $('#mobile_num').keypress(function (evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
		        return false;
		    return true;
        });


       /* $('#company_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z0-9 \s]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            return false;
            }
        });*/


         @if(session()->has('message.added'))

            swal({

                title: "Confirmation",

                text: 'Thank you for registering an Account with HW Select. Please Check your email to verify account',

                icon: "success",

                button: "OK",

            });
        @endif

        @if(session()->has('message.not_verified_seeker'))

            swal({

                title: "Not Verified",

                text: 'Please Verify your Number to Continue.',

                icon: "error",

                button: "OK",

            });
        @endif

        @if(session()->has('message.not_verified_emp'))

            swal({

                title: "Not Verified",

                text: 'Please Verify your Number to Continue.',

                icon: "error",

                button: "OK",

            });
        @endif


        var config = {
            // apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
            // authDomain: "hwselect-62f4f.firebaseapp.com",
            // databaseURL: "https://hwselect-62f4f.firebaseio.com",
            // projectId: "hw-select-uk",
            // storageBucket: "hwselect-62f4f.appspot.com",
            // messagingSenderId: "269417794687",
            // appId: "1:401245972574:web:207e49cc038d66ee976ccf",
            // measurementId: "G-TSVHYD80QW"
            apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
            authDomain: "hw-select-uk.firebaseapp.com",
            projectId: "hw-select-uk",
            storageBucket: "hw-select-uk.appspot.com",
            messagingSenderId: "269417794687",
            appId: "1:269417794687:web:b8aee097fea11a0895434a"
        };

  firebase.initializeApp(config);

  function isPhoneNumberValid(phoneNumber) {
    var pattern = /^\+[0-9\s\-\(\)]+$/;
    return phoneNumber.search(pattern) !== -1;
  }
  
  $('#phone').on('keyup',function(){
  		var length = $(this).val().length;
  		
  		if(length>=13){
  			$('.phone_verify_btn').removeClass("disabled");
  		}else{
  			$('.phone_verify_btn').addClass("disabled");
  		}
  })


  $('#mobile_num').on('keyup',function(){
  		var length = $(this).val().length;
  		
  		if(length>=13){
  			$('.phone_verify_btn_emp').removeClass("disabled");
  		}else{
  			$('.phone_verify_btn_emp').addClass("disabled");
  		}
  })
  $(document).on("click", ".phone_verify_btn.verify", function(){
    $(this).removeClass("verify");
    var uphone = $.trim($("#phone").val());
    //alert(uphone);
    window.uphone = uphone;
    if(true){
      var appVerifier = new firebase.auth.RecaptchaVerifier('phone_verify', { 'size': 'invisible' });
      $(this).attr("disabled", "disabled");
      firebase.auth().signInWithPhoneNumber(uphone, appVerifier)
        .then(function (confirmationResult) {
          //console.log(confirmationResult);
          window.confirmationResult = confirmationResult;
          $(".step_1").append('<div style="margin-top:5px;" class="alert alert-success alert-dismissible">\
  <button type="button" class="close" data-dismiss="alert">&times;</button>\
  <strong>Success!</strong> Code sent Successfully. Please enter SMS code below and click - VERIFY CODE.\
</div>');
          $(".step_2").show();
          $('.phone_verify_btn').removeAttr("disabled");
          $('.phone_verify_btn').html('');
          $('#phone_text').html('After verifying your Mobile Number you will receive an SMS CODE, enter this code into the SMS field above and click verify code to continue with registration');
          $('.phone_verify_btn').html('Resend Code');
          //console.log("verification code sent");
        }).catch(function (error) {
          console.error('Error during signInWithPhoneNumber', error);
        });
    } else { alert("Please Enter a Valid Mobile Number and Click Verify to Continue"); $(this).addClass("verify"); }
  })

  
  $(document).on("click", ".phone_code_verify_btn", function(){
      var code = $.trim($(".phone_code_input").val());
      if ( code.length > 3 ) {
        confirmationResult.confirm(code).then(result=>{
          //console.log(result.user['xa']);
          $('#user_id').val(result.user['uid']);
          //console.log(result.user);
          $.ajax({
           type: "POST",
           url: "{{route('verify_token')}}",
           data: {
	        	"_token": "{{ csrf_token() }}",
	        	"csrf_token": result.user['xa']
	        },
	        success: function(result){
			    $('#auth_time').val(result);
                 var first_name = $('.first_name').val();
          $(".step_2").hide();
          $(".step_1").show();
          const el = document.createElement('div')
          el.innerHTML ='Mobile Number Verified Successfully <br> <br> Click OK to close this window and then click Register to Continue';
          swal({

                title: "Thank You "+first_name,

                content: el,

                icon: "success",

                button: "OK",

            });
          $('#refresh-form').html('');
          $('#phone').attr('readonly','readonly');

          $('#phone_verified').val('yes');
          $('.phone_verify_btn').removeClass('verify');
          $('.phone_verify_btn').addClass('verified');
          $('.phone_verify_btn').html('Verified');
          $('.phone_verify_btn').addClass('disabled');
			}
         });
         

        })
        .catch(error=>{ alert(error.message);
          console.log(error);
        });
      } else { alert("A valid SMS verification code must be entered"); }
  })


  $(document).on("click", ".phone_verify_btn_emp.verify", function(){
    $(this).removeClass("verify");
    var uphone = $.trim($("#mobile_num").val());
    //alert(uphone);
    window.uphone = uphone;
    if(true){
      var appVerifier = new firebase.auth.RecaptchaVerifier('phone_verify', { 'size': 'invisible' });
      $(this).attr("disabled", "disabled");
      firebase.auth().signInWithPhoneNumber(uphone, appVerifier)
        .then(function (confirmationResult) {
          //console.log(confirmationResult);
          window.confirmationResult = confirmationResult;
           $(".step_1_emp").append('<div style="margin-top:5px;" class="alert alert-success alert-dismissible">\
  <button type="button" class="close" data-dismiss="alert">&times;</button>\
  <strong>Success!</strong> Code sent Successfully. Please enter SMS code below and click - VERIFY CODE.\
</div>');
          $(".step_2_emp").show();
          $('#mobile_code_text').html('After verifying your Mobile Number you will receive an SMS CODE, enter this code into the SMS field above and click verify code to continue with registration');
        $('.phone_verify_btn_emp').removeAttr("disabled");
          $('.phone_verify_btn_emp').html('');
          $('.phone_verify_btn_emp').html('Resend Code');
          console.log("verification code sent");
        }).catch(function (error) {
          console.error('Error during signInWithPhoneNumber', error);
        });
    } else { alert("Please Enter a Valid Mobile Number and Click Verify to Continue"); $(this).addClass("verify"); }
  })

  $(document).on("click", ".phone_code_verify_btn_emp", function(){
      var code = $.trim($(".phone_code_input_emp").val());
      if ( code.length > 3 ) {
        confirmationResult.confirm(code).then(result=>{
          $('#user_id_emp').val(result.user['uid']);
          $.ajax({
           type: "POST",
           url: "{{route('verify_token')}}",
           data: {
	        	"_token": "{{ csrf_token() }}",
	        	"csrf_token": result.user['xa']
	        },
	        success: function(result){
			    $('#auth_time_emp').val(result);
                var company_name = $('#company_name').val();
          $(".step_2_emp").hide();
          $(".step_1_emp").show();
          const el = document.createElement('div')
          el.innerHTML ='Mobile Number Verified Successfully <br> <br> Click OK to close this window and then click Register to Continue';
          swal({

                title: "Thank You "+company_name,

                content: el,

                icon: "success",

                button: "OK",

            });
          $('#refresh-form-emp').html('');
          $('#mobile_num').attr('readonly','readonly');
            $('#mobile_verified').val('yes');
            $('.phone_verify_btn_emp').removeClass('verify');
          $('.phone_verify_btn_emp').addClass('verified_emp');
            $('.phone_verify_btn_emp').html('Verified');
          $('.phone_verify_btn_emp').addClass('disabled');
			}
         });
          
        })
        .catch(error=>{ alert(error.message);
          console.log(error);
        });
      } else { alert("A valid SMS verification code must be entered"); }
  })

  var langArray = [];

$('.vodiapicker option').each(function(){
  var img = $(this).attr("data-thumbnail");
  var val = $(this).attr("data-val");
  var text = this.innerText;
  var value = $(this).val();
  var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
  langArray.push(item);
})

$('#a').html(langArray);

//Set the button value to the first el of the array


//change button stuff on click
$('#a li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fas fa-caret-down"></i></li>';
  $('.btn-select').html(item);
  $('.btn-select').attr('value', value);
  $('#phone').val(val);
  $('#phone_num_code').val(val);
  $(".b").toggle();
  //console.log(value);
});

$(".btn-select").click(function(){
        $(".b").toggle();
    });

//  $('#confirm-email').bind("cut copy paste",function(e) {
//      e.preventDefault();
//  });

//  $('#confirm-email-emp').bind("cut copy paste",function(e) {
//      e.preventDefault();
//  });

var langArray_mobile_num = [];
$('.vodiapicker_mobile_num option').each(function(){
  var img = $(this).attr("data-thumbnail");
  var val = $(this).attr("data-val");
  var text = this.innerText;
  var value = $(this).val();
  var item = '<li><img src="'+ img +'" alt="" value="'+value+'" val="'+val+'"/><span>'+ text +'</span></li>';
  langArray_mobile_num.push(item);
})

$('#a-mobile_num').html(langArray_mobile_num);

//Set the button value to the first el of the array

//change button stuff on click
$('#a-mobile_num li').click(function(){
   var img = $(this).find('img').attr("src");
   var value = $(this).find('img').attr('value');
   var val = $(this).find('img').attr('val');
   var text = this.innerText;
   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fas fa-caret-down"></i></li>';
  $('.btn-select-mobile_num').html(item);
  $('.btn-select-mobile_num').attr('value', value);
  $('#mobile_num').val(val);
  $('#mobile_num_code').val(val);
  $(".b-mobile_num").toggle();
  //console.log(value);
});

$(".btn-select-mobile_num").click(function(){
        $(".b-mobile_num").toggle();
    });

//check local storage for the lang
var sessionLang = localStorage.getItem('lang');
if (sessionLang){
  //find an item with value of sessionLang
  var langIndex = langArray.indexOf(sessionLang);
  $('.btn-select-mobile_num').html(langArray[langIndex]);
  $('.btn-select-mobile_num').attr('value', sessionLang);
} else {
   var langIndex = langArray.indexOf('ch');
  console.log(langIndex);
  $('.btn-select-mobile_num').html(langArray[langIndex]);
  //$('.btn-select').attr('value', 'en');
}


function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    ul = document.getElementById("a");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function myFunction_emp() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search_emp");
    filter = input.value.toUpperCase();
    ul = document.getElementById("a-mobile_num");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
function staticData(){
  var data=$("#phone");
  
  	var code = $('#phone_num_code').val();

  if(data.val().length<3)
    {
     data.val(code);
     data.attr("readonly",true);
    }
    else
    {
     data.attr("readonly",false);
    }
}

$('.refresh-form').on('click',function(){
	 $.ajax({
            type: "GET",
            url: "{{route('refresh-form-seeker')}}",
            data: $('#candidate_form').serialize(),
            }).done(function (data) {
                location.reload();
        });
})


$('.refresh-form-emp').on('click',function(){
	 $.ajax({
            type: "GET",
            url: "{{route('refresh-form-emp')}}?type=emp",
            data: $('#employer_form').serialize()+ '&type=emp',
            }).done(function (data) {
                location.href = "{{route('register')}}?type=emp";
        });
})





function staticDataMobile(){
  var data=$("#mobile_num");
  var code = $('#mobile_num_code').val();
  if(data.val().length<3)
    {
     data.val(code);
     data.attr("readonly",true);
    }
    else
    {
     data.attr("readonly",false);
    }
}
	
	
// window.onload = () => {
//  const confirmEamil = document.getElementById('confirm-email-emp');
//  confirmEamil.onpaste = e => e.preventDefault();
// }	
	
</script>
@endpush

