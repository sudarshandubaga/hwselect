@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Dear Admin</h1>
			
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #444;text-align: left; margin:0 0 10px 0;">{{$name}} has successfully <strong style="color: #000;">{{$is_subscribed}}</strong> from the newsletter.</p>
	
		</td>
    </tr>
	
   
	
</table>
@endsection