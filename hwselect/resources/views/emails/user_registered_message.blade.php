@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			
	<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello Admin</h1>


	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px;font-weight:400;color: #333;text-align: left; margin:0 0 0 0;">Date of Registration : {{date('d-M-Y')}}</p>
		
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">Jobseeker "<strong>{{ $name }}</strong>" has registered on "{{ $siteSetting->site_name }}"</p>
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">Profile link:</p>
	
	<a href="{{route('admin.view.public.profile',$user->id)}}" style="background: #17d27c; color: #fff; border: none; font-size: 16px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif; display: inline-block;font-weight: 700; text-decoration: none; height: 45px; line-height: 45px; padding: 0; width: 180px; text-align: center;">View Profile</a>
		
		</td>
    </tr>
 
	
</table>
@endsection