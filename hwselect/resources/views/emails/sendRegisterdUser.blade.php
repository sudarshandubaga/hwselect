@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello {{$name}}</h1>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin: 0;">
			You have successfully registered an account with {{ $siteSetting->site_name }}.
			</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:20px 0 0 0;">
			<strong>Registered Name:</strong> {{ $name }}
			</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:20px 0 0 0;">
			<strong>Email Address: </strong> {{ $email }}
			</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:20px 0 0 0;">
			<strong>Password: </strong> {{ $password }}
			</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:20px 0 0 0; color: #666;">
			Thank you from the team at ({{ $siteSetting->site_name }})
			</p>
			
		</td>
    </tr>
   
	
</table>
@endsection