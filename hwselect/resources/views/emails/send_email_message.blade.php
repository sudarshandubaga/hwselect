@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">
    <tr>
        <td>			
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Dear {{ $name }}</h1>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin: 0;">Someone has sent you a message on your <a href="{{url('/')}}">{{ $siteSetting->site_name }}</a> profle.<br>
			Please <a href="{{route('login')}}">login</a> and respond.</p>
           
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0; color: #777;">
			Thank you from the team at ({{ $siteSetting->site_name }})
			</p>
        </td>
    </tr>  
</table>
@endsection