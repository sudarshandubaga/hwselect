ˀ@extends('admin.layouts.email_template')

@section('content')

<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">

    <tr>

        <td>
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello Admin</h1>
			
            <p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin: 0;">{{ $name }} has uploaded a new job vacancy – Please See attachment</p>
		
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:5px 0 0 0; color: #555;">Uploaded On: {{date('d-M-Y')}}</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:20px 0 0 0;">
			<strong>Company Name</strong><br>
			{{$name}}
			</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">
			<strong>Email Address</strong><br>
			{{$email}}
			</p>
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">
			<strong>Contact Number</strong><br>
			{{$phone}}
			</p>
			</td>

    </tr>


</table>

@endsection