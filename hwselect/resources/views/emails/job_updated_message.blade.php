@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">
    <tr>
        <td>			
		@if(null!==($update_fields))
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="font-family: Arial, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'">
		<tbody>		
		<tr>
		<td>			
		<!-- content -->
		<table align="center" width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tbody>
			
		 
			
			
		<tr>
		<td align="center" bgcolor="#DDDFEC" style="padding:20px 0;">
		<h2 style="margin: 0; font-size: 20px; color: #000;">Details of Job Posting</h2>
		</td>
		</tr>	
			
			
		<tr>
		<td align="center" style="padding:20px 40px 10px 40px;">
		<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello Admin</h1>	
			
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 18px;font-weight:400;color: #444;text-align: left; margin:0 0 0 0;">Job Updated On: {{date('d-M-Y')}}</p>
			
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">Company "{{ $name }}" has updated a job for Admin Approval.</p>
		
		</td>
		</tr>
		<tr>
		<td align="left" style="padding:0 40px 20px 40px;">
			
		<h3 style="font-size: 18px; color: #043C55; margin-bottom: 0;">Job Details:</h3>
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">
			Job Title: {!!$update_fields['title']!!}
		</p>
		<h4 style="font-size: 16px; color: #555; margin-bottom: 10px; margin-top: 5px;">{!!$update_fields['city']!!}, {!!$update_fields['state']!!}, {!!$update_fields['country']!!}</h4>
		<p style="margin-top: 5px; font-size: 14px; color: #555;">Contractual Hours: {!!$update_fields['job_type']!!}</p>
			
		    
			
		<p style="margin-top: 5px; font-size: 14px; color: #555;">Salary From: {!!$update_fields['salary_currency']!!} {!!$update_fields['salary_from']!!} </p>
		
		@if(!empty($update_fields['salary_to']))
		<p style="margin-top: 5px; font-size: 14px; color: #555;">Salary To: {!!$update_fields['salary_currency']!!} {!!$update_fields['salary_to']!!} </p>
		@endif


		<p style="margin-top: 5px; font-size: 14px; color: #555;">Bonus: {!!$update_fields['bonus']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Benifits: {!!$update_fields['benifits']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Skills: {!!$update_fields['skills']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Address: {!!$update_fields['address']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Salary Period: {!!$update_fields['salary_period']!!}</p>		

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Career Level: {!!$update_fields['career_level']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Functional Area: {!!$update_fields['functional_area']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Job Type: {!!$update_fields['job_type']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Job Shift: {!!$update_fields['job_shift']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Num Of Positions: {!!$update_fields['num_of_positions']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Expiry Date: {!!$update_fields['expiry_datee']!!}(s)</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Degree Level: {!!$update_fields['degree_level']!!}</p>

		<p style="margin-top: 5px; font-size: 14px; color: #555;">Job Experience: {!!$update_fields['job_experience']!!}</p>
			
			
		<p style="font-size: 14px; color: #555; line-height: 22px; margin-bottom: 30px;">{!! $update_fields['description'] !!}</p>
			
			

		</td>
		</tr>
		<tr>
		<td style="padding: 20px 40px;">
		<a href="{{ $link }}" style="background-color:#17d27c; color: #fff; padding: 12px 25px; display: inline-block; border-radius: 5px; text-decoration: none;">Login to admin to view job details</a>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- footer_alt -->
		<table align="center" width="650" border="0" cellspacing="0" cellpadding="0">
		<tbody>
		<tr>
		<td class="email_body email_end tc"><table role="presentation" class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
		<tr>
		<td height="16">&nbsp;</td>
		</tr>
		<tr>

		</tbody>
		</table></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>		
		</tbody>
		</table>
		
		@endif
		
			
		 
		</td>
    </tr>
	
</table>
@endsection