@extends('admin.layouts.email_template')

@section('content')

<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello Admin</h1>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin: 0;">{{ $your_name }}" has reported a job on "{{ $siteSetting->site_name }}", you can review job at:</p>
			
			
			<a style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block; margin-top: 10px;" href="{{ $job_url }}">{{ $job_url }}</a>
			

           
		</td>

    </tr>

   

</table>

@endsection