@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 0 0;">Hello Admin</h1>
			
			
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">Company want to delete a job :</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;"><strong>Job Title</strong> : {{$job}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;"><strong>Compny Name</strong> : {{$full_name}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;"><strong>Email</strong> : {{$email}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;"><strong>Subject</strong> : "{{ $subject }}" </p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;"><strong>Reason</strong> : "{{ $reason }}" </p>
			
			
	<a href="{{route('public.job', [$id])}}"  style="background: #17d27c; color: #fff; border: none; font-size: 16px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif; display: inline-block;font-weight: 700; text-decoration: none; height: 45px; line-height: 45px; padding: 0; width: 220px; text-align: center;">View Job Details</a>
		
		</td>
    </tr>
 
	
</table>
@endsection