@extends('admin.layouts.email_template')

@section('content')

<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">

    <tr>

        <td align="left">
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello {{$contact_name}}</h1>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin: 0;">{{$msg}} <strong>{{$title}}</strong> {{$msg2}}</p>

           
	         
			<a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" target="_blank" style="background: #17d27c; color: #fff; border: none; font-size: 16px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif; display: inline-block;font-weight: 700; text-decoration: none; height: 45px; line-height: 45px; padding: 0; width: 150px; text-align: center;">View Job</a>
			
			
			
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:20px 0 0 0;">Please feel free to contact us with any questions you may have.</p>
        @if(isset($contact_phone))
        <p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">Company Number: {{ $contact_phone }}</p>
        @endif
        
        <p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">Thank you from the team at ({{ $siteSetting->site_name }})</p>
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">www.hwselect.com</p>
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">Office Number: {{ $siteSetting->site_phone_primary }}</p>
			
		
		</td>

    </tr>
   
	

</table>

@endsection