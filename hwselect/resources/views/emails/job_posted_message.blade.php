@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			
	<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello Admin</h1>
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #444;text-align: left; margin:10px 0 0 0;">Date Job Posted: {{date('d-M-Y')}}</p>
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">Company "{{ $name }}" has posted a job for vetting</p>   

	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">You must first login to your admin account to be taken directly to the links shown below</p>          
	
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">
	<strong>Job Title:</strong><br>
	{{ $job_title }}
	</p> 
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">	
	<strong>Admin Link: </strong>View Posted Job<br>
	<a href="{{ $link }}" style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block; margin-top: 10px;">{{ $link }}</a>	
	</p> 
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">	
	<strong>Admin link: Edit Posted Job Details</strong><br>
	<a href="{{ $link_admin }}" style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block; margin-top: 10px;">{{ $link_admin }}</a>	
	</p> 
		
		
    </td>


    </tr>
	
	
</table>
@endsection