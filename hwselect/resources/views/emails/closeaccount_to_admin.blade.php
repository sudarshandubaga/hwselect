@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin: 0;">
				{{ $name }} has closed ({{ $type }} Account) <br>
				Email ID: {{ $email }} <br>
				Reason: {{ $reason }} <br>
				Mobile number:  ********{{ substr($phone, -4) }}
			</p>
		</td>
    </tr>
   
	
</table>
@endsection