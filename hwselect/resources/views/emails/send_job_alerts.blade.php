@extends('admin.layouts.email_template')

@section('content')

<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc; padding: 25px;">
	
	<tr>

        <td class="content-wrapper">
			
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">{{count($jobs)}} New Job(s) matching your Job Alert profile has been posted.</p>
			
			
		<?php if(null!==($jobs)){?> 
		<?php foreach ($jobs as $key => $job) {?>
		<?php $company = $job->getCompany(); ?> 
		<?php if(isset($company)){?>
			<div style="border-top: 1px solid #ddd; padding-top: 15px; margin-top: 15px;">
			<a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" target="_blank" data-saferedirecturl="https://www.google.com/url?q={{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block;">{{$job->title}}</a>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">{{$company->name}}</p>
				
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">{{$company->location}}</p>
				
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">{{ \Carbon\Carbon::parse($job->created_at)->diffForHumans()}}</p>
				
			@if(isset($link))
				<a style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block; margin-top: 10px;" href="{{$link}}">Make Disable</a>
			@endif
			</div>
		<?php } ?>
		<?php } ?>
		<?php } ?>
		
		
		</td>

    </tr>

</table>

@endsection