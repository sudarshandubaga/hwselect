@extends('layouts.app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.paginate.css')}}">
@endpush
@section('content') 
<!-- Header start --> 

@include('includes.header') 

<!-- Header end --> 

<!-- Inner Page Title start --> 

@include('includes.inner_page_title', ['page_title'=>__('Dashboard')]) 

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">@include('flash::message')

        <div class="row">

            <div class="col-lg-3 col-md-4">

            <div class="compinbox"> <i class="fas fa-clock" aria-hidden="true"></i>
            <h6><a href="javascript:;" onclick="displayjobs('active')">{{Auth::guard('company')->user()->countOpenJobs()}}</a></h6>
            <strong>{{__('View Active Jobs')}}</strong> </div>
                
             @include('includes.company_dashboard_menu')    

            </div>

            

            <div class="col-lg-9 col-md-8">                 
                <?php $company = Auth::guard('company')->user(); ?>
                <div class="job-header">

            <div class="jobinfo">

                <div class="row">

                    <div class="col-md-7 col-sm-8">

                        <!-- Candidate Info -->

                        <div class="candidateinfo">

                            <div class="userPic"><a href="{{route('company.detail',$company->slug)}}">{{$company->printCompanyImage()}}</a>

                            </div>

                            <div class="title">{{$company->name}}</div>

                            <div class="desi">{{$company->getIndustry('industry')}}</div>

                            <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i>

                                {{__('Member Since')}}, {{$company->created_at->format('d-M-Y')}}</div>

                            <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i>

                                {{$company->location}}</div>

                            <div class="clearfix"></div>

                        </div>

                    </div>

                    <div class="col-md-5 col-sm-4">

                        <!-- Candidate Contact -->

                        @if(!Auth::user() && !Auth::guard('company')->user())

                            <h5>Login to View contact details</h5>

                            <a href="{{route('login')}}" class="btn">Login</a>

                        @else

                        <div class="candidateinfo">

                            @if(!empty($company->phone))

                            <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{{$company->phone}}">{{$company->phone}}</a></div>

                            @endif

                            @if(!empty($company->email))

                            <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:{{$company->email}}">{{$company->email}}</a></div>

                            @endif

                            @if(!empty($company->website))

                            <div class="loctext"><i class="fa fa-globe" aria-hidden="true"></i> <a href="{{$company->website}}" target="_blank">{{$company->website}}</a></div>

                            @endif

                            <div class="cadsocial"> {!!$company->getSocialNetworkHtml()!!} </div>

                        </div>                      

                        @endif

                        

                    </div>

                </div>

            </div>



        </div>
                
                <div class="myads">


                    <div class="jobsbtns">
                        <a href="javascript:;" onclick="displayjobs('all')" class="all-jobs btn-show-jobs">View All &nbsp<span class="badge badge-light">{{count($total)}}</span></a>
                        <a href="javascript:;" onclick="displayjobs('expired')" class="expired-jobs btn-show-jobs" >View Expired &nbsp<span class="badge badge-light">{{$expired}}</span></a>
                        <a href="javascript:;" onclick="displayjobs('active')" class="active-jobs btn-show-jobs">View Active Jobs &nbsp<span class="badge badge-light">{{$active}}</span></a>
                        <a href="javascript:;" onclick="displayjobs('inactive')" class="inactive-jobs btn-show-jobs">View Inactive &nbsp<span class="badge badge-light">{{$inactive}}</span></a>
                    </div>
                    <div class="all_jobs display-jobs">
                     <h3>{{__('All Jobs')}}</h3>
                    
                    
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                         <?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
    if(null!==($job->salary_from)){
$salary = '<strong>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
} 


?>  

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)
                        
                        @if($job->isJobExpired())
                        <li id="job_li_{{$job->id}}" class="jbexpired">
                        @else
                        <li id="job_li_{{$job->id}}">
                        @endif
                            
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                                            @if($job->isJobExpired())
                                             - <span class="expire">Job Expired ({{date('d-M-Y',strtotime($job->expiry_date))}})</span>
                                            @endif
                      @if($job->status=='rejected')
                       -<span>On-hold by admin on {{date('d-M-Y',strtotime($job->updated_at))}}</span>
                      @else

                      @if($job->status=='blocked')
                       - <span>Job Inactive @if(null!==($job->blocked_by) && $job->blocked_by!='')by {{$job->blocked_by}}@endif on {{date('d-M-Y',strtotime($job->updated_at))}}</span> 
                      @endif
                       
                       @if($job->status=='request_to_delete')
                       -<span class="expire">Awaiting admin review for deletion - Requested by {{$company->name}} On {{date('d-M-Y',strtotime($job->updated_at))}}</span> 
                      
                      @else
                      @if($job->status=='admin_review')
                      - <span class="expire">Job Awaiting Approval</span> 
                      @endif
                      @endif
                      @endif
                                        </h3>
                                        <div class="row">
                                            <div class="col-lg-6">
                                            <div class="jobitem itemsalary" title="Salary">{!!$salary!!}
         <?php 
                      $all_bouns = '';
                      if(null!==($job->bonus)){
                        $bon = json_decode($job->bonus);
                        if(null!==($bon)){
                          foreach ($bon as $key => $value) {
                            $bonus = App\Bonus::findorFail($value);
                            $all_bouns .=$bonus->bonus.', ';
                          }
                        }

                      }


                      $all_benifits = '';
                      if(null!==($job->benifits)){
                        $beni = json_decode($job->benifits);
                        if(null!==($beni)){
                          foreach ($beni as $key => $val) {
                            $benifits = App\Benifits::findorFail($val);
                            $all_benifits .=$benifits->benifits.', ';
                          }
                        }

                      }
                     ?>


           - {{$job->getSalaryPeriod('salary_period')}}  @if(null!==($job->bonus)) + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits)) + <span>{{trim($all_benifits,', ')}}</span>@endif 
                                            </div>
                                                <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                                        <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                                            </div>
                                            <div class="col-lg-6">
                                            <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                                        <div class="jobitem" title="Posted Date"><i class="far fa-calendar-alt"></i>
                                            
                                            Posted: {{$job->created_at->format('d-M-Y')}}
                                            
                                            </div>
                                                
                                            <div class="jobitem" title="Job Expires"><i class="far fa-calendar-alt"></i>
                                            
                                            Expires: {{$job->expiry_date->format('d-M-Y')}}
                                            
                                            </div>
                                                
                                                
                                            </div>
                                        </div>                                       

                                    <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                                    </div>
                                     </div>
                                  
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                                      <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                                      <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit- Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                                       <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
                        
                                       @if($job->status=='active')
                                    <a href="javascript:;" onclick="makeNotActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Make Inactive')}}</a>   
                                       @endif

                                       @if($job->status=='blocked')
                     <a href="javascript:;" onclick="makeActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Reactivate')}}</a>
                     @endif
                                 
                                </div>
                            </div>
                            </div>
                        </li>
                        
                        
                        <!-- job end --> 
                        @endif

                        @endforeach
                        @else
                        <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>



                    <div class="expired_jobs display-jobs">
           <h3>{{__('Expired Jobs')}}</h3>
          
          
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                        <?php 

if($job->salary_type == 'single_salary'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else if($job->salary_type == 'salary_in_range'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - '.number_format($job->salary_to).'</strong>';

}else{
    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
} 


?>

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)
            
            @if($job->isJobExpired())
            <li id="job_li_{{$job->id}}" class="jbexpired">
            
              
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                      @if($job->isJobExpired())
                       - <span class="expire">Job Expired ({{date('d-M-Y',strtotime($job->expiry_date))}})</span>
                      @endif
                    </h3>
                    <div class="row">
                      <div class="col-lg-6">
                      <div class="jobitem itemsalary" title="Salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}} @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif 
                                            </div>
                        <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                    <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                      </div>
                      <div class="col-lg-6">
                      <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                    <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>
                      
                      Posted: {{$job->created_at->format('d-M-Y')}}
                      
                      </div>
                          <div class="jobitem" title="Job Expires"><i class="far fa-calendar-alt"></i>
                                            
                                            Expires: {{$job->expiry_date->format('d-M-Y')}}
                                            
                                            </div>
                      </div>
                    </div>                                       

                  <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                  </div>
                   </div>

                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                    <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit - Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                     <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
 
                                 
                                </div>
                            </div>
              </div>
                        </li>
            @endif
            
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
            <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>


                    <div class="active_jobs display-jobs">
           <h3>{{__('Active Jobs')}}</h3>
          
          
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                        <?php 

if($job->salary_type == 'single_salary'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else if($job->salary_type == 'salary_in_range'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - '.number_format($job->salary_to).'</strong>';

}else{
    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
} 


?>

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)
            
            @if($job->status =='active' && !$job->isJobExpired())
            <li id="job_li_{{$job->id}}">
            
              
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                      @if($job->isJobExpired())
                       - <span class="expire">Job Expired</span>
                      @endif
                    </h3>
                    <div class="row">
                      <div class="col-lg-6">
                      <div class="jobitem itemsalary" title="Salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}} @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif 
                                            </div>
                        <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                    <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                      </div>
                      <div class="col-lg-6">
                      <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                    <div class="jobitem" title="Posted Date"><i class="far fa-calendar-alt"></i>                      
                      Posted: {{$job->created_at->format('d-M-Y')}}                      
                      </div>
                          
                    <div class="jobitem" title="Job Expires"><i class="far fa-calendar-alt"></i>                                            
                    Expires: {{$job->expiry_date->format('d-M-Y')}}
                    </div>
                          
                      </div>
                    </div>                                       

                  <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                  </div>
                   </div>
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                    <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit - Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                     <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
<a href="javascript:;" onclick="makeNotActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Make Inactive')}}</a>  
                                 
                                </div>
                            </div>
              </div>
                        </li>
            @endif
            
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
            <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>


                    <div class="inactive_jobs display-jobs">
           <h3>{{__('Inactive Jobs')}}</h3>
          
          
                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)

                        <?php 

if($job->salary_type == 'single_salary'){

    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else if($job->salary_type == 'salary_in_range'){

     $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).' - <span class="symbol">'.$job->salary_currency.'</span> '.number_format($job->salary_to).'</strong>';

}else{
    $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
} 


?>

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)
            
            @if($job->is_active!==1)


            <li id="job_li_{{$job->id}}" class="jbexpired">
            
              
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                      @if($job->status=='rejected')
                       -<span>On-hold by admin on {{date('d-M-Y',strtotime($job->updated_at))}}</span>
                      @else

                      @if($job->status=='blocked')
                       - <span>Job Inactive @if(null!==($job->blocked_by) && $job->blocked_by!='')by {{$job->blocked_by}}@endif on {{date('d-M-Y',strtotime($job->updated_at))}}</span> 
                      @endif
                       
                       @if($job->status=='request_to_delete')
                       -<span class="expire">Awaiting admin review for deletion - Requested by {{$company->name}} On {{date('d-M-Y',strtotime($job->updated_at))}}</span>
                      
                      @else
                      @if($job->status=='admin_review')
                      - <span class="expire">Job Awaiting Approval</span> 
                      @endif
                      @endif
                      @endif
                    </h3>
                    <div class="row">
                      @if($job->reason)
                      <div class="col-lg-12">
                        <div class="jobitem" title="reason"><i class="fas fa-eye"></i><b>Reason :</b> {{$job->reason}}</div>
                      </div>
                      @endif
                      <div class="col-lg-6">
                      <div class="jobitem itemsalary" title="Salary">{!!$salary!!} {{$job->getSalaryPeriod('salary_period')}} @if($job->is_bonus) + <span>{{$job->is_bonus}}</span>@endif 
                                                @if($job->is_benifits) + <span>{{$job->is_benifits}}</span>@endif 
                                            </div>
                        <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                    <div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                      </div>
                      <div class="col-lg-6">
                      <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
                    <div class="jobitem" title="Posted Date"><i class="far fa-calendar-alt"></i>
                      
                      Posted: {{$job->created_at->format('d-M-Y')}}
                      
                      </div>
                          
                    <div class="jobitem" title="Job Expires"><i class="far fa-calendar-alt"></i>                                            
                    Expires: {{$job->expiry_date->format('d-M-Y')}}
                    </div>                    
                          
                          
                      </div>
                    </div>                                       

                  <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>
                                    <div class="clearfix"></div>
                  </div>
                   </div>
                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn compact">
                    <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="preview" target="_blank">{{__('Preview')}}</a>
                    <a href="{{route('edit.front.job', [$job->id])}}" class="editjb">{{__('Edit - Extend')}}</a>
                    @if($job->request_to_delete=='yes')
                    <span class="btn btn-danger mt-2">Awaiting Deletion</span>
                    @else

                     <a href="javascript:;" onclick="deleteJob({{$job->id}},'{{$job->title}}');" class="remove">{{__('Delete')}}</a>
                     @endif
                     @if($job->status=='blocked')
                     <a href="javascript:;" onclick="makeActive({{$job->id}},'{{$job->title}}')" class="inactive">{{__('Reactivate')}}</a>
                     @endif

                                 
                                </div>
                            </div>
              </div>
                        </li>
            @endif
            
                        <!-- job end --> 
                        @endif

                        @endforeach
@else
            <li>No Jobs Posted</li>
                        @endif

                    </ul>

                    </div>

                </div>

                    

                     @if(isset($jobs) && count($jobs))

                    @else

                    <p>No Records found.</p>    

                    @endif
        </div>
        </div>
    </div>
</div>
<div class="modal fade" id=requesttodeletejob role=dialog>
<div class=modal-dialog>
<div class=modal-content>
<form id=requestdelete>
    @csrf
<input type=hidden name=id id=job_del_id>
<div class=modal-header>
<h4 class=modal-title>Request Admin to Delete Job</h4>
<button type=button class=close data-dismiss=modal>&times;</button>
</div>
<div class=modal-body>
    <div class=form-group>
    
        {!! Form::label('reason', 'A reason must be selected from dropdown', ['class' => 'font-weight-bold']) !!}
        <br>
        <br>
        {!! Form::select('reason', [''=>'Select Reason']+dropdown(16)+['other' => 'Other'], null, array('class'=>'form-control', 'id'=>'reason', 'required'=>'required')) !!}
    </div>
    <div class="form-group" id="other_reason_box">
        {!! Form::label('other_reason', 'Please specify other reason', ['class' => 'font-weight-bold']) !!}
        <br>
        <br>
        {!! Form::textarea('other_reason', '', ['placeholder' => 'Other reason', 'class' => 'form-control']) !!}
    </div>
</div>
<div class=modal-footer>
<button type=button class="btn btn-default" data-dismiss=modal>Close</button>
<button type=button class="btn btn-primary submitActiontoDelete">Submit</button>
</div>
</form>
</div>
</div>
</div>

<div class="modal fade" id=requesttoreactivatejob role=dialog>
<div class=modal-dialog>
<div class=modal-content>
<form id=requestreactivation>
    @csrf
<input type=hidden name=id id=job_reactivation_id>
<div class=modal-header>
<h4>Admin will receive notification of Job Reactivation by {{Auth::guard('company')->user()->name}} on {{date('d-M-Y')}}</h4>
<button type=button class=close data-dismiss=modal>&times;</button>
</div>
<div class=modal-body>
<div class=form-group>
  
    {!! Form::label('reason', 'A reason must be selected from dropdown', ['class' => 'font-weight-bold']) !!}
<br><br>
    {!! Form::select('reactivation_reason', [''=>'Select Reason']+dropdown(15), null, array('class'=>'form-control', 'id'=>'reactivation_reason', 'required'=>'required')) !!}
</div>
</div>
<div class=modal-footer>
<button type=button class="btn btn-default" data-dismiss=modal>Close</button>
<button type=button class="btn btn-primary submitActiontoReactivation">Submit</button>
</div>
</form>
</div>
</div>
</div>

<div class="modal fade" id="requesttodeactivatejob" role=dialog>
<div class=modal-dialog>
<div class=modal-content>
<form id="requestdeactivation">
    @csrf
<input type=hidden name="id" id="job_deactivation_id">
<input type=hidden name="job_title" id="job_title">
<div class=modal-header>
<h4>Admin will receive notification of Job Deactivation by {{Auth::guard('company')->user()->name}} on {{date('d-M-Y')}}</h4>
<button type=button class=close data-dismiss=modal>&times;</button>
</div>
<div class=modal-body>
<div class=form-group>
  
    {!! Form::label('reason', 'A reason must be selected from dropdown', ['class' => 'font-weight-bold']) !!}
    <br>
    <br>
    {!! Form::select('deactivation_reason', [''=>'Select Reason']+dropdown(14), null, array('class'=>'form-control', 'id'=>'deactivation_reason', 'required'=>'required')) !!}
</div>
</div>
<div class=modal-footer>
<button type=button class="btn btn-default" data-dismiss=modal>Close</button>
<button type=button class="btn btn-primary submitActiontoDeactivation">Confirm Deactivation</button>
</div>
</form>
</div>
</div>
</div>

@include('includes.footer')
@endsection
@push('scripts')
@include('includes.immediate_available_btn')
<script type="text/javascript" src="{{asset('js/jquery.paginate.js')}}"></script>
<script type="text/javascript">
$(function  () {
        $('#other_reason_box').hide();
        $(document).on("change", "#reason", function () {
            if($(this).val() === 'other') {
                $('#other_reason_box').show();
                $('#other_reason').attr('required', 'required');
            } else {
                $('#other_reason_box').hide();
                $('#other_reason').removeAttr( 'required');
            }
        });
    })
      function deleteJob(id,title) {

        $('#job_del_id').val(id);
        $('.job-title-lable').html(title);
        $('#requesttodeletejob').modal('show');
  /*  var msg = 'Are you sure?';

    if (confirm(msg)) {

    $.post("{{ route('delete.front.job') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            if (response == 'ok')

            {

            $('#job_li_' + id).remove();

            } else

            {

            alert('Request Failed!');

            }

            });

    }*/

    }


    $('.submitActiontoDelete').on('click',function() {
        var reason = $('#reason').val();
        var other_reason = $('#other_reason').val();

        error = false;

        if(!reason && reason == '' || (reason === 'other' && other_reason == '')) {
            error = true;
            if(reason === '') {
                swal('Please select reason.');
            } else {
                swal('Please specify your other reason.');
            }

            return false;
        }

        // alert(error);

        if(error == false) {
            $.ajax({
                url: "{{route('delete.front.job')}}",

                type: "POST",

                data: $('#requestdelete').serialize(),

                success: function(response) {

                    $("#requestdelete").trigger("reset");

                    $('#requesttodeletejob').modal('hide');

                    swal({

                        title: "Success",

                        text: response,

                        icon: "success",

                        button: "OK",

                    }).then(() => {
                        location.reload();
                    });


                }

            });
        }
    });

    function makeNotActive(id,title) {
        $('#job_deactivation_id').val(id);
        $('#job_title').val(title);
        $('#requesttodeactivatejob').modal('show');
    }

$('.submitActiontoDeactivation').on('click',function(){
$.post("{{ route('front.make.not.active.job') }}", {id: $('#job_deactivation_id').val(),reason: $('#deactivation_reason').val(), _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {


                    if (response == 'ok')

                    {

                        swal("Thank You", "Job: "+$('#job_title').val()+" Successfully Inactivated", "success");
                        location.reload();

                    } else

                    {

                        alert('Request Failed!');

                    }

                }); 
 
})


/* function makeActive(id,title) {
        $('#job_reactivation_id').val(id);
        $('#requesttoreactivatejob').modal('show');
    }

$('.submitActiontoReactivation').on('click',function(){
$.post("{{ route('front.make.active.job') }}", {id: $('#job_reactivation_id').val(),reason: $('#reactivation_reason').val(), _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response.msg == 'ok')

                    {

                       swal("Thank You", "Job: "+response.job_title+" Successfully Activated", "success");
                        location.reload();

                    } else

                    {

                        alert('Request Failed!');

                    }

                }); 
 
})*/

    


    $(document).on('click','.swal-icon--success .swal-button--confirm',function(){
        location.reload();
    }); 


    function makeActive(id,title) {
       const el = document.createElement('div')
          el.innerHTML ='Click OK to make Job Active or Cancel to Exit <br> <br> <p>To Activate a Job, click <strong>View Inactive Jobs,</strong> and select <strong>Reactivate,</strong> if this job has expired, clicking reactivate will extend the job by <strong>3 months,</strong> if you want to modify  this Job Select <strong>Edit-Extend</strong> and <strong>UPDATE JOB</strong></p>';
swal({
  title: "Job: "+title,
  content: el,
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    $.post("{{ route('front.make.active.job') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})
        .done(function (response) {
            if (response == 'ok') {
                swal("Thank You", "Job: " + title + " Successfully Activated", "success");
                //location.reload();
            } else {
                alert('Request Failed!');
            }
        }); 
  } else {
    return false;
  }
});
    }
    displayjobs('active');
function displayjobs(type){
    $('.display-jobs').hide();
    $('.'+type+'_jobs').show();
    $('.btn-show-jobs').removeClass('active');
    $('.'+type+'-jobs').addClass('active');
}
$('.searchList').paginate({
    perPage:10,
});
</script>
@endpush